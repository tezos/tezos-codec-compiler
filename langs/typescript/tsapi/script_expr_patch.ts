import { decoder_argument } from "../src/decoder";

// REVIEW[epic=stopgap] :add the following line to all broken script.expr source files
// import { X_8, X_8_encoder, X_8_decoder } from '../tsapi/script_expr_patch'

export type X_8 = any;
export const X_8_decoder = (input: decoder_argument): X_8 => { throw new Error("Bug in data-encoding, X_8 undefined"); };
export const X_8_encoder = (val: X_8): string => { throw new Error("Bug in data-encoding, X_8 undefined"); }