import { oversize } from './oversize'

export const cases: Array<{ op: { branch: string; contents: Array<{}>; }; bytes: string; }> = [
    { // ANCHOR[epic=testcase] - case 0
        op: {
            branch: 'BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX',
            contents: [
                {
                    kind: 'transaction',
                    counter: '1',
                    source: 'tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn',
                    fee: '10000',
                    gas_limit: '10',
                    storage_limit: '10',
                    parameters: {
                        entrypoint: 'set_delegate',
                        value: { string: 'tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn' },
                    },
                    destination: 'KT1JHqHQdHSgWBKo6H4UfG8dw3JnZSyjGkHA',
                    amount: '1000',
                },
            ],
        },
        bytes: "a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807016a7d4a43f51be0934a441fba4f13f9beaa47575100ff03000000290100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e",
    },
    { // ANCHOR[epic=testcase] - case 1
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "delegation",
                    delegate: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616e0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0aff0035e993d8c7aaa42b5e3ccd86a33390ececc73abd'
    },
    { // ANCHOR[epic=testcase] - case 2
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "reveal",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    public_key: "edpkvS5QFv7KRGfa3b87gg9DBpxSm3NpSwnjhUjNBQrRUUR66F7C9g",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616b0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a00ebcf82872f4942052704e95dc4bfa0538503dbece27414a39b6650bcecbff896',
    },
    { // ANCHOR[epic=testcase] - case 3
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "ballot",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    period: -300,
                    ballot: "yay",
                    proposal: "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e561060035e993d8c7aaa42b5e3ccd86a33390ececc73abdfffffed43e5e3a606afab74a59ca09e333633e2770b6492c5e594455b71e9a2f0ea92afb00'
    },
    { // ANCHOR[epic=testcase] - case 4
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "endorsement",
                    level: -300
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e56100fffffed4'
    },
    { // ANCHOR[epic=testcase] - case 5
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "seed_nonce_revelation",
                    level: 25550,
                    nonce: "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e56101000063ceffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',
    },
    { // ANCHOR[epic=testcase] - case 6
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "proposals",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    period: 25550,
                    proposals: [
                        'PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb',
                        'PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb'
                    ]
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e561050035e993d8c7aaa42b5e3ccd86a33390ececc73abd000063ce000000403e5e3a606afab74a59ca09e333633e2770b6492c5e594455b71e9a2f0ea92afb3e5e3a606afab74a59ca09e333633e2770b6492c5e594455b71e9a2f0ea92afb'
    },
    { // ANCHOR[epic=testcase] - case 7
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    destination: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807000035e993d8c7aaa42b5e3ccd86a33390ececc73abd00'
    },
    { // ANCHOR[epic=testcase] - case 8
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: {
                        entrypoint: "do", value: { bytes: "0202" }
                    },
                    destination: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807000035e993d8c7aaa42b5e3ccd86a33390ececc73abdff02000000070a000000020202'
    },
    { // ANCHOR[epic=testcase] - case 9
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: {
                        entrypoint: "default",
                        value: {
                            prim: "Pair", args: [{ int: "2" }, { string: "hello" }]
                        }
                    },
                    destination: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807000035e993d8c7aaa42b5e3ccd86a33390ececc73abdff000000000e07070002010000000568656c6c6f'
    },
    { // ANCHOR[epic=testcase] - case 10
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: {
                        entrypoint: "Tps0RV2UISBvTV6m2z16VnfCVnN5dzX",
                        value: { prim: "Pair", args: [{ int: "2" }, { string: "hello" }] }
                    },
                    destination: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807000035e993d8c7aaa42b5e3ccd86a33390ececc73abdffff1f5470733052563255495342765456366d327a3136566e6643566e4e35647a580000000e07070002010000000568656c6c6f'
    },
    { // ANCHOR[epic=testcase] - case 11
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: { entrypoint: "entrypoint©Ͻ", value: { string: "Copyright ©Ͻ" } },
                    destination: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807000035e993d8c7aaa42b5e3ccd86a33390ececc73abdffff0e656e747279706f696e74c2a9cfbd00000013010000000e436f7079726967687420c2a9cfbd'
    },
    { // ANCHOR[epic=testcase] - case 12
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: { entrypoint: "default", value: { prim: "Unit" } },
                    destination: "KT1JHqHQdHSgWBKo6H4UfG8dw3JnZSyjGkHA",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807016a7d4a43f51be0934a441fba4f13f9beaa4757510000'
    },
    { // ANCHOR[epic=testcase] - case 13
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: {
                        entrypoint: "set_delegate",
                        value: {
                            string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn"
                        }
                    },
                    destination: "KT1JHqHQdHSgWBKo6H4UfG8dw3JnZSyjGkHA",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807016a7d4a43f51be0934a441fba4f13f9beaa47575100ff03000000290100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e'
    },
    { // ANCHOR[epic=testcase] - case 14
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: {
                        entrypoint: "remove_delegate",
                        value: {
                            string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn"
                        }
                    },
                    destination: "KT1JHqHQdHSgWBKo6H4UfG8dw3JnZSyjGkHA",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807016a7d4a43f51be0934a441fba4f13f9beaa47575100ff04000000290100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e'
    },
    { // ANCHOR[epic=testcase] - case 15
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: {
                        entrypoint: "root",
                        value: {
                            string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn"
                        }
                    },
                    destination: "KT1JHqHQdHSgWBKo6H4UfG8dw3JnZSyjGkHA",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807016a7d4a43f51be0934a441fba4f13f9beaa47575100ff01000000290100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e'
    },
    { // ANCHOR[epic=testcase] - case 16
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: {
                        entrypoint: "do",
                        value: {
                            prim: "Unit"
                        }
                    },
                    destination: "KT1JHqHQdHSgWBKo6H4UfG8dw3JnZSyjGkHA",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807016a7d4a43f51be0934a441fba4f13f9beaa47575100ff0200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 17
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: {
                        entrypoint: "main",
                        value: {
                            prim: "Pair",
                            args: [
                                { prim: "Pair", args: [{ bytes: "0202" }, { int: "202" }] },
                                { string: "hello" }
                            ]
                        }
                    },
                    destination: "KT1HPaJE1QNtuiYPgMAGhzTrs446K9wptmsR",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae80701609b26857d9053ecda49f13369a3c8908abc30ef00ffff046d61696e00000018070707070a000000020202008a03010000000568656c6c6f'
    },
    { // ANCHOR[epic=testcase] - case 18
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "transaction",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    parameters: {
                        entrypoint: "default",
                        value: {
                            prim: "Pair",
                            args: [{ bytes: "0202" }, { string: "hello" }],
                            annots: ["%test"]
                        }
                    },
                    destination: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    amount: "1000"
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807000035e993d8c7aaa42b5e3ccd86a33390ececc73abdff000000001c08070a000000020202010000000568656c6c6f000000052574657374'
    },
    { // ANCHOR[epic=testcase] - case 19
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "origination",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    balance: "0",
                    script: {
                        code: [
                            { prim: "parameter", args: [{ prim: "option", args: [{ prim: "key_hash" }] }] },
                            {
                                prim: "storage",
                                args: [{
                                    prim: "pair", args: [
                                        { prim: "pair", args: [{ prim: "address", annots: ["%addr"] }, { prim: "option", args: [{ prim: "key_hash" }], annots: ["%key"] }], annots: ["%mgr1"] },
                                        { prim: "pair", args: [{ prim: "address", annots: ["%addr"] }, { prim: "option", args: [{ prim: "key_hash" }], annots: ["%key"] }], annots: ["%mgr2"] }
                                    ]
                                }]
                            },
                            {
                                prim: "code",
                                args: [[
                                    { prim: "DUP" },
                                    [{ prim: "CDR" }, { prim: "CAR" }, { prim: "CAR", annots: ["%addr", "@%"] }],
                                    { prim: "SENDER" },
                                    { prim: "PAIR", annots: ["%@", "%@"] },
                                    [[{ prim: "DUP" }, { prim: "CAR" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }]],
                                    [
                                        { prim: "COMPARE" },
                                        { prim: "EQ" },
                                        {
                                            prim: "IF",
                                            args: [[
                                                [[{ prim: "DUP" }, { prim: "CAR" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }]],
                                                { prim: "SWAP" },
                                                [
                                                    { prim: "DUP" },
                                                    {
                                                        prim: "DIP",
                                                        args: [[
                                                            { prim: "CAR", annots: ["@%%"] },
                                                            [
                                                                { prim: "DUP" },
                                                                { prim: "CDR", annots: ["%key"] },
                                                                { prim: "DROP" },
                                                                { prim: "CAR", annots: ["@%%"] },
                                                                { prim: "PAIR", annots: ["%@", "%key"] }
                                                            ]
                                                        ]]
                                                    },
                                                    { prim: "CDR", annots: ["@%%"] },
                                                    { prim: "SWAP" },
                                                    { prim: "PAIR", annots: ["%@", "%@", "@changed_mgr1_key"] }
                                                ]
                                            ],
                                            [
                                                { prim: "DUP" },
                                                [{ prim: "CDR" }, { prim: "CDR" }, { prim: "CAR" }],
                                                { prim: "SENDER" },
                                                [
                                                    { prim: "COMPARE" },
                                                    { prim: "EQ" },
                                                    {
                                                        prim: "IF", args: [[
                                                            [[{ prim: "DUP" },
                                                            { prim: "CAR" },
                                                            { prim: "DIP", args: [[{ prim: "CDR" }]] }
                                                            ]],
                                                            { prim: "SWAP" },
                                                            [
                                                                { prim: "DUP" },
                                                                {
                                                                    prim: "DIP", args: [[
                                                                        { prim: "CDR", annots: ["@%%"] },
                                                                        [
                                                                            { prim: "DUP" },
                                                                            { prim: "CDR", annots: ["%key"] },
                                                                            { prim: "DROP" },
                                                                            { prim: "CAR", annots: ["@%%"] },
                                                                            { prim: "PAIR", annots: ["%@", "%key"] }
                                                                        ]
                                                                    ]]
                                                                },
                                                                { prim: "CAR", annots: ["@%%"] },
                                                                { prim: "PAIR", annots: ["%@", "%@"] }
                                                            ]
                                                        ], [[{ prim: "UNIT" }, { prim: "FAILWITH" }]]
                                                        ]
                                                    }
                                                ]
                                            ]]
                                        }
                                    ],
                                    { prim: "DUP" },
                                    [{ prim: "CAR" }, { prim: "CDR" }],
                                    { prim: "DIP", args: [[{ prim: "DUP" }, [{ prim: "CDR" }, { prim: "CDR" }]]] },
                                    {
                                        prim: "IF_NONE",
                                        args: [
                                            [
                                                {
                                                    prim: "IF_NONE",
                                                    args: [
                                                        [{ prim: "NONE", args: [{ prim: "key_hash" }] },
                                                        { prim: "SET_DELEGATE" },
                                                        { prim: "NIL", args: [{ prim: "operation" }] },
                                                        { prim: "SWAP" },
                                                        { prim: "CONS" }
                                                        ],
                                                        [
                                                            { prim: "DROP" },
                                                            { prim: "NIL", args: [{ prim: "operation" }] }
                                                        ]
                                                    ]
                                                }
                                            ],
                                            [
                                                { prim: "SWAP" },
                                                [{
                                                    prim: "IF_NONE",
                                                    args: [
                                                        [{ prim: "DROP" }, { prim: "NIL", args: [{ prim: "operation" }] }],
                                                        [{ prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                        [{ prim: "COMPARE" }, { prim: "EQ" }, {
                                                            prim: "IF", args: [
                                                                [
                                                                    { prim: "SOME" },
                                                                    { prim: "SET_DELEGATE" },
                                                                    { prim: "NIL", args: [{ prim: "operation" }] },
                                                                    { prim: "SWAP" }, { prim: "CONS" }
                                                                ],
                                                                [{ prim: "DROP" }, { prim: "NIL", args: [{ prim: "operation" }] }]
                                                            ]
                                                        }]
                                                        ]
                                                    ]
                                                }]
                                            ]
                                        ]
                                    },
                                    { prim: "PAIR" }
                                ]]
                            }
                        ],
                        storage: {
                            prim: "Pair",
                            args: [
                                { prim: "Pair", args: [{ string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn" }, { prim: "None" }] },
                                { prim: "Pair", args: [{ string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn" }, { prim: "None" }] }
                            ]
                        }
                    }
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a00000000025b020000025605000563035d050107650865046e0000000525616464720663035d00000004256b657900000005256d6772310865046e0000000525616464720663035d00000004256b657900000005256d6772320502020000020103210200000012031703160416000000082561646472204025034804420000000525402025400200000012020000000d03210316051f02000000020317020000012b03190325072c02000000810200000012020000000d03210316051f02000000020317034c02000000630321051f020000003204160000000340252502000000240321041700000004256b65790320041600000003402525044200000007254020256b6579041700000003402525034c044200000017254020254020406368616e6765645f6d6772315f6b6579020000009a032102000000060317031703160348020000008603190325072c020000006d0200000012020000000d03210316051f02000000020317034c020000004f0321051f020000003204170000000340252502000000240321041700000004256b65790320041600000003402525044200000007254020256b6579041600000003402525044200000005254020254002000000090200000004034f03270321020000000403160317051f020000000b0321020000000403170317072f0200000020072f020000000e053e035d034e053d036d034c031b02000000060320053d036d0200000049034c0200000042072f02000000060320053d036d0200000030051f02000000020321020000002203190325072c020000000c0346034e053d036d034c031b02000000060320053d036d03420000005c070707070100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e030607070100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e0306'
    },
    { // ANCHOR[epic=testcase] - case 20
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "origination",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    balance: "0",
                    script: {
                        code: [
                            { prim: "parameter", args: [{ prim: "unit" }] },
                            { prim: "storage", args: [{ prim: "unit" }] },
                            { prim: "code", args: [{ prim: "parameter" }] }
                        ],
                        storage: { prim: "Unit" }
                    }
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030000000002030b'
    },
    { // ANCHOR[epic=testcase] - case 21
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "origination",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    balance: "0",
                    script: {
                        code: [
                            { prim: "parameter", args: [{ prim: "unit" }] },
                            { prim: "storage", args: [{ prim: "unit" }] },
                            { prim: "code", args: [{ prim: "storage" }] }
                        ],
                        storage: { prim: "Unit" }
                    }
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030100000002030b'
    },
    { // ANCHOR[epic=testcase] - case 22
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "origination",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    balance: "0",
                    script: {
                        code: [
                            { prim: "parameter", args: [{ prim: "unit" }] },
                            { prim: "storage", args: [{ prim: "unit" }] },
                            { prim: "code", args: [{ prim: "code" }] }
                        ],
                        storage: { prim: "Unit" }
                    }
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 23
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "origination",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    balance: "0",
                    script: {
                        code: [
                            { prim: "parameter", args: [{ prim: "unit" }] },
                            { prim: "storage", args: [{ prim: "unit" }] },
                            { prim: "code", args: [{ prim: "False" }] }
                        ],
                        storage: { prim: "Unit" }
                    }
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030300000002030b'
    },
    { // ANCHOR[epic=testcase] - case 24
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "origination",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    balance: "0",
                    script: {
                        code: [
                            { prim: "parameter", args: [{ prim: "unit" }] },
                            { prim: "storage", args: [{ prim: "unit" }] },
                            { prim: "code", args: [{ prim: "Elt" }] }
                        ],
                        storage: { prim: "Unit" }
                    }
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030400000002030b'
    },
    { // ANCHOR[epic=testcase] - case 25
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "origination",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    balance: "0",
                    script: {
                        code: [
                            {
                                prim: "parameter",
                                args: [
                                    {
                                        prim: "unit"
                                    }
                                ]
                            },
                            {
                                prim: "storage",
                                args: [
                                    {
                                        prim: "unit"
                                    }
                                ]
                            },
                            {
                                prim: "code",
                                args: [
                                    {
                                        prim: "Left"
                                    }
                                ]
                            }
                        ],
                        storage: {
                            prim: "Unit"
                        }
                    }
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030500000002030b'
    },
    { // ANCHOR[epic=testcase] - case 26
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "parameter" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030000000002030b'
    },
    { // ANCHOR[epic=testcase] - case 27
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "storage" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030100000002030b'
    },
    { // ANCHOR[epic=testcase] - case 28
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "code" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 29
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "False" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030300000002030b'
    },
    { // ANCHOR[epic=testcase] - case 30
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "Elt" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030400000002030b'
    },
    { // ANCHOR[epic=testcase] - case 31
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "Left" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030500000002030b'
    },
    { // ANCHOR[epic=testcase] - case 32
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "Pair" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030700000002030b'
    },
    { // ANCHOR[epic=testcase] - case 33
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "Right" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030800000002030b'
    },
    { // ANCHOR[epic=testcase] - case 34
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "Some" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030900000002030b'
    },
    { // ANCHOR[epic=testcase] - case 35
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "True" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030a00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 36
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "Unit" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030b00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 37
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "PACK" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030c00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 38
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "UNPACK" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030d00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 39
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "BLAKE2B" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030e00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 40
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SHA256" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502030f00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 41
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SHA512" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031000000002030b'
    },
    { // ANCHOR[epic=testcase] - case 42
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "ABS" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031100000002030b'
    },
    { // ANCHOR[epic=testcase] - case 43
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "ADD" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 44
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "AMOUNT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031300000002030b'
    },
    { // ANCHOR[epic=testcase] - case 45
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "AND" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031400000002030b'
    },
    { // ANCHOR[epic=testcase] - case 46
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "BALANCE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031500000002030b'
    },
    { // ANCHOR[epic=testcase] - case 47
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "CAR" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031600000002030b'
    },
    { // ANCHOR[epic=testcase] - case 48
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "CDR" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031700000002030b'
    },
    { // ANCHOR[epic=testcase] - case 49
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "CHECK_SIGNATURE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031800000002030b'
    },
    { // ANCHOR[epic=testcase] - case 50
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "COMPARE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031900000002030b'
    },
    { // ANCHOR[epic=testcase] - case 51
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "CONCAT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031a00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 52
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "CONS" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031b00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 53
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "CREATE_ACCOUNT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031c00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 54
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "CREATE_CONTRACT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031d00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 55
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "IMPLICIT_ACCOUNT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031e00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 57
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "DIP" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502031f00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 58
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "DROP" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032000000002030b'
    },
    { // ANCHOR[epic=testcase] - case 59
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "DUP" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032100000002030b'
    },
    { // ANCHOR[epic=testcase] - case 60
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "EDIV" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 61
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "EMPTY_MAP" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032300000002030b'
    },
    { // ANCHOR[epic=testcase] - case 62
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "EMPTY_SET" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032400000002030b'
    },
    { // ANCHOR[epic=testcase] - case 63
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "EQ" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032500000002030b'
    },
    { // ANCHOR[epic=testcase] - case 64
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "EXEC" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032600000002030b'
    },
    { // ANCHOR[epic=testcase] - case 65
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "FAILWITH" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032700000002030b'
    },
    { // ANCHOR[epic=testcase] - case 66
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "GE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032800000002030b'
    },
    { // ANCHOR[epic=testcase] - case 67
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "GET" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032900000002030b'
    },
    { // ANCHOR[epic=testcase] - case 68
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "GT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032a00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 69
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "HASH_KEY" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032b00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 70
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "IF" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032c00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 71
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "IF_CONS" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032d00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 72
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "IF_LEFT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032e00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 73
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "IF_NONE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502032f00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 74
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "INT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033000000002030b'
    },
    { // ANCHOR[epic=testcase] - case 75
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "LAMBDA" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033100000002030b'
    },
    { // ANCHOR[epic=testcase] - case 75
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "LE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 76
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "LEFT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033300000002030b'
    },
    { // ANCHOR[epic=testcase] - case 77
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "LOOP" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033400000002030b'
    },
    { // ANCHOR[epic=testcase] - case 78
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "LSL" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033500000002030b'
    },
    { // ANCHOR[epic=testcase] - case 79
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "LSR" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033600000002030b'
    },
    { // ANCHOR[epic=testcase] - case 80
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "LT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033700000002030b'
    },
    { // ANCHOR[epic=testcase] - case 81
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "MAP" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033800000002030b'
    },
    { // ANCHOR[epic=testcase] - case 82
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "MEM" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033900000002030b'
    },
    { // ANCHOR[epic=testcase] - case 83
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "MUL" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033a00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 84
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "NEG" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033b00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 85
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "NEQ" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033c00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 86
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "NIL" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033d00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 87
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "NONE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033e00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 88
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "NOT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502033f00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 89
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "NOW" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034000000002030b'
    },
    { // ANCHOR[epic=testcase] - case 90
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "OR" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034100000002030b'
    },
    { // ANCHOR[epic=testcase] - case 91
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "PAIR" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 92
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "PUSH" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034300000002030b'
    },
    { // ANCHOR[epic=testcase] - case 93
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "RIGHT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034400000002030b'
    },
    { // ANCHOR[epic=testcase] - case 94
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SIZE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034500000002030b'
    },
    { // ANCHOR[epic=testcase] - case 95
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SOME" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034600000002030b'
    },
    { // ANCHOR[epic=testcase] - case 96
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SOURCE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034700000002030b'
    },
    { // ANCHOR[epic=testcase] - case 97
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SENDER" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034800000002030b'
    },
    { // ANCHOR[epic=testcase] - case 98
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SELF" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034900000002030b'
    },
    { // ANCHOR[epic=testcase] - case 99
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "STEPS_TO_QUOTA" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034a00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 100
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SUB" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034b00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 101
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SWAP" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034c00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 102
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "TRANSFER_TOKENS" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034d00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 103
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SET_DELEGATE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034e00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 104
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "UNIT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502034f00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 105
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "UPDATE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035000000002030b'
    },
    { // ANCHOR[epic=testcase] - case 106
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "XOR" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035100000002030b'
    },
    { // ANCHOR[epic=testcase] - case 107
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "ITER" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 108
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "LOOP_LEFT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035300000002030b'
    },
    { // ANCHOR[epic=testcase] - case 109
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "ADDRESS" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035400000002030b'
    },
    { // ANCHOR[epic=testcase] - case 110
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "CONTRACT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035500000002030b'
    },
    { // ANCHOR[epic=testcase] - case 111
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "ISNAT" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035600000002030b'
    },
    { // ANCHOR[epic=testcase] - case 112
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "CAST" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035700000002030b'
    },
    { // ANCHOR[epic=testcase] - case 113
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "RENAME" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035800000002030b'
    },
    { // ANCHOR[epic=testcase] - case 114
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "bool" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035900000002030b'
    },
    { // ANCHOR[epic=testcase] - case 115
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "contract" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035a00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 116
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "int" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035b00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 117
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "key" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035c00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 118
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "key_hash" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035d00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 119
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "lambda" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035e00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 120
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "list" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502035f00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 121
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "map" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036000000002030b'
    },
    { // ANCHOR[epic=testcase] - case 122
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "big_map" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036100000002030b'
    },
    { // ANCHOR[epic=testcase] - case 123
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "nat" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 124
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "option" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036300000002030b'
    },
    { // ANCHOR[epic=testcase] - case 125
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "or" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036400000002030b'
    },
    { // ANCHOR[epic=testcase] - case 126
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "pair" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036500000002030b'
    },
    { // ANCHOR[epic=testcase] - case 127
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "set" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036600000002030b'
    },
    { // ANCHOR[epic=testcase] - case 128
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "signature" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036700000002030b'
    },
    { // ANCHOR[epic=testcase] - case 129
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "string" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036800000002030b'
    },
    { // ANCHOR[epic=testcase] - case 130
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "bytes" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036900000002030b'
    },
    { // ANCHOR[epic=testcase] - case 131
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "mutez" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036a00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 132
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "timestamp" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036b00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 133
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "unit" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036c00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 134
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "operation" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036d00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 135
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "address" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036e00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 136
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SLICE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502036f00000002030b'
    },
    { // ANCHOR[epic=testcase] - case 137
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "DIG" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037000000002030b'
    },
    { // ANCHOR[epic=testcase] - case 138
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "DUG" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037100000002030b'
    },
    { // ANCHOR[epic=testcase] - case 139
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "EMPTY_BIG_MAP" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 140
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "APPLY" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037300000002030b'
    },
    { // ANCHOR[epic=testcase] - case 141
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "chain_id" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037400000002030b'
    },
    { // ANCHOR[epic=testcase] - case 142
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "CHAIN_ID" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037500000002030b'
    },
    /* ANCHOR[epic=testcase] - case 143 */
    oversize[0],
    /* ANCHOR[epic=testcase] - case 144 */
    oversize[1],
    /* ANCHOR[epic=testcase] - case 145 */
    oversize[2],
    { // ANCHOR[epic=testcase] - case 146
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "LEVEL" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037600000002030b'
    },

    { // ANCHOR[epic=testcase] - case 147
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SELF_ADDRESS" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037700000002030b'
    },

    { // ANCHOR[epic=testcase] - case 148
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "never" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037800000002030b'
    },

    { // ANCHOR[epic=testcase] - case 149
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "NEVER" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037900000002030b'
    },

    { // ANCHOR[epic=testcase] - case 150
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "UNPAIR" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037a00000002030b'
    },

    { // ANCHOR[epic=testcase] - case 151
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "VOTING_POWER" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037b00000002030b'
    },

    { // ANCHOR[epic=testcase] - case 152
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "TOTAL_VOTING_POWER" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037c00000002030b'
    },

    { // ANCHOR[epic=testcase] - case 153
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "KECCAK" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037d00000002030b'
    },

    { // ANCHOR[epic=testcase] - case 154
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SHA3" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037e00000002030b'
    },

    { // ANCHOR[epic=testcase] - case 155
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "PAIRING_CHECK" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502037f00000002030b'
    },

    { // ANCHOR[epic=testcase] - case 156
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "bls12_381_g1" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038000000002030b'
    },

    { // ANCHOR[epic=testcase] - case 157
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "bls12_381_g2" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038100000002030b'
    },

    { // ANCHOR[epic=testcase] - case 158
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "bls12_381_fr" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038200000002030b'
    },

    { // ANCHOR[epic=testcase] - case 159
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "sapling_state" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038300000002030b'
    },

    { // ANCHOR[epic=testcase] - case 160
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "sapling_transaction" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038400000002030b'
    },

    { // ANCHOR[epic=testcase] - case 161
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SAPLING_EMPTY_STATE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038500000002030b'
    },

    { // ANCHOR[epic=testcase] - case 162
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SAPLING_VERIFY_UPDATE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038600000002030b'
    },

    { // ANCHOR[epic=testcase] - case 163
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "ticket" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038700000002030b'
    },

    { // ANCHOR[epic=testcase] - case 164
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "TICKET" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038800000002030b'
    },

    { // ANCHOR[epic=testcase] - case 165
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "READ_TICKET" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038900000002030b'
    },

    { // ANCHOR[epic=testcase] - case 166
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "SPLIT_TICKET" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038a00000002030b'
    },

    { // ANCHOR[epic=testcase] - case 167
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "JOIN_TICKETS" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038b00000002030b'
    },

    { // ANCHOR[epic=testcase] - case 168
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "unit" }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        { prim: "code", args: [{ prim: "GET_AND_UPDATE" }] }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000011020000000c0500036c0501036c0502038c00000002030b'
    },
    /* case 169 */ oversize[3],
    { // ANCHOR[epic=testcase] - case 170
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        {
                            prim: "parameter",
                            args: [{ prim: "pair", args: [{ prim: "sapling_transaction", args: [{ int: "8" }] }, { prim: "option", args: [{ prim: "key_hash" }], annots: ["%key"] }] }]
                        },
                        {
                            prim: "storage",
                            args: [{
                                prim: "pair",
                                args: [
                                    { prim: "mutez", annots: ["%balance"] },
                                    { prim: "sapling_state", args: [{ int: "8" }], annots: ["%ledger1"] },
                                    { prim: "sapling_state", args: [{ int: "8" }], annots: ["%ledger2"] }
                                ]
                            }]
                        },
                        { prim: "code", args: [[{ prim: "DUP" }]] }
                    ],
                    storage: { prim: "Pair", args: [{ int: "0" }, [], []] }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a00000000005c020000005705000765058400080663035d00000004256b6579050109650000002e046a000000082562616c616e63650683000800000008256c6564676572310683000800000008256c656467657232000000000502020000000203210000001609070000000c00000200000000020000000000000000'
    },
    { // ANCHOR[epic=testcase] - case 171
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        {
                            prim: "parameter",
                            args: [{
                                prim: "pair",
                                args: [
                                    { prim: "bool" },
                                    { prim: "sapling_transaction", args: [{ int: "8" }], annots: [":left"] },
                                    { prim: "sapling_transaction", args: [{ int: "8" }], annots: [":right"] }
                                ]
                            }]
                        },
                        {
                            prim: "storage",
                            args: [{
                                prim: "pair",
                                args: [
                                    { prim: "sapling_state", args: [{ int: "8" }], annots: [":left"] },
                                    { prim: "sapling_state", args: [{ int: "8" }], annots: [":right"] }
                                ]
                            }]
                        },
                        {
                            prim: "code",
                            args: [[
                                { prim: "UNPAIR" },
                                { prim: "UNPAIR" },
                                { prim: "DIP", args: [[{ prim: "UNPAIR" }]] },
                                { prim: "DIP", args: [{ int: "3" }, [{ prim: "UNPAIR" }]] },
                                { prim: "DIP", args: [{ int: "2" }, [{ prim: "SWAP" }]] },
                                {
                                    prim: "IF",
                                    args: [
                                        [
                                            { prim: "SAPLING_VERIFY_UPDATE" },
                                            [{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], []] }],
                                            { prim: "UNPAIR" },
                                            { prim: "DROP" },
                                            {
                                                prim: "DIP",
                                                args: [[
                                                    { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                    { prim: "SAPLING_VERIFY_UPDATE" },
                                                    [{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], []] }],
                                                    { prim: "UNPAIR" },
                                                    { prim: "DROP" },
                                                    { prim: "DROP" }
                                                ]]
                                            }
                                        ],
                                        [
                                            { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                            { prim: "SAPLING_VERIFY_UPDATE" },
                                            [{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], []] }],
                                            { prim: "UNPAIR" },
                                            { prim: "DROP" },
                                            { prim: "DROP" },
                                            {
                                                prim: "DIP",
                                                args: [[
                                                    { prim: "SAPLING_VERIFY_UPDATE" },
                                                    [{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], []] }],
                                                    { prim: "UNPAIR" },
                                                    { prim: "DROP" }
                                                ]]
                                            }
                                        ]
                                    ]
                                },
                                { prim: "PAIR" },
                                { prim: "NIL", args: [{ prim: "operation" }] },
                                { prim: "PAIR" }
                            ]]
                        }
                    ],
                    storage: { prim: "Pair", args: [[], []] }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a00000000012f020000012a050009650000001d035906840008000000053a6c65667406840008000000063a7269676874000000000501076506830008000000053a6c65667406830008000000063a7269676874050202000000db037a037a051f0200000002037a071f00030200000002037a071f00020200000002034c072c020000005203860200000015072f02000000090200000004034f03270200000000037a0320051f020000002b051f0200000002032103860200000015072f02000000090200000004034f03270200000000037a032003200200000052051f0200000002032103860200000015072f02000000090200000004034f03270200000000037a03200320051f020000002003860200000015072f02000000090200000004034f03270200000000037a03200342053d036d03420000000c070702000000000200000000'
    },
    { // ANCHOR[epic=testcase] - case 172
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "list", args: [{ prim: "sapling_transaction", args: [{ int: "8" }] }] }] },
                        { prim: "storage", args: [{ prim: "unit" }] },
                        {
                            prim: "code",
                            args: [[
                                { prim: "UNPAIR" },
                                { prim: "SAPLING_EMPTY_STATE", args: [{ int: "8" }] },
                                { prim: "SWAP" },
                                {
                                    prim: "ITER",
                                    args: [[
                                        { prim: "SAPLING_VERIFY_UPDATE" },
                                        [{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], []] }],
                                        { prim: "UNPAIR" },
                                        { prim: "DROP" }
                                    ]]
                                },
                                { prim: "DROP" },
                                { prim: "NIL", args: [{ prim: "operation" }] },
                                { prim: "PAIR" }
                            ]]
                        }
                    ],
                    storage: { prim: "Unit" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a00000000004f020000004a0500055f058400080501036c05020200000037037a05850008034c0552020000002003860200000015072f02000000090200000004034f03270200000000037a03200320053d036d034200000002030b'
    },
    { // ANCHOR[epic=testcase] - case 173
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        { prim: "parameter", args: [{ prim: "or", args: [{ prim: "sapling_transaction", args: [{ int: "8" }] }, { prim: "sapling_state", args: [{ int: "8" }] }] }] },
                        { prim: "storage", args: [{ prim: "option", args: [{ prim: "sapling_transaction", args: [{ int: "8" }] }] }] },
                        {
                            prim: "code",
                            args: [[
                                { prim: "UNPAIR" },
                                {
                                    prim: "IF_LEFT",
                                    args: [
                                        [
                                            { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                            { prim: "SOME" }
                                        ],
                                        [
                                            { prim: "DIP", args: [[[{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], []] }]]] },
                                            { prim: "SWAP" },
                                            { prim: "SAPLING_VERIFY_UPDATE" },
                                            [{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], []] }],
                                            { prim: "DROP" },
                                            { prim: "NONE", args: [{ prim: "sapling_transaction", args: [{ int: "8" }] }] }
                                        ]
                                    ]
                                },
                                { prim: "NIL", args: [{ prim: "operation" }] },
                                { prim: "PAIR" }
                            ]]
                        }
                    ],
                    storage: { prim: "None" }
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a0000000000860200000081050007640584000805830008050105630584000805020200000066037a072e020000000b051f0200000002032003460200000047051f020000001a0200000015072f02000000090200000004034f03270200000000034c03860200000015072f02000000090200000004034f032702000000000320053e05840008053d036d0342000000020306'
    },
    { // ANCHOR[epic=testcase] - case 174
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [{
                kind: "origination",
                counter: "1",
                source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                fee: "10000",
                gas_limit: "10",
                storage_limit: "10",
                balance: "0",
                script: {
                    code: [
                        {
                            prim: "parameter",
                            args: [{
                                prim: "pair",
                                args: [
                                    { prim: "sapling_transaction", args: [{ int: "8" }] },
                                    { prim: "sapling_state", args: [{ int: "8" }] },
                                    { prim: "int", annots: ["%test"] },
                                    { prim: "int", annots: ["%test2"] },
                                    { prim: "mutez", annots: ["%test3"] },
                                    { prim: "nat", annots: ["%test4"] },
                                    { prim: "int", annots: ["%test5"] },
                                    { prim: "int", annots: ["%test6"] },
                                    { prim: "mutez", annots: ["%test7"] },
                                    { prim: "nat", annots: ["%test8"] },
                                    { prim: "int", annots: ["%test9"] },
                                    { prim: "int", annots: ["%test10"] },
                                    { prim: "mutez", annots: ["%test11"] },
                                    { prim: "nat", annots: ["%test12"] },
                                    { prim: "int", annots: ["%test13"] },
                                    { prim: "int", annots: ["%test14"] },
                                    { prim: "mutez", annots: ["%test15"] },
                                    { prim: "nat", annots: ["%test16"] }
                                ],
                                annots: ["%combPairAnnot"]
                            }]
                        },
                        { prim: "storage", args: [{ prim: "sapling_state", args: [{ int: "8" }] }] },
                        { prim: "code", args: [[{ prim: "DUP" }]] }
                    ],
                    storage: []
                }
            }]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a0000000000fc02000000f705000965000000ce0584000805830008045b000000052574657374045b00000006257465737432046a00000006257465737433046200000006257465737434045b00000006257465737435045b00000006257465737436046a00000006257465737437046200000006257465737438045b00000006257465737439045b0000000725746573743130046a000000072574657374313104620000000725746573743132045b0000000725746573743133045b0000000725746573743134046a0000000725746573743135046200000007257465737431360000000e25636f6d6250616972416e6e6f74050105830008050202000000020321000000050200000000'
    },
    { // ANCHOR[epic=testcase] - case 175
        op: {
            branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
            contents: [
                {
                    kind: "origination",
                    counter: "1",
                    source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
                    fee: "10000",
                    gas_limit: "10",
                    storage_limit: "10",
                    balance: "0",
                    script: {
                        code: [
                            {
                                prim: "parameter",
                                args: [
                                    {
                                        prim: "pair",
                                        args: [
                                            { prim: "sapling_transaction", args: [{ int: "8" }] },
                                            { prim: "sapling_state", args: [{ int: "8" }] },
                                            { prim: "int" },
                                            { prim: "int" },
                                            { prim: "mutez" },
                                            { prim: "nat" },
                                            { prim: "int" },
                                            { prim: "int" },
                                            { prim: "mutez" },
                                            { prim: "nat" },
                                            { prim: "int" },
                                            { prim: "int" },
                                            { prim: "mutez" },
                                            { prim: "nat" },
                                            { prim: "int" },
                                            { prim: "int" },
                                            { prim: "mutez" },
                                            { prim: "nat" }
                                        ]
                                    }
                                ]
                            },
                            { prim: "storage", args: [{ prim: "sapling_state", args: [{ int: "8" }] }] },
                            { prim: "code", args: [[{ prim: "DUP" }]] }
                        ],
                        storage: []
                    }
                }
            ]
        },
        bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000048020000004305000965000000280584000805830008035b035b036a0362035b035b036a0362035b035b036a0362035b035b036a036200000000050105830008050202000000020321000000050200000000'
    },
];
