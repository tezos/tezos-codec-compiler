import { convert } from './dev'
import { CODECTYPE } from './schema'
import { cases } from './cases'
import * as codec from './codec';

import operation_unsigned = codec.Proto010_PtGRANAD__operation__unsigned;

describe('Encodes as expected', () => {
    cases.forEach((sample, index) => {
        it(`encode@${index}`, () => {
            expect(operation_unsigned.t_encoder(convert[CODECTYPE.MANAGER](sample.op) as operation_unsigned.t)).toBe(sample.bytes);
        });
    });
});

describe('Roundtrip equality', () => {
    cases.forEach((sample, index) => {
        it(`roundtrip@${index}`, () => {
            let raw_op: operation_unsigned.t;
            let rt_op: operation_unsigned.t;
            let encoder = operation_unsigned.t_encoder;
            let decoder = operation_unsigned.t_decoder;

            raw_op = convert[CODECTYPE.MANAGER](sample.op) as operation_unsigned.t;
            rt_op = decoder(encoder(raw_op));
            expect(rt_op).toStrictEqual(raw_op);
        });
    });
});