export const oversize: Array<{ op: { branch: string; contents: Array<{}> }; bytes: string }> = [
  { // ANCHOR[epic=oversize_testcase] - oversize 0
    op: {
      branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
      contents: [{
        kind: "origination",
        counter: "1",
        source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
        fee: "10000",
        gas_limit: "10",
        storage_limit: "10",
        balance: "0",
        script: {
          code: [
            {
              prim: "parameter",
              args: [
                {
                  prim: "pair",
                  args: [
                    { prim: "pair", args: [{ prim: "address" }, { prim: "address" }] },
                    { prim: "int" }
                  ]
                }
              ]
            },
            {
              prim: "storage",
              args: [{
                prim: "pair",
                args: [
                  {
                    prim: "pair",
                    args: [
                      {
                        prim: "big_map",
                        args: [
                          { prim: "address" },
                          {
                            prim: "pair",
                            args: [
                              { prim: "map", args: [{ prim: "address" }, { prim: "int" }], annots: ["%allowances"] },
                              { prim: "int", annots: ["%balance"] }]
                          }
                        ],
                        annots: ["%accounts"]
                      },
                      { prim: "address", annots: ["%owner"] }
                    ]
                  },
                  { prim: "int", annots: ["%totalSupply"] }
                ]
              }]
            },
            {
              prim: "code",
              args: [
                [{ prim: "DUP" },
                {
                  prim: "LAMBDA",
                  args: [
                    {
                      prim: "pair",
                      args: [
                        { prim: "pair", args: [{ prim: "address" }, { prim: "address" }] },
                        {
                          prim: "pair",
                          args: [
                            { prim: "int" },
                            {
                              prim: "pair",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    {
                                      prim: "big_map",
                                      args: [
                                        { prim: "address" },
                                        { prim: "pair", args: [{ prim: "map", args: [{ prim: "address" }, { prim: "int" }] }, { prim: "int" }] }
                                      ]
                                    },
                                    { prim: "address" }
                                  ]
                                },
                                { prim: "int" }
                              ]
                            }
                          ]
                        }
                      ]
                    },
                    {
                      prim: "pair",
                      args: [
                        {
                          prim: "pair",
                          args: [
                            {
                              prim: "big_map",
                              args: [
                                { prim: "address" },
                                { prim: "pair", args: [{ prim: "map", args: [{ prim: "address" }, { prim: "int" }] }, { prim: "int" }] }
                              ]
                            },
                            { prim: "address" }
                          ]
                        },
                        { prim: "int" }
                      ]
                    },
                    [
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "DIP", args: [[{ prim: "DUP" }]] },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "DIP", args: [{ int: "2" }, [{ prim: "DUP" }]] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIP", args: [{ int: "3" }, [{ prim: "DUP" }]] },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "SOURCE" },
                      { prim: "GET" },
                      { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "string" }, { string: "GET_FORCE" }] }, { prim: "FAILWITH" }], []] },
                      { prim: "PUSH", args: [{ prim: "int" }, { int: "0" }] },
                      { prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "int" }] },
                      { prim: "PAIR" },
                      { prim: "DIP", args: [{ int: "4" }, [{ prim: "DUP" }]] },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DIP", args: [[{ prim: "DIP", args: [{ int: "2" }, [{ prim: "DUP" }]] }, { prim: "DIG", args: [{ int: "2" }] }, { prim: "CAR" }, { prim: "CAR" }]] },
                      { prim: "GET" },
                      { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "unit" }, { prim: "Unit" }] }], [{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }, { prim: "DROP" }]] }, { prim: "SWAP" }, { prim: "DROP" }, { prim: "DIP", args: [[{ prim: "DROP" }]] }, { prim: "PUSH", args: [{ prim: "unit" }, { prim: "Unit" }] }]] },
                      { prim: "DROP" },
                      { prim: "DIP", args: [{ int: "3" }, [{ prim: "DUP" }]] },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }, { prim: "CDR" }]] },
                      { prim: "COMPARE" },
                      { prim: "GT" },
                      { prim: "IF", args: [[{ prim: "PUSH", args: [{ prim: "string" }, { string: "Balance is too low" }] }, { prim: "FAILWITH" }], [{ prim: "PUSH", args: [{ prim: "unit" }, { prim: "Unit" }] }]] },
                      { prim: "DROP" },
                      { prim: "DIP", args: [[{ prim: "DUP" }]] },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DIP", args: [[{ prim: "DIP", args: [{ int: "3" }, [{ prim: "DUP" }]] }, { prim: "DIG", args: [{ int: "3" }] }]] },
                      { prim: "SUB" },
                      { prim: "DUP" },
                      { prim: "PUSH", args: [{ prim: "int" }, { int: "0" }] },
                      { prim: "SWAP" },
                      { prim: "COMPARE" },
                      { prim: "LT" },
                      {
                        prim: "IF",
                        args: [
                          [{ prim: "PUSH", args: [{ prim: "string" }, { string: "Balance cannot be negative" }] }, { prim: "FAILWITH" }],
                          [{ prim: "DUP" },
                          { prim: "DIP", args: [[{ prim: "DIP", args: [{ int: "2" }, [{ prim: "DUP" }]] }, { prim: "DIG", args: [{ int: "2" }] }, { prim: "CAR" }]] },
                          { prim: "SWAP" },
                          { prim: "PAIR" },
                          { prim: "DIP", args: [{ int: "3" }, [{ prim: "DROP" }]] },
                          { prim: "DUG", args: [{ int: "2" }] },
                          { prim: "PUSH", args: [{ prim: "unit" }, { prim: "Unit" }] }]
                        ]
                      },
                      { prim: "DROP" },
                      { prim: "DIP", args: [[{ prim: "DUP" }]] },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DIP", args: [[{ prim: "DIP", args: [{ int: "4" }, [{ prim: "DUP" }]] }, { prim: "DIG", args: [{ int: "4" }] }]] },
                      { prim: "ADD" },
                      { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }, { prim: "CAR" }]] },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DROP" }]] }]] },
                      { prim: "DIP", args: [{ int: "2" }, [{ prim: "DUP" }]] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "SOME" },
                      {
                        prim: "DIP",
                        args: [[
                          { prim: "DIP", args: [{ int: "3" }, [{ prim: "DUP" }]] },
                          { prim: "DIG", args: [{ int: "3" }] },
                          { prim: "CAR" },
                          { prim: "CAR" }
                        ]]
                      },
                      { prim: "SOURCE" },
                      { prim: "UPDATE" },
                      {
                        prim: "DIP",
                        args: [[
                          { prim: "DIP", args: [{ int: "3" }, [{ prim: "DUP" }]] },
                          { prim: "DIG", args: [{ int: "3" }] },
                          { prim: "DUP" },
                          { prim: "CDR" },
                          { prim: "SWAP" },
                          { prim: "CAR" },
                          { prim: "CDR" }
                        ]]
                      },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "DIP", args: [{ int: "4" }, [{ prim: "DROP" }]] },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "DIP", args: [{ int: "5" }, [{ prim: "DUP" }]] },
                      { prim: "DIG", args: [{ int: "5" }] },
                      {
                        prim: "DIP",
                        args: [[
                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                          { prim: "SWAP" },
                          { prim: "SOME" },
                          { prim: "DIP", args: [[{ prim: "DIP", args: [{ int: "3" }, [{ prim: "DUP" }]] }, { prim: "DIG", args: [{ int: "3" }] }, { prim: "CAR" }, { prim: "CAR" }]] }
                        ]]
                      },
                      { prim: "UPDATE" },
                      {
                        prim: "DIP",
                        args: [[
                          { prim: "DIP", args: [{ int: "3" }, [{ prim: "DUP" }]] },
                          { prim: "DIG", args: [{ int: "3" }] },
                          { prim: "DUP" },
                          { prim: "CDR" },
                          { prim: "SWAP" },
                          { prim: "CAR" },
                          { prim: "CDR" }
                        ]]
                      },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "DIP", args: [{ int: "4" }, [{ prim: "DROP" }]] },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "DIP", args: [{ int: "3" }, [{ prim: "DUP" }]] },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DIP", args: [[{ prim: "DROP", args: [{ int: "8" }] }]] }
                    ]
                  ]
                },
                { prim: "SWAP" },
                { prim: "CAR" },
                { prim: "DIP", args: [{ int: "2" }, [{ prim: "DUP" }]] },
                { prim: "DIG", args: [{ int: "2" }] },
                { prim: "CDR" },
                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                { prim: "SWAP" },
                { prim: "DUP" },
                { prim: "CAR" },
                { prim: "CAR" },
                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }, { prim: "CDR" }]] },
                { prim: "PAIR" },
                {
                  prim: "DIP",
                  args: [[
                    { prim: "DUP" },
                    { prim: "CDR" },
                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                    { prim: "PAIR" }
                  ]]
                },
                { prim: "PAIR" },
                {
                  prim: "DIP",
                  args: [[
                    { prim: "DIP", args: [{ int: "3" }, [{ prim: "DUP" }]] },
                    { prim: "DIG", args: [{ int: "3" }] }
                  ]]
                },
                { prim: "EXEC" },
                { prim: "DIP", args: [[{ prim: "DROP" }]] },
                { prim: "NIL", args: [{ prim: "operation" }] },
                { prim: "PAIR" },
                { prim: "DIP", args: [[{ prim: "DROP", args: [{ int: "4" }] }]] }
                ]
              ]
            }
          ],
          storage: {
            prim: "Pair",
            args: [
              { prim: "Pair", args: [[], { string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn" }] },
              { int: "100" }
            ]
          }
        }
      }]
    },
    bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a00000000044c0200000447050007650765036e036e035b0501076507650861036e07650860036e035b0000000b25616c6c6f77616e636573045b000000082562616c616e636500000009256163636f756e7473046e00000006256f776e6572045b0000000c25746f74616c537570706c79050202000003da032109310000034607650765036e036e0765035b076507650761036e07650760036e035b035b036e035b076507650761036e07650760036e035b035b036e035b0200000309032103160316051f02000000020321034c03160317071f0002020000000203210570000203170316071f000302000000020321057000030317031703210316031603470329072f02000000140743036801000000094745545f464f524345032702000000000743035b00000723036e035b0342071f00040200000002032105700004051f0200000013071f00020200000002032105700002031603160329072f02000000060743036c030b02000000290321051f020000000d051f02000000020321034c0320034c0320051f020000000203200743036c030b0320071f00030200000002032105700003051f020000000d051f02000000020321034c03170319032a072c020000001d07430368010000001242616c616e636520697320746f6f206c6f77032702000000060743036c030b0320051f02000000020321034c0317051f020000000f071f00030200000002032105700003034b03210743035b0000034c03190337072c020000002507430368010000001a42616c616e63652063616e6e6f74206265206e65676174697665032702000000330321051f0200000011071f000202000000020321057000020316034c0342071f000302000000020320057100020743036c030b0320051f02000000020321034c0317051f020000000f071f000402000000020321057000040312051f020000000d051f02000000020321034c0316034c0342034c051f0200000009051f02000000020320071f000202000000020321057000020346051f0200000013071f000302000000020321057000030316031603470350051f0200000019071f0003020000000203210570000303210317034c0316031703420342071f00040200000002032005710003071f00050200000002032105700005051f0200000027051f02000000020321034c0346051f0200000013071f00030200000002032105700003031603160350051f0200000019071f0003020000000203210570000303210317034c0316031703420342071f00040200000002032005710003071f00030200000002032105700003051f02000000040520000800000000034c0316071f000202000000020321057000020317051f02000000020321034c032103160316051f02000000060321031603170342051f020000001803210317051f020000000b051f02000000020321034c03420342051f020000000f071f000302000000020321057000030326051f02000000020320053d036d0342051f020000000405200004000000350707070702000000000100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e00a401'
  },
  { // ANCHOR[epic=oversize_testcase] - oversize 1
    op: {
      branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
      contents: [{
        kind: "origination",
        counter: "1",
        source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
        fee: "10000",
        gas_limit: "10",
        storage_limit: "10",
        balance: "0",
        script: {
          code: [
            {
              prim: "parameter",
              args: [
                {
                  prim: "or",
                  args: [
                    {
                      prim: "or",
                      args: [
                        {
                          prim: "or",
                          args: [
                            { prim: "pair", args: [{ prim: "address" }, { prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }] },
                            {
                              prim: "or",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "address" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        { prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }
                                      ]
                                    }
                                  ]
                                },
                                { prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }
                              ]
                            }
                          ]
                        },
                        {
                          prim: "or",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "address" },
                                { prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }
                              ]
                            },
                            {
                              prim: "or",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "pair", args: [{ prim: "address" }, { prim: "address" }] },
                                    { prim: "contract", args: [{ prim: "nat" }] }
                                  ]
                                },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "address" },
                                    { prim: "contract", args: [{ prim: "nat" }] }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    },
                    {
                      prim: "or",
                      args: [
                        {
                          prim: "or",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "unit" },
                                { prim: "contract", args: [{ prim: "nat" }] }
                              ]
                            },
                            { prim: "or", args: [{ prim: "bool" }, { prim: "address" }] }
                          ]
                        },
                        {
                          prim: "or",
                          args: [
                            {
                              prim: "or",
                              args: [
                                { prim: "pair", args: [{ prim: "unit" }, { prim: "contract", args: [{ prim: "address" }] }] },
                                { prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }
                              ]
                            },
                            { prim: "or", args: [{ prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }, { prim: "address" }] }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              prim: "storage",
              args: [
                {
                  prim: "pair",
                  args: [
                    {
                      prim: "big_map",
                      args: [
                        { prim: "address" },
                        { prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }
                      ]
                    },
                    {
                      prim: "pair",
                      args: [
                        { prim: "pair", args: [{ prim: "address" }, { prim: "bool" }] },
                        { prim: "pair", args: [{ prim: "nat" }, { prim: "or", args: [{ prim: "address" }, { prim: "address" }] }] }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              prim: "code",
              args: [
                [
                  { prim: "DUP" },
                  { prim: "CAR" },
                  { prim: "DIP", args: [[{ prim: "CDR" }]] },
                  {
                    prim: "IF_LEFT",
                    args: [
                      [
                        {
                          prim: "IF_LEFT",
                          args: [
                            [
                              {
                                prim: "IF_LEFT",
                                args: [
                                  [
                                    { prim: "SENDER" },
                                    { prim: "PAIR" },
                                    {
                                      prim: "DIP",
                                      args: [
                                        [
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "CDR" },
                                          { prim: "IF", args: [[{ prim: "PUSH", args: [{ prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] }, { prim: "Pair", args: [{ string: "OperationsArePaused" }, { prim: "Unit" }] }] }, { prim: "FAILWITH" }], []] }
                                        ]
                                      ]
                                    },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                    { prim: "COMPARE" },
                                    { prim: "EQ" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [{ prim: "DROP" }],
                                        [
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                          { prim: "SWAP" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "COMPARE" },
                                          { prim: "EQ" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [{ prim: "DROP" }],
                                              [
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                {
                                                  prim: "DIP",
                                                  args: [[
                                                    { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                    { prim: "SWAP" },
                                                    {
                                                      prim: "DIP",
                                                      args: [[
                                                        {
                                                          prim: "DIP",
                                                          args: [[
                                                            { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }]] },
                                                            { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                            { prim: "PAIR" },
                                                            { prim: "DUP" },
                                                            {
                                                              prim: "DIP",
                                                              args: [
                                                                [
                                                                  { prim: "CDR" },
                                                                  { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                                  { prim: "GET" },
                                                                  {
                                                                    prim: "IF_NONE",
                                                                    args: [
                                                                      [{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }],
                                                                      [{ prim: "CDR" }]
                                                                    ]
                                                                  }
                                                                ]
                                                              ]
                                                            },
                                                            { prim: "CAR" },
                                                            { prim: "GET" },
                                                            { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], []] }
                                                          ]]
                                                        }
                                                      ]
                                                      ]
                                                    },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                    { prim: "SWAP" },
                                                    {
                                                      prim: "DIP",
                                                      args: [
                                                        [
                                                          { prim: "DUP" },
                                                          {
                                                            prim: "DIP",
                                                            args: [
                                                              [
                                                                {
                                                                  prim: "DIP",
                                                                  args: [
                                                                    [
                                                                      { prim: "DUP" },
                                                                      { prim: "CDR" },
                                                                      { prim: "CDR" },
                                                                      { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                                      { prim: "SWAP" },
                                                                      { prim: "SUB" },
                                                                      { prim: "ISNAT" },
                                                                      {
                                                                        prim: "IF_NONE",
                                                                        args: [
                                                                          [
                                                                            { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                                            { prim: "SWAP" },
                                                                            { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                                            { prim: "SWAP" },
                                                                            { prim: "CDR" },
                                                                            { prim: "CDR" },
                                                                            { prim: "PAIR" },
                                                                            { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughAllowance" }] },
                                                                            { prim: "PAIR" },
                                                                            { prim: "FAILWITH" }
                                                                          ],
                                                                          []
                                                                        ]
                                                                      }
                                                                    ]
                                                                  ]
                                                                },
                                                                { prim: "SWAP" }
                                                              ]
                                                            ]
                                                          },
                                                          { prim: "PAIR" }
                                                        ]
                                                      ]
                                                    },
                                                    { prim: "PAIR" },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }, { prim: "DROP" }, { prim: "DROP" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "GET" },
                                                    {
                                                      prim: "IF_NONE",
                                                      args: [
                                                        [
                                                          { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                                          { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                                          { prim: "PAIR" },
                                                          { prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }
                                                        ],
                                                        [{ prim: "DUP" }, { prim: "CDR" }]
                                                      ]
                                                    },
                                                    {
                                                      prim: "DIP",
                                                      args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]]
                                                    },
                                                    { prim: "SWAP" },
                                                    { prim: "CDR" },
                                                    { prim: "CDR" },
                                                    { prim: "DUP" },
                                                    { prim: "INT" },
                                                    { prim: "EQ" },
                                                    { prim: "IF", args: [[{ prim: "DROP" }, { prim: "NONE", args: [{ prim: "nat" }] }], [{ prim: "SOME" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] }, { prim: "SWAP" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "CDR" },
                                                    { prim: "CAR" },
                                                    { prim: "UPDATE" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "PAIR" },
                                                    { prim: "SOME" },
                                                    { prim: "SWAP" },
                                                    { prim: "CAR" },
                                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                                    { prim: "UPDATE" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "PAIR" }
                                                  ]]
                                                }
                                              ]
                                            ]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "GET" },
                                          { prim: "IF_NONE", args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "CDR" }, { prim: "INT" }, { prim: "EQ" }, { prim: "IF", args: [[{ prim: "NONE", args: [{ prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }] }], [{ prim: "DUP" }, { prim: "CDR" }, { prim: "CDR" }, { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] }, { prim: "PAIR" }, { prim: "SOME" }]] }], [{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }, { prim: "CDR" }, { prim: "CDR" }, { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }, { prim: "ADD" }, { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] }, { prim: "DIP", args: [[{ prim: "DROP" }]] }, { prim: "PAIR" }, { prim: "SOME" }]] },
                                          { prim: "SWAP" },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                          { prim: "DUP" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }, { prim: "CAR" }, { prim: "UPDATE" }, { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] }, { prim: "DIP", args: [[{ prim: "DROP" }]] }, { prim: "PAIR" }]] },
                                          { prim: "DUP" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }, { prim: "CDR" }, { prim: "INT" }, { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "DUP" }, { prim: "CDR" }, { prim: "CAR" }]] }, { prim: "ADD" }, { prim: "ISNAT" }, { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "string" }, { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }] }, { prim: "FAILWITH" }], []] }, { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] }, { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] }, { prim: "DIP", args: [[{ prim: "DROP" }]] }, { prim: "PAIR" }, { prim: "SWAP" }, { prim: "PAIR" }, { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] }, { prim: "DIP", args: [[{ prim: "DROP" }]] }, { prim: "SWAP" }, { prim: "PAIR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "GET" },
                                          { prim: "IF_NONE", args: [[{ prim: "CDR" }, { prim: "CDR" }, { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }, { prim: "SWAP" }, { prim: "PAIR" }, { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] }, { prim: "PAIR" }, { prim: "FAILWITH" }], []] },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "SUB" },
                                          { prim: "ISNAT" },
                                          { prim: "IF_NONE", args: [[{ prim: "CAR" }, { prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }, { prim: "CDR" }, { prim: "CDR" }, { prim: "PAIR" }, { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] }, { prim: "PAIR" }, { prim: "FAILWITH" }], []] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          {
                                            prim: "DIP",
                                            args: [
                                              [
                                                { prim: "DUP" },
                                                { prim: "CAR" },
                                                { prim: "INT" },
                                                { prim: "EQ" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [
                                                      { prim: "DUP" },
                                                      { prim: "CDR" },
                                                      { prim: "SIZE" },
                                                      { prim: "INT" },
                                                      { prim: "EQ" },
                                                      { prim: "IF", args: [[{ prim: "DROP" }, { prim: "NONE", args: [{ prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }] }], [{ prim: "SOME" }]] }
                                                    ],
                                                    [{ prim: "SOME" }]
                                                  ]
                                                },
                                                { prim: "SWAP" },
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                                { prim: "UPDATE" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                { prim: "PAIR" }
                                              ]
                                            ]
                                          },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [
                                              [
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                { prim: "NEG" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "DUP" }, { prim: "CDR" }, { prim: "CAR" }]] },
                                                { prim: "ADD" },
                                                { prim: "ISNAT" },
                                                { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "string" }, { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }] }, { prim: "FAILWITH" }], []] },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                { prim: "PAIR" },
                                                { prim: "SWAP" },
                                                { prim: "PAIR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "PAIR" }
                                              ]
                                            ]
                                          },
                                          { prim: "DROP" }
                                        ]
                                      ]
                                    },
                                    { prim: "NIL", args: [{ prim: "operation" }] },
                                    { prim: "PAIR" }
                                  ],
                                  [
                                    {
                                      prim: "IF_LEFT",
                                      args: [
                                        [
                                          {
                                            prim: "DIP",
                                            args: [
                                              [
                                                { prim: "DUP" },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                {
                                                  prim: "IF_LEFT",
                                                  args: [
                                                    [
                                                      {
                                                        prim: "PUSH",
                                                        args: [
                                                          { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                          { prim: "Pair", args: [{ string: "ProxyIsNotSet" }, { prim: "Unit" }] }
                                                        ]
                                                      },
                                                      { prim: "FAILWITH" }
                                                    ],
                                                    [
                                                      { prim: "SENDER" },
                                                      { prim: "COMPARE" },
                                                      { prim: "EQ" },
                                                      {
                                                        prim: "IF",
                                                        args: [
                                                          [],
                                                          [
                                                            {
                                                              prim: "PUSH",
                                                              args: [
                                                                { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                                { prim: "Pair", args: [{ string: "CallerIsNotProxy" }, { prim: "Unit" }] }
                                                              ]
                                                            },
                                                            { prim: "FAILWITH" }
                                                          ]
                                                        ]
                                                      }
                                                    ]
                                                  ]
                                                }
                                              ]
                                            ]
                                          },
                                          {
                                            prim: "DIP",
                                            args: [
                                              [
                                                { prim: "DUP" },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                { prim: "CDR" },
                                                { prim: "IF", args: [[{ prim: "PUSH", args: [{ prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] }, { prim: "Pair", args: [{ string: "OperationsArePaused" }, { prim: "Unit" }] }] }, { prim: "FAILWITH" }], []] }
                                              ]
                                            ]
                                          },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                          { prim: "COMPARE" },
                                          { prim: "EQ" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [{ prim: "DROP" }],
                                              [
                                                { prim: "DUP" },
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                { prim: "SWAP" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "COMPARE" },
                                                { prim: "EQ" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [{ prim: "DROP" }],
                                                    [
                                                      { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                      { prim: "SWAP" },
                                                      {
                                                        prim: "DIP",
                                                        args: [[
                                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                          { prim: "SWAP" },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                          { prim: "SWAP" },
                                                          {
                                                            prim: "DIP",
                                                            args: [[
                                                              {
                                                                prim: "DIP",
                                                                args: [[
                                                                  { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }]] },
                                                                  { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                                  { prim: "PAIR" },
                                                                  { prim: "DUP" },
                                                                  {
                                                                    prim: "DIP",
                                                                    args: [[
                                                                      { prim: "CDR" },
                                                                      { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                                      { prim: "GET" },
                                                                      {
                                                                        prim: "IF_NONE",
                                                                        args: [
                                                                          [{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }],
                                                                          [{ prim: "CDR" }]
                                                                        ]
                                                                      }
                                                                    ]]
                                                                  },
                                                                  {
                                                                    prim: "CAR"
                                                                  },
                                                                  { prim: "GET" },
                                                                  { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], []] }
                                                                ]]
                                                              }
                                                            ]]
                                                          },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                          { prim: "SWAP" },
                                                          {
                                                            prim: "DIP",
                                                            args: [[
                                                              { prim: "DUP" },
                                                              {
                                                                prim: "DIP",
                                                                args: [
                                                                  [
                                                                    {
                                                                      prim: "DIP",
                                                                      args: [[
                                                                        { prim: "DUP" },
                                                                        { prim: "CDR" },
                                                                        { prim: "CDR" },
                                                                        { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                                        { prim: "SWAP" },
                                                                        { prim: "SUB" },
                                                                        { prim: "ISNAT" },
                                                                        {
                                                                          prim: "IF_NONE",
                                                                          args: [
                                                                            [
                                                                              { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                                              { prim: "SWAP" },
                                                                              { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                                              { prim: "SWAP" },
                                                                              { prim: "CDR" },
                                                                              { prim: "CDR" },
                                                                              { prim: "PAIR" },
                                                                              { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughAllowance" }] },
                                                                              { prim: "PAIR" },
                                                                              { prim: "FAILWITH" }
                                                                            ],
                                                                            []
                                                                          ]
                                                                        }
                                                                      ]]
                                                                    },
                                                                    { prim: "SWAP" }
                                                                  ]
                                                                ]
                                                              },
                                                              { prim: "PAIR" }
                                                            ]]
                                                          },
                                                          { prim: "PAIR" },
                                                          { prim: "DIP", args: [[{ prim: "DROP" }, { prim: "DROP" }, { prim: "DROP" }]] },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                          { prim: "SWAP" },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                          { prim: "SWAP" },
                                                          { prim: "GET" },
                                                          {
                                                            prim: "IF_NONE",
                                                            args: [
                                                              [
                                                                { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                                                { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                                                { prim: "PAIR" },
                                                                { prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }
                                                              ],
                                                              [{ prim: "DUP" }, { prim: "CDR" }]
                                                            ]
                                                          },
                                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                          { prim: "SWAP" },
                                                          { prim: "CDR" },
                                                          { prim: "CDR" },
                                                          { prim: "DUP" },
                                                          { prim: "INT" },
                                                          { prim: "EQ" },
                                                          { prim: "IF", args: [[{ prim: "DROP" }, { prim: "NONE", args: [{ prim: "nat" }] }], [{ prim: "SOME" }]] },
                                                          {
                                                            prim: "DIP",
                                                            args: [[{ prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] }, { prim: "SWAP" }]]
                                                          },
                                                          { prim: "SWAP" },
                                                          { prim: "CDR" },
                                                          { prim: "CAR" },
                                                          { prim: "UPDATE" },
                                                          {
                                                            prim: "DIP",
                                                            args: [
                                                              [
                                                                { prim: "DUP" },
                                                                { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                                { prim: "CDR" }
                                                              ]
                                                            ]
                                                          },
                                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                          { prim: "SWAP" },
                                                          { prim: "PAIR" },
                                                          { prim: "SOME" },
                                                          { prim: "SWAP" },
                                                          { prim: "CAR" },
                                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                                          { prim: "UPDATE" },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                          { prim: "PAIR" }
                                                        ]]
                                                      }
                                                    ]
                                                  ]
                                                },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      { prim: "DUP" },
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "INT" },
                                                      { prim: "EQ" },
                                                      {
                                                        prim: "IF",
                                                        args: [
                                                          [
                                                            {
                                                              prim: "NONE",
                                                              args: [
                                                                {
                                                                  prim: "pair",
                                                                  args: [
                                                                    { prim: "nat" },
                                                                    { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }
                                                                  ]
                                                                }
                                                              ]
                                                            }
                                                          ],
                                                          [
                                                            { prim: "DUP" },
                                                            { prim: "CDR" },
                                                            { prim: "CDR" },
                                                            { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                                            { prim: "PAIR" },
                                                            { prim: "SOME" }
                                                          ]
                                                        ]
                                                      }
                                                    ],
                                                    [
                                                      { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                      { prim: "SWAP" },
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                      { prim: "ADD" },
                                                      { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                      { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                      { prim: "PAIR" },
                                                      { prim: "SOME" }
                                                    ]
                                                  ]
                                                },
                                                { prim: "SWAP" },
                                                { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DIP",
                                                  args: [[
                                                    { prim: "CDR" },
                                                    { prim: "CAR" },
                                                    { prim: "UPDATE" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "PAIR" }
                                                  ]]
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DIP",
                                                  args: [
                                                    [
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "INT" },
                                                      {
                                                        prim: "DIP",
                                                        args: [[
                                                          { prim: "DUP" },
                                                          { prim: "CDR" },
                                                          { prim: "DUP" },
                                                          { prim: "CDR" },
                                                          { prim: "CAR" }
                                                        ]]
                                                      },
                                                      { prim: "ADD" },
                                                      { prim: "ISNAT" },
                                                      {
                                                        prim: "IF_NONE",
                                                        args: [
                                                          [
                                                            {
                                                              prim: "PUSH",
                                                              args: [
                                                                { prim: "string" },
                                                                { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }
                                                              ]
                                                            },
                                                            { prim: "FAILWITH" }
                                                          ],
                                                          []
                                                        ]
                                                      },
                                                      { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                      { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                      { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                      { prim: "PAIR" },
                                                      { prim: "SWAP" },
                                                      { prim: "PAIR" },
                                                      { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                      { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                      { prim: "SWAP" },
                                                      { prim: "PAIR" }
                                                    ]
                                                  ]
                                                },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CAR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                                      { prim: "SWAP" },
                                                      { prim: "PAIR" },
                                                      { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] },
                                                      { prim: "PAIR" },
                                                      { prim: "FAILWITH" }
                                                    ],
                                                    []
                                                  ]
                                                },
                                                { prim: "DUP" },
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                { prim: "SWAP" },
                                                { prim: "SUB" },
                                                { prim: "ISNAT" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      { prim: "CAR" },
                                                      { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                      { prim: "SWAP" },
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "PAIR" },
                                                      { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] },
                                                      { prim: "PAIR" },
                                                      { prim: "FAILWITH" }
                                                    ],
                                                    []
                                                  ]
                                                },
                                                {
                                                  prim: "DIP",
                                                  args: [[
                                                    { prim: "DUP" },
                                                    { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                                    { prim: "CAR" }
                                                  ]]
                                                },
                                                { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                { prim: "PAIR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                {
                                                  prim: "DIP",
                                                  args: [
                                                    [
                                                      { prim: "DUP" },
                                                      { prim: "CAR" },
                                                      { prim: "INT" },
                                                      { prim: "EQ" },
                                                      {
                                                        prim: "IF",
                                                        args: [
                                                          [
                                                            { prim: "DUP" },
                                                            { prim: "CDR" },
                                                            { prim: "SIZE" },
                                                            { prim: "INT" },
                                                            { prim: "EQ" },
                                                            {
                                                              prim: "IF",
                                                              args: [
                                                                [
                                                                  { prim: "DROP" },
                                                                  {
                                                                    prim: "NONE",
                                                                    args: [
                                                                      {
                                                                        prim: "pair",
                                                                        args: [
                                                                          { prim: "nat" },
                                                                          { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }
                                                                        ]
                                                                      }
                                                                    ]
                                                                  }
                                                                ],
                                                                [
                                                                  {
                                                                    prim: "SOME"
                                                                  }
                                                                ]
                                                              ]
                                                            }
                                                          ],
                                                          [{ prim: "SOME" }]
                                                        ]
                                                      },
                                                      { prim: "SWAP" },
                                                      { prim: "CAR" },
                                                      { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                                      { prim: "UPDATE" },
                                                      {
                                                        prim: "DIP",
                                                        args: [
                                                          [
                                                            { prim: "DUP" },
                                                            { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                                            { prim: "CAR" }
                                                          ]
                                                        ]
                                                      },
                                                      { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                      { prim: "PAIR" }
                                                    ]
                                                  ]
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DIP",
                                                  args: [
                                                    [
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "NEG" },
                                                      {
                                                        prim: "DIP",
                                                        args: [[
                                                          { prim: "DUP" },
                                                          { prim: "CDR" },
                                                          { prim: "DUP" },
                                                          { prim: "CDR" },
                                                          { prim: "CAR" }
                                                        ]]
                                                      },
                                                      { prim: "ADD" },
                                                      { prim: "ISNAT" },
                                                      {
                                                        prim: "IF_NONE",
                                                        args: [
                                                          [
                                                            {
                                                              prim: "PUSH",
                                                              args: [
                                                                { prim: "string" },
                                                                { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }
                                                              ]
                                                            },
                                                            { prim: "FAILWITH" }
                                                          ],
                                                          []
                                                        ]
                                                      },
                                                      {
                                                        prim: "DIP",
                                                        args: [[
                                                          { prim: "DUP" },
                                                          { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                          { prim: "CDR" }
                                                        ]]
                                                      },
                                                      {
                                                        prim: "DIP",
                                                        args: [[
                                                          { prim: "DUP" },
                                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                                          { prim: "CAR" }
                                                        ]]
                                                      },
                                                      { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                      { prim: "PAIR" },
                                                      { prim: "SWAP" },
                                                      { prim: "PAIR" },
                                                      {
                                                        prim: "DIP",
                                                        args: [[
                                                          { prim: "DUP" },
                                                          { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                          { prim: "CDR" }
                                                        ]]
                                                      },
                                                      { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                      { prim: "SWAP" },
                                                      { prim: "PAIR" }
                                                    ]
                                                  ]
                                                },
                                                { prim: "DROP" }
                                              ]
                                            ]
                                          },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ],
                                        [
                                          { prim: "SENDER" },
                                          { prim: "PAIR" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "CDR" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "OperationsArePaused" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ],
                                                  []
                                                ]
                                              }
                                            ]]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "CAR" },
                                              { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                              { prim: "GET" },
                                              { prim: "IF_NONE", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }], [{ prim: "CDR" }]] }
                                            ]]
                                          },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "GET" },
                                          { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], []] },
                                          { prim: "DUP" },
                                          { prim: "INT" },
                                          { prim: "EQ" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [{ prim: "DROP" }],
                                              [
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                { prim: "INT" },
                                                { prim: "EQ" },
                                                { prim: "IF", args: [[{ prim: "DROP" }], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "UnsafeAllowanceChange" }] }, { prim: "PAIR" }, { prim: "FAILWITH" }]] }
                                              ]
                                            ]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                          { prim: "SWAP" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                          { prim: "SWAP" },
                                          { prim: "GET" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                                { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                                { prim: "PAIR" },
                                                { prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }
                                              ],
                                              [{ prim: "DUP" }, { prim: "CDR" }]
                                            ]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "DUP" },
                                          { prim: "INT" },
                                          { prim: "EQ" },
                                          { prim: "IF", args: [[{ prim: "DROP" }, { prim: "NONE", args: [{ prim: "nat" }] }], [{ prim: "SOME" }]] },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "UPDATE" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "SOME" },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                          { prim: "UPDATE" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ]
                                      ]
                                    }
                                  ]
                                ]
                              }
                            ],
                            [
                              {
                                prim: "IF_LEFT",
                                args: [
                                  [
                                    {
                                      prim: "DIP",
                                      args: [[
                                        { prim: "DUP" },
                                        { prim: "CDR" },
                                        { prim: "CDR" },
                                        { prim: "CDR" },
                                        {
                                          prim: "IF_LEFT",
                                          args: [
                                            [
                                              {
                                                prim: "PUSH",
                                                args: [
                                                  { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                  { prim: "Pair", args: [{ string: "ProxyIsNotSet" }, { prim: "Unit" }] }
                                                ]
                                              },
                                              { prim: "FAILWITH" }
                                            ],
                                            [
                                              { prim: "SENDER" },
                                              { prim: "COMPARE" },
                                              { prim: "EQ" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [],
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "CallerIsNotProxy" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ]
                                                ]
                                              }
                                            ]
                                          ]
                                        }
                                      ]]
                                    },
                                    {
                                      prim: "DIP",
                                      args: [[
                                        { prim: "DUP" },
                                        { prim: "CDR" },
                                        { prim: "CAR" },
                                        { prim: "CDR" },
                                        {
                                          prim: "IF",
                                          args: [
                                            [
                                              {
                                                prim: "PUSH",
                                                args: [
                                                  { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                  { prim: "Pair", args: [{ string: "OperationsArePaused" }, { prim: "Unit" }] }
                                                ]
                                              },
                                              { prim: "FAILWITH" }
                                            ],
                                            []
                                          ]
                                        }
                                      ]]
                                    },
                                    { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                    { prim: "SWAP" },
                                    { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    {
                                      prim: "DIP",
                                      args: [[
                                        { prim: "CAR" },
                                        { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                        { prim: "GET" },
                                        { prim: "IF_NONE", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }], [{ prim: "CDR" }]] }
                                      ]]
                                    },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "GET" },
                                    { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], []] },
                                    { prim: "DUP" },
                                    { prim: "INT" },
                                    { prim: "EQ" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [{ prim: "DROP" }],
                                        [
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "INT" },
                                          { prim: "EQ" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [{ prim: "DROP" }],
                                              [{ prim: "PUSH", args: [{ prim: "string" }, { string: "UnsafeAllowanceChange" }] }, { prim: "PAIR" }, { prim: "FAILWITH" }]
                                            ]
                                          }
                                        ]
                                      ]
                                    },
                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                    { prim: "SWAP" },
                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                    { prim: "SWAP" },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                          { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                          { prim: "PAIR" },
                                          { prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }
                                        ],
                                        [{ prim: "DUP" }, { prim: "CDR" }]
                                      ]
                                    },
                                    {
                                      prim: "DIP",
                                      args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]]
                                    },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "INT" },
                                    { prim: "EQ" },
                                    { prim: "IF", args: [[{ prim: "DROP" }, { prim: "NONE", args: [{ prim: "nat" }] }], [{ prim: "SOME" }]] },
                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] }, { prim: "SWAP" }]] },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "UPDATE" },
                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "SOME" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                    { prim: "UPDATE" },
                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                    { prim: "PAIR" },
                                    { prim: "NIL", args: [{ prim: "operation" }] },
                                    { prim: "PAIR" }
                                  ],
                                  [
                                    {
                                      prim: "IF_LEFT",
                                      args: [
                                        [
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "CAR" },
                                              { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                              { prim: "GET" },
                                              { prim: "IF_NONE", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }], [{ prim: "CDR" }]] }
                                            ]]
                                          },
                                          { prim: "CDR" },
                                          { prim: "GET" },
                                          { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], []] },
                                          { prim: "DIP", args: [[{ prim: "AMOUNT" }]] },
                                          { prim: "TRANSFER_TOKENS" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "SWAP" },
                                          { prim: "CONS" },
                                          { prim: "PAIR" }
                                        ],
                                        [
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                          { prim: "GET" },
                                          { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], [{ prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "AMOUNT" }]] },
                                          { prim: "TRANSFER_TOKENS" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "SWAP" },
                                          { prim: "CONS" },
                                          { prim: "PAIR" }
                                        ]
                                      ]
                                    }
                                  ]
                                ]
                              }
                            ]
                          ]
                        }
                      ],
                      [
                        {
                          prim: "IF_LEFT",
                          args: [
                            [
                              {
                                prim: "IF_LEFT",
                                args: [
                                  [
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                    { prim: "PAIR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIP", args: [[{ prim: "AMOUNT" }]] },
                                    { prim: "TRANSFER_TOKENS" },
                                    { prim: "NIL", args: [{ prim: "operation" }] },
                                    { prim: "SWAP" },
                                    { prim: "CONS" },
                                    { prim: "PAIR" }
                                  ],
                                  [
                                    {
                                      prim: "IF_LEFT",
                                      args: [
                                        [
                                          {
                                            prim: "DIP",
                                            args: [
                                              [
                                                { prim: "DUP" },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                { prim: "CAR" },
                                                { prim: "SENDER" },
                                                { prim: "COMPARE" },
                                                { prim: "EQ" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [],
                                                    [
                                                      {
                                                        prim: "PUSH",
                                                        args: [
                                                          { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                          { prim: "Pair", args: [{ string: "SenderIsNotAdmin" }, { prim: "Unit" }] }
                                                        ]
                                                      },
                                                      { prim: "FAILWITH" }
                                                    ]
                                                  ]
                                                }
                                              ]
                                            ]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "PAIR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ],
                                        [
                                          {
                                            prim: "DIP",
                                            args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "CAR" }, { prim: "CAR" }, { prim: "SENDER" }, { prim: "COMPARE" }, { prim: "EQ" }, { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] }, { prim: "Pair", args: [{ string: "SenderIsNotAdmin" }, { prim: "Unit" }] }] }, { prim: "FAILWITH" }]] }]]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "PAIR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ]
                                      ]
                                    }
                                  ]
                                ]
                              }
                            ],
                            [{
                              prim: "IF_LEFT",
                              args: [
                                [
                                  {
                                    prim: "IF_LEFT",
                                    args: [
                                      [
                                        { prim: "DUP" },
                                        { prim: "CAR" },
                                        { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                        { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                        { prim: "PAIR" },
                                        { prim: "CDR" },
                                        { prim: "CDR" },
                                        { prim: "CAR" },
                                        { prim: "CAR" },
                                        { prim: "DIP", args: [[{ prim: "AMOUNT" }]] },
                                        { prim: "TRANSFER_TOKENS" },
                                        { prim: "NIL", args: [{ prim: "operation" }] },
                                        { prim: "SWAP" },
                                        { prim: "CONS" },
                                        { prim: "PAIR" }
                                      ],
                                      [
                                        {
                                          prim: "DIP",
                                          args: [
                                            [
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "CAR" },
                                              { prim: "SENDER" },
                                              { prim: "COMPARE" },
                                              { prim: "EQ" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [],
                                                  [
                                                    { prim: "PUSH", args: [{ prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] }, { prim: "Pair", args: [{ string: "SenderIsNotAdmin" }, { prim: "Unit" }] }] },
                                                    { prim: "FAILWITH" }
                                                  ]
                                                ]
                                              }
                                            ]
                                          ]
                                        },
                                        { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                        { prim: "SWAP" },
                                        { prim: "CAR" },
                                        { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                        { prim: "SWAP" },
                                        { prim: "CAR" },
                                        { prim: "GET" },
                                        {
                                          prim: "IF_NONE",
                                          args: [
                                            [
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "INT" },
                                              { prim: "EQ" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [{ prim: "NONE", args: [{ prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }] }],
                                                  [{ prim: "DUP" }, { prim: "CDR" }, { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] }, { prim: "PAIR" }, { prim: "SOME" }]
                                                ]
                                              }
                                            ],
                                            [
                                              { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                              { prim: "SWAP" },
                                              { prim: "CDR" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                              { prim: "ADD" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "PAIR" },
                                              { prim: "SOME" }
                                            ]
                                          ]
                                        },
                                        { prim: "SWAP" },
                                        { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                        { prim: "DUP" },
                                        {
                                          prim: "DIP",
                                          args: [[
                                            { prim: "CAR" },
                                            { prim: "UPDATE" },
                                            { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                            { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                            { prim: "PAIR" }
                                          ]]
                                        },
                                        {
                                          prim: "DUP"
                                        },
                                        {
                                          prim: "DIP",
                                          args: [
                                            [
                                              { prim: "CDR" },
                                              { prim: "INT" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "DUP" }, { prim: "CDR" }, { prim: "CAR" }]] },
                                              { prim: "ADD" },
                                              { prim: "ISNAT" },
                                              {
                                                prim: "IF_NONE",
                                                args: [
                                                  [
                                                    { prim: "PUSH", args: [{ prim: "string" }, { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }] },
                                                    { prim: "FAILWITH" }
                                                  ],
                                                  []
                                                ]
                                              },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "PAIR" },
                                              { prim: "SWAP" },
                                              { prim: "PAIR" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "SWAP" },
                                              { prim: "PAIR" }
                                            ]
                                          ]
                                        },
                                        { prim: "DROP" },
                                        { prim: "NIL", args: [{ prim: "operation" }] },
                                        { prim: "PAIR" }
                                      ]
                                    ]
                                  }
                                ],
                                [
                                  {
                                    prim: "IF_LEFT",
                                    args: [
                                      [
                                        {
                                          prim: "DIP",
                                          args: [
                                            [
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "CAR" },
                                              { prim: "SENDER" },
                                              { prim: "COMPARE" },
                                              { prim: "EQ" },
                                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] }, { prim: "Pair", args: [{ string: "SenderIsNotAdmin" }, { prim: "Unit" }] }] }, { prim: "FAILWITH" }]] }
                                            ]
                                          ]
                                        },
                                        { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                        { prim: "SWAP" },
                                        { prim: "CAR" },
                                        { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                        { prim: "SWAP" },
                                        { prim: "CAR" },
                                        { prim: "GET" },
                                        { prim: "IF_NONE", args: [[{ prim: "CDR" }, { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }, { prim: "SWAP" }, { prim: "PAIR" }, { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] }, { prim: "PAIR" }, { prim: "FAILWITH" }], []] },
                                        { prim: "DUP" },
                                        { prim: "CAR" },
                                        { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                        { prim: "SWAP" },
                                        { prim: "CDR" },
                                        { prim: "SWAP" },
                                        { prim: "SUB" },
                                        { prim: "ISNAT" },
                                        { prim: "IF_NONE", args: [[{ prim: "CAR" }, { prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }, { prim: "CDR" }, { prim: "PAIR" }, { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] }, { prim: "PAIR" }, { prim: "FAILWITH" }], []] },
                                        { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                        { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                        { prim: "PAIR" },
                                        { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                        { prim: "SWAP" },
                                        {
                                          prim: "DIP",
                                          args: [[
                                            { prim: "DUP" },
                                            { prim: "CAR" },
                                            { prim: "INT" },
                                            { prim: "EQ" },
                                            {
                                              prim: "IF",
                                              args: [
                                                [
                                                  { prim: "DUP" },
                                                  { prim: "CDR" },
                                                  { prim: "SIZE" },
                                                  { prim: "INT" },
                                                  { prim: "EQ" },
                                                  { prim: "IF", args: [[{ prim: "DROP" }, { prim: "NONE", args: [{ prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }] }], [{ prim: "SOME" }]] }
                                                ],
                                                [{ prim: "SOME" }]
                                              ]
                                            },
                                            { prim: "SWAP" },
                                            { prim: "CAR" },
                                            { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                            { prim: "UPDATE" },
                                            { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                            { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                            { prim: "PAIR" }
                                          ]]
                                        },
                                        { prim: "DUP" },
                                        {
                                          prim: "DIP",
                                          args: [[
                                            { prim: "CDR" },
                                            { prim: "NEG" },
                                            { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "DUP" }, { prim: "CDR" }, { prim: "CAR" }]] },
                                            { prim: "ADD" },
                                            { prim: "ISNAT" },
                                            { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "string" }, { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }] }, { prim: "FAILWITH" }], []] },
                                            { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                            { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                            { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                            { prim: "PAIR" },
                                            { prim: "SWAP" },
                                            { prim: "PAIR" },
                                            { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                            { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                            { prim: "SWAP" },
                                            { prim: "PAIR" }
                                          ]]
                                        },
                                        { prim: "DROP" },
                                        { prim: "NIL", args: [{ prim: "operation" }] },
                                        { prim: "PAIR" }
                                      ],
                                      [
                                        {
                                          prim: "DIP",
                                          args: [[
                                            { prim: "DUP" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "IF_LEFT",
                                              args: [
                                                [
                                                  { prim: "SENDER" },
                                                  { prim: "COMPARE" },
                                                  { prim: "EQ" },
                                                  {
                                                    prim: "IF",
                                                    args: [
                                                      [],
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                            { prim: "Pair", args: [{ string: "NotAllowedToSetProxy" }, { prim: "Unit" }] }
                                                          ]
                                                        },
                                                        { prim: "FAILWITH" }
                                                      ]
                                                    ]
                                                  }
                                                ],
                                                [
                                                  {
                                                    prim: "PUSH",
                                                    args: [
                                                      { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                      { prim: "Pair", args: [{ string: "ProxyAlreadySet" }, { prim: "Unit" }] }
                                                    ]
                                                  },
                                                  { prim: "FAILWITH" }
                                                ]
                                              ]
                                            }
                                          ]]
                                        },
                                        { prim: "RIGHT", args: [{ prim: "address" }] },
                                        { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }]] },
                                        { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                        { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                        { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                        { prim: "SWAP" },
                                        { prim: "PAIR" },
                                        { prim: "SWAP" },
                                        { prim: "PAIR" },
                                        { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                        { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                        { prim: "SWAP" },
                                        { prim: "PAIR" },
                                        { prim: "NIL", args: [{ prim: "operation" }] },
                                        { prim: "PAIR" }
                                      ]
                                    ]
                                  }
                                ]
                              ]
                            }]
                          ]
                        }
                      ]
                    ]
                  }
                ]
              ]
            }
          ],
          storage: {
            prim: "Pair",
            args: [
              [{ prim: "Elt", args: [{ string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn" }, { prim: "Pair", args: [{ int: "17" }, []] }] }],
              { prim: "Pair", args: [{ prim: "Pair", args: [{ string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn" }, { prim: "False" }] }, { prim: "Pair", args: [{ int: "17" }, { prim: "Left", args: [{ string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn" }] }] }] }
            ]
          }
        }
      }]
    },
    bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000001f760200001f7105000764076407640765036e0765036e036207640765036e0765036e0765036e03620765036e036207640765036e0765036e0362076407650765036e036e055a03620765036e055a0362076407640765036c055a036207640359036e076407640765036c055a036e0765036e036207640765036e0362036e050107650761036e076503620760036e036207650765036e0359076503620764036e036e05020200001ece03210316051f02000000020317072e02000015ef072e020000128a072e020000080b03480342051f020000003a0321031703160317072c0200000026074307650368036c070701000000134f7065726174696f6e73417265506175736564030b0327020000000003210317032103170316051f0200000002031603190325072c02000000020320020000079b03210316051f02000000020317051f020000000403210316034c051f0200000002032103190325072c020000000203200200000223051f02000000020321034c051f0200000211051f02000000020321034c051f02000000020321034c051f0200000062051f020000005b051f0200000009051f02000000020321051f0200000002031603420321051f02000000210317051f020000000203160329072f02000000060723036e03620200000002031703160329072f02000000060743036200000200000000051f020000000403210316034c051f02000000790321051f020000006e051f0200000065032103170317051f020000000b051f02000000020321034c034c034b0356072f020000003b051f02000000020321034c051f02000000020321034c0317031703420743036801000000124e6f74456e6f756768416c6c6f77616e6365034203270200000000034c03420342051f0200000006032003200320051f020000000403210316034c051f020000000403210316034c0329072f020000001b074303620000051f02000000060723036e036203420723036e0362020000000403210317051f020000000b051f02000000020321034c034c03170317032103300325072c02000000060320053e036202000000020346051f0200000014051f020000000b051f02000000020321034c034c034c031703160350051f020000000d0321051f020000000203160317051f02000000020320034c03420346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f020000000203200342051f02000000020321034c0316051f02000000020321034c031703160329072f020000003903210317031703300325072c020000000c053e076503620760036e03620200000017032103170317051f02000000060723036e036203420346020000003d051f02000000020321034c03170317051f0200000004032103160312051f020000000d0321051f020000000203170316051f0200000002032003420346034c051f020000000b051f0200000004032103160321051f0200000025031703160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017c031703170330051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c0342051f02000000020321034c0316051f02000000020321034c03160329072f020000002b03170317074303620000034c03420743036801000000104e6f74456e6f75676842616c616e636503420327020000000003210316051f020000000b051f02000000020321034c034c03170317034c034b0356072f02000000300316051f02000000020321034c0317031703420743036801000000104e6f74456e6f75676842616c616e6365034203270200000000051f020000000d0321051f020000000203170316051f020000000203200342051f02000000020321034c051f02000000730321031603300325072c020000002603210317034503300325072c020000000e0320053e076503620760036e03620200000002034602000000020346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017c03170317033b051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c03420320053d036d03420200000a73072e0200000877051f02000000690321031703170317072e0200000020074307650368036c0707010000000d50726f787949734e6f74536574030b03270200000035034803190325072c02000000000200000023074307650368036c0707010000001043616c6c657249734e6f7450726f7879030b0327051f020000003a0321031703160317072c0200000026074307650368036c070701000000134f7065726174696f6e73417265506175736564030b0327020000000003210317032103170316051f0200000002031603190325072c02000000020320020000079b03210316051f02000000020317051f020000000403210316034c051f0200000002032103190325072c020000000203200200000223051f02000000020321034c051f0200000211051f02000000020321034c051f02000000020321034c051f0200000062051f020000005b051f0200000009051f02000000020321051f0200000002031603420321051f02000000210317051f020000000203160329072f02000000060723036e03620200000002031703160329072f02000000060743036200000200000000051f020000000403210316034c051f02000000790321051f020000006e051f0200000065032103170317051f020000000b051f02000000020321034c034c034b0356072f020000003b051f02000000020321034c051f02000000020321034c0317031703420743036801000000124e6f74456e6f756768416c6c6f77616e6365034203270200000000034c03420342051f0200000006032003200320051f020000000403210316034c051f020000000403210316034c0329072f020000001b074303620000051f02000000060723036e036203420723036e0362020000000403210317051f020000000b051f02000000020321034c034c03170317032103300325072c02000000060320053e036202000000020346051f0200000014051f020000000b051f02000000020321034c034c034c031703160350051f020000000d0321051f020000000203160317051f02000000020320034c03420346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f020000000203200342051f02000000020321034c0316051f02000000020321034c031703160329072f020000003903210317031703300325072c020000000c053e076503620760036e03620200000017032103170317051f02000000060723036e036203420346020000003d051f02000000020321034c03170317051f0200000004032103160312051f020000000d0321051f020000000203170316051f0200000002032003420346034c051f020000000b051f0200000004032103160321051f0200000025031703160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017c031703170330051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c0342051f02000000020321034c0316051f02000000020321034c03160329072f020000002b03170317074303620000034c03420743036801000000104e6f74456e6f75676842616c616e636503420327020000000003210316051f020000000b051f02000000020321034c034c03170317034c034b0356072f02000000300316051f02000000020321034c0317031703420743036801000000104e6f74456e6f75676842616c616e6365034203270200000000051f020000000d0321051f020000000203170316051f020000000203200342051f02000000020321034c051f02000000730321031603300325072c020000002603210317034503300325072c020000000e0320053e076503620760036e03620200000002034602000000020346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017c03170317033b051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c03420320053d036d034202000001f003480342051f020000003a0321031703160317072c0200000026074307650368036c070701000000134f7065726174696f6e73417265506175736564030b03270200000000051f02000000020321034c051f02000000020321034c0321051f02000000210316051f020000000203160329072f02000000060723036e036202000000020317031703160329072f02000000060743036200000200000000032103300325072c020000000203200200000043051f02000000020321034c0317031703300325072c020000000203200200000022074303680100000015556e73616665416c6c6f77616e63654368616e676503420327051f020000000403210316034c051f020000000403210316034c0329072f020000001b074303620000051f02000000060723036e036203420723036e0362020000000403210317051f020000000b051f02000000020321034c034c03170317032103300325072c02000000060320053e036202000000020346051f0200000014051f020000000b051f02000000020321034c034c034c031703160350051f020000000d0321051f020000000203160317051f02000000020320034c03420346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f020000000203200342053d036d03420200000359072e020000025c051f02000000690321031703170317072e0200000020074307650368036c0707010000000d50726f787949734e6f74536574030b03270200000035034803190325072c02000000000200000023074307650368036c0707010000001043616c6c657249734e6f7450726f7879030b0327051f020000003a0321031703160317072c0200000026074307650368036c070701000000134f7065726174696f6e73417265506175736564030b03270200000000051f02000000020321034c051f02000000020321034c0321051f02000000210316051f020000000203160329072f02000000060723036e036202000000020317031703160329072f02000000060743036200000200000000032103300325072c020000000203200200000043051f02000000020321034c0317031703300325072c020000000203200200000022074303680100000015556e73616665416c6c6f77616e63654368616e676503420327051f020000000403210316034c051f020000000403210316034c0329072f020000001b074303620000051f02000000060723036e036203420723036e0362020000000403210317051f020000000b051f02000000020321034c034c03170317032103300325072c02000000060320053e036202000000020346051f0200000014051f020000000b051f02000000020321034c034c034c031703160350051f020000000d0321051f020000000203160317051f02000000020320034c03420346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f020000000203200342053d036d034202000000f1072e020000008303210316051f02000000020317051f020000000b051f02000000020321034c034203210316051f020000000203170321051f02000000210316051f020000000203160329072f02000000060723036e03620200000002031703170329072f02000000060743036200000200000000051f02000000020313034d053d036d034c031b0342020000006203210316051f02000000020317051f020000000b051f02000000020321034c034203210316051f02000000020317051f020000000203160329072f020000000607430362000002000000020316051f02000000020313034d053d036d034c031b034202000008c6072e02000001ae072e020000003e03210316051f02000000020317051f020000000b051f02000000020321034c03420317031703170316051f02000000020313034d053d036d034c031b03420200000164072e02000000ad051f020000003d0321031703160316034803190325072c02000000000200000023074307650368036c0707010000001053656e64657249734e6f7441646d696e030b0327051f020000000403210317051f020000000d0321051f020000000203170316051f020000000d0321051f020000000203160317051f02000000020320034c03420342051f020000000d0321051f020000000203160317051f02000000020320034c0342053d036d034202000000ab051f020000003d0321031703160316034803190325072c02000000000200000023074307650368036c0707010000001053656e64657249734e6f7441646d696e030b0327051f020000000403210317051f020000000d0321051f020000000203170316051f020000000d0321051f020000000203170316051f0200000002032003420342051f020000000d0321051f020000000203160317051f02000000020320034c0342053d036d0342020000070c072e02000002f1072e020000003e03210316051f02000000020317051f020000000b051f02000000020321034c03420317031703160316051f02000000020313034d053d036d034c031b034202000002a7051f020000003d0321031703160316034803190325072c02000000000200000023074307650368036c0707010000001053656e64657249734e6f7441646d696e030b0327051f02000000020321034c0316051f02000000020321034c03160329072f02000000350321031703300325072c020000000c053e076503620760036e0362020000001503210317051f02000000060723036e036203420346020000003b051f02000000020321034c0317051f0200000004032103160312051f020000000d0321051f020000000203170316051f0200000002032003420346034c051f020000000b051f0200000004032103160321051f020000002303160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017a03170330051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c03420320053d036d0342020000040f072e020000031e051f020000003d0321031703160316034803190325072c02000000000200000023074307650368036c0707010000001053656e64657249734e6f7441646d696e030b0327051f02000000020321034c0316051f02000000020321034c03160329072f02000000290317074303620000034c03420743036801000000104e6f74456e6f75676842616c616e636503420327020000000003210316051f020000000b051f02000000020321034c034c0317034c034b0356072f020000002e0316051f02000000020321034c031703420743036801000000104e6f74456e6f75676842616c616e6365034203270200000000051f020000000d0321051f020000000203170316051f020000000203200342051f02000000020321034c051f02000000730321031603300325072c020000002603210317034503300325072c020000000e0320053e076503620760036e03620200000002034602000000020346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017a0317033b051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c03420320053d036d034202000000e5051f020000006f0321031703170317072e0200000039034803190325072c02000000000200000027074307650368036c070701000000144e6f74416c6c6f776564546f53657450726f7879030b03270200000022074307650368036c0707010000000f50726f7879416c7265616479536574030b03270544036e051f020000000403210317051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203160317051f02000000020320034c0342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c0342053d036d0342000000990707020000003407040100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e070700110200000000070707070100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e03030707001105050100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e'
  },
  { // ANCHOR[epic=oversize_testcase] - oversize 2
    op: {
      branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
      contents: [
        {
          kind: "reveal",
          counter: "1",
          source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
          public_key: "edpkvS5QFv7KRGfa3b87gg9DBpxSm3NpSwnjhUjNBQrRUUR66F7C9g",
          fee: "10000",
          gas_limit: "10",
          storage_limit: "10"
        },
        {
          kind: "origination",
          counter: "1",
          source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
          fee: "10000",
          gas_limit: "10",
          storage_limit: "10",
          balance: "0",
          script: {
            code: [
              {
                prim: "parameter",
                args: [
                  {
                    prim: "or",
                    args: [
                      {
                        prim: "or",
                        args: [
                          {
                            prim: "or",
                            args: [
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address" },
                                  { prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }
                                ]
                              },
                              {
                                prim: "or",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "pair", args: [{ prim: "address" }, { prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }] }
                                    ]
                                  },
                                  { prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }
                                ]
                              }
                            ]
                          },
                          {
                            prim: "or",
                            args: [
                              { prim: "pair", args: [{ prim: "address" }, { prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }] },
                              {
                                prim: "or",
                                args: [
                                  { prim: "pair", args: [{ prim: "pair", args: [{ prim: "address" }, { prim: "address" }] }, { prim: "contract", args: [{ prim: "nat" }] }] },
                                  { prim: "pair", args: [{ prim: "address" }, { prim: "contract", args: [{ prim: "nat" }] }] }
                                ]
                              }
                            ]
                          }
                        ]
                      },
                      {
                        prim: "or",
                        args: [
                          {
                            prim: "or",
                            args: [
                              { prim: "pair", args: [{ prim: "unit" }, { prim: "contract", args: [{ prim: "nat" }] }] },
                              { prim: "or", args: [{ prim: "bool" }, { prim: "address" }] }
                            ]
                          },
                          {
                            prim: "or",
                            args: [
                              {
                                prim: "or",
                                args: [
                                  { prim: "pair", args: [{ prim: "unit" }, { prim: "contract", args: [{ prim: "address" }] }] },
                                  { prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }
                                ]
                              },
                              { prim: "or", args: [{ prim: "pair", args: [{ prim: "address" }, { prim: "nat" }] }, { prim: "address" }] }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                prim: "storage",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "big_map", args: [{ prim: "address" }, { prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "pair", args: [{ prim: "address" }, { prim: "bool" }] },
                          { prim: "pair", args: [{ prim: "nat" }, { prim: "or", args: [{ prim: "address" }, { prim: "address" }] }] }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                prim: "code",
                args: [[
                  { prim: "DUP" },
                  { prim: "CAR" },
                  { prim: "DIP", args: [[{ prim: "CDR" }]] },
                  {
                    prim: "IF_LEFT",
                    args: [
                      [
                        {
                          prim: "IF_LEFT",
                          args: [
                            [
                              {
                                prim: "IF_LEFT",
                                args: [
                                  [
                                    { prim: "SENDER" },
                                    { prim: "PAIR" },
                                    {
                                      prim: "DIP",
                                      args: [
                                        [
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "CDR" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [
                                                {
                                                  prim: "PUSH",
                                                  args: [
                                                    { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                    { prim: "Pair", args: [{ string: "OperationsArePaused" }, { prim: "Unit" }] }
                                                  ]
                                                },
                                                { prim: "FAILWITH" }
                                              ],
                                              []
                                            ]
                                          }
                                        ]
                                      ]
                                    },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                    { prim: "COMPARE" },
                                    { prim: "EQ" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [{ prim: "DROP" }],
                                        [
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                          { prim: "SWAP" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "COMPARE" },
                                          { prim: "EQ" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [{ prim: "DROP" }],
                                              [
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                {
                                                  prim: "DIP",
                                                  args: [[
                                                    { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                    { prim: "SWAP" },
                                                    {
                                                      prim: "DIP",
                                                      args: [[{
                                                        prim: "DIP",
                                                        args: [
                                                          [
                                                            { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }]] },
                                                            { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                            { prim: "PAIR" },
                                                            { prim: "DUP" },
                                                            {
                                                              prim: "DIP",
                                                              args: [
                                                                [
                                                                  { prim: "CDR" },
                                                                  { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                                  { prim: "GET" },
                                                                  {
                                                                    prim: "IF_NONE",
                                                                    args: [
                                                                      [{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }],
                                                                      [{ prim: "CDR" }]
                                                                    ]
                                                                  }
                                                                ]
                                                              ]
                                                            },
                                                            { prim: "CAR" },
                                                            { prim: "GET" },
                                                            { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], []] }
                                                          ]
                                                        ]
                                                      }]]
                                                    },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                    { prim: "SWAP" },
                                                    {
                                                      prim: "DIP",
                                                      args: [
                                                        [
                                                          { prim: "DUP" },
                                                          {
                                                            prim: "DIP",
                                                            args: [
                                                              [
                                                                {
                                                                  prim: "DIP",
                                                                  args: [[
                                                                    { prim: "DUP" },
                                                                    { prim: "CDR" },
                                                                    { prim: "CDR" },
                                                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                                    { prim: "SWAP" },
                                                                    { prim: "SUB" },
                                                                    { prim: "ISNAT" },
                                                                    {
                                                                      prim: "IF_NONE",
                                                                      args: [
                                                                        [
                                                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                                          { prim: "SWAP" },
                                                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                                          { prim: "SWAP" },
                                                                          { prim: "CDR" },
                                                                          { prim: "CDR" },
                                                                          { prim: "PAIR" },
                                                                          { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughAllowance" }] },
                                                                          { prim: "PAIR" },
                                                                          { prim: "FAILWITH" }
                                                                        ],
                                                                        []
                                                                      ]
                                                                    }
                                                                  ]]
                                                                },
                                                                { prim: "SWAP" }
                                                              ]
                                                            ]
                                                          },
                                                          { prim: "PAIR" }
                                                        ]
                                                      ]
                                                    },
                                                    { prim: "PAIR" },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }, { prim: "DROP" }, { prim: "DROP" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "GET" },
                                                    {
                                                      prim: "IF_NONE",
                                                      args: [
                                                        [
                                                          { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                                          { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                                          { prim: "PAIR" },
                                                          { prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }
                                                        ],
                                                        [{ prim: "DUP" }, { prim: "CDR" }]
                                                      ]
                                                    },
                                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "CDR" },
                                                    { prim: "CDR" },
                                                    { prim: "DUP" },
                                                    { prim: "INT" },
                                                    { prim: "EQ" },
                                                    { prim: "IF", args: [[{ prim: "DROP" }, { prim: "NONE", args: [{ prim: "nat" }] }], [{ prim: "SOME" }]] },
                                                    {
                                                      prim: "DIP",
                                                      args: [
                                                        [
                                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                          { prim: "SWAP" }
                                                        ]
                                                      ]
                                                    },
                                                    { prim: "SWAP" },
                                                    { prim: "CDR" },
                                                    { prim: "CAR" },
                                                    { prim: "UPDATE" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "PAIR" },
                                                    { prim: "SOME" },
                                                    { prim: "SWAP" },
                                                    { prim: "CAR" },
                                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                                    { prim: "UPDATE" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "PAIR" }
                                                  ]]
                                                }
                                              ]
                                            ]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "GET" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                { prim: "DUP" },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                { prim: "INT" },
                                                { prim: "EQ" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [{ prim: "NONE", args: [{ prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }] }],
                                                    [
                                                      { prim: "DUP" },
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                                      { prim: "PAIR" },
                                                      { prim: "SOME" }
                                                    ]
                                                  ]
                                                }
                                              ],
                                              [
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                { prim: "ADD" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                { prim: "PAIR" },
                                                { prim: "SOME" }
                                              ]
                                            ]
                                          },
                                          { prim: "SWAP" },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "UPDATE" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "PAIR" }
                                            ]]
                                          },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "CDR" },
                                              { prim: "CDR" },
                                              { prim: "INT" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "DUP" }, { prim: "CDR" }, { prim: "CAR" }]] },
                                              { prim: "ADD" },
                                              { prim: "ISNAT" },
                                              {
                                                prim: "IF_NONE",
                                                args: [
                                                  [
                                                    { prim: "PUSH", args: [{ prim: "string" }, { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }] },
                                                    { prim: "FAILWITH" }
                                                  ],
                                                  []
                                                ]
                                              },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "PAIR" },
                                              { prim: "SWAP" },
                                              { prim: "PAIR" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "SWAP" },
                                              { prim: "PAIR" }
                                            ]]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "GET" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                                { prim: "SWAP" },
                                                { prim: "PAIR" },
                                                { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] },
                                                { prim: "PAIR" },
                                                { prim: "FAILWITH" }
                                              ],
                                              []
                                            ]
                                          },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "SUB" },
                                          { prim: "ISNAT" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                { prim: "PAIR" },
                                                { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] },
                                                { prim: "PAIR" },
                                                { prim: "FAILWITH" }
                                              ],
                                              []
                                            ]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CAR" },
                                              { prim: "INT" },
                                              { prim: "EQ" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [
                                                    { prim: "DUP" },
                                                    { prim: "CDR" },
                                                    { prim: "SIZE" },
                                                    { prim: "INT" },
                                                    { prim: "EQ" },
                                                    {
                                                      prim: "IF",
                                                      args: [
                                                        [
                                                          { prim: "DROP" },
                                                          {
                                                            prim: "NONE",
                                                            args: [{ prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }]
                                                          }
                                                        ],
                                                        [{ prim: "SOME" }]
                                                      ]
                                                    }
                                                  ],
                                                  [{ prim: "SOME" }]
                                                ]
                                              },
                                              { prim: "SWAP" },
                                              { prim: "CAR" },
                                              { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                              { prim: "UPDATE" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "PAIR" }
                                            ]]
                                          },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "CDR" },
                                              { prim: "CDR" },
                                              { prim: "NEG" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "DUP" }, { prim: "CDR" }, { prim: "CAR" }]] },
                                              { prim: "ADD" },
                                              { prim: "ISNAT" },
                                              {
                                                prim: "IF_NONE",
                                                args: [
                                                  [
                                                    { prim: "PUSH", args: [{ prim: "string" }, { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }] },
                                                    { prim: "FAILWITH" }
                                                  ],
                                                  []
                                                ]
                                              },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "PAIR" },
                                              { prim: "SWAP" },
                                              { prim: "PAIR" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "SWAP" },
                                              { prim: "PAIR" }
                                            ]]
                                          },
                                          { prim: "DROP" }
                                        ]
                                      ]
                                    },
                                    { prim: "NIL", args: [{ prim: "operation" }] },
                                    { prim: "PAIR" }
                                  ],
                                  [
                                    {
                                      prim: "IF_LEFT",
                                      args: [
                                        [
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CDR" },
                                              { prim: "CDR" },
                                              {
                                                prim: "IF_LEFT",
                                                args: [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "ProxyIsNotSet" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ],
                                                  [
                                                    { prim: "SENDER" },
                                                    { prim: "COMPARE" },
                                                    { prim: "EQ" },
                                                    {
                                                      prim: "IF",
                                                      args: [
                                                        [],
                                                        [
                                                          {
                                                            prim: "PUSH",
                                                            args: [
                                                              { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                              { prim: "Pair", args: [{ string: "CallerIsNotProxy" }, { prim: "Unit" }] }
                                                            ]
                                                          },
                                                          { prim: "FAILWITH" }
                                                        ]
                                                      ]
                                                    }
                                                  ]
                                                ]
                                              }
                                            ]]
                                          },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "CDR" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "OperationsArePaused" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ],
                                                  []
                                                ]
                                              }
                                            ]]
                                          },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                          { prim: "COMPARE" },
                                          { prim: "EQ" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [{ prim: "DROP" }],
                                              [
                                                { prim: "DUP" },
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                { prim: "SWAP" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "COMPARE" },
                                                { prim: "EQ" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [{ prim: "DROP" }],
                                                    [
                                                      { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                      { prim: "SWAP" },
                                                      {
                                                        prim: "DIP",
                                                        args: [[
                                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                          { prim: "SWAP" },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                          { prim: "SWAP" },
                                                          {
                                                            prim: "DIP",
                                                            args: [[
                                                              {
                                                                prim: "DIP",
                                                                args: [[
                                                                  { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }]] },
                                                                  { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                                  { prim: "PAIR" },
                                                                  { prim: "DUP" },
                                                                  {
                                                                    prim: "DIP",
                                                                    args: [[
                                                                      { prim: "CDR" },
                                                                      { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                                                      { prim: "GET" },
                                                                      {
                                                                        prim: "IF_NONE",
                                                                        args: [
                                                                          [{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }],
                                                                          [{ prim: "CDR" }]
                                                                        ]
                                                                      }
                                                                    ]]
                                                                  },
                                                                  { prim: "CAR" },
                                                                  { prim: "GET" },
                                                                  { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], []] }
                                                                ]]
                                                              }
                                                            ]]
                                                          },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                          { prim: "SWAP" },
                                                          {
                                                            prim: "DIP",
                                                            args: [[
                                                              { prim: "DUP" },
                                                              {
                                                                prim: "DIP",
                                                                args: [[
                                                                  {
                                                                    prim: "DIP",
                                                                    args: [[
                                                                      { prim: "DUP" },
                                                                      { prim: "CDR" },
                                                                      { prim: "CDR" },
                                                                      { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                                      { prim: "SWAP" },
                                                                      { prim: "SUB" },
                                                                      { prim: "ISNAT" },
                                                                      {
                                                                        prim: "IF_NONE",
                                                                        args: [
                                                                          [
                                                                            { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                                            { prim: "SWAP" },
                                                                            { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                                            { prim: "SWAP" },
                                                                            { prim: "CDR" },
                                                                            { prim: "CDR" },
                                                                            { prim: "PAIR" },
                                                                            { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughAllowance" }] },
                                                                            { prim: "PAIR" },
                                                                            { prim: "FAILWITH" }
                                                                          ],
                                                                          []
                                                                        ]
                                                                      }
                                                                    ]]
                                                                  },
                                                                  { prim: "SWAP" }
                                                                ]]
                                                              },
                                                              { prim: "PAIR" }
                                                            ]]
                                                          },
                                                          { prim: "PAIR" },
                                                          { prim: "DIP", args: [[{ prim: "DROP" }, { prim: "DROP" }, { prim: "DROP" }]] },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                          { prim: "SWAP" },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                          { prim: "SWAP" },
                                                          { prim: "GET" },
                                                          {
                                                            prim: "IF_NONE",
                                                            args: [
                                                              [
                                                                { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                                                { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                                                { prim: "PAIR" },
                                                                { prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }
                                                              ],
                                                              [{ prim: "DUP" }, { prim: "CDR" }]
                                                            ]
                                                          },
                                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                          { prim: "SWAP" },
                                                          { prim: "CDR" },
                                                          { prim: "CDR" },
                                                          { prim: "DUP" },
                                                          { prim: "INT" },
                                                          { prim: "EQ" },
                                                          {
                                                            prim: "IF",
                                                            args: [
                                                              [{ prim: "DROP" }, { prim: "NONE", args: [{ prim: "nat" }] }],
                                                              [{ prim: "SOME" }]
                                                            ]
                                                          },
                                                          {
                                                            prim: "DIP",
                                                            args: [[
                                                              { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                              { prim: "SWAP" }
                                                            ]]
                                                          },
                                                          { prim: "SWAP" },
                                                          { prim: "CDR" },
                                                          { prim: "CAR" },
                                                          { prim: "UPDATE" },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                          { prim: "SWAP" },
                                                          { prim: "PAIR" },
                                                          { prim: "SOME" },
                                                          { prim: "SWAP" },
                                                          { prim: "CAR" },
                                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                                          { prim: "UPDATE" },
                                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                          { prim: "PAIR" }
                                                        ]]
                                                      }
                                                    ]
                                                  ]
                                                },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      { prim: "DUP" },
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "INT" },
                                                      { prim: "EQ" },
                                                      {
                                                        prim: "IF",
                                                        args: [
                                                          [{ prim: "NONE", args: [{ prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }] }],
                                                          [
                                                            { prim: "DUP" },
                                                            { prim: "CDR" },
                                                            { prim: "CDR" },
                                                            { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                                            { prim: "PAIR" },
                                                            { prim: "SOME" }
                                                          ]
                                                        ]
                                                      }
                                                    ],
                                                    [
                                                      { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                      { prim: "SWAP" },
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                      { prim: "ADD" },
                                                      { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                      { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                      { prim: "PAIR" },
                                                      { prim: "SOME" }
                                                    ]
                                                  ]
                                                },
                                                { prim: "SWAP" },
                                                { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DIP",
                                                  args: [[
                                                    { prim: "CDR" },
                                                    { prim: "CAR" },
                                                    { prim: "UPDATE" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "PAIR" }
                                                  ]]
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DIP",
                                                  args: [[
                                                    { prim: "CDR" },
                                                    { prim: "CDR" },
                                                    { prim: "INT" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "DUP" }, { prim: "CDR" }, { prim: "CAR" }]] },
                                                    { prim: "ADD" },
                                                    { prim: "ISNAT" },
                                                    {
                                                      prim: "IF_NONE",
                                                      args: [
                                                        [
                                                          { prim: "PUSH", args: [{ prim: "string" }, { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }] },
                                                          { prim: "FAILWITH" }
                                                        ],
                                                        []
                                                      ]
                                                    },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "PAIR" },
                                                    { prim: "SWAP" },
                                                    { prim: "PAIR" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "PAIR" }
                                                  ]]
                                                },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CAR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                                      { prim: "SWAP" },
                                                      { prim: "PAIR" },
                                                      { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] },
                                                      { prim: "PAIR" },
                                                      { prim: "FAILWITH" }
                                                    ],
                                                    []
                                                  ]
                                                },
                                                { prim: "DUP" },
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                { prim: "SWAP" },
                                                { prim: "SUB" },
                                                { prim: "ISNAT" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      { prim: "CAR" },
                                                      { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                      { prim: "SWAP" },
                                                      { prim: "CDR" },
                                                      { prim: "CDR" },
                                                      { prim: "PAIR" },
                                                      { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] },
                                                      { prim: "PAIR" },
                                                      { prim: "FAILWITH" }
                                                    ],
                                                    []
                                                  ]
                                                },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                { prim: "PAIR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                {
                                                  prim: "DIP",
                                                  args: [[
                                                    { prim: "DUP" },
                                                    { prim: "CAR" },
                                                    { prim: "INT" },
                                                    { prim: "EQ" },
                                                    {
                                                      prim: "IF",
                                                      args: [
                                                        [
                                                          { prim: "DUP" },
                                                          { prim: "CDR" },
                                                          { prim: "SIZE" },
                                                          { prim: "INT" },
                                                          { prim: "EQ" },
                                                          {
                                                            prim: "IF",
                                                            args: [
                                                              [
                                                                { prim: "DROP" },
                                                                { prim: "NONE", args: [{ prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }] }
                                                              ],
                                                              [{ prim: "SOME" }]
                                                            ]
                                                          }
                                                        ],
                                                        [{ prim: "SOME" }]
                                                      ]
                                                    },
                                                    { prim: "SWAP" },
                                                    { prim: "CAR" },
                                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                                    { prim: "UPDATE" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "PAIR" }
                                                  ]]
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DIP",
                                                  args: [[
                                                    { prim: "CDR" },
                                                    { prim: "CDR" },
                                                    { prim: "NEG" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "DUP" }, { prim: "CDR" }, { prim: "CAR" }]] },
                                                    { prim: "ADD" },
                                                    { prim: "ISNAT" },
                                                    {
                                                      prim: "IF_NONE",
                                                      args: [
                                                        [
                                                          { prim: "PUSH", args: [{ prim: "string" }, { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }] },
                                                          { prim: "FAILWITH" }
                                                        ],
                                                        []
                                                      ]
                                                    },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "PAIR" },
                                                    { prim: "SWAP" },
                                                    { prim: "PAIR" },
                                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                    { prim: "SWAP" },
                                                    { prim: "PAIR" }
                                                  ]]
                                                },
                                                { prim: "DROP" }
                                              ]
                                            ]
                                          },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ],
                                        [
                                          { prim: "SENDER" },
                                          { prim: "PAIR" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "CDR" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "OperationsArePaused" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ],
                                                  []
                                                ]
                                              }
                                            ]]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "CAR" },
                                              { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                              { prim: "GET" },
                                              { prim: "IF_NONE", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }], [{ prim: "CDR" }]] }
                                            ]]
                                          },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "GET" },
                                          { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], []] },
                                          { prim: "DUP" },
                                          { prim: "INT" },
                                          { prim: "EQ" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [{ prim: "DROP" }],
                                              [
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                { prim: "INT" },
                                                { prim: "EQ" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [{ prim: "DROP" }],
                                                    [
                                                      { prim: "PUSH", args: [{ prim: "string" }, { string: "UnsafeAllowanceChange" }] },
                                                      { prim: "PAIR" },
                                                      { prim: "FAILWITH" }
                                                    ]
                                                  ]
                                                }
                                              ]
                                            ]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                          { prim: "SWAP" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                          { prim: "SWAP" },
                                          { prim: "GET" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                                { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                                { prim: "PAIR" },
                                                { prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }
                                              ],
                                              [{ prim: "DUP" }, { prim: "CDR" }]
                                            ]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "DUP" },
                                          { prim: "INT" },
                                          { prim: "EQ" },
                                          { prim: "IF", args: [[{ prim: "DROP" }, { prim: "NONE", args: [{ prim: "nat" }] }], [{ prim: "SOME" }]] },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "UPDATE" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "SOME" },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                          { prim: "UPDATE" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ]
                                      ]
                                    }
                                  ]
                                ]
                              }
                            ],
                            [
                              {
                                prim: "IF_LEFT",
                                args: [
                                  [
                                    {
                                      prim: "DIP",
                                      args: [[
                                        { prim: "DUP" },
                                        { prim: "CDR" },
                                        { prim: "CDR" },
                                        { prim: "CDR" },
                                        {
                                          prim: "IF_LEFT",
                                          args: [
                                            [
                                              {
                                                prim: "PUSH",
                                                args: [
                                                  { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                  { prim: "Pair", args: [{ string: "ProxyIsNotSet" }, { prim: "Unit" }] }
                                                ]
                                              },
                                              { prim: "FAILWITH" }
                                            ],
                                            [
                                              { prim: "SENDER" },
                                              { prim: "COMPARE" },
                                              { prim: "EQ" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [],
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "CallerIsNotProxy" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ]
                                                ]
                                              }
                                            ]
                                          ]
                                        }
                                      ]]
                                    },
                                    {
                                      prim: "DIP",
                                      args: [[
                                        { prim: "DUP" },
                                        { prim: "CDR" },
                                        { prim: "CAR" },
                                        { prim: "CDR" },
                                        {
                                          prim: "IF",
                                          args: [
                                            [
                                              {
                                                prim: "PUSH",
                                                args: [
                                                  { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                  { prim: "Pair", args: [{ string: "OperationsArePaused" }, { prim: "Unit" }] }
                                                ]
                                              },
                                              { prim: "FAILWITH" }
                                            ],
                                            []
                                          ]
                                        }
                                      ]]
                                    },
                                    { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                    { prim: "SWAP" },
                                    { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    {
                                      prim: "DIP",
                                      args: [[
                                        { prim: "CAR" },
                                        { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                        { prim: "GET" },
                                        { prim: "IF_NONE", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }], [{ prim: "CDR" }]] }
                                      ]]
                                    },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "GET" },
                                    { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], []] },
                                    { prim: "DUP" },
                                    { prim: "INT" },
                                    { prim: "EQ" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [{ prim: "DROP" }],
                                        [
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "INT" },
                                          { prim: "EQ" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [{ prim: "DROP" }],
                                              [{ prim: "PUSH", args: [{ prim: "string" }, { string: "UnsafeAllowanceChange" }] }, { prim: "PAIR" }, { prim: "FAILWITH" }]
                                            ]
                                          }
                                        ]
                                      ]
                                    },
                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                    { prim: "SWAP" },
                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                    { prim: "SWAP" },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                          { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                          { prim: "PAIR" },
                                          { prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }
                                        ],
                                        [{ prim: "DUP" }, { prim: "CDR" }]
                                      ]
                                    },
                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "INT" },
                                    { prim: "EQ" },
                                    { prim: "IF", args: [[{ prim: "DROP" }, { prim: "NONE", args: [{ prim: "nat" }] }], [{ prim: "SOME" }]] },
                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] }, { prim: "SWAP" }]] },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "UPDATE" },
                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "SOME" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                    { prim: "UPDATE" },
                                    { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                    { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                    { prim: "PAIR" },
                                    { prim: "NIL", args: [{ prim: "operation" }] },
                                    { prim: "PAIR" }
                                  ],
                                  [
                                    {
                                      prim: "IF_LEFT",
                                      args: [
                                        [
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "CAR" },
                                              { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                              { prim: "GET" },
                                              {
                                                prim: "IF_NONE",
                                                args: [
                                                  [{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }],
                                                  [{ prim: "CDR" }]
                                                ]
                                              }
                                            ]]
                                          },
                                          { prim: "CDR" },
                                          { prim: "GET" },
                                          { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], []] },
                                          { prim: "DIP", args: [[{ prim: "AMOUNT" }]] },
                                          { prim: "TRANSFER_TOKENS" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "SWAP" },
                                          { prim: "CONS" },
                                          { prim: "PAIR" }
                                        ],
                                        [
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "CAR" }]] },
                                          { prim: "GET" },
                                          { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] }], [{ prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "AMOUNT" }]] },
                                          { prim: "TRANSFER_TOKENS" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "SWAP" },
                                          { prim: "CONS" },
                                          { prim: "PAIR" }
                                        ]
                                      ]
                                    }
                                  ]
                                ]
                              }
                            ]
                          ]
                        }
                      ],
                      [
                        {
                          prim: "IF_LEFT",
                          args: [
                            [
                              {
                                prim: "IF_LEFT",
                                args: [
                                  [
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                    { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                    { prim: "PAIR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIP", args: [[{ prim: "AMOUNT" }]] },
                                    { prim: "TRANSFER_TOKENS" },
                                    { prim: "NIL", args: [{ prim: "operation" }] },
                                    { prim: "SWAP" },
                                    { prim: "CONS" },
                                    { prim: "PAIR" }
                                  ],
                                  [
                                    {
                                      prim: "IF_LEFT",
                                      args: [
                                        [
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "CAR" },
                                              { prim: "SENDER" },
                                              { prim: "COMPARE" },
                                              { prim: "EQ" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [],
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "SenderIsNotAdmin" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ]
                                                ]
                                              }
                                            ]]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "PAIR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ],
                                        [
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "CAR" },
                                              { prim: "SENDER" },
                                              { prim: "COMPARE" },
                                              { prim: "EQ" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [],
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "SenderIsNotAdmin" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ]
                                                ]
                                              }
                                            ]]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "PAIR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ]
                                      ]
                                    }
                                  ]
                                ]
                              }
                            ],
                            [
                              {
                                prim: "IF_LEFT",
                                args: [
                                  [
                                    {
                                      prim: "IF_LEFT",
                                      args: [
                                        [
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "AMOUNT" }]] },
                                          { prim: "TRANSFER_TOKENS" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "SWAP" },
                                          { prim: "CONS" },
                                          { prim: "PAIR" }
                                        ],
                                        [
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "CAR" },
                                              { prim: "SENDER" },
                                              { prim: "COMPARE" },
                                              { prim: "EQ" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [],
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "SenderIsNotAdmin" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ]
                                                ]
                                              }
                                            ]]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "GET" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                { prim: "DUP" },
                                                { prim: "CDR" },
                                                { prim: "INT" },
                                                { prim: "EQ" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [{ prim: "NONE", args: [{ prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }] }],
                                                    [
                                                      { prim: "DUP" },
                                                      { prim: "CDR" },
                                                      { prim: "DIP", args: [[{ prim: "EMPTY_MAP", args: [{ prim: "address" }, { prim: "nat" }] }]] },
                                                      { prim: "PAIR" },
                                                      { prim: "SOME" }
                                                    ]
                                                  ]
                                                }
                                              ],
                                              [
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CDR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] },
                                                { prim: "ADD" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                                { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                                { prim: "PAIR" },
                                                { prim: "SOME" }
                                              ]
                                            ]
                                          },
                                          { prim: "SWAP" },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "CAR" },
                                              { prim: "UPDATE" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "PAIR" }
                                            ]]
                                          },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "CDR" },
                                              { prim: "INT" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }, { prim: "DUP" }, { prim: "CDR" }, { prim: "CAR" }]] },
                                              { prim: "ADD" },
                                              { prim: "ISNAT" },
                                              {
                                                prim: "IF_NONE",
                                                args: [
                                                  [{ prim: "PUSH", args: [{ prim: "string" }, { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }] }, { prim: "FAILWITH" }],
                                                  []
                                                ]
                                              },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "PAIR" },
                                              { prim: "SWAP" },
                                              { prim: "PAIR" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "SWAP" },
                                              { prim: "PAIR" }
                                            ]]
                                          },
                                          { prim: "DROP" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ]
                                      ]
                                    }
                                  ],
                                  [
                                    {
                                      prim: "IF_LEFT",
                                      args: [
                                        [
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "CAR" },
                                              { prim: "SENDER" },
                                              { prim: "COMPARE" },
                                              { prim: "EQ" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [],
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "SenderIsNotAdmin" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ]
                                                ]
                                              }
                                            ]]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "GET" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                { prim: "CDR" },
                                                { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                                { prim: "SWAP" },
                                                { prim: "PAIR" },
                                                { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] },
                                                { prim: "PAIR" },
                                                { prim: "FAILWITH" }
                                              ],
                                              []
                                            ]
                                          },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }]] }, { prim: "SWAP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "SUB" },
                                          { prim: "ISNAT" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                { prim: "CAR" },
                                                { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                                { prim: "SWAP" },
                                                { prim: "CDR" },
                                                { prim: "PAIR" },
                                                { prim: "PUSH", args: [{ prim: "string" }, { string: "NotEnoughBalance" }] },
                                                { prim: "PAIR" },
                                                { prim: "FAILWITH" }
                                              ],
                                              []
                                            ]
                                          },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "PAIR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }]] },
                                          { prim: "SWAP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CAR" },
                                              { prim: "INT" },
                                              { prim: "EQ" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [
                                                    { prim: "DUP" },
                                                    { prim: "CDR" },
                                                    { prim: "SIZE" },
                                                    { prim: "INT" },
                                                    { prim: "EQ" },
                                                    {
                                                      prim: "IF",
                                                      args: [
                                                        [
                                                          { prim: "DROP" },
                                                          {
                                                            prim: "NONE",
                                                            args: [{ prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "address" }, { prim: "nat" }] }] }]
                                                          }
                                                        ],
                                                        [{ prim: "SOME" }]
                                                      ]
                                                    }
                                                  ],
                                                  [{ prim: "SOME" }]
                                                ]
                                              },
                                              { prim: "SWAP" },
                                              { prim: "CAR" },
                                              { prim: "DIP", args: [[{ prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CAR" }]] }]] },
                                              { prim: "UPDATE" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "PAIR" }
                                            ]]
                                          },
                                          { prim: "DUP" },
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "CDR" },
                                              { prim: "NEG" },
                                              {
                                                prim: "DIP",
                                                args: [[
                                                  { prim: "DUP" },
                                                  { prim: "CDR" },
                                                  { prim: "DUP" },
                                                  { prim: "CDR" },
                                                  { prim: "CAR" }
                                                ]]
                                              },
                                              { prim: "ADD" },
                                              { prim: "ISNAT" },
                                              {
                                                prim: "IF_NONE",
                                                args: [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        { string: "Unexpected failure: Negative total supply\nCallStack (from HasCallStack):\n  failUnexpected, called at src/Lorentz/Contracts/ManagedLedger/Impl.hs:158:27 in lorentz-contracts-0.2.0.1.2-CWNYAYQdqCJAhKtTd1tWlU:Lorentz.Contracts.ManagedLedger.Impl" }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ],
                                                  []
                                                ]
                                              },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CDR" }]] }, { prim: "CAR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "PAIR" },
                                              { prim: "SWAP" },
                                              { prim: "PAIR" },
                                              { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                              { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                              { prim: "SWAP" },
                                              { prim: "PAIR" }
                                            ]]
                                          },
                                          { prim: "DROP" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ],
                                        [
                                          {
                                            prim: "DIP",
                                            args: [[
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CDR" },
                                              { prim: "CDR" },
                                              {
                                                prim: "IF_LEFT",
                                                args: [
                                                  [
                                                    { prim: "SENDER" },
                                                    { prim: "COMPARE" },
                                                    { prim: "EQ" },
                                                    {
                                                      prim: "IF",
                                                      args: [
                                                        [],
                                                        [
                                                          {
                                                            prim: "PUSH",
                                                            args: [
                                                              { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                              { prim: "Pair", args: [{ string: "NotAllowedToSetProxy" }, { prim: "Unit" }] }
                                                            ]
                                                          },
                                                          { prim: "FAILWITH" }
                                                        ]
                                                      ]
                                                    }
                                                  ],
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "pair", args: [{ prim: "string" }, { prim: "unit" }] },
                                                        { prim: "Pair", args: [{ string: "ProxyAlreadySet" }, { prim: "Unit" }] }
                                                      ]
                                                    },
                                                    { prim: "FAILWITH" }
                                                  ]
                                                ]
                                              }
                                            ]]
                                          },
                                          { prim: "RIGHT", args: [{ prim: "address" }] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "DIP", args: [[{ prim: "DUP" }, { prim: "DIP", args: [[{ prim: "CAR" }]] }, { prim: "CDR" }]] },
                                          { prim: "DIP", args: [[{ prim: "DROP" }]] },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "PAIR" }
                                        ]
                                      ]
                                    }
                                  ]
                                ]
                              }
                            ]
                          ]
                        }
                      ]
                    ]
                  }
                ]]
              }
            ],
            storage: {
              prim: "Pair",
              args: [
                [{ prim: "Elt", args: [{ string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn" }, { prim: "Pair", args: [{ int: "17" }, []] }] }],
                {
                  prim: "Pair",
                  args: [
                    { prim: "Pair", args: [{ string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn" }, { prim: "False" }] },
                    { prim: "Pair", args: [{ int: "17" }, { prim: "Left", args: [{ string: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn" }] }] }
                  ]
                }
              ]
            }
          }
        },
        {
          kind: "transaction",
          counter: "1",
          source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
          fee: "10000",
          gas_limit: "10",
          storage_limit: "10",
          parameters: {
            entrypoint: "default",
            value: { prim: "Pair", args: [{ bytes: "0202" }, { string: "hello" }], annots: ["%test"] }
          },
          destination: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
          amount: "1000"
        },
        {
          kind: "transaction",
          counter: "1",
          source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
          fee: "10000",
          gas_limit: "10",
          storage_limit: "10",
          destination: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
          amount: "1000"
        }
      ]
    },
    bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616b0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a00ebcf82872f4942052704e95dc4bfa0538503dbece27414a39b6650bcecbff8966d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000001f760200001f7105000764076407640765036e0765036e036207640765036e0765036e0765036e03620765036e036207640765036e0765036e0362076407650765036e036e055a03620765036e055a0362076407640765036c055a036207640359036e076407640765036c055a036e0765036e036207640765036e0362036e050107650761036e076503620760036e036207650765036e0359076503620764036e036e05020200001ece03210316051f02000000020317072e02000015ef072e020000128a072e020000080b03480342051f020000003a0321031703160317072c0200000026074307650368036c070701000000134f7065726174696f6e73417265506175736564030b0327020000000003210317032103170316051f0200000002031603190325072c02000000020320020000079b03210316051f02000000020317051f020000000403210316034c051f0200000002032103190325072c020000000203200200000223051f02000000020321034c051f0200000211051f02000000020321034c051f02000000020321034c051f0200000062051f020000005b051f0200000009051f02000000020321051f0200000002031603420321051f02000000210317051f020000000203160329072f02000000060723036e03620200000002031703160329072f02000000060743036200000200000000051f020000000403210316034c051f02000000790321051f020000006e051f0200000065032103170317051f020000000b051f02000000020321034c034c034b0356072f020000003b051f02000000020321034c051f02000000020321034c0317031703420743036801000000124e6f74456e6f756768416c6c6f77616e6365034203270200000000034c03420342051f0200000006032003200320051f020000000403210316034c051f020000000403210316034c0329072f020000001b074303620000051f02000000060723036e036203420723036e0362020000000403210317051f020000000b051f02000000020321034c034c03170317032103300325072c02000000060320053e036202000000020346051f0200000014051f020000000b051f02000000020321034c034c034c031703160350051f020000000d0321051f020000000203160317051f02000000020320034c03420346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f020000000203200342051f02000000020321034c0316051f02000000020321034c031703160329072f020000003903210317031703300325072c020000000c053e076503620760036e03620200000017032103170317051f02000000060723036e036203420346020000003d051f02000000020321034c03170317051f0200000004032103160312051f020000000d0321051f020000000203170316051f0200000002032003420346034c051f020000000b051f0200000004032103160321051f0200000025031703160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017c031703170330051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c0342051f02000000020321034c0316051f02000000020321034c03160329072f020000002b03170317074303620000034c03420743036801000000104e6f74456e6f75676842616c616e636503420327020000000003210316051f020000000b051f02000000020321034c034c03170317034c034b0356072f02000000300316051f02000000020321034c0317031703420743036801000000104e6f74456e6f75676842616c616e6365034203270200000000051f020000000d0321051f020000000203170316051f020000000203200342051f02000000020321034c051f02000000730321031603300325072c020000002603210317034503300325072c020000000e0320053e076503620760036e03620200000002034602000000020346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017c03170317033b051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c03420320053d036d03420200000a73072e0200000877051f02000000690321031703170317072e0200000020074307650368036c0707010000000d50726f787949734e6f74536574030b03270200000035034803190325072c02000000000200000023074307650368036c0707010000001043616c6c657249734e6f7450726f7879030b0327051f020000003a0321031703160317072c0200000026074307650368036c070701000000134f7065726174696f6e73417265506175736564030b0327020000000003210317032103170316051f0200000002031603190325072c02000000020320020000079b03210316051f02000000020317051f020000000403210316034c051f0200000002032103190325072c020000000203200200000223051f02000000020321034c051f0200000211051f02000000020321034c051f02000000020321034c051f0200000062051f020000005b051f0200000009051f02000000020321051f0200000002031603420321051f02000000210317051f020000000203160329072f02000000060723036e03620200000002031703160329072f02000000060743036200000200000000051f020000000403210316034c051f02000000790321051f020000006e051f0200000065032103170317051f020000000b051f02000000020321034c034c034b0356072f020000003b051f02000000020321034c051f02000000020321034c0317031703420743036801000000124e6f74456e6f756768416c6c6f77616e6365034203270200000000034c03420342051f0200000006032003200320051f020000000403210316034c051f020000000403210316034c0329072f020000001b074303620000051f02000000060723036e036203420723036e0362020000000403210317051f020000000b051f02000000020321034c034c03170317032103300325072c02000000060320053e036202000000020346051f0200000014051f020000000b051f02000000020321034c034c034c031703160350051f020000000d0321051f020000000203160317051f02000000020320034c03420346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f020000000203200342051f02000000020321034c0316051f02000000020321034c031703160329072f020000003903210317031703300325072c020000000c053e076503620760036e03620200000017032103170317051f02000000060723036e036203420346020000003d051f02000000020321034c03170317051f0200000004032103160312051f020000000d0321051f020000000203170316051f0200000002032003420346034c051f020000000b051f0200000004032103160321051f0200000025031703160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017c031703170330051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c0342051f02000000020321034c0316051f02000000020321034c03160329072f020000002b03170317074303620000034c03420743036801000000104e6f74456e6f75676842616c616e636503420327020000000003210316051f020000000b051f02000000020321034c034c03170317034c034b0356072f02000000300316051f02000000020321034c0317031703420743036801000000104e6f74456e6f75676842616c616e6365034203270200000000051f020000000d0321051f020000000203170316051f020000000203200342051f02000000020321034c051f02000000730321031603300325072c020000002603210317034503300325072c020000000e0320053e076503620760036e03620200000002034602000000020346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017c03170317033b051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c03420320053d036d034202000001f003480342051f020000003a0321031703160317072c0200000026074307650368036c070701000000134f7065726174696f6e73417265506175736564030b03270200000000051f02000000020321034c051f02000000020321034c0321051f02000000210316051f020000000203160329072f02000000060723036e036202000000020317031703160329072f02000000060743036200000200000000032103300325072c020000000203200200000043051f02000000020321034c0317031703300325072c020000000203200200000022074303680100000015556e73616665416c6c6f77616e63654368616e676503420327051f020000000403210316034c051f020000000403210316034c0329072f020000001b074303620000051f02000000060723036e036203420723036e0362020000000403210317051f020000000b051f02000000020321034c034c03170317032103300325072c02000000060320053e036202000000020346051f0200000014051f020000000b051f02000000020321034c034c034c031703160350051f020000000d0321051f020000000203160317051f02000000020320034c03420346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f020000000203200342053d036d03420200000359072e020000025c051f02000000690321031703170317072e0200000020074307650368036c0707010000000d50726f787949734e6f74536574030b03270200000035034803190325072c02000000000200000023074307650368036c0707010000001043616c6c657249734e6f7450726f7879030b0327051f020000003a0321031703160317072c0200000026074307650368036c070701000000134f7065726174696f6e73417265506175736564030b03270200000000051f02000000020321034c051f02000000020321034c0321051f02000000210316051f020000000203160329072f02000000060723036e036202000000020317031703160329072f02000000060743036200000200000000032103300325072c020000000203200200000043051f02000000020321034c0317031703300325072c020000000203200200000022074303680100000015556e73616665416c6c6f77616e63654368616e676503420327051f020000000403210316034c051f020000000403210316034c0329072f020000001b074303620000051f02000000060723036e036203420723036e0362020000000403210317051f020000000b051f02000000020321034c034c03170317032103300325072c02000000060320053e036202000000020346051f0200000014051f020000000b051f02000000020321034c034c034c031703160350051f020000000d0321051f020000000203160317051f02000000020320034c03420346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f020000000203200342053d036d034202000000f1072e020000008303210316051f02000000020317051f020000000b051f02000000020321034c034203210316051f020000000203170321051f02000000210316051f020000000203160329072f02000000060723036e03620200000002031703170329072f02000000060743036200000200000000051f02000000020313034d053d036d034c031b0342020000006203210316051f02000000020317051f020000000b051f02000000020321034c034203210316051f02000000020317051f020000000203160329072f020000000607430362000002000000020316051f02000000020313034d053d036d034c031b034202000008c6072e02000001ae072e020000003e03210316051f02000000020317051f020000000b051f02000000020321034c03420317031703170316051f02000000020313034d053d036d034c031b03420200000164072e02000000ad051f020000003d0321031703160316034803190325072c02000000000200000023074307650368036c0707010000001053656e64657249734e6f7441646d696e030b0327051f020000000403210317051f020000000d0321051f020000000203170316051f020000000d0321051f020000000203160317051f02000000020320034c03420342051f020000000d0321051f020000000203160317051f02000000020320034c0342053d036d034202000000ab051f020000003d0321031703160316034803190325072c02000000000200000023074307650368036c0707010000001053656e64657249734e6f7441646d696e030b0327051f020000000403210317051f020000000d0321051f020000000203170316051f020000000d0321051f020000000203170316051f0200000002032003420342051f020000000d0321051f020000000203160317051f02000000020320034c0342053d036d0342020000070c072e02000002f1072e020000003e03210316051f02000000020317051f020000000b051f02000000020321034c03420317031703160316051f02000000020313034d053d036d034c031b034202000002a7051f020000003d0321031703160316034803190325072c02000000000200000023074307650368036c0707010000001053656e64657249734e6f7441646d696e030b0327051f02000000020321034c0316051f02000000020321034c03160329072f02000000350321031703300325072c020000000c053e076503620760036e0362020000001503210317051f02000000060723036e036203420346020000003b051f02000000020321034c0317051f0200000004032103160312051f020000000d0321051f020000000203170316051f0200000002032003420346034c051f020000000b051f0200000004032103160321051f020000002303160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017a03170330051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c03420320053d036d0342020000040f072e020000031e051f020000003d0321031703160316034803190325072c02000000000200000023074307650368036c0707010000001053656e64657249734e6f7441646d696e030b0327051f02000000020321034c0316051f02000000020321034c03160329072f02000000290317074303620000034c03420743036801000000104e6f74456e6f75676842616c616e636503420327020000000003210316051f020000000b051f02000000020321034c034c0317034c034b0356072f020000002e0316051f02000000020321034c031703420743036801000000104e6f74456e6f75676842616c616e6365034203270200000000051f020000000d0321051f020000000203170316051f020000000203200342051f02000000020321034c051f02000000730321031603300325072c020000002603210317034503300325072c020000000e0320053e076503620760036e03620200000002034602000000020346034c0316051f020000000b051f0200000004032103160350051f020000000d0321051f020000000203170316051f0200000002032003420321051f020000017a0317033b051f020000000a0321031703210317031603120356072f02000000fd0743036801000000f2556e6578706563746564206661696c7572653a204e6567617469766520746f74616c20737570706c790a43616c6c537461636b202866726f6d2048617343616c6c537461636b293a0a20206661696c556e65787065637465642c2063616c6c6564206174207372632f4c6f72656e747a2f436f6e7472616374732f4d616e616765644c65646765722f496d706c2e68733a3135383a323720696e206c6f72656e747a2d636f6e7472616374732d302e322e302e312e322d43574e594159516471434a41684b7454643174576c553a4c6f72656e747a2e436f6e7472616374732e4d616e616765644c65646765722e496d706c03270200000000051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203170316051f020000000203200342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c03420320053d036d034202000000e5051f020000006f0321031703170317072e0200000039034803190325072c02000000000200000027074307650368036c070701000000144e6f74416c6c6f776564546f53657450726f7879030b03270200000022074307650368036c0707010000000f50726f7879416c7265616479536574030b03270544036e051f020000000403210317051f020000000d0321051f020000000203160317051f020000000d0321051f020000000203160317051f02000000020320034c0342034c0342051f020000000d0321051f020000000203160317051f02000000020320034c0342053d036d0342000000990707020000003407040100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e070700110200000000070707070100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e03030707001105050100000024747a31515a364b5937643342755a4454316431396455786f51727446504e32514a33686e6c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807000035e993d8c7aaa42b5e3ccd86a33390ececc73abdff000000001c08070a000000020202010000000568656c6c6f0000000525746573746c0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0ae807000035e993d8c7aaa42b5e3ccd86a33390ececc73abd00'
  },
  { // ANCHOR[epic=oversize_testcase] - oversize 3
    op: {
      branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
      contents: [{
        kind: "origination",
        counter: "1",
        source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
        fee: "10000",
        gas_limit: "10",
        storage_limit: "10",
        balance: "0",
        script: {
          code: [
            {
              prim: "storage",
              args: [{
                prim: "pair",
                args: [
                  { prim: "option", args: [{ prim: "ticket", args: [{ prim: "int" }] }], annots: ["%x"] },
                  { prim: "option", args: [{ prim: "ticket", args: [{ prim: "string" }] }], annots: ["%y"] }
                ]
              }]
            },
            {
              prim: "parameter",
              args: [{
                prim: "or",
                args: [
                  { prim: "unit", annots: ["%auto_call"] },
                  { prim: "ticket", args: [{ prim: "int" }], annots: ["%run"] }
                ]
              }]
            },
            {
              prim: "code",
              args: [[
                { prim: "UNPAIR" },
                {
                  prim: "IF_LEFT",
                  args: [
                    [
                      { prim: "DROP" },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "43" }] },
                      { prim: "PUSH", args: [{ prim: "int" }, { int: "1" }] },
                      { prim: "TICKET" },
                      { prim: "NIL", args: [{ prim: "operation" }] },
                      { prim: "SELF", annots: ["%run"] },
                      { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "TRANSFER_TOKENS" },
                      { prim: "CONS" }
                    ],
                    [
                      { prim: "READ_TICKET" },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "42" }] },
                      { prim: "PUSH", args: [{ prim: "string" }, { string: "abc" }] },
                      { prim: "TICKET" },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "SWAP" },
                      { prim: "SOME" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "PAIR" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "GET", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "3" }] },
                      { prim: "SWAP" },
                      { prim: "EDIV" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [{ prim: "PUSH", args: [{ prim: "int" }, { int: "20" }] }, { prim: "FAILWITH" }],
                          [{ prim: "CAR" }]
                        ]
                      },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "SUB" },
                      { prim: "ISNAT" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [{ prim: "PUSH", args: [{ prim: "int" }, { int: "20" }] }, { prim: "FAILWITH" }],
                          []
                        ]
                      },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "3" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "EDIV" },
                      { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "int" }, { int: "20" }] }, { prim: "FAILWITH" }], [{ prim: "CAR" }]] },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "SPLIT_TICKET" },
                      { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "int" }, { int: "20" }] }, { prim: "FAILWITH" }], []] },
                      { prim: "UNPAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "JOIN_TICKETS" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "NIL", args: [{ prim: "operation" }] }
                    ]
                  ]
                },
                { prim: "PAIR" }
              ]]
            }
          ],
          storage: { prim: "Pair", args: [{ prim: "None" }, { prim: "None" }] }
        }
      }]
    },
    bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000142020000013d0501076506630587035b00000002257806630587036800000002257905000764046c0000000a256175746f5f63616c6c0687035b000000042572756e050202000000fa037a072e020000002c032007430362002b0743035b00010388053d036d0449000000042572756e0743036a000005700003034d031b02000000be038907430362002a074303680100000003616263038805700003034c0346034c0316034205710002052900040321074303620003034c0322072f02000000080743035b0014032702000000020316034c032105710002034b0356072f02000000080743035b001403270200000000074303620003057000020322072f02000000080743035b00140327020000000203160342034c038a072f02000000080743035b001403270200000000037a034c0342038b034c0317034c0342053d036d034200000006070703060306'
  },
  { // ANCHOR[epic=oversize_testcase] - oversize 4
    op: {
      branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
      contents: [
        {
          kind: "origination",
          counter: "1",
          source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
          fee: "10000",
          gas_limit: "10",
          storage_limit: "10",
          balance: "0",
          script: {
            code: [
              {
                prim: "parameter",
                args: [
                  {
                    prim: "or",
                    args: [
                      { prim: "ticket", args: [{ prim: "unit" }], annots: ["%receive"] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "contract", args: [{ prim: "ticket", args: [{ prim: "unit" }] }], annots: ["%destination"] },
                          { prim: "nat", annots: ["%amount"] },
                          { prim: "address", annots: ["%ticketer"] }
                        ],
                        annots: ["%send"]
                      }
                    ]
                  }
                ]
              },
              {
                prim: "storage",
                args: [{
                  prim: "pair",
                  args: [
                    { prim: "address", annots: ["%manager"] },
                    { prim: "big_map", args: [{ prim: "address" }, { prim: "ticket", args: [{ prim: "unit" }] }], annots: ["%tickets"] }
                  ]
                }]
              },
              {
                prim: "code",
                args: [[
                  { prim: "AMOUNT" },
                  { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                  [
                    [{ prim: "COMPARE" }, { prim: "EQ" }],
                    { prim: "IF", args: [[], [[{ prim: "UNIT" }, { prim: "FAILWITH" }]]] }
                  ],
                  { prim: "UNPAIR", args: [{ int: "3" }] },
                  {
                    prim: "IF_LEFT",
                    args: [
                      [
                        { prim: "READ_TICKET" },
                        { prim: "CAR", annots: ["@ticketer"] },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "NONE", args: [{ prim: "ticket", args: [{ prim: "unit" }] }] },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "GET_AND_UPDATE" },
                        [{
                          prim: "IF_NONE",
                          args: [
                            [{ prim: "DIG", args: [{ int: "2" }] }],
                            [
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "PAIR" },
                              { prim: "JOIN_TICKETS" },
                              [{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], []] }]
                            ]
                          ]
                        }],
                        { prim: "SOME" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "GET_AND_UPDATE" },
                        [{ prim: "IF_NONE", args: [[], [[{ prim: "UNIT" }, { prim: "FAILWITH" }]]] }],
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "NIL", args: [{ prim: "operation" }] }
                      ],
                      [
                        { prim: "DUP", args: [{ int: "2" }], annots: ["@manager"] },
                        { prim: "SENDER" },
                        [[{ prim: "COMPARE" }, { prim: "EQ" }], { prim: "IF", args: [[], [[{ prim: "UNIT" }, { prim: "FAILWITH" }]]] }],
                        { prim: "UNPAIR", args: [{ int: "3" }] },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "NONE", args: [{ prim: "ticket", args: [{ prim: "unit" }] }] },
                        { prim: "DUP", args: [{ int: "5" }], annots: ["@ticketer"] },
                        { prim: "GET_AND_UPDATE" },
                        [{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], []] }],
                        { prim: "READ_TICKET" },
                        { prim: "GET", args: [{ int: "4" }], annots: ["@total_amount"] },
                        { prim: "DUP", args: [{ int: "5" }], annots: ["@amount"] },
                        { prim: "SWAP" },
                        { prim: "SUB" },
                        { prim: "ISNAT" },
                        [{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], [{ prim: "RENAME", annots: ["@remaining_amount"] }]] }],
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "SPLIT_TICKET" },
                        [{ prim: "IF_NONE", args: [[[{ prim: "UNIT" }, { prim: "FAILWITH" }]], []] }],
                        { prim: "UNPAIR", annots: ["@to_send", "@to_keep"] },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "SOME" },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "GET_AND_UPDATE" },
                        [{ prim: "IF_NONE", args: [[], [[{ prim: "UNIT" }, { prim: "FAILWITH" }]]] }],
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "TRANSFER_TOKENS" },
                        { prim: "NIL", args: [{ prim: "operation" }] },
                        { prim: "SWAP" },
                        { prim: "CONS" }
                      ]
                    ]
                  },
                  { prim: "PAIR" }
                ]]
              }
            ],
            storage: { prim: "Pair", args: [{ bytes: "00006dba164f4293b862a5e2c5ab84888ea8d7f8cbe6" }, { int: "39" }] }
          }
        }
      ]
    },
    bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a000000000292020000028d050007640687036c000000082572656365697665096500000032065a0587036c0000000c2564657374696e6174696f6e04620000000725616d6f756e74046e00000009257469636b65746572000000052573656e6405010765046e00000008256d616e616765720861036e0587036c00000008257469636b6574730502020000020b03130743036a0000020000001e020000000403190325072c020000000002000000090200000004034f0327057a0003072e02000000840389041600000009407469636b65746572032105700004053e0587036c05700002038c0200000032072f0200000004057000020200000022057000030342038b0200000015072f02000000090200000004034f03270200000000034605700002038c0200000015072f020000000002000000090200000004034f0327034c0342053d036d020000014a0621000200000008406d616e616765720348020000001e020000000403190325072c020000000002000000090200000004034f0327057a000305700004053e0587036c0621000500000009407469636b65746572038c0200000015072f02000000090200000004034f032702000000000389062900040000000d40746f74616c5f616d6f756e74062100050000000740616d6f756e74034c034b0356020000002c072f02000000090200000004034f032702000000170458000000114072656d61696e696e675f616d6f756e74057000040342034c038a0200000015072f02000000090200000004034f03270200000000047a0000001140746f5f73656e642040746f5f6b65657005710005034605700003038c0200000015072f020000000002000000090200000004034f0327057000020342034c0743036a000005700003034d053d036d034c031b03420000001f07070a0000001600006dba164f4293b862a5e2c5ab84888ea8d7f8cbe60027'
  },
  { // ANCHOR[epic=oversize_testcase] - oversize 5
    op: {
      branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
      contents: [{
        kind: "origination",
        counter: "1",
        source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
        fee: "10000",
        gas_limit: "10",
        storage_limit: "10",
        balance: "0",
        script: {
          code: [
            {
              prim: "parameter",
              args: [{
                prim: "or",
                args: [
                  {
                    prim: "or",
                    args: [
                      {
                        prim: "or",
                        args: [
                          { prim: "contract", args: [{ prim: "ticket", args: [{ prim: "nat" }] }], annots: ["%buy"] },
                          { prim: "contract", args: [{ prim: "ticket", args: [{ prim: "nat" }] }], annots: ["%cancel"] }
                        ]
                      },
                      {
                        prim: "or",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat", annots: ["%opening_price"] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat", annots: ["%set_reserve_price"] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "timestamp", annots: ["%set_start_time"] },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "int", annots: ["%set_round_time"] },
                                          { prim: "ticket", args: [{ prim: "nat" }], annots: ["%ticket"] }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              }
                            ],
                            annots: ["%configure"]
                          },
                          { prim: "nat", annots: ["%drop_price"] }
                        ]
                      }
                    ]
                  },
                  { prim: "unit", annots: ["%start"] }
                ]
              }]
            },
            {
              prim: "storage",
              args: [
                {
                  prim: "pair",
                  args: [
                    {
                      prim: "pair",
                      args: [
                        { prim: "address", annots: ["%admin"] },
                        {
                          prim: "pair",
                          args: [
                            { prim: "nat", annots: ["%current_price"] },
                            {
                              prim: "pair",
                              args: [
                                { prim: "nat", annots: ["%reserve_price"] },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "bool", annots: ["%in_progress"] },
                                    { prim: "pair", args: [{ prim: "timestamp", annots: ["%start_time"] }, { prim: "int", annots: ["%round_time"] }] }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ],
                      annots: ["%data"]
                    },
                    { prim: "big_map", args: [{ prim: "nat" }, { prim: "ticket", args: [{ prim: "nat" }] }], annots: ["%tickets"] }
                  ]
                }
              ]
            },
            {
              prim: "code",
              args: [[
                { prim: "UNPAIR" },
                { prim: "SWAP" },
                { prim: "UNPAIR" },
                { prim: "DIG", args: [{ int: "2" }] },
                {
                  prim: "IF_LEFT",
                  args: [
                    [
                      {
                        prim: "IF_LEFT",
                        args: [[{
                          prim: "IF_LEFT",
                          args: [
                            [
                              { prim: "NOW" },
                              { prim: "AMOUNT" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "4" }] },
                              { prim: "CAR" },
                              { prim: "SENDER" },
                              { prim: "COMPARE" },
                              { prim: "NEQ" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "4" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "PUSH", args: [{ prim: "mutez" }, { int: "1" }] },
                              { prim: "DIG", args: [{ int: "4" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "5" }] },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "MUL" },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "4" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "4" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "5" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "ADD" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "COMPARE" },
                              { prim: "LE" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CONTRACT", args: [{ prim: "unit" }] },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    { prim: "DROP", args: [{ int: "4" }] },
                                    { prim: "PUSH", args: [{ prim: "string" }, { string: "contract does not match" }] },
                                    { prim: "FAILWITH" }
                                  ],
                                  [
                                    { prim: "SWAP" },
                                    { prim: "PUSH", args: [{ prim: "unit" }, { prim: "Unit" }] },
                                    { prim: "TRANSFER_TOKENS" },
                                    { prim: "DIG", args: [{ int: "3" }] },
                                    { prim: "NONE", args: [{ prim: "ticket", args: [{ prim: "nat" }] }] },
                                    { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                                    { prim: "GET_AND_UPDATE" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          { prim: "DROP", args: [{ int: "4" }] },
                                          { prim: "PUSH", args: [{ prim: "string" }, { string: "ticket does not exist" }] },
                                          { prim: "FAILWITH" }
                                        ],
                                        [
                                          { prim: "DIG", args: [{ int: "3" }] },
                                          { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                                          { prim: "DIG", args: [{ int: "2" }] },
                                          { prim: "TRANSFER_TOKENS" },
                                          { prim: "SWAP" },
                                          { prim: "DIG", args: [{ int: "3" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "4" }] },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "PUSH", args: [{ prim: "bool" }, { prim: "False" }] },
                                          { prim: "PAIR" },
                                          { prim: "DIG", args: [{ int: "4" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "5" }] },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "PAIR" },
                                          { prim: "DIG", args: [{ int: "4" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "5" }] },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "PAIR" },
                                          { prim: "DIG", args: [{ int: "4" }] },
                                          { prim: "CAR" },
                                          { prim: "PAIR" },
                                          { prim: "PAIR" },
                                          { prim: "NIL", args: [{ prim: "operation" }] },
                                          { prim: "DIG", args: [{ int: "2" }] },
                                          { prim: "CONS" },
                                          { prim: "DIG", args: [{ int: "2" }] },
                                          { prim: "CONS" },
                                          { prim: "PAIR" }
                                        ]
                                      ]
                                    }
                                  ]
                                ]
                              }
                            ],
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "SENDER" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "NONE", args: [{ prim: "ticket", args: [{ prim: "nat" }] }] },
                              { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                              { prim: "GET_AND_UPDATE" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    { prim: "DROP", args: [{ int: "3" }] },
                                    { prim: "PUSH", args: [{ prim: "string" }, { string: "ticket does not exist" }] },
                                    { prim: "FAILWITH" }
                                  ],
                                  [
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "TRANSFER_TOKENS" },
                                    { prim: "SWAP" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "PUSH", args: [{ prim: "bool" }, { prim: "False" }] },
                                    { prim: "PAIR" },
                                    { prim: "DIG", args: [{ int: "3" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "4" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "PAIR" },
                                    { prim: "DIG", args: [{ int: "3" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "4" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "PAIR" },
                                    { prim: "DIG", args: [{ int: "3" }] },
                                    { prim: "CAR" },
                                    { prim: "PAIR" },
                                    { prim: "PAIR" },
                                    { prim: "NIL", args: [{ prim: "operation" }] },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "CONS" },
                                    { prim: "PAIR" }
                                  ]
                                ]
                              }
                            ]
                          ]
                        }],
                        [{
                          prim: "IF_LEFT",
                          args: [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "SOURCE" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "NOT" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "UNPAIR" },
                              { prim: "SWAP" },
                              { prim: "UNPAIR" },
                              { prim: "SWAP" },
                              { prim: "UNPAIR" },
                              { prim: "SWAP" },
                              { prim: "UNPAIR" },
                              { prim: "DIG", args: [{ int: "6" }] },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "SOME" },
                              { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                              { prim: "UPDATE" },
                              { prim: "SWAP" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "PAIR" },
                              { prim: "PUSH", args: [{ prim: "bool" }, { prim: "False" }] },
                              { prim: "PAIR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "PAIR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "PAIR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "NIL", args: [{ prim: "operation" }] },
                              { prim: "PAIR" }
                            ],
                            [
                              { prim: "NOW" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "SENDER" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "COMPARE" },
                              { prim: "GE" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "4" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "ADD" },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "COMPARE" },
                              { prim: "GT" },
                              { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "4" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "PAIR" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "PAIR" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "NIL", args: [{ prim: "operation" }] },
                              { prim: "PAIR" }
                            ]
                          ]
                        }]]
                      }
                    ],
                    [
                      { prim: "DROP" },
                      { prim: "NOW" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      { prim: "SOURCE" },
                      { prim: "COMPARE" },
                      { prim: "EQ" },
                      { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "NOT" },
                      { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "COMPARE" },
                      { prim: "GE" },
                      { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "NONE", args: [{ prim: "ticket", args: [{ prim: "nat" }] }] },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                      { prim: "GET_AND_UPDATE" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            { prim: "DROP", args: [{ int: "3" }] },
                            { prim: "PUSH", args: [{ prim: "string" }, { string: "no ticket" }] },
                            { prim: "FAILWITH" }
                          ],
                          [
                            { prim: "SOME" },
                            { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "PUSH", args: [{ prim: "bool" }, { prim: "True" }] },
                            { prim: "PAIR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "CAR" },
                            { prim: "PAIR" },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "PAIR" },
                            { prim: "PAIR" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "PAIR" }
                          ]
                        ]
                      }
                    ]
                  ]
                }
              ]]
            }
          ],
          storage: {
            prim: "Pair",
            args: [
              [
                { bytes: "00000d4f0cf2fae2437f924120ef030f53abd4d4e520" },
                { int: "90" },
                { int: "10" },
                { prim: "False" },
                { int: "1609898008" },
                { int: "600" }
              ],
              { int: "139" }
            ]
          }
        }
      }]
    },
    bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a00000000088d02000008880500076407640764065a058703620000000425627579065a05870362000000072563616e63656c0764086504620000000e256f70656e696e675f70726963650765046200000012257365745f726573657276655f70726963650765046b0000000f257365745f73746172745f74696d650765045b0000000f257365745f726f756e645f74696d650687036200000007257469636b65740000000a25636f6e66696775726504620000000b2564726f705f7072696365046c00000006257374617274050107650865046e000000062561646d696e076504620000000e2563757272656e745f7072696365076504620000000e25726573657276655f7072696365076504590000000c25696e5f70726f67726573730765046b0000000b2573746172745f74696d65045b0000000b25726f756e645f74696d65000000052564617461086103620587036200000008257469636b6574730502020000072d037a034c037a05700002072e020000058a072e0200000330072e02000002100340031305700003032105710004031603480319033c072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327057000030321057100040317031703170316072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e03270743036a00010570000403210571000503170316033a034c03210571000203190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e03270570000303210571000403170317031703170317057000040321057100050317031703170317031603120570000203190332072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e03270570000203210571000303160555036c072f020000002605200004074303680100000017636f6e747261637420646f6573206e6f74206d61746368032702000000b6034c0743036c030b034d05700003053e05870362074303620000038c072f0200000024052000040743036801000000157469636b657420646f6573206e6f742065786973740327020000006a057000030743036a000005700002034d034c05700003032105710004031703170317031707430359030303420570000403210571000503170317031603420570000403210571000503170316034205700004031603420342053d036d05700002031b05700002031b03420200000114034c0321057100020316034803190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327034c0321057100020317031703170316072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e032705700002053e05870362074303620000038c072f0200000024052000030743036801000000157469636b657420646f6573206e6f7420657869737403270200000064057000020743036a000005700002034d034c05700002032105710003031703170317031707430359030303420570000303210571000403170317031603420570000303210571000403170316034205700003031603420342053d036d05700002031b0342020000024e072e02000000bc034c0321057100020316034703190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327034c0321057100020317031703170316033f072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327037a034c037a034c037a034c037a057000060570000203460743036200000350034c057000020342074303590303034205700002034205700002034205700002031603420342053d036d034202000001860340057000020321057100030316034803190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327057000020321057100030317031703170316072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327057000020321057100030317031703160570000203210571000303190328072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327057000020321057100030317031703170317031705700003032105710004031703170317031703160312034c0321057100020319032a072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e03270570000305700003032105710004031703170570000303420570000303160342032103170317031703170317057000030342034c03210571000203170317031703160342034c0321057100020317031703160342034c032105710002031703160342034c031603420342053d036d0342020000018d03200340034c0321057100020316034703190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327034c0321057100020317031703170316033f072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327034c03210571000203170317031703170316034c03210571000203190328072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e032705700002053e05870362074303620000038c072f0200000018052000030743036801000000096e6f207469636b65740327020000009e0346074303620000035005700002032105710003031703170317031707430359030a0342057000030321057100040317031703160342057000030321057100040317031603420570000303160342032103170317031703170317057000030342034c03210571000203170317031703160342034c0321057100020317031703160342034c032105710002031703160342034c031603420342053d036d0342000000350707020000002b0a0000001600000d4f0cf2fae2437f924120ef030f53abd4d4e520009a01000a03030098e0a8ff0b009809008b02'
  },
  { // ANCHOR[epic=oversize_testcase] - oversize 6
    op: {
      branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
      contents: [
        {
          kind: "origination",
          counter: "1",
          source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
          fee: "10000",
          gas_limit: "10",
          storage_limit: "10",
          balance: "0",
          script: {
            code: [
              {
                prim: "parameter",
                args: [{
                  prim: "or",
                  args: [
                    {
                      prim: "or",
                      args: [
                        {
                          prim: "or",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                {
                                  prim: "contract",
                                  args: [{
                                    prim: "pair",
                                    args: [
                                      { prim: "nat", annots: ["%opening_price"] },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "nat", annots: ["%set_reserve_price"] },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "timestamp", annots: ["%set_start_time"] },
                                              {
                                                prim: "pair",
                                                args: [
                                                  { prim: "int", annots: ["%set_round_time"] },
                                                  { prim: "ticket", args: [{ prim: "nat" }], annots: ["%ticket"] }
                                                ]
                                              }
                                            ]
                                          }
                                        ]
                                      }
                                    ]
                                  }],
                                  annots: ["%destination"]
                                },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat", annots: ["%opening_price"] },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat", annots: ["%reserve_price"] },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "timestamp", annots: ["%start_time"] },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "int", annots: ["%round_time"] },
                                                { prim: "nat", annots: ["%ticket_id"] }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ],
                              annots: ["%auction"]
                            },
                            { prim: "nat", annots: ["%burn"] }
                          ]
                        },
                        {
                          prim: "or",
                          args: [
                            { prim: "map", args: [{ prim: "string" }, { prim: "bytes" }], annots: ["%mint"] },
                            { prim: "ticket", args: [{ prim: "nat" }], annots: ["%receive"] }
                          ]
                        }
                      ]
                    },
                    {
                      prim: "pair",
                      args: [
                        { prim: "contract", args: [{ prim: "ticket", args: [{ prim: "nat" }] }], annots: ["%destination"] },
                        { prim: "nat", annots: ["%ticket_id"] }
                      ],
                      annots: ["%send"]
                    }
                  ]
                }]
              },
              {
                prim: "storage",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%admin"] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "big_map", args: [{ prim: "nat" }, { prim: "ticket", args: [{ prim: "nat" }] }], annots: ["%tickets"] },
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat", annots: ["%current_id"] },
                              {
                                prim: "big_map",
                                args: [
                                  { prim: "nat" },
                                  { prim: "pair", args: [{ prim: "nat" }, { prim: "map", args: [{ prim: "string" }, { prim: "bytes" }] }] }
                                ],
                                annots: ["%token_metadata"]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                prim: "code",
                args: [
                  [
                    { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                    { prim: "AMOUNT" },
                    { prim: "COMPARE" },
                    { prim: "EQ" },
                    { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                    { prim: "UNPAIR" },
                    { prim: "SWAP" },
                    { prim: "UNPAIR" },
                    { prim: "SWAP" },
                    { prim: "UNPAIR" },
                    { prim: "SWAP" },
                    { prim: "UNPAIR" },
                    { prim: "DIG", args: [{ int: "4" }] },
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          {
                            prim: "IF_LEFT",
                            args: [
                              [
                                {
                                  prim: "IF_LEFT",
                                  args: [
                                    [
                                      { prim: "DIG", args: [{ int: "4" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "5" }] },
                                      { prim: "SENDER" },
                                      { prim: "COMPARE" },
                                      { prim: "EQ" },
                                      { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "NONE", args: [{ prim: "ticket", args: [{ prim: "nat" }] }] },
                                      { prim: "DIG", args: [{ int: "2" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "3" }] },
                                      { prim: "CDR" },
                                      { prim: "CDR" },
                                      { prim: "CDR" },
                                      { prim: "CDR" },
                                      { prim: "CDR" },
                                      { prim: "GET_AND_UPDATE" },
                                      {
                                        prim: "IF_NONE",
                                        args: [
                                          [
                                            { prim: "DROP", args: [{ int: "5" }] },
                                            { prim: "PUSH", args: [{ prim: "string" }, { string: "no tickets" }] },
                                            { prim: "FAILWITH" }
                                          ],
                                          [
                                            { prim: "DIG", args: [{ int: "2" }] },
                                            { prim: "DUP" },
                                            { prim: "DUG", args: [{ int: "3" }] },
                                            { prim: "CAR" },
                                            { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                                            { prim: "DIG", args: [{ int: "2" }] },
                                            { prim: "DIG", args: [{ int: "4" }] },
                                            { prim: "DUP" },
                                            { prim: "DUG", args: [{ int: "5" }] },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            { prim: "PAIR" },
                                            { prim: "DIG", args: [{ int: "4" }] },
                                            { prim: "DUP" },
                                            { prim: "DUG", args: [{ int: "5" }] },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            { prim: "PAIR" },
                                            { prim: "DIG", args: [{ int: "4" }] },
                                            { prim: "DUP" },
                                            { prim: "DUG", args: [{ int: "5" }] },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            { prim: "PAIR" },
                                            { prim: "DIG", args: [{ int: "4" }] },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            { prim: "PAIR" },
                                            { prim: "TRANSFER_TOKENS" },
                                            { prim: "DIG", args: [{ int: "3" }] },
                                            { prim: "DIG", args: [{ int: "3" }] },
                                            { prim: "PAIR" },
                                            { prim: "DIG", args: [{ int: "2" }] },
                                            { prim: "PAIR" },
                                            { prim: "DIG", args: [{ int: "2" }] },
                                            { prim: "PAIR" },
                                            { prim: "NIL", args: [{ prim: "operation" }] },
                                            { prim: "DIG", args: [{ int: "2" }] },
                                            { prim: "CONS" },
                                            { prim: "PAIR" }
                                          ]
                                        ]
                                      }
                                    ],
                                    [
                                      { prim: "DIG", args: [{ int: "4" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "5" }] },
                                      { prim: "SENDER" },
                                      { prim: "COMPARE" },
                                      { prim: "EQ" },
                                      { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                                      { prim: "DIG", args: [{ int: "2" }] },
                                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "1" }] },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "ADD" },
                                      { prim: "PAIR" },
                                      { prim: "DIG", args: [{ int: "2" }] },
                                      { prim: "NONE", args: [{ prim: "ticket", args: [{ prim: "nat" }] }] },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "UPDATE" },
                                      { prim: "PAIR" },
                                      { prim: "SWAP" },
                                      { prim: "PAIR" },
                                      { prim: "NIL", args: [{ prim: "operation" }] },
                                      { prim: "PAIR" }
                                    ]
                                  ]
                                }
                              ],
                              [
                                {
                                  prim: "IF_LEFT",
                                  args: [
                                    [
                                      { prim: "DIG", args: [{ int: "4" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "5" }] },
                                      { prim: "SENDER" },
                                      { prim: "COMPARE" },
                                      { prim: "EQ" },
                                      { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "1" }] },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "4" }] },
                                      { prim: "TICKET" },
                                      { prim: "SOME" },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "4" }] },
                                      { prim: "GET_AND_UPDATE" },
                                      { prim: "DROP" },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "DIG", args: [{ int: "2" }] },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "4" }] },
                                      { prim: "PAIR" },
                                      { prim: "SOME" },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "4" }] },
                                      { prim: "UPDATE" },
                                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "1" }] },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "ADD" },
                                      { prim: "PAIR" },
                                      { prim: "SWAP" },
                                      { prim: "PAIR" },
                                      { prim: "SWAP" },
                                      { prim: "PAIR" },
                                      { prim: "NIL", args: [{ prim: "operation" }] },
                                      { prim: "PAIR" }
                                    ],
                                    [
                                      { prim: "READ_TICKET" },
                                      { prim: "UNPAIR" },
                                      { prim: "DROP" },
                                      { prim: "UNPAIR" },
                                      { prim: "DROP" },
                                      { prim: "DIG", args: [{ int: "4" }] },
                                      { prim: "DIG", args: [{ int: "2" }] },
                                      { prim: "SOME" },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "4" }] },
                                      { prim: "GET_AND_UPDATE" },
                                      { prim: "DROP" },
                                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "1" }] },
                                      { prim: "DIG", args: [{ int: "2" }] },
                                      { prim: "COMPARE" },
                                      { prim: "EQ" },
                                      { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                                      { prim: "DIG", args: [{ int: "2" }] },
                                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "1" }] },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "ADD" },
                                      { prim: "PAIR" },
                                      { prim: "SWAP" },
                                      { prim: "PAIR" },
                                      { prim: "SWAP" },
                                      { prim: "PAIR" },
                                      { prim: "NIL", args: [{ prim: "operation" }] },
                                      { prim: "PAIR" }
                                    ]
                                  ]
                                }
                              ]
                            ]
                          }
                        ],
                        [
                          { prim: "DIG", args: [{ int: "4" }] },
                          { prim: "DUP" },
                          { prim: "DUG", args: [{ int: "5" }] },
                          { prim: "SENDER" },
                          { prim: "COMPARE" },
                          { prim: "EQ" },
                          { prim: "IF", args: [[], [{ prim: "PUSH", args: [{ prim: "string" }, { string: "failed assertion" }] }, { prim: "FAILWITH" }]] },
                          { prim: "DIG", args: [{ int: "3" }] },
                          { prim: "NONE", args: [{ prim: "ticket", args: [{ prim: "nat" }] }] },
                          { prim: "DIG", args: [{ int: "2" }] },
                          { prim: "DUP" },
                          { prim: "DUG", args: [{ int: "3" }] },
                          { prim: "CDR" },
                          { prim: "GET_AND_UPDATE" },
                          {
                            prim: "IF_NONE",
                            args: [
                              [
                                { prim: "DROP", args: [{ int: "5" }] },
                                { prim: "PUSH", args: [{ prim: "string" }, { string: "no tickets" }] },
                                { prim: "FAILWITH" }
                              ],
                              [
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "CAR" },
                                { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "TRANSFER_TOKENS" },
                                { prim: "DIG", args: [{ int: "3" }] },
                                { prim: "DIG", args: [{ int: "3" }] },
                                { prim: "PAIR" },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "PAIR" },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "PAIR" },
                                { prim: "NIL", args: [{ prim: "operation" }] },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "CONS" },
                                { prim: "PAIR" }
                              ]
                            ]
                          }
                        ]
                      ]
                    }
                  ]
                ]
              }
            ],
            storage: [
              { bytes: "00000d4f0cf2fae2437f924120ef030f53abd4d4e520" },
              { int: "140" },
              { int: "1" },
              { int: "141" }
            ]
          }
        }
      ]
    },
    bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a00000000054a020000054505000764076407640865065a076504620000000e256f70656e696e675f70726963650765046200000012257365745f726573657276655f70726963650765046b0000000f257365745f73746172745f74696d650765045b0000000f257365745f726f756e645f74696d650687036200000007257469636b65740000000c2564657374696e6174696f6e076504620000000e256f70656e696e675f7072696365076504620000000e25726573657276655f70726963650765046b0000000b2573746172745f74696d650765045b0000000b25726f756e645f74696d6504620000000a257469636b65745f6964000000082561756374696f6e046200000005256275726e076408600368036900000005256d696e74068703620000000825726563656976650865065a058703620000000c2564657374696e6174696f6e04620000000a257469636b65745f6964000000052573656e6405010765046e000000062561646d696e0765086103620587036200000008257469636b657473076504620000000b2563757272656e745f696408610362076503620760036803690000000f25746f6b656e5f6d65746164617461050202000003900743036a0000031303190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327037a034c037a034c037a034c037a05700004072e0200000297072e020000016d072e02000000fc05700004032105710005034803190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e032705700003053e058703620570000203210571000303170317031703170317038c072f02000000190520000507430368010000000a6e6f207469636b657473032702000000800570000203210571000303160743036a00000570000205700004032105710005031703170317031703160342057000040321057100050317031703170316034205700004032105710005031703170316034205700004031703160342034d05700003057000030342057000020342057000020342053d036d05700002031b0342020000006505700004032105710005034803190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e032705700002074303620001057000030312034205700002053e058703620570000303500342034c0342053d036d0342020000011e072e020000009b05700004032105710005034803190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e032705700003074303620001057000030321057100040388034605700003032105710004038c0320057000030570000205700003032105710004034203460570000303210571000403500743036200010570000303120342034c0342034c0342053d036d034202000000770389037a0320037a03200570000405700002034605700003032105710004038c03200743036200010570000203190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e0327057000020743036200010570000303120342034c0342034c0342053d036d034202000000a805700004032105710005034803190325072c0200000000020000001b0743036801000000106661696c656420617373657274696f6e032705700003053e05870362057000020321057100030317038c072f02000000190520000507430368010000000a6e6f207469636b657473032702000000340570000203160743036a000005700002034d05700003057000030342057000020342057000020342053d036d05700002031b03420000002802000000230a0000001600000d4f0cf2fae2437f924120ef030f53abd4d4e520008c020001008d02'
  },
  { // ANCHOR[epic=oversize_testcase] - oversize 7
    op: {
      branch: "BLzyjjHKEKMULtvkpSHxuZxx6ei6fpntH2BTkYZiLgs8zLVstvX",
      contents: [
        {
          kind: "origination",
          counter: "1",
          source: "tz1QZ6KY7d3BuZDT1d19dUxoQrtFPN2QJ3hn",
          fee: "10000",
          gas_limit: "10",
          storage_limit: "10",
          balance: "0",
          script: {
            code: [
              {
                prim: "parameter",
                args: [
                  {
                    prim: "list",
                    args: [
                      {
                        prim: "pair",
                        args: [
                          { prim: "option", args: [{ prim: "key_hash" }], annots: ["%key"] },
                          { prim: "sapling_transaction", args: [{ int: "8" }], annots: ["%transaction"] }
                        ]
                      }
                    ]
                  }
                ]
              },
              { prim: "storage", args: [{ prim: "sapling_state", args: [{ int: "8" }] }] },
              {
                prim: "code",
                args: [
                  [
                    { prim: "DUP" },
                    { prim: "CDR" },
                    { prim: "SWAP" },
                    { prim: "CAR" },
                    { prim: "DUP" },
                    { prim: "NIL", args: [{ prim: "operation" }] },
                    { prim: "SWAP" },
                    {
                      prim: "ITER",
                      args: [
                        [
                          { prim: "DIG", args: [{ int: "3" }] },
                          { prim: "SWAP" },
                          { prim: "DUP" },
                          { prim: "DUG", args: [{ int: "2" }] },
                          { prim: "CDR" },
                          { prim: "SAPLING_VERIFY_UPDATE" },
                          { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "int" }, { int: "10" }] }, { prim: "FAILWITH" }], []] },
                          { prim: "DUP" },
                          { prim: "CDR" },
                          { prim: "DUG", args: [{ int: "4" }] },
                          { prim: "DUP" },
                          { prim: "CAR" },
                          { prim: "DUP" },
                          { prim: "ABS" },
                          { prim: "PUSH", args: [{ prim: "mutez" }, { int: "1" }] },
                          { prim: "MUL" },
                          { prim: "PUSH", args: [{ prim: "int" }, { int: "0" }] },
                          { prim: "DIG", args: [{ int: "2" }] },
                          { prim: "DUP" },
                          { prim: "DUG", args: [{ int: "3" }] },
                          { prim: "COMPARE" },
                          { prim: "GT" },
                          {
                            prim: "IF",
                            args: [
                              [
                                { prim: "SWAP" },
                                { prim: "DROP" },
                                { prim: "SWAP" },
                                { prim: "DROP" },
                                { prim: "DUG", args: [{ int: "2" }] },
                                { prim: "CAR" },
                                { prim: "IF_NONE", args: [[{ prim: "PUSH", args: [{ prim: "int" }, { int: "15" }] }, { prim: "FAILWITH" }], []] },
                                { prim: "IMPLICIT_ACCOUNT" },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "UNIT" },
                                { prim: "TRANSFER_TOKENS" },
                                { prim: "CONS" }
                              ],
                              [
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "DROP" },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "CAR" },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      { prim: "SWAP" },
                                      { prim: "DROP" }
                                    ],
                                    [
                                      { prim: "PUSH", args: [{ prim: "string" }, { string: "WrongCondition: ~ operation.key.is_some()" }] },
                                      { prim: "FAILWITH" }
                                    ]
                                  ]
                                },
                                { prim: "AMOUNT" },
                                { prim: "COMPARE" },
                                { prim: "EQ" },
                                {
                                  prim: "IF",
                                  args: [
                                    [],
                                    [
                                      { prim: "PUSH", args: [{ prim: "string" }, { string: "WrongCondition: sp.amount == amount_tez.value" }] },
                                      { prim: "FAILWITH" }
                                    ]
                                  ]
                                }
                              ]
                            ]
                          }
                        ]
                      ]
                    },
                    { prim: "SWAP" },
                    { prim: "DROP" },
                    { prim: "NIL", args: [{ prim: "operation" }] },
                    { prim: "SWAP" },
                    { prim: "ITER", args: [[{ prim: "CONS" }]] },
                    { prim: "PAIR" }
                  ]
                ]
              }
            ],
            storage: []
          }
        }
      ]
    },
    bytes: 'a99b946c97ada0f42c1bdeae0383db7893351232a832d00d0cd716eb6f66e5616d0035e993d8c7aaa42b5e3ccd86a33390ececc73abd904e010a0a00000000018802000001830500055f07650663035d00000004256b6579068400080000000c257472616e73616374696f6e0501058300080502020000015003210317034c03160321053d036d034c0552020000012405700003034c03210571000203170386072f02000000080743035b000a03270200000000032103170571000403210316032103110743036a0001033a0743035b0000057000020321057100030319032a072c020000002e034c0320034c0320057100020316072f02000000080743035b000f03270200000000031e05700002034f034d031b020000009a057000020320057000020316072f0200000004034c0320020000003407430368010000002957726f6e67436f6e646974696f6e3a207e206f7065726174696f6e2e6b65792e69735f736f6d6528290327031303190325072c0200000000020000003807430368010000002d57726f6e67436f6e646974696f6e3a2073702e616d6f756e74203d3d20616d6f756e745f74657a2e76616c75650327034c0320053d036d034c05520200000002031b0342000000050200000000'
  },
]
