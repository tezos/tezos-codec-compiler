import { bytesdecoder } from '../src/primitive';
import { Maybe, Optional } from '../src/util';
import * as codec from './codec';
import { string_to_ballot, string_to_branch, string_to_contract_id, string_to_pkh, string_to_proposal, string_to_proposal_array, string_to_public_key, string_to_raw, uni_to_hex } from './common';
import * as schema from './schema';
import { CODECTYPE, schema_converter } from './schema';
import { convert_alphacontents } from './operation';
import { isPrim, is_unit_param, MichelsonValue, value_to_expression } from './value';
import operation_unsigned = codec.Proto010_PtGRANAD__operation__unsigned;
import NSentrypoint = operation_unsigned.NSentrypoint;
import NSmicheline__michelson_v1__expression = codec.Proto010_PtGRANAD__script__expr.NSmicheline__michelson_v1__expression;

const is_default_unit = (parameters: { entrypoint: string; value: MichelsonValue }): boolean => {
    return (parameters.entrypoint === 'default' && isPrim(parameters.value) && is_unit_param(parameters.value));
}

export const convert_parameters = (parameters?: { entrypoint: string; value: MichelsonValue }): Optional<operation_unsigned.X_0> => {
    if (parameters && !is_default_unit(parameters)) {
        var entrypoint: NSentrypoint.entrypoint;
        var _tag: NSentrypoint.entrypoint_tag;
        switch (parameters.entrypoint) {
            case 'default':
            case 'root':
            case 'do':
            case 'set_delegate':
            case 'remove_delegate':
                _tag = NSentrypoint.entrypoint_tag[parameters.entrypoint];
                entrypoint = { _tag };
                break;
            default:
                _tag = NSentrypoint.entrypoint_tag.named;
                var _anon255 = uni_to_hex(parameters.entrypoint);
                entrypoint = { _tag, _anon255 } as NSentrypoint.entrypoint;
                break;
        }
        var value = bytesdecoder()(NSmicheline__michelson_v1__expression.expression_encoder(value_to_expression(parameters.value)));
        return Maybe.Some({ entrypoint, value });
    } else {
        return Maybe.None();
    }
}

export const convert_delegate = (delegate?: string): Optional<operation_unsigned.NSpublic_key_hash.public_key_hash> => {
    if (delegate === undefined) {
        return Maybe.None();
    } else {
        return Maybe.Some(string_to_pkh(delegate));
    }
}

export const convert_script = (script: { code: MichelsonValue, storage: MichelsonValue }): operation_unsigned.scripted__contracts => {
    var code = bytesdecoder()(NSmicheline__michelson_v1__expression.expression_encoder(value_to_expression(script.code)));
    var storage = bytesdecoder()(NSmicheline__michelson_v1__expression.expression_encoder(value_to_expression(script.storage)));
    return { code, storage };
}


export const convert: { [key: string]: (val: {}) => unknown } = {
    [CODECTYPE.ZARITH]: (n: string) => BigInt(n),
    [CODECTYPE.BRANCH]: string_to_branch,
    [CODECTYPE.PKH]: string_to_pkh,
    [CODECTYPE.INT32]: n => n,
    [CODECTYPE.RAW]: string_to_raw,
    [CODECTYPE.PROPOSAL]: string_to_proposal,
    [CODECTYPE.PROPOSAL_ARR]: string_to_proposal_array,
    [CODECTYPE.ADDRESS]: string_to_contract_id,
    [CODECTYPE.PARAMETERS]: convert_parameters,
    [CODECTYPE.DELEGATE]: convert_delegate,
    [CODECTYPE.PUBLIC_KEY]: string_to_public_key,
    [CODECTYPE.BALLOT_STATEMENT]: string_to_ballot,
    [CODECTYPE.SCRIPT]: convert_script,
}

convert[CODECTYPE.OPERATION] = convert_alphacontents;
convert[CODECTYPE.MANAGER] = schema_converter(convert)(schema.ManagerOperationSchema);