import { TypedParser } from './byteparser'

describe('parse', () => {
    test('enforces parity', () => {
        expect(() => TypedParser.parse("abcde")).toThrow();
    });

    test('enforces alphabet', () => {
        expect(() => TypedParser.parse("nonsense")).toThrow();
    });

    test('case-insensitive', () => {
        var lower = TypedParser.parse("abcdef0a1b2c");
        var upper = TypedParser.parse("ABCDEF0A1B2C");
        var mixed = TypedParser.parse("AbCdEf0A1b2C");

        [1, 2, 3].forEach(width => {
            const x = lower.consume(width);
            const y = upper.consume(width);
            const z = mixed.consume(width);

            expect(x).toStrictEqual(y);
            expect(y).toStrictEqual(z);
        });
    });
});

describe('consume', () => {
    test('disallows backtracking', () => {
        var p: TypedParser = TypedParser.parse("00000000");
        expect(() => p.consume(-1)).toThrow();
    });

    test('prevents overrun', () => {
        var p: TypedParser = TypedParser.parse("00000000");
        expect(() => p.consume(8)).toThrow();
    });
});

describe('integral get methods', () => {
    test('get_uint8', () => {
        var p: TypedParser = TypedParser.parse("010280ff");
        expect(p.length()).toBe(4);
        expect(p.get_uint8()).toBe(1);
        expect(p.get_uint8()).toBe(2);
        expect(p.get_uint8()).toBe(2 ** 7);
        expect(p.get_uint8()).toBe((2 ** 8) - 1);
    });

    test('get_multi_int8', () => {
        var p: TypedParser = TypedParser.parse("010280ff");
        expect(p.length()).toBe(4);
        expect(p.get_int8()).toBe(1);
        expect(p.get_int8()).toBe(2);
        expect(p.get_int8()).toBe(-(2 ** 7));
        expect(p.get_int8()).toBe(-1);
    });

    test('get_multi_uint16', () => {
        var p: TypedParser = TypedParser.parse("000100028000ffff");
        expect(p.length()).toBe(8);
        expect(p.get_uint16()).toBe(1);
        expect(p.get_uint16()).toBe(2);
        expect(p.get_uint16()).toBe(2 ** 15);
        expect(p.get_uint16()).toBe((2 ** 16) - 1);
    });

    test('get_multi_int16', () => {
        var p: TypedParser = TypedParser.parse("000100028000ffff");
        expect(p.length()).toBe(8);
        expect(p.get_int16()).toBe(1);
        expect(p.get_int16()).toBe(2);
        expect(p.get_int16()).toBe(-(2 ** 15));
        expect(p.get_int16()).toBe(-1);
    });

    test('get_multi_uint32', () => {
        var p: TypedParser = TypedParser.parse("00000001" + "00000002" + "80000000" + "ffffffff");
        expect(p.length()).toBe(16);
        expect(p.get_uint32()).toBe(1);
        expect(p.get_uint32()).toBe(2);
        expect(p.get_uint32()).toBe(2 ** 31);
        expect(p.get_uint32()).toBe((2 ** 32) - 1);
    });

    test('get_multi_int32', () => {
        var p: TypedParser = TypedParser.parse("00000001" + "00000002" + "80000000" + "ffffffff");
        expect(p.length()).toBe(16);
        expect(p.get_int32()).toBe(1);
        expect(p.get_int32()).toBe(2);
        expect(p.get_int32()).toBe(-(2 ** 31));
        expect(p.get_int32()).toBe(-1);
    });

    test('get_multi_uint64', () => {
        var p: TypedParser = TypedParser.parse("0000000000000001" + "0000000000000002" + "8000000000000000" + "ffffffffffffffff");
        expect(p.length()).toBe(32);
        expect(p.get_uint64()).toBe(1n);
        expect(p.get_uint64()).toBe(2n);
        expect(p.get_uint64()).toBe(2n ** 63n);
        expect(p.get_uint64()).toBe((2n ** 64n) - 1n);
    });

    test('get_multi_int64', () => {
        var p: TypedParser = TypedParser.parse("0000000000000001" + "0000000000000002" + "8000000000000000" + "ffffffffffffffff");
        expect(p.length()).toBe(32);
        expect(p.get_int64()).toBe(1n);
        expect(p.get_int64()).toBe(2n);
        expect(p.get_int64()).toBe(-(2n ** 63n));
        expect(p.get_int64()).toBe(-1n);
    });
});

test('get_boolean', () => {
    var p: TypedParser = TypedParser.parse("00ffabcd");
    expect(p.get_boolean()).toBe(false);
    expect(p.get_boolean()).toBe(true);
    expect(() => p.get_boolean()).toThrow();
    expect(() => p.get_boolean()).toThrow();
});

test('skip_padding', () => {
    var p: TypedParser = TypedParser.parse("00000001000000");
    expect(p.get_int32()).toBe(1);
    expect(() => p.skip_padding(4)).toThrow();
    expect(() => p.skip_padding(1)).not.toThrow();
    expect(() => p.skip_padding(1)).not.toThrow();
    expect(() => p.skip_padding(1)).not.toThrow();
    expect(() => p.skip_padding(1)).toThrow();
})

describe('get_self_terminating', () => {
    test('constant-true predicate consumes one byte', () => {
        var p: TypedParser = TypedParser.parse("00000000");
        expect(p.get_self_terminating((_) => true)).toStrictEqual(new Uint8Array([0x00]));
        expect(p.length()).toBe(3);
    });

    test('constant-false predicate causes attempted overrun', () => {
        var p: TypedParser = TypedParser.parse("00000000");
        expect(() => p.get_self_terminating((_) => false)).toThrow();
        p.set_fit(2);
        expect(() => p.get_self_terminating((_) => false)).toThrow();
    });

    test('zarith predicate', () => {
        var p: TypedParser = TypedParser.parse("808f810c00");
        expect(p.get_self_terminating((byte) => !(byte & 0x80))).toStrictEqual(new Uint8Array([0x80, 0x8f, 0x81, 0x0c]));
        expect(p.get_self_terminating((byte) => !(byte & 0x80))).toStrictEqual(new Uint8Array([0x00]));
    });
});

describe('length', () => {
    test('over entire buffer', () => {
        var i: number;
        for (i = 1; i <= 4; i++) {
            const p: TypedParser = TypedParser.parse("01020304");
            p.consume(i);
            expect(p.length()).toBe(4 - i);
        }
    });

    test('inside context window', () => {
        var i: number;
        for (i = 1; i <= 8; i++) {
            for (var j = i; j <= 8; j++) {
                const p: TypedParser = TypedParser.parse("0102030401020304");
                p.set_fit(j);
                p.consume(i);
                expect(p.length()).toBe(j - i);
            }
        }
    });

    test('inside nested context windows', () => {
        var i: number;
        var p: TypedParser = TypedParser.parse("0102030401020304");
        for (i = 8; i >= 0; i--) {
            p.set_fit(i);
            expect(p.length()).toBe(i);
        }
        for (i = 0; i < 8; i++) {
            const avail = (i == 0) ? 0 : 1;
            expect(p.test_goal()).toEqual(i == 0);
            expect(p.length()).toEqual(avail);
            expect(p.consume(avail).length).toBe(avail);
            expect(p.test_goal()).toEqual(true);
            expect(() => p.enforce_goal()).not.toThrow();
        }
        expect(p.test_goal()).toEqual(false);
        expect(p.length()).toEqual(1);
        expect(() => p.consume(1)).not.toThrow();
        expect(p.test_goal()).toEqual(true);
        expect(() => p.enforce_goal()).not.toThrow();
        expect(p.length()).toEqual(0);
        expect(() => p.test_goal()).toThrow();
        expect(() => p.enforce_goal()).toThrow();
    });
});


test('persistent state after closure', () => {
    const f = (p: TypedParser): number => {
        return p.get_uint8();
    };

    var p: TypedParser = TypedParser.parse("01020304");
    expect(p.get_uint8()).toBe(1);
    expect(f(p)).toBe(2);
    expect(f(p)).toBe(3);
    expect(p.get_uint8()).toBe(4);
});

describe('context window behavior', () => {
    test('cannot consume past target-offset', () => {
        var p: TypedParser = TypedParser.parse("00000001");
        p.set_fit(3);
        expect(() => p.consume(4)).toThrow();
        expect(() => p.get_int32()).toThrow();
    });

    test('set_fit cannot set illegal context-window', () => {
        var p: TypedParser = TypedParser.parse("00000001");

        expect(() => p.set_fit(-1)).toThrow(); // width must be non-negative
        expect(() => p.set_fit(100)).toThrow(); // width cannot exceed buffer bounds
        p.set_fit(3);
        expect(() => p.set_fit(4)).toThrow(); // new window cannot be wider than old window
        p.consume(2);
        expect(() => p.set_fit(2)).toThrow(); // narrower inner window cannot overrun end of wider old window
    });

    test('test_goal + enforce_goal correctness', () => {
        var p: TypedParser = TypedParser.parse("00000001" + "00000002");

        /* test should fail and enforce should fail before we have opened any windows */
        expect(() => p.test_goal()).toThrow();
        expect(() => p.enforce_goal()).toThrow();

        p.set_fit(8);
        p.set_fit(4);

        /* we can test and enforce a goal of 0 without consuming */
        p.set_fit(0);
        expect(p.test_goal()).toBe(true);
        expect(() => p.enforce_goal()).not.toThrow();

        for (var i = 0; i < 4; i++) {
            /* test should be negative and enforce should fail while we are still consuming */
            expect(p.test_goal()).toBe(false);
            expect(() => p.enforce_goal()).toThrow();
            p.consume(1);
        }
        /* test should be positive and enforce should succeed once we have exhausted the inner window */
        expect(p.test_goal()).toBe(true);
        expect(() => p.enforce_goal()).not.toThrow();

        for (var i = 0; i < 4; i++) {
            /* test should be negative and enforce should fail while we are still consuming */
            expect(p.test_goal()).toBe(false);
            expect(() => p.enforce_goal()).toThrow();
            p.consume(1);
        }
        /* test should be positive and enforce should succeed once we have exhausted the outer window */
        expect(p.test_goal()).toBe(true);
        expect(() => p.enforce_goal()).not.toThrow();

        /* test should fail and enforce should fail once all windows have been closed */
        expect(() => p.test_goal()).toThrow();
        expect(() => p.enforce_goal()).toThrow();
    });

    test('tighten_fit cannot set illegal context-window', () => {
        var p: TypedParser = TypedParser.parse("00000001");
        const new_fit = 3;
        expect(() => p.tighten_fit(8)).toThrow();
        p.set_fit(new_fit);
        expect(() => p.tighten_fit(4)).toThrow();
        for (var i = new_fit - 1; i >= 0; i--) {
            expect(() => { p.tighten_fit(1); }).not.toThrow();
        }
        expect(() => { p.tighten_fit(1); }).toThrow();
    });

    test('tighten_fit cannot underrun current offset', () => {
        var p: TypedParser = TypedParser.parse("00000001");
        p.tighten_fit(1);
        p.consume(2);
        expect(() => p.tighten_fit(2)).toThrow();
    });
});
