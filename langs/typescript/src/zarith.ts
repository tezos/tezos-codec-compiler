import { TypedParser } from "./byteparser";
import { Decoder, decoder_argument } from "./decoder";
import { Encoder } from "./encoder";
import { uint8 } from "./integral";
import { arr2hex, buf2hex, bytes } from "./primitive";

/**
 * NOTE - We are using opaque byte-arrays to represent Zarith numbers,
 * and we are not checking for positivity of naturals (as of now).
 * 
 * Consider making this numeric?
 */

/** Namespace view of Zarith naturals so [N.t] typechecks */
export namespace N {
    /** Unparsed Zarith byte-sequence */
    export type t = bigint;
    type u = bytes;

    function nbytes(n: t): number {
        const bits = n.toString(2).length;
        const bytes = Math.ceil(bits / 7);
        return bytes;
    }

    function to_bytes(n: t): u {
        if (n < 0) {
            throw new Error(`Invalid (negative) Zarith natural: ${n}`);
        }

        const n_bytes = nbytes(n);
        let buf: u = new Uint8Array(n_bytes);
        var i: number = 0;

        var nn = n;
        var last = false;

        while (!last) {
            var byte: uint8 = Number(nn & 0x7fn);
            nn >>= 7n;
            last = !nn;
            if (!last) byte |= 0x80;
            buf[i++] = byte;
        }

        return buf;
    }

    function from_bytes(bytes: u): t {
        var n: bigint = 0n;
        var bits: bigint = 0n;

        if (bytes.length === 0) {
            throw new Error('Cannot parse empty byte-array as Zarith natural');
        }

        bytes.forEach((byte, index) => {
            var val = byte & 0x7f;
            n |= (BigInt(val) << bits);
            bits += 7n;
            if (!byte && index > 0) throw new Error("Unexpected trailing zero byte in Zarith natural byte-array");
        });

        return n;
    }

    /** Convert a Zarith natural to its hex encoding */
    export const t_encoder: Encoder<t> = (val: t) => arr2hex(to_bytes(val));

    /** Convert a hex-string encoded Zarith natural to its internal representation */
    export const t_decoder: Decoder<t> = (input: decoder_argument): t => {
        var p = TypedParser.parse(input);
        return from_bytes(p.get_self_terminating((byte: number) => (byte & 0x80) === 0));
    }

}

/** Namespace view of Zarith integers so [Z.t] typechecks */
export namespace Z {
    /** Unparsed Zarith byte-sequence */
    export type t = bigint;
    type u = bytes;

    function nbytes(n: t): number {
        const bits = n.toString(2).length;
        const bytes = Math.ceil((bits - 6) / 7) + 1;
        return bytes;
    }


    function to_bytes(n: t): u {
        var nn = (n < 0 ? -n : n);
        const n_bytes = nbytes(nn);
        let buf: u = new Uint8Array(n_bytes);
        var i: number = 0;

        var last = false;
        const sign: uint8 = (n < 0 ? 0x40 : 0x00);

        /* Encode first byte with sign bit */
        var byte: uint8 = Number(nn & 0x3fn) | sign;
        nn >>= 6n;
        last = !nn;
        if (!last) byte |= 0x80;
        buf[i++] = byte;

        while (!last) {
            var byte: uint8 = Number(nn & 0x7fn);
            nn >>= 7n;
            last = !nn;
            if (!last) byte |= 0x80;
            buf[i++] = byte;
        }

        return buf;
    }

    function from_bytes(bytes: u): t {
        var n: bigint = 0n;
        var bits: bigint = 0n;
        var is_negative: boolean;

        if (bytes.length === 0) {
            throw new Error('Cannot parse empty byte-array as Zarith integer');
        }

        bytes.forEach((byte, index) => {
            var val: uint8;
            if (index === 0) {
                is_negative = !!(byte & 0x40);
                val = byte & 0x3f;
                n |= (BigInt(val) << bits);
                bits += 6n;
            } else {
                val = byte & 0x7f;
                n |= (BigInt(val) << bits);
                bits += 7n;
                if (!byte) throw new Error("Unexpected trailing zero byte in Zarith integer byte-array");
            }
        });

        if (is_negative) {
            return -n;
        } else {
            return n;
        }
    }

    /** Convert a Zarith integer to its hex encoding */
    export const t_encoder: Encoder<t> = (val: t) => arr2hex(to_bytes(val));

    /** Convert a hex-string encoded Zarith integer to its internal representation */
    export const t_decoder: Decoder<t> = (input: decoder_argument): t => {
        var p = TypedParser.parse(input);
        return from_bytes(p.get_self_terminating((byte: number) => (byte & 0x80) === 0));
    }
}

/** standard names for zarith types */
export type zarith_z = Z.t;
export type zarith_n = N.t;

export const zarith_nencoder = N.t_encoder;

export const zarith_zencoder = Z.t_encoder;

export const zarith_ndecoder = N.t_decoder;

export const zarith_zdecoder = Z.t_decoder;

/**
export const zarithEncoder = (n: string): string => {
    const fn: Array<string> = [];
    let nn = new BigNumber(n, 10);
    if (nn.isNaN()) {
        throw new TypeError(`Invalid zarith number ${n}`);
    }
    while (true) {
        // eslint-disable-line
        if (nn.lt(128)) {
            if (nn.lt(16)) fn.push('0');
            fn.push(nn.toString(16));
            break;
        } else {
            let b = nn.mod(128);
            nn = nn.minus(b);
            nn = nn.dividedBy(128);
            b = b.plus(128);
            fn.push(b.toString(16));
        }
    }
    return fn.join('');
};

export const zarithDecoder = (n: Uint8ArrayConsumer): string => {
    let mostSignificantByte = 0;
    while (mostSignificantByte < n.length() && (n.get(mostSignificantByte) & 128) !== 0) {
        mostSignificantByte += 1;
    }

    let num = new BigNumber(0);
    for (let i = mostSignificantByte; i >= 0; i -= 1) {
        let tmp = n.get(i) & 0x7f;
        num = num.multipliedBy(128);
        num = num.plus(tmp);
    }

    n.consume(mostSignificantByte + 1);
    return new BigNumber(num).toString();
};
*/