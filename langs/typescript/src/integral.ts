import { TypedParser } from './byteparser';
import { buf2hex } from './primitive';
import { Decoder, decoder_argument } from './decoder';
import { Encoder } from './encoder'

export type uint8 = number;
export type int8 = number;
export type uint16 = number;
export type int16 = number;
export type uint32 = number;
export type int32 = number;
export type uint64 = bigint;
export type int64 = bigint;


/**
 * Checks whether a given value of a generic integer-like type (either [[number]] or [[bigint]])
 * falls within an inclusive range from a minimum value to a maximum value. Throws an error if the
 * given value is out of range, and otherwise returns silently.
 * 
 * @param min Inclusive lower bound of the valid range
 * @param max Inclusive upper bound of the valid range
 * @param n Value used for in-range asssertion.
 * @param tname (optional) type-name to include in error message
 */
function check_range<TNum>(min: TNum, max: TNum, n: TNum, tname?: string): void {
    if (n < min || n > max) {
        const constraint = ((tname !== undefined) ? `of ${tname} values` : `[${min},${max}]`);
        throw new Error(`check_range: constraint failure: value ${n} is not in the range ${constraint}.`);
    }
    return;
}


export const uint8encoder: Encoder<uint8> = (val) => {
    check_range(0x0, 0xff, val, "uint8");
    var arr = new ArrayBuffer(1);
    var view = new DataView(arr);
    view.setUint8(0, val);
    return buf2hex(arr);
};

export const int8encoder: Encoder<int8> = (val) => {
    check_range(-0x80, 0x7f, val, "int8");
    var arr = new ArrayBuffer(1);
    var view = new DataView(arr);
    view.setInt8(0, val);
    return buf2hex(arr);
};

export const uint16encoder: Encoder<uint16> = (val) => {
    check_range(0x0, 0xffff, val, "uint16");
    var arr = new ArrayBuffer(2);
    var view = new DataView(arr);
    view.setUint16(0, val, false);
    return buf2hex(arr);
};

export const int16encoder: Encoder<int16> = (val) => {
    check_range(-0x8000, 0x7fff, val, "int16");
    var arr = new ArrayBuffer(2);
    var view = new DataView(arr);
    view.setInt16(0, val, false);
    return buf2hex(arr);
};

export const uint32encoder: Encoder<uint32> = (val) => {
    check_range(0x0, 0xffff_ffff, val, "uint32");
    var arr = new ArrayBuffer(4);
    var view = new DataView(arr);
    view.setUint32(0, val, false);
    return buf2hex(arr);
};

export const int32encoder: Encoder<int32> = (val) => {
    check_range(-0x8000_0000, 0x7fff_ffff, val, "int32");
    var arr = new ArrayBuffer(4);
    var view = new DataView(arr);
    view.setInt32(0, val, false);
    return buf2hex(arr);
};

export const uint64encoder: Encoder<uint64> = (val) => {
    check_range(0x0n, 0xffff_ffff_ffff_ffffn, val, "uint64");
    var arr = new ArrayBuffer(8);
    var view = new DataView(arr);
    view.setBigInt64(0, val, false);
    return buf2hex(arr);
};

export const int64encoder: Encoder<int64> = (val) => {
    check_range(-0x8000_0000_0000_0000n, 0x7fff_ffff_ffff_ffffn, val, "int64");
    var arr = new ArrayBuffer(8);
    var view = new DataView(arr);
    view.setBigInt64(0, val, false);
    return buf2hex(arr);
};

export const uint8decoder: Decoder<uint8> = (input) => {
    return TypedParser.parse(input).get_uint8();
};

export const int8decoder: Decoder<int8> = (input) => {
    return TypedParser.parse(input).get_int8();
};

export const uint16decoder: Decoder<uint16> = (input) => {
    return TypedParser.parse(input).get_uint16();
};

export const int16decoder: Decoder<int16> = (input) => {
    return TypedParser.parse(input).get_int16();
};

export const uint32decoder: Decoder<uint32> = (input) => {
    return TypedParser.parse(input).get_uint32();
};

export const int32decoder: Decoder<int32> = (input) => {
    return TypedParser.parse(input).get_int32();
};

export const uint64decoder: Decoder<uint64> = (input) => {
    return TypedParser.parse(input).get_uint64();
};

export const int64decoder: Decoder<int64> = (input) => {
    return TypedParser.parse(input).get_int64();
};