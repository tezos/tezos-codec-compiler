import { Foo, Foo_encoder, Foo_decoder } from './enum_example'

test('Encoder works', () => {
    expect(Foo_encoder(Foo.A)).toBe("00000001");
});

test('Decoder works', () => {
    expect(Foo_decoder("00000001")).toBe(Foo.A);
    expect(() => Foo_decoder("00000003")).toThrow();
});