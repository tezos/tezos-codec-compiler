import { Decoder } from "./decoder";
import { Encoder } from "./encoder";
import { uint16, int32, uint16encoder, int32encoder, int32decoder, uint16decoder, uint32encoder, uint32decoder, uint32 } from "./integral";
import { varlen_string, stringdecoder, stringencoder, lenpref_encoder, lenpref_decoder } from "./primitive";
import { Variant, Tagless, variant_encoder, variant_decoder } from "./adt";
import * as fc from 'fast-check';
import { Either } from './adt_example';

// SECTION - simple ADT to use as a case model
describe('ADT Case Study', () => {
    type FooTag = 1 | 2 | 4;

    type FooVariant = Variant<FooTag>;

    interface Foo1 extends FooVariant { _tag: 1; foo: int32; }

    interface Foo2 extends FooVariant { _tag: 2; bar: uint16; }

    interface Foo4 extends FooVariant { _tag: 4; baz: varlen_string; }

    type Foo = Foo1 | Foo2 | Foo4;

    const Foo1_encoder: Encoder<Tagless<Foo1>> = val => int32encoder(val.foo);

    const Foo1_decoder: Decoder<Tagless<Foo1>> = input => ({ foo: int32decoder(input) });

    const Foo2_encoder: Encoder<Tagless<Foo2>> = val => uint16encoder(val.bar);

    const Foo2_decoder: Decoder<Tagless<Foo2>> = input => ({ bar: uint16decoder(input) });

    const Foo4_encoder: Encoder<Tagless<Foo4>> = val => lenpref_encoder()(stringencoder)(val.baz);

    const Foo4_decoder: Decoder<Tagless<Foo4>> = input => ({ baz: lenpref_decoder<string, "uint30">()(stringdecoder)(input) });

    const Foo_encoder: Encoder<Foo> =
        variant_encoder<FooTag, Foo>({
            [1]: Foo1_encoder,
            [2]: Foo2_encoder,
            [4]: Foo4_encoder
        })("uint16");

    const Foo_decoder: Decoder<Foo> =
        variant_decoder<FooTag, Foo>({
            [1]: Foo1_decoder,
            [2]: Foo2_decoder,
            [4]: Foo4_decoder
        })("uint16");

    test('encoder correctness', () => {
        fc.assert(
            fc.property(
                fc.integer({ min: -0x8000_0000, max: 0x7fff_ffff }),
                foo => {
                    Foo_encoder({ _tag: 1, foo }) === ("0001" + int32encoder(foo));
                }
            )
        );
        fc.assert(
            fc.property(
                fc.integer({ min: 0x0, max: 0xffff }),
                bar => {
                    Foo_encoder({ _tag: 2, bar }) === ("0002" + uint16encoder(bar));
                }
            )
        );
        fc.assert(
            fc.property(
                fc.hexaString({ minLength: 0, maxLength: 128 }).filter(str => str.length % 2 === 0),
                baz => {
                    Foo_encoder({ _tag: 4, baz }) === ("0004" + lenpref_encoder()(stringencoder)(baz));
                }
            )
        );
    });

    test('decoder correctness', () => {
        fc.assert(
            fc.property(
                fc.integer({ min: -0x8000_0000, max: 0x7fff_ffff }),
                foo => {
                    ({ _tag: 1, foo }) === Foo_decoder("0001" + int32encoder(foo));
                }
            )
        );
        fc.assert(
            fc.property(
                fc.integer({ min: 0x0, max: 0xffff }),
                bar => {
                    ({ _tag: 2, bar }) === Foo_decoder("0002" + uint16encoder(bar));
                }
            )
        );
        fc.assert(
            fc.property(
                fc.hexaString({ minLength: 0, maxLength: 128 }).filter(str => str.length % 2 === 0),
                baz => {
                    ({ _tag: 4, baz }) === Foo_decoder("0004" + lenpref_encoder()(stringencoder)(baz));
                }
            )
        );
    });

    test('decoder fails for invalid tags', () => {
        expect(() => Foo_decoder("0000")).toThrow();
        expect(() => Foo_decoder("0003ff")).toThrow();
        expect(() => Foo_decoder("00055555")).toThrow();
        expect(() => Foo_decoder("0006deadbeef")).toThrow();
    });

    test('roundtrip', () => {
        fc.assert(
            fc.property(
                fc.integer({ min: -0x8000_0000, max: 0x7fff_ffff }),
                foo => {
                    let val: Foo = { _tag: 1, foo };
                    let input = ("0001" + int32encoder(foo));
                    val === Foo_decoder(Foo_encoder(val));
                    input === Foo_encoder(Foo_decoder(input));
                }
            )
        );
        fc.assert(
            fc.property(
                fc.integer({ min: 0x0, max: 0xffff }),
                bar => {
                    let val: Foo = { _tag: 2, bar };
                    let input = ("0002" + uint16encoder(bar));
                    val === Foo_decoder(Foo_encoder(val));
                    input === Foo_encoder(Foo_decoder(input));
                }
            )
        );
        fc.assert(
            fc.property(
                fc.hexaString({ minLength: 0, maxLength: 128 }).filter(str => str.length % 2 === 0),
                baz => {
                    let val: Foo = { _tag: 4, baz };
                    let input = ("0004" + lenpref_encoder()(stringencoder)(baz));
                    val === Foo_decoder(Foo_encoder(val));
                    input === Foo_encoder(Foo_decoder(input));
                }
            )
        );
    });
});
// !SECTION



// SECTION - ADT using namespaces and enums
// LINK - src/adt_example.ts
describe('Enum + Namespace ADT', () => {
    type Homogenous<A> = Either.Either<A, A>;
    const homogenous_encoder = <A>(enc: Encoder<A>): Encoder<Homogenous<A>> =>
        Either.either_encoder(enc, enc);
    const homogenous_decoder = <A>(dec: Decoder<A>): Decoder<Homogenous<A>> =>
        Either.either_decoder(dec, dec);
    const HomLeft = <A>(left: A): Homogenous<A> => (Either.Left<A>(left) as Homogenous<A>);
    const HomRight = <A>(right: A): Homogenous<A> => (Either.Right<A>(right) as Homogenous<A>);

    test('composite encoder correctness', () => {
        expect(homogenous_encoder(int32encoder)(HomLeft(42))).toBe("00" + int32encoder(42));
        expect(homogenous_encoder(int32encoder)(HomRight(42))).toBe("01" + int32encoder(42));
    });

    test('composite decoder correctness', () => {
        expect(homogenous_decoder(int32decoder)("00" + int32encoder(42))).toStrictEqual(HomLeft(42));
        expect(homogenous_decoder(int32decoder)("01" + int32encoder(42))).toStrictEqual(HomRight(42));
    });

});
// !SECTION

describe('Partial transcoder', () => {
    enum Partial {
        Included = 0,
        Excluded = 1,
    };

    type PartialVariant = Variant<Partial>

    interface Within<A> extends PartialVariant { _tag: Partial.Included; within: A };
    interface Without<A> extends PartialVariant { _tag: Partial.Excluded; without: A };

    type With<A> = Within<A> | Without<A>;

    const mkWithin = <A>(within: A): With<A> => ({ _tag: Partial.Included, within } as With<A>);
    const mkWithout = <A>(without: A): With<A> => ({ _tag: Partial.Excluded, without } as With<A>);

    const within_encoder = <A>(enc: Encoder<A>): Encoder<Tagless<Within<A>>> => (val) => enc(val.within);
    const within_decoder = <A>(dec: Decoder<A>): Decoder<Tagless<Within<A>>> => (input) => ({ within: dec(input) });

    const with_encoder = <A>(enc: Encoder<A>) => variant_encoder<Partial, With<A>>({ [Partial.Included]: within_encoder(enc) })("uint8");
    const with_decoder = <A>(dec: Decoder<A>) => variant_decoder<Partial, With<A>>({ [Partial.Included]: within_decoder(dec) })("uint8");

    test('Within succeeds', () => {
        expect(with_encoder(uint32encoder)(mkWithin(0xdeadbeef))).toBe("00deadbeef");
        expect((with_decoder(uint32decoder)("00deadbeef") as Within<uint32>).within).toBe(0xdeadbeef);
    });

    test('Without fails', () => {
        expect(() => with_encoder(uint32encoder)(mkWithout(0xdeadbeef))).toThrow();
        expect(() => with_decoder(uint32decoder)("01deadbeef")).toThrow();
    });
});