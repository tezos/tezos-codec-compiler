import { TypedParser } from "./byteparser";
import { Decoder, decoder_argument } from "./decoder";
import { Encoder } from "./encoder";
import { buf2hex } from "./primitive";

export type float32 = number;
export type float64 = number;

export const float32encoder: Encoder<float32> = (val: float32): string => {
    var arr = new ArrayBuffer(4);
    var view = new DataView(arr);
    view.setFloat32(0, val);
    return buf2hex(arr);
}

export const float64encoder: Encoder<float64> = (val: float64): string => {
    var arr = new ArrayBuffer(8);
    var view = new DataView(arr);
    view.setFloat64(0, val);
    return buf2hex(arr);
}

export const float32decoder: Decoder<float32> = (input: decoder_argument): float32 => {
    var p = TypedParser.parse(input);
    return p.get_float32();
}

export const float64decoder: Decoder<float64> = (input: decoder_argument): float64 => {
    var p = TypedParser.parse(input);
    return p.get_float64();
}