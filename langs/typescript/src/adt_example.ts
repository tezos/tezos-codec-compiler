import {
    Variant,
    Tagless,
    variant_decoder, variant_encoder,
} from './adt'
import { Decoder } from './decoder';
import { Encoder } from './encoder'

export namespace Either {
    export enum EitherTag {
        LEFT = 0,
        RIGHT = 1,
    };

    interface EitherLeft<A> extends Variant<EitherTag> {
        _tag: EitherTag.LEFT,
        left: A
    }

    const eitherleft_encoder = <A>(enc: Encoder<A>): Encoder<Tagless<EitherLeft<A>>> => val => enc(val.left);
    const eitherleft_decoder = <A>(dec: Decoder<A>): Decoder<Tagless<EitherLeft<A>>> => input => ({ left: dec(input) });

    export interface EitherRight<B> extends Variant<EitherTag> {
        _tag: EitherTag.RIGHT,
        right: B
    }

    const eitherright_encoder = <B>(enc: Encoder<B>): Encoder<Tagless<EitherRight<B>>> => val => enc(val.right);
    const eitherright_decoder = <B>(dec: Decoder<B>): Decoder<Tagless<EitherRight<B>>> => input => ({ right: dec(input) });

    export type Either<A, B> = EitherLeft<A> | EitherRight<B>
    export const Left = <A>(left: A): Either<A, unknown> => ({ _tag: EitherTag.LEFT, left });
    export const Right = <B>(right: B): Either<unknown, B> => ({ _tag: EitherTag.RIGHT, right });


    export const either_encoder = <A, B>(left_enc: Encoder<A>, right_enc: Encoder<B>): Encoder<Either<A, B>> =>
        variant_encoder<EitherTag, Either<A, B>>({
            [EitherTag.LEFT]: eitherleft_encoder(left_enc),
            [EitherTag.RIGHT]: eitherright_encoder(right_enc)
        })("uint8");

    export const either_decoder = <A, B>(left_dec: Decoder<A>, right_dec: Decoder<B>): Decoder<Either<A, B>> =>
        variant_decoder<EitherTag, Either<A, B>>({
            [EitherTag.LEFT]: eitherleft_decoder(left_dec),
            [EitherTag.RIGHT]: eitherright_decoder(right_dec)
        })("uint8");
}