import {
    uint8decoder, uint8encoder,
    int8decoder, int8encoder,
    uint16decoder, uint16encoder,
    int16decoder, int16encoder,
    uint32decoder, uint32encoder,
    int32decoder, int32encoder,
    uint64decoder, uint64encoder,
    int64decoder, int64encoder
} from './integral'

import * as fc from 'fast-check';

/* uint8 tests */
describe('unit tests for uint8', () => {
    const cases: Array<[number, string]> = [
        [0x00, "00"],
        [0x42, "42"],
        [0x7f, "7f"],
        [0x80, "80"],
        [0xff, "ff"]
    ];
    const encoder = uint8encoder;
    const decoder = uint8decoder;
    const min_val = 0x0;
    const max_val = 0xff;
    const length = 2;
    const hexbounds = { minLength: length, maxLength: length };
    const intbounds = { min: min_val, max: max_val };



    test('encode in range', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toBe(image)
        );
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('encode out of range', () => {
        expect(() => encoder(min_val - 1)).toThrow();
        expect(() => encoder(max_val + 1)).toThrow();
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.hexaString(hexbounds),
            str => { encoder(decoder(str)) === str; }
        ));
        fc.assert(fc.property(
            fc.integer(intbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});

/* int8 tests */
describe('unit tests for int8', () => {
    const cases: Array<[number, string]> = [
        [0x00, "00"],
        [0x42, "42"],
        [0x7f, "7f"],
        [-0x80, "80"],
        [-0x01, "ff"]
    ];
    const encoder = int8encoder;
    const decoder = int8decoder;
    const min_val = -0x80;
    const max_val = 0x7f;
    const length = 2;
    const hexbounds = { minLength: length, maxLength: length };
    const intbounds = { min: min_val, max: max_val };

    test('encode in range', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toBe(image)
        );
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('encode out of range', () => {
        expect(() => encoder(min_val - 1)).toThrow();
        expect(() => encoder(max_val + 1)).toThrow();
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.hexaString(hexbounds),
            str => { encoder(decoder(str)) === str; }
        ));
        fc.assert(fc.property(
            fc.integer(intbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});


/* uint16 tests */
describe('unit tests for uint16', () => {
    const cases: Array<[number, string]> = [
        [0x0000, "0000"],
        [0x4242, "4242"],
        [0x7fff, "7fff"],
        [0x8000, "8000"],
        [0xffff, "ffff"]
    ];
    const encoder = uint16encoder;
    const decoder = uint16decoder;
    const min_val = 0x0;
    const max_val = 0xffff;
    const length = 4;
    const hexbounds = { minLength: length, maxLength: length };
    const intbounds = { min: min_val, max: max_val };

    test('encode in range', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toBe(image)
        );
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('encode out of range', () => {
        expect(() => encoder(min_val - 1)).toThrow();
        expect(() => encoder(max_val + 1)).toThrow();
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.hexaString(hexbounds),
            str => { encoder(decoder(str)) === str; }
        ));
        fc.assert(fc.property(
            fc.integer(intbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});

/* int16 tests */
describe('unit tests for int16', () => {
    const cases: Array<[number, string]> = [
        [0x0000, "0000"],
        [0x4242, "4242"],
        [0x7fff, "7fff"],
        [-0x8000, "8000"],
        [-0x1, "ffff"]
    ];
    const encoder = int16encoder;
    const decoder = int16decoder;
    const min_val = -0x8000;
    const max_val = 0x7fff;
    const length = 4;
    const hexbounds = { minLength: length, maxLength: length };
    const intbounds = { min: min_val, max: max_val };

    test('encode in range', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toBe(image)
        );
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('encode out of range', () => {
        expect(() => encoder(min_val - 1)).toThrow();
        expect(() => encoder(max_val + 1)).toThrow();
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.hexaString(hexbounds),
            str => { encoder(decoder(str)) === str; }
        ));
        fc.assert(fc.property(
            fc.integer(intbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});


/* uint32 tests */
describe('unit tests for uint32', () => {
    const cases: Array<[number, string]> = [
        [0x0000_0000, "00000000"],
        [0x4242_4242, "42424242"],
        [0x7fff_ffff, "7fffffff"],
        [0x8000_0000, "80000000"],
        [0xffff_ffff, "ffffffff"]
    ];
    const encoder = uint32encoder;
    const decoder = uint32decoder;
    const min_val = 0x0;
    const max_val = 0xffff_ffff;
    const length = 8;
    const hexbounds = { minLength: length, maxLength: length };
    const intbounds = { min: min_val, max: max_val };

    test('encode in range', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toBe(image)
        );
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('encode out of range', () => {
        expect(() => encoder(min_val - 1)).toThrow();
        expect(() => encoder(max_val + 1)).toThrow();
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.hexaString(hexbounds),
            str => { encoder(decoder(str)) === str; }
        ));
        fc.assert(fc.property(
            fc.integer(intbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});

/* int32 tests */
describe('unit tests for int32', () => {
    const cases: Array<[number, string]> = [
        [0x0000_0000, "00000000"],
        [0x4242_4242, "42424242"],
        [0x7fff_ffff, "7fffffff"],
        [-0x8000_0000, "80000000"],
        [-0x1, "ffffffff"]
    ];
    const encoder = int32encoder;
    const decoder = int32decoder;
    const min_val = -0x8000_0000;
    const max_val = 0x7fff_ffff;
    const length = 8;
    const hexbounds = { minLength: length, maxLength: length };
    const intbounds = { min: min_val, max: max_val };

    test('encode in range', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toBe(image)
        );
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('encode out of range', () => {
        expect(() => encoder(min_val - 1)).toThrow();
        expect(() => encoder(max_val + 1)).toThrow();
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.hexaString(hexbounds),
            str => { encoder(decoder(str)) === str; }
        ));
        fc.assert(fc.property(
            fc.integer(intbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});

/* uint64 tests */
describe('unit tests for uint64', () => {
    const cases: Array<[bigint, string]> = [
        [0x0000_0000_0000_0000n, "0000000000000000"],
        [0x4242_4242_4242_4242n, "4242424242424242"],
        [0x7fff_ffff_ffff_ffffn, "7fffffffffffffff"],
        [0x8000_0000_0000_0000n, "8000000000000000"],
        [0xffff_ffff_ffff_ffffn, "ffffffffffffffff"]
    ];
    const encoder = uint64encoder;
    const decoder = uint64decoder;
    const min_val = 0x0n;
    const max_val = 0xffff_ffff_ffff_ffffn;
    const length = 16;
    const hexbounds = { minLength: length, maxLength: length };
    const intbounds = { min: min_val, max: max_val };

    test('encode in range', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toBe(image)
        );
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('encode out of range', () => {
        expect(() => encoder(min_val - 1n)).toThrow();
        expect(() => encoder(max_val + 1n)).toThrow();
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.hexaString(hexbounds),
            str => { encoder(decoder(str)) === str; }
        ));
        fc.assert(fc.property(
            fc.bigUint(intbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});

/* int64 tests */
describe('unit tests for int64', () => {
    const cases: Array<[bigint, string]> = [
        [0x0000_0000_0000_0000n, "0000000000000000"],
        [0x4242_4242_4242_4242n, "4242424242424242"],
        [0x7fff_ffff_ffff_ffffn, "7fffffffffffffff"],
        [-0x8000_0000_0000_0000n, "8000000000000000"],
        [-0x1n, "ffffffffffffffff"]
    ];
    const encoder = int64encoder;
    const decoder = int64decoder;
    const min_val = -0x8000_0000_0000_0000n;
    const max_val = 0x7fff_ffff_ffff_ffffn;
    const length = 16;
    const hexbounds = { minLength: length, maxLength: length };
    const intbounds = { min: min_val, max: max_val };

    test('encode in range', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toBe(image)
        );
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('encode out of range', () => {
        expect(() => encoder(min_val - 1n)).toThrow();
        expect(() => encoder(max_val + 1n)).toThrow();
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.hexaString(hexbounds),
            str => { encoder(decoder(str)) === str; }
        ));
        fc.assert(fc.property(
            fc.bigInt(intbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});