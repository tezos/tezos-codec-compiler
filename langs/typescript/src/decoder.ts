import { TypedParser } from './byteparser';

export type decoder_argument = string | TypedParser;

export type Decoder<T> = (input: decoder_argument) => T;