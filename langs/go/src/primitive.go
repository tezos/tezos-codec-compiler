package main

import "fmt"

type maybeint = map[struct{}]int

func main() {
    var someint maybeint = map[struct{}]int{{}: 2}
    var noneint maybeint = map[struct{}]int{}
    if some, oksome := someint[struct{}{}]; oksome {
        fmt.Printf("Some %d\n", some)
    } else {
        fmt.Printf("None\n")
    }
    if none, oknone := noneint[struct{}{}]; oknone {
        fmt.Printf("Some %d\n", none)
    } else {
        fmt.Printf("None\n")
    }
}

