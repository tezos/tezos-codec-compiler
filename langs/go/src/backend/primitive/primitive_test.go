package primitive

import (
	"backend/byteparser"
	"backend/zarith"
	"math/big"
	"reflect"
	"testing"
)

func TestWidth_Constrain(t *testing.T) {
	type args struct {
		p *byteparser.T
	}
	tests := []struct {
		name    string
		w       Width
		args    args
		after   uint
		wantErr bool
	}{
		{"valid (empty) Uint8 constrain", WidthUint8, args{byteparser.Hexstring("00").UnsafeParse()}, 0, false},
		{"valid (empty) Uint30 constrain", WidthUint30, args{byteparser.Hexstring("0000" + "0000").UnsafeParse()}, 0, false},
		{"valid (one-byte) Uint8 constrain", WidthUint8, args{byteparser.Hexstring("01" + "00").UnsafeParse()}, 1, false},
		{"valid (one-byte) Uint30 constrain", WidthUint30, args{byteparser.Hexstring("0000" + "0001" + "00").UnsafeParse()}, 1, false},
		{"valid (long) Uint8 constrain", WidthUint8, args{byteparser.Hexstring("10" + "0000" + "0000" + "0000" + "0000" + "0000" + "0000" + "0000" + "0000").UnsafeParse()}, 16, false},
		{"valid (long) Uint30 constrain", WidthUint30, args{byteparser.Hexstring("0000" + "0010" + "0000" + "0000" + "0000" + "0000" + "0000" + "0000" + "0000" + "0000").UnsafeParse()}, 16, false},
		{"invalid (empty) Uint8 constrain", WidthUint8, args{byteparser.Hexstring("").UnsafeParse()}, 0, true},
		{"invalid (empty) Uint30 constrain", WidthUint30, args{byteparser.Hexstring("").UnsafeParse()}, 0, true},
		{"invalid (one-byte) Uint8 constrain", WidthUint8, args{byteparser.Hexstring("01").UnsafeParse()}, 0, true},
		{"invalid (one-byte) Uint30 constrain", WidthUint30, args{byteparser.Hexstring("00000001").UnsafeParse()}, 0, true},
		{"invalid (long) Uint8 constrain", WidthUint8, args{byteparser.Hexstring("100000").UnsafeParse()}, 0, true},
		{"invalid (long) Uint30 constrain", WidthUint30, args{byteparser.Hexstring("00000010" + "0000").UnsafeParse()}, 0, true},
		{"invalid (nil) Uint8 constrain", WidthUint8, args{}, 0, true},
		{"invalid (nil) Uint30 constrain", WidthUint30, args{}, 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.w.Constrain(tt.args.p); (err != nil) != tt.wantErr {
				t.Errorf("Width.Constrain() error = %v, wantErr %v", err, tt.wantErr)
			} else if err == nil {
				if l := tt.args.p.Length(); l != tt.after {
					t.Errorf("Width.Constrain() Length = %v, want %v", l, tt.after)
				}
			}
		})
	}
}

func TestReserveSuffix_Constrain(t *testing.T) {
	type args struct {
		p *byteparser.T
	}
	tests := []struct {
		name    string
		r       Suffix
		args    args
		after   uint
		wantErr bool
	}{
		{"valid (empty) constrain", 0, args{byteparser.Hexstring("").UnsafeParse()}, 0, false},
		{"valid (one-byte) constrain", 1, args{byteparser.Hexstring("0000").UnsafeParse()}, 1, false},
		{"valid (long) constrain", 8, args{byteparser.Hexstring("0000" + "0000" + "0000" + "0000" + "0000" + "0000" + "0000" + "0000").UnsafeParse()}, 8, false},
		{"invalid (empty) constrain", 1, args{byteparser.Hexstring("").UnsafeParse()}, 0, true},
		{"invalid (one-byte) constrain", 2, args{byteparser.Hexstring("00").UnsafeParse()}, 0, true},
		{"invalid (nil) constrain", 0, args{}, 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.r.Constrain(tt.args.p); (err != nil) != tt.wantErr {
				t.Errorf("ReserveSuffix.Constrain() error = %v, wantErr %v", err, tt.wantErr)
			} else if err == nil {
				if l := tt.args.p.Length(); l != tt.after {
					t.Errorf("ReserveSuffix.Constrain() Length = %v, want %v", l, tt.after)
				}
			}
		})
	}
}

func TestDecodeSequence(t *testing.T) {
	type args struct {
		c     Constrainer
		gen  func() Transcoder
		input byteparser.DecoderArgument
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		want    []Transcoder
	}{
		/* length of sequence, length of input, constrainer */
		{"000",
			args{ Suffix(0), func() Transcoder { return new(zarith.Z) }, byteparser.Hexstring("")},
			false,
			[]Transcoder{},
		},
		{"12u",
			args{ WidthUint8, func() Transcoder { return new(zarith.Z) }, byteparser.Hexstring("0100")},
			false,
			[]Transcoder{&zarith.Z{Int: big.NewInt(0)}},
		},
		{"220",
			args{ Suffix(0), func() Transcoder { return new(zarith.Z) }, byteparser.Hexstring("01" + "00")},
			false,
			[]Transcoder{&zarith.Z{Int: big.NewInt(1)}, &zarith.Z{Int: big.NewInt(0)}},
		},
		{"26U",
			args{ WidthUint30, func() Transcoder { return new(zarith.Z) }, byteparser.Hexstring("00000002" + "00" + "00")},
			false,
			[]Transcoder{&zarith.Z{Int: big.NewInt(0)}, &zarith.Z{Int: big.NewInt(0)}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var seq []Transcoder = nil
			if err := DecodeSequence(tt.args.c, &seq, tt.args.gen, tt.args.input); (err != nil) != tt.wantErr {
				t.Errorf("Sequence.Decode() error = %v, wantErr %v", err, tt.wantErr)
			} else if !reflect.DeepEqual(seq, tt.want) {
				t.Errorf("Sequence.Decode() fills in contents %v, want %v", seq, tt.want)
			}
		})
	}
}

func TestEncodePadding(t *testing.T) {
	type args struct {
		amount uint
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "Zero padding", args: args{0}, want: ""},
		{name: "One padding", args: args{1}, want: "00"},
		{name: "Eight padding", args: args{8}, want: "00000000" + "00000000"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EncodePadding(tt.args.amount); got != tt.want {
				t.Errorf("EncodePadding() = %v, want %v", got, tt.want)
			}
		})
	}
}
