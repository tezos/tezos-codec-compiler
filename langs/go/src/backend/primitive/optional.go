package primitive

import (
	"backend/byteparser"
)

func EncodeOption(v *Transcoder) string {
	if v == nil {
		return "00"
	}
	return "ff" + (*v).Encode()
}

func DecodeOption(gen func() Transcoder, input byteparser.DecoderArgument) (*Transcoder, error) {
	p, err := input.Parse()
	if err != nil {
		return nil, err
	}

	if hasValue, err := p.GetBoolean(); err != nil || !hasValue {
		return nil, err
	}

	val := gen()
	val.Decode(p)
	return &val, nil
}