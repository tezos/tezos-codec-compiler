package primitive

import (
	"backend/byteparser"
	"reflect"
	"testing"
)

func TestEncodeDyn(t *testing.T) {
	type args struct {
		dyn Constrainer
		v   Transcoder
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"uint8, ''", args{dyn:WidthUint8, v:NewString("")}, "00"},
		{"uint8, 'deadbeef'", args{dyn:WidthUint8, v:NewString("deadbeef")}, "04deadbeef"},
		{"uint30, ''", args{dyn:WidthUint30, v:NewString("")}, "00000000"},
		{"uint30, 'deadbeef'", args{dyn:WidthUint30, v:NewString("deadbeef")}, "00000004deadbeef"},
		{"42, ''", args{dyn:Suffix(42), v:NewString("")}, ""},
		{"42, 'deadbeef'", args{dyn:Suffix(42), v:NewString("deadbeef")}, "deadbeef"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EncodeDyn(tt.args.dyn, tt.args.v); got != tt.want {
				t.Errorf("EncodeDyn() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecodeDyn(t *testing.T) {
	type args struct {
		dyn   Constrainer
		gen   func() Transcoder
		input byteparser.DecoderArgument
	}
	tests := []struct {
		name    string
		args    args
		want    Transcoder
		wantErr bool
	}{
		{"uint8, ''", args{dyn:WidthUint8, gen:func () Transcoder { return new(String) }, input: byteparser.Hexstring("00")}, NewString(""), false},
		{"uint8, 'deadbeef'", args{dyn:WidthUint8, gen:func () Transcoder { return new(String) }, input: byteparser.Hexstring("04deadbeef")}, NewString("deadbeef"), false},
		{"uint30, ''", args{dyn:WidthUint30, gen:func () Transcoder { return new(String) }, input: byteparser.Hexstring("00000000")}, NewString(""), false},
		{"uint30, 'deadbeef'", args{dyn:WidthUint30, gen:func () Transcoder { return new(String) }, input: byteparser.Hexstring("00000004deadbeef")}, NewString("deadbeef"), false},
		{"1, ''", args{dyn:Suffix(1), gen:func () Transcoder { return new(String) }, input: byteparser.Hexstring("42")}, NewString(""), false},
		{"1, 'deadbeef'", args{dyn:Suffix(1), gen:func () Transcoder { return new(String) }, input: byteparser.Hexstring("deadbeef42")}, NewString("deadbeef"), false},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecodeDyn(tt.args.dyn, tt.args.gen, tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodeDyn() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecodeDyn() = %v, want %v", got, tt.want)
			}
		})
	}
}
