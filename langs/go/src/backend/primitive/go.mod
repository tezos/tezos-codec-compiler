module backend/primitive

go 1.16

replace backend/byteparser => ../byteparser

replace backend/util => ../util

require (
	backend/byteparser v0.0.0-00010101000000-000000000000
	backend/zarith v0.0.0-00010101000000-000000000000
)

replace backend/zarith => ../zarith
