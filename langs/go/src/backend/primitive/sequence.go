package primitive

import (
	"backend/byteparser"
)

func EncodeSequence(dyn Constrainer, seq []Transcoder) string {
	var bytes string = ""

	var bytecount uint = 0
	for _, element := range seq {
		var elem string = element.Encode()
		bytes += elem
		elembc := uint(len(elem)) / 2
		bytecount += elembc
	}
	return dyn.Represent(bytecount) + bytes
}

func DecodeSequence(dyn Constrainer, seq *[]Transcoder, gen func () Transcoder, input byteparser.DecoderArgument) error {
	p, err := input.Parse()
	if err != nil {
		return err
	}
	if err := dyn.Constrain(p); err != nil {
		return err
	}

	ret := make([]Transcoder, 0, p.Length())

	for i := 0; ; i++ {
		if met, err := p.TestGoal(); err != nil {
			return err
		} else if met {
			break
		}
		ret = append(ret, gen())
		if err := ret[i].Decode(p); err != nil {
			return err
		}
	}
	if err := p.EnforceGoal(); err != nil {
		return err
	}

	*seq = ret
	return nil
}