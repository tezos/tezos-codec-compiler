package primitive

import (
	"backend/byteparser"
	"reflect"
	"testing"
)

func TestFooTag_Encode(t *testing.T) {
	tests := []struct {
		name string
		i    FooTag
		want string
	}{
		{"a-tag", FooATag, "00"},
		{"b-tag", FooBTag, "01"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.i.Encode(); got != tt.want {
				t.Errorf("FooTag.Encode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFooTag_Decode(t *testing.T) {
	type args struct {
		input byteparser.DecoderArgument
	}
	tests := []struct {
		name    string
		i       *FooTag
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.i.Decode(tt.args.input); (err != nil) != tt.wantErr {
				t.Errorf("FooTag.Decode() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFooA_Tag(t *testing.T) {
	type fields struct {
		I *Int32
	}
	tests := []struct {
		name   string
		fields fields
		want   FooTag
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := FooA{
				I: tt.fields.I,
			}
			if got := f.Tag(); got != tt.want {
				t.Errorf("FooA.Tag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFooB_Tag(t *testing.T) {
	type fields struct {
		S *String
	}
	tests := []struct {
		name   string
		fields fields
		want   FooTag
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := FooB{
				S: tt.fields.S,
			}
			if got := f.Tag(); got != tt.want {
				t.Errorf("FooB.Tag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFooA_Payload(t *testing.T) {
	type fields struct {
		I *Int32
	}
	tests := []struct {
		name   string
		fields fields
		want   Transcoder
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			x := FooA{
				I: tt.fields.I,
			}
			if got := x.Payload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FooA.Payload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFooB_Payload(t *testing.T) {
	type fields struct {
		S *String
	}
	tests := []struct {
		name   string
		fields fields
		want   Transcoder
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			y := FooB{
				S: tt.fields.S,
			}
			if got := y.Payload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FooB.Payload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFooA_Encode(t *testing.T) {
	type fields struct {
		I *Int32
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := FooA{
				I: tt.fields.I,
			}
			if got := a.Encode(); got != tt.want {
				t.Errorf("FooA.Encode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFooB_Encode(t *testing.T) {
	type fields struct {
		S *String
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := FooB{
				S: tt.fields.S,
			}
			if got := b.Encode(); got != tt.want {
				t.Errorf("FooB.Encode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFooA_Decode(t *testing.T) {
	type fields struct {
		I *Int32
	}
	type args struct {
		input byteparser.DecoderArgument
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{"A of 0", fields{I: NewInt32(0)}, args{byteparser.Hexstring("00000000")}, false},
		{"A of 0x2a", fields{I: NewInt32(0x2a)}, args{byteparser.Hexstring("0000002a")}, false},
		{"A of 0xffff", fields{I: NewInt32(0xffff)}, args{byteparser.Hexstring("0000ffff")}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := FooA{I: new(Int32)}
			if err := a.Decode(tt.args.input); (err != nil) != tt.wantErr {
				t.Errorf("FooA.Decode() error = %v, wantErr %v", err, tt.wantErr)
			} else if err == nil {
				c := FooA{I: tt.fields.I}
				if !reflect.DeepEqual(a, c) {
					t.Errorf("FooB.Decode() got = %v, want %v", a, c)
				}
			}
		})
	}
}

func TestFooB_Decode(t *testing.T) {
	type fields struct {
		S *String
	}
	type args struct {
		input byteparser.DecoderArgument
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{"B of empty", fields{S: NewString("")}, args{byteparser.Hexstring("00000000" + "")}, false},
		{`B of "deadbeef"`, fields{S: NewString("deadbeef")}, args{byteparser.Hexstring("00000004" + "deadbeef")}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := FooB{S: new(String)}
			if err := b.Decode(tt.args.input); (err != nil) != tt.wantErr {
				t.Errorf("FooB.Decode() error = %v, wantErr %v", err, tt.wantErr)
			} else if err == nil {
				c := FooB{S: tt.fields.S}
				if !reflect.DeepEqual(b, c) {
					t.Errorf("FooB.Decode() got = %v, want %v", b, c)
				}
			}
		})
	}
}

func TestFooADT_Encode(t *testing.T) {
	type fields struct {
		val FooVariant
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{"A of 0", fields{val: FooA{I: NewInt32(0)}}, "00" + "00000000"},
		{"A of 0x2a", fields{val: FooA{I: NewInt32(0x2a)}}, "00" + "0000002a"},
		{"A of 0xffff", fields{val: FooA{I: NewInt32(0xffff)}}, "00" + "0000ffff"},
		{"B of empty", fields{val: FooB{S: NewString("")}}, "01" + "00000000" + ""},
		{`B of "deadbeef"`, fields{val: FooB{S: NewString("deadbeef")}}, "01" + "00000004" + "deadbeef"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := FooADT{
				val: tt.fields.val,
			}
			if got := v.Encode(); got != tt.want {
				t.Errorf("FooADT.Encode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFooADT_Decode(t *testing.T) {
	type fields struct {
		val FooVariant
	}
	type args struct {
		input byteparser.DecoderArgument
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{"A of 0", fields{val: FooA{I: NewInt32(0)}}, args{byteparser.Hexstring("00" + "00000000")}, false},
		{"A of 0x2a", fields{val: FooA{I: NewInt32(0x2a)}}, args{byteparser.Hexstring("00" + "0000002a")}, false},
		{"A of 0xffff", fields{val: FooA{I: NewInt32(0xffff)}}, args{byteparser.Hexstring("00" + "0000ffff")}, false},
		{"B of empty", fields{val: FooB{S: NewString("")}}, args{byteparser.Hexstring("01" + "00000000" + "")}, false},
		{`B of "deadbeef"`, fields{val: FooB{S: NewString("deadbeef")}}, args{byteparser.Hexstring("01" + "00000004" + "deadbeef")}, false},
		{`? of anything`, fields{}, args{byteparser.Hexstring("02" + "face")}, true},
		{`A of tooshort`, fields{}, args{byteparser.Hexstring("00" + "face")}, true},
		{`B of tooshort`, fields{}, args{byteparser.Hexstring("01" + "00000004" + "2a")}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &FooADT{}
			if err := v.Decode(tt.args.input); (err != nil) != tt.wantErr {
				t.Errorf("FooADT.Decode() error = %v, wantErr %v", err, tt.wantErr)
			} else if err == nil {
				w := &FooADT{val: tt.fields.val}
				if !reflect.DeepEqual(v, w) {
					t.Errorf("FooADT.Decode() got = %v, want %v", v, w)

				}
			}

		})
	}
}
