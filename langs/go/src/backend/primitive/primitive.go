package primitive

import (
	"backend/byteparser"
	"fmt"
)

type Decoder interface {
	Decode(byteparser.DecoderArgument) error
}

type Encoder interface {
	Encode() string
}

type Transcoder interface {
	Encoder
	Decoder
}

type Width uint8

const (
	WidthUint8 Width = 1 - iota
	WidthUint30
)

type Constrainer interface {
	Constrain(*byteparser.T) error
	Represent(uint) string
}

type Suffix uint

func (w Width) Constrain(p *byteparser.T) error {
	var length uint
	if p == nil {
		return fmt.Errorf("cannot constrain nil *byteparser.T")
	}
	switch w {
	case WidthUint8:
		length8, err := p.GetUint8()
		if err != nil {
			return err
		}
		length = uint(length8)
	case WidthUint30:
		length30, err := p.GetUint32()
		if err != nil {
			return err
		}
		length = uint(length30)
	}
	return p.SetFit(length)
}

func (w Width) Represent(bc uint) string {
	switch w {
	case WidthUint30:
		if bc >= 1<<30 {
			panic(fmt.Errorf("sequence byte-length %v exceeds max Uint30", bc))
		}
		return Uint32(bc).Encode()
	case WidthUint8:
		if bc >= 1<<8 {
			panic(fmt.Errorf("sequence byte-length %v exceeds max Uint8", bc))
		}
		return Uint8(bc).Encode()
	default:
		panic(fmt.Errorf("unrecognized Width value %v", w))
	}
}

func (s Suffix) Constrain(p *byteparser.T) error {
	if p == nil {
		return fmt.Errorf("cannot constrain nil *byteparser.T")
	}
	return p.TightenFit(uint(s))
}

func (Suffix) Represent(_ uint) string {
	return ""
}

func EncodePadding(amount uint) string {
	switch amount {
	case 0:
		return ""
	default:
		return fmt.Sprintf("%0"+fmt.Sprintf("%d", 2*amount)+"d", 0)
	}
}

func DecodePadding(amount uint, input byteparser.DecoderArgument) error {
	p, err := input.Parse()
	if err != nil {
		return err
	}
	return p.SkipPadding(amount)
}
