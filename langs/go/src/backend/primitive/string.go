package primitive

import (
	"backend/byteparser"
	"encoding/hex"
	"fmt"
)

type String string

func (s String) Encode() string {
	str := string(s)
	if _, err := hex.DecodeString(str); err != nil {
		panic(err)
	}
	return str
}

func (s *String) Decode(input byteparser.DecoderArgument) error {
	if s == nil {
		return fmt.Errorf("cannot decode into nil pointer of type %T", s)
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	bytes, err := p.GetDynamicString()
	if err != nil {
		return err
	}
	*s = String(hex.EncodeToString(bytes))
	return nil
}

func NewString(s string) *String {
	ret := new(String)
	*ret = String(s)
	return ret
}