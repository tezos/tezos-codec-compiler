package primitive

import (
	"backend/byteparser"
	"reflect"
	"testing"
)

func TestEncodeOption(t *testing.T) {
	type args struct {
		v *Transcoder
	}
	var a Transcoder = NewBool(false)
	var b Transcoder = NewBool(true)

	tests := []struct {
		name string
		args args
		want string
	}{
		{"none", args{v: nil}, "00"},
		{"some false", args{v: &a}, "ff00"},
		{"some true", args{v: &b}, "ffff"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EncodeOption(tt.args.v); got != tt.want {
				t.Errorf("EncodeOption() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecodeOption(t *testing.T) {
	type args struct {
		gen   func() Transcoder
		input byteparser.DecoderArgument
	}

	var a Transcoder = NewBool(false)
	var b Transcoder = NewBool(true)

	tests := []struct {
		name    string
		args    args
		want    *Transcoder
		wantErr bool
	}{
		{"none", args{gen: func() Transcoder { return new(Bool) }, input: byteparser.Hexstring("00")}, nil, false},
		{"some false", args{gen: func() Transcoder { return new(Bool) }, input: byteparser.Hexstring("ff00")}, &a, false},
		{"some true", args{gen: func() Transcoder { return new(Bool) }, input: byteparser.Hexstring("ffff")}, &b, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecodeOption(tt.args.gen, tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodeOption() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecodeOption() = %v, want %v", got, tt.want)
			}
		})
	}
}
