package primitive

import (
	"backend/byteparser"
	"fmt"
)

type Bool bool

func (b Bool) Encode() string {
	if b {
		return "ff"
	}
	return "00"
}


func (b *Bool) Decode(input byteparser.DecoderArgument) error {
	if b == nil {
		return fmt.Errorf("cannot decode into nil pointer of type %T", b)
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	ret, err := p.GetBoolean()
	if err != nil {
		return err
	}
	*b = Bool(ret)
	return nil
}

func NewBool(b bool) *Bool {
	ret := new(Bool)
	*ret = Bool(b)
	return ret
}