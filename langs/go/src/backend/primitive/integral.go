package primitive

import (
	"backend/byteparser"
	"encoding/binary"
	"encoding/hex"
	"fmt"
)

type Int8 int8
type Int16 int16
type Int32 int32
type Int64 int64
type Uint8 uint8
type Uint16 uint16
type Uint32 uint32
type Uint64 uint64

func (i Int8) Encode() string {
	return Uint8(i).Encode()
}

func (u Uint8) Encode() string {
	return hex.EncodeToString([]byte{byte(u)})
}

func (i Int16) Encode() string {
	return Uint16(i).Encode()
}

func (u Uint16) Encode() string {
	buf := make([]byte, 2)
	binary.BigEndian.PutUint16(buf, uint16(u))
	return hex.EncodeToString(buf)
}

func (i Int32) Encode() string {
	return Uint32(i).Encode()
}

func (u Uint32) Encode() string {
	buf := make([]byte, 4)
	binary.BigEndian.PutUint32(buf, uint32(u))
	return hex.EncodeToString(buf)
}

func (i Int64) Encode() string {
	return Uint64(i).Encode()
}

func (u Uint64) Encode() string {
	buf := make([]byte, 8)
	binary.BigEndian.PutUint64(buf, uint64(u))
	return hex.EncodeToString(buf)
}

func (v *Int8) Decode(input byteparser.DecoderArgument) error {
	if v == nil {
		return fmt.Errorf("cannot decode into nil pointer of type %T", v)
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	ret, err := p.GetInt8()
	if err != nil {
		return err
	}
	*v = Int8(ret)
	return nil
}

func (v *Uint8) Decode(input byteparser.DecoderArgument) error {
	if v == nil {
		return fmt.Errorf("cannot decode into nil pointer of type %T", v)
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	ret, err := p.GetUint8()
	if err != nil {
		return err
	}
	*v = Uint8(ret)
	return nil
}

func (v *Int16) Decode(input byteparser.DecoderArgument) error {
	if v == nil {
		return fmt.Errorf("cannot decode into nil pointer of type %T", v)
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	ret, err := p.GetInt16()
	if err != nil {
		return err
	}
	*v = Int16(ret)
	return nil
}

func (v *Uint16) Decode(input byteparser.DecoderArgument) error {
	if v == nil {
		return fmt.Errorf("cannot decode into nil pointer of type %T", v)
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	ret, err := p.GetUint16()
	if err != nil {
		return err
	}
	*v = Uint16(ret)
	return nil
}

func (v *Int32) Decode(input byteparser.DecoderArgument) error {
	if v == nil {
		return fmt.Errorf("cannot decode into nil pointer of type %T", v)
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	ret, err := p.GetInt32()
	if err != nil {
		return err
	}
	*v = Int32(ret)
	return nil
}

func (v *Uint32) Decode(input byteparser.DecoderArgument) error {
	if v == nil {
		return fmt.Errorf("cannot decode into nil pointer of type %T", v)
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	ret, err := p.GetUint32()
	if err != nil {
		return err
	}
	*v = Uint32(ret)
	return nil
}

func (v *Int64) Decode(input byteparser.DecoderArgument) error {
	if v == nil {
		return fmt.Errorf("cannot decode into nil pointer of type %T", v)
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	ret, err := p.GetInt64()
	if err != nil {
		return err
	}
	*v = Int64(ret)
	return nil
}

func (v *Uint64) Decode(input byteparser.DecoderArgument) error {
	if v == nil {
		return fmt.Errorf("cannot decode into nil pointer of type %T", v)
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	ret, err := p.GetUint64()
	if err != nil {
		return err
	}
	*v = Uint64(ret)
	return nil
}

func NewInt8(i int8) *Int8 {
	ret := new(Int8)
	*ret = Int8(i)
	return ret
}

func NewInt16(i int16) *Int16 {
	ret := new(Int16)
	*ret = Int16(i)
	return ret
}

func NewInt32(i int32) *Int32 {
	ret := new(Int32)
	*ret = Int32(i)
	return ret
}

func NewInt64(i int64) *Int64 {
	ret := new(Int64)
	*ret = Int64(i)
	return ret
}

func NewUint8(i uint8) *Uint8 {
	ret := new(Uint8)
	*ret = Uint8(i)
	return ret
}

func NewUint16(i uint16) *Uint16 {
	ret := new(Uint16)
	*ret = Uint16(i)
	return ret
}

func NewUint32(i uint32) *Uint32 {
	ret := new(Uint32)
	*ret = Uint32(i)
	return ret
}

func NewUint64(i uint64) *Uint64 {
	ret := new(Uint64)
	*ret = Uint64(i)
	return ret
}

