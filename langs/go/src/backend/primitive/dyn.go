package primitive

import (
	"backend/byteparser"
	"fmt"
)

func EncodeDyn(dyn Constrainer, v Transcoder) string {
	payload := v.Encode()
	l := uint(len(payload) / 2)
	return dyn.Represent(l) + payload
}

func DecodeDyn(dyn Constrainer, gen func () Transcoder, input byteparser.DecoderArgument) (Transcoder, error) {
	p, err := input.Parse()
	if err != nil {
		return nil, err
	}
	if err := dyn.Constrain(p); err != nil {
		return nil, fmt.Errorf("DecodeDyn unable to constrain: %v", err);
	}
	ret := gen()
	if err := ret.Decode(p); err != nil {
		return nil, fmt.Errorf("DecodeDyn unable to decode: %v", err);
	}
	return ret, nil
}