package primitive

import (
	"backend/byteparser"
	"fmt"
)

type FooTag Uint8

const (
	FooATag FooTag = iota
	FooBTag
)

func (i FooTag) Encode() string {
	switch i {
	case FooATag, FooBTag: 
		return Uint8(i).Encode()
	default:
		panic(fmt.Errorf("illegal enum value for FooTag: %v", i))
	}
}

func (i *FooTag) Decode(input byteparser.DecoderArgument) error {
	ret := new(Uint8)
	if err := ret.Decode(input); err != nil {
		return err
	}
	switch FooTag(*ret) {
	case FooATag, FooBTag:
		*i = FooTag(*ret)
		return nil
	default:
		return fmt.Errorf("illegal enum value for FooTag: %v", *ret)
	}
}

type FooVariant interface {
	Tag() FooTag
	Payload() Transcoder
}

type FooADT struct {
	val FooVariant
}

func MakeFooADT() FooADT {
	return FooADT{};
}

type FooA struct {
	I *Int32
}

func MakeFooA() FooA {
	return FooA{new(Int32)}
}
type FooB struct {
	S *String
}

func MakeFooB() FooB {
	return FooB{new(String)}
}


func (FooA) Tag() FooTag { return FooATag }
func (FooB) Tag() FooTag { return FooBTag }

func (x FooA) Payload() Transcoder { return x }
func (y FooB) Payload() Transcoder { return y }

func (a FooA) Encode() string {
	return a.I.Encode()
}

func (b FooB) Encode() string {
	return EncodeDyn(WidthUint30, b.S)
}

func (a FooA) Decode(input byteparser.DecoderArgument) error {
	return a.I.Decode(input)
}

func (b FooB) Decode(input byteparser.DecoderArgument) error {
	ret, err := DecodeDyn(WidthUint30, func() Transcoder{return new(String)}, input)
	if err != nil {
		return err
	}
	*(b.S) = *(ret.(*String))
	return nil
}

func (v FooADT) Encode() string {
	return v.val.Tag().Encode() + v.val.Payload().Encode()
}

func (v *FooADT) Decode(input byteparser.DecoderArgument) error {
	if v == nil {
		return fmt.Errorf("cannot decode nil ADT pointer")
	}
	p, err := input.Parse()
	if err != nil {
		return err
	}
	ptag := new(FooTag)
	if err := ptag.Decode(p); err != nil {
		return err
	}
	switch *ptag {
	case FooATag:
		a := MakeFooA()
		err := a.Decode(p)
		if err != nil {
			return err
		}
		v.val = a
		return nil
	case FooBTag:
		b := MakeFooB()
		err := b.Decode(p)
		if err != nil {
			return err
		}
		v.val = b
		return nil
	default:
		return fmt.Errorf("invalid tag %v for FooADT", *ptag)
	}
}
