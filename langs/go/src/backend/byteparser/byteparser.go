package byteparser

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"math"

	"backend/util"
)

type T struct {
	arr []byte;
	offset uint;
	length uint;
	goals *util.Uintstack;
}

type Hexstring string

type Parser interface {
	Parse() (*T, error)
}

type UnsafeParser interface {
	UnsafeParse() *T
}

type DecoderArgument interface {
	Parser
	UnsafeParser
}

func (p *T) Parse() (*T, error) {
	return p, nil;
}

func (p *T) UnsafeParse() *T {
	return p
}

func (s Hexstring) Parse() (*T, error) {
	arr, err := hex.DecodeString(string(s));

	var offset, length uint;	
	offset = 0;
	goals := util.New();

	if err != nil {
		length = 0;
	} else {
		length = uint(len(arr));
	}
	return &T{ arr, offset, length, goals }, err
}

func (s Hexstring) UnsafeParse() *T {
	if t, err := s.Parse(); err != nil {
		panic(err.Error())
	} else {
		return t
	}
}

func (p *T) consume(nbytes uint) ([]byte, error) {
	if p.offset + nbytes > p.length {
		return []byte{}, fmt.Errorf("cannot consume past end of buffer");
	}
	current_goal, err := p.goals.Peek();
	if err == nil && p.offset + nbytes > current_goal {
		return []byte{}, fmt.Errorf("cannot consume beyond the most recently set target offset");
	}

	ret := p.arr[p.offset:p.offset+nbytes];
	p.offset += nbytes;
	return ret, nil
}

func (p *T) TestGoal() (bool, error) {
	current_goal, err := p.goals.Peek();

	if err != nil {
		return false, fmt.Errorf("no target offset to check against")	
	} else if p.offset > current_goal {
		return false, fmt.Errorf("invariant failed: offset managed to exceed its target")
	}
	return (p.offset == current_goal), nil;
}

func (p *T) EnforceGoal() (ret error) {
	current_goal, err := p.goals.Peek();

	switch {
	case err != nil:
		ret = fmt.Errorf("no target offset to enforce");
	case p.offset > current_goal:
		ret = fmt.Errorf("invariant failed: offset managed to exceed its target");
	case p.offset < current_goal:
		ret = fmt.Errorf("assertion failed: offset failed to reach target");
	default:
		p.goals.Pop();
		ret = nil;
	}
	return
}

func (p *T) SetFit(width uint) error {
	if width == 0 {
		p.goals.Push(p.offset);
		return nil;
	}

	limit := p.goals.Peek_def(p.length);

	if p.offset + width <= limit {
		p.goals.Push(p.offset + width);
		return nil;
	}

	if p.goals.Size() == 0 {
		return fmt.Errorf("cannot set goal past end of buffer")
	} else {
		return fmt.Errorf("cannot set goal outside of current context window")
	}
}

func (p *T) TightenFit(delta uint) error {
	limit := p.goals.Peek_def(p.length);

	if p.offset + delta <= limit {
		p.goals.Push(limit - delta);
		return nil;
	} else {
		return fmt.Errorf("target offset delta wider than remaining context window")
	}
}

func (p *T) Length() uint {
	limit := p.goals.Peek_def(p.length);

	return uint(limit - p.offset);
}

func (p *T) GetUint8() (uint8, error) {
	arr, err := p.consume(1);
	if err != nil {
		return 0, err
	} else {
		return uint8(arr[0]), nil;
	}
}

func (p *T) GetInt8() (int8, error) {
	arr, err := p.consume(1);
	if err != nil {
		return 0, err
	} else {
		return int8(arr[0]), nil;
	}
}

func (p *T) GetUint16() (uint16, error) {
	arr, err := p.consume(2);
	if err != nil {
		return 0, err
	} else {
		return binary.BigEndian.Uint16(arr), nil;
	}
}

func (p *T) GetInt16() (int16, error) {
	arr, err := p.consume(2);
	if err != nil {
		return 0, err
	} else {
		return int16(binary.BigEndian.Uint16(arr)), nil;
	}
}

func (p *T) GetUint32() (uint32, error) {
	arr, err := p.consume(4);
	if err != nil {
		return 0, err
	} else {
		return binary.BigEndian.Uint32(arr), nil;
	}
}

func (p *T) GetInt32() (int32, error) {
	arr, err := p.consume(4);
	if err != nil {
		return 0, err
	} else {
		return int32(binary.BigEndian.Uint32(arr)), nil;
	}
}

func (p *T) GetUint64() (uint64, error) {
	arr, err := p.consume(8);
	if err != nil {
		return 0, err
	} else {
		return binary.BigEndian.Uint64(arr), nil;
	}
}

func (p *T) GetInt64() (int64, error) {
	arr, err := p.consume(8);
	if err != nil {
		return 0, err
	} else {
		return int64(binary.BigEndian.Uint64(arr)), nil;
	}
}

func (p *T) GetFloat32() (float32, error) {
	arr, err := p.consume(4);
	if err != nil {
		return 0, err
	} else {
		return math.Float32frombits(binary.BigEndian.Uint32(arr)), nil;
	}
}

func (p *T) GetFloat64() (float64, error) {
	arr, err := p.consume(8);
	if err != nil {
		return 0, err
	} else {
		return math.Float64frombits(binary.BigEndian.Uint64(arr)), nil;
	}
}

func (p *T) GetBoolean() (bool, error) {
	arr, err := p.consume(1);
	if err != nil {
		return false, err;
	}
	switch arr[0] {
	case 0x00: return false, nil;
	case 0xff: return true, nil;
	default:
		return false, fmt.Errorf("expected boolean value (i.e. either 0xff or 0x00), found %02x", arr[0]);
	}
}

func (p *T) GetDynamicString(args ...uint) ([]byte, error) {
	var length uint;
	switch len(args) {
	case 0:
		length = p.Length();
	case 1:
		length = args[0];
	default:
		return []byte{}, fmt.Errorf("too many arguments: expected 0 or 1");
	}
	return p.consume(length);
}

func (p *T) SkipPadding(length uint) error {
	_, err := p.consume(length);	
	return err;
}

func (p *T) seekTo(ix uint) (byte, error) {
	if p.offset + ix >= p.length {
		return 0x00, fmt.Errorf("cannot seek past end of buffer")
	}

	currentGoal := p.goals.Peek_def(p.length)

	if p.offset + ix >= currentGoal {
		return 0x00, fmt.Errorf("cannot seek beyond the most recently set target offset")
	}

	return p.arr[p.offset + ix], nil
}

func (p *T) GetSelfTerminating(is_final func(byte) bool) ([]byte, error) {
	var nbytes uint = 0;

	for {
		next, err := p.seekTo(nbytes);
		if err != nil {
			return []byte{}, err;
		}
		nbytes++;
		if is_final(next) {
			break
		}
	}

	return p.consume(nbytes);
}