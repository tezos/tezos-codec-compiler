module backend/byteparser

go 1.16

replace backend/util => ../util

require (
	backend/util v0.0.0-00010101000000-000000000000
	github.com/stretchr/testify v1.7.0
)
