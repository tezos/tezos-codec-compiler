package byteparser_test

import (
	"testing"

	"backend/byteparser"

	"github.com/stretchr/testify/assert"
)

func parse(t *testing.T, s string) *byteparser.T {
	p, err := byteparser.Hexstring(s).Parse();
	assert.Nil(t, err);
	return p;
}

func TestParse(t *testing.T) {
	// enforces length parity
	_, err := byteparser.Hexstring("abcde").Parse();
	assert.NotNil(t, err)

	// enforces alphabet
	_, err = byteparser.Hexstring("nonsense").Parse();
	assert.NotNil(t, err)

	// case-insentivity
	lower := parse(t, "abcdef0a1b2c");
	upper := parse(t, "ABCDEF0A1B2C");
	mixed := parse(t, "AbCdEf0A1b2C");

	for _, width := range [3]uint{1,2,3} {
		x, errx := lower.GetDynamicString(width);
		y, erry := upper.GetDynamicString(width);
		z, errz := mixed.GetDynamicString(width);
		assert.Nil(t, errx); 
		assert.Nil(t, erry); 
		assert.Nil(t, errz); 

		assert.Equal(t, x, y);
		assert.Equal(t, y, z);
	}
}

func TestGetUint8(t *testing.T) {
	p := parse(t, "010280ff");
	assert.Equal(t,  uint(4), p.Length());
	var expected = []uint8{0x01,0x02,0x80,0xff}
	for _, exp := range expected {
		x, err := p.GetUint8();
		assert.Nil(t, err);
		assert.Equal(t, exp, x);
	}
}

func TestGetInt8(t *testing.T) {
	p := parse(t, "010280ff");
	assert.Equal(t, uint(4), p.Length());
	var expected = []int8{0x01,0x02,-0x80,-0x01};
	for _, exp := range expected {
		x, err := p.GetInt8();
		assert.Nil(t, err);
		assert.Equal(t, exp, x);
	}
}

func TestGetUint16(t *testing.T) {
	p := parse(t, "000100028000ffff");
	assert.Equal(t, uint(8), p.Length());
	var expected = []uint16{ 0x0001, 0x0002, 0x8000, 0xffff};
	for _, exp := range expected {
		x, err := p.GetUint16();
		assert.Nil(t, err);
		assert.Equal(t, exp, x);
	}
}

func TestGetInt16(t *testing.T) {
	p := parse(t, "000100028000ffff");
	assert.Equal(t, uint(8), p.Length());
	var expected = []int16{0x0001, 0x0002, -0x8000, -0x0001}
	for _, exp := range expected {
		x, err := p.GetInt16();
		assert.Nil(t, err);
		assert.Equal(t, exp, x);
	}
}

func TestGetUint32(t *testing.T) {
	p := parse(t, "00000001" + "00000002" + "80000000" + "ffffffff");
	assert.Equal(t, uint(16), p.Length());
	var expected = []uint32{1, 2, (1 << 31), 0xffff_ffff};
	for _, exp := range expected {
		x, err := p.GetUint32();
		assert.Nil(t, err);
		assert.Equal(t, exp, x);
	}
}

func TestGetInt32(t *testing.T) {
	p := parse(t, "00000001" + "00000002" + "80000000" + "ffffffff");
	assert.Equal(t, uint(16), p.Length());
	var expected = []int32{1, 2, -(1 << 31), -1};
	for _, exp := range expected {
		x, err := p.GetInt32();
		assert.Nil(t, err);
		assert.Equal(t, x, exp);
	}
}

func TestGetUint64(t *testing.T) {
	p := parse(t, 
		"0000000000000001" +
		"0000000000000002" +
		"8000000000000000" +
		"ffffffffffffffff");
	assert.Equal(t, p.Length(), uint(32));
	var expected = []uint64{1, 2, (1 << 63), 0xffff_ffff_ffff_ffff};
	for _, exp := range expected {
		x, err := p.GetUint64();
		if err != nil {
			t.Error(err);
		}
		assert.Equal(t, x, exp);
	}
}


func TestGetInt64(t *testing.T) {
	p := parse(t, 
		"0000000000000001" +
		"0000000000000002" +
		"8000000000000000" +
		"ffffffffffffffff");
	assert.Equal(t, uint(32), p.Length());
	var expected = []int64{1, 2, -(1 << 63), -1};
	for _, exp := range expected {
		x, err := p.GetInt64();
		assert.Nil(t, err);
		assert.Equal(t, x, exp);
	}
}

func TestGetBoolean(t *testing.T) {
	p := parse(t, "00ffabcd");
	assert.Equal(t, uint(4), p.Length());
	var cases = []struct { expected, isError bool }{
		{ expected: false, isError: false },
		{ expected: true, isError: false },
		{ expected: false, isError: true },
		{ expected: false, isError: true },
	};
	for _, c := range cases {
		x, err := p.GetBoolean();
		assert.Equal(t, c.isError, (err != nil));
		assert.Equal(t, x, c.expected);
	}
}

func TestSkipPadding(t *testing.T) {
	assert := assert.New(t);
	p := parse(t, "00000001000000");
	x, err := p.GetInt32();
	assert.Nil(err);
	assert.Equal(x, int32(1));
	assert.NotNil(p.SkipPadding(4));
	assert.Nil(p.SkipPadding(1));
	assert.Nil(p.SkipPadding(1));
	assert.Nil(p.SkipPadding(1));
	assert.NotNil(p.SkipPadding(1));
}

func TestGetSelfTerminating(t *testing.T) {
	assert := assert.New(t);
	p := parse(t, "00000000");
	x, err := p.GetSelfTerminating(func(_ byte) bool {return true;});
	assert.Nil(err);
	assert.Equal([]byte{0x00}, x);
	assert.Equal(uint(3), p.Length());

	p, err = byteparser.Hexstring("00000000").Parse();
	assert.Nil(err);
	_, err = p.GetSelfTerminating(func(_ byte) bool {return false;});
	assert.NotNil(err);
	p.SetFit(2);
	_, err = p.GetSelfTerminating(func(_ byte) bool {return false;});
	assert.NotNil(err);


	p, err = byteparser.Hexstring("808f810c00").Parse();
	assert.Nil(err);
	f := func(b byte) bool { return (b & 0x80) == 0 };
	x, err = p.GetSelfTerminating(f);
	assert.Nil(err);
	assert.Equal([]byte{0x80, 0x8f, 0x81, 0x0c}, x);
	x, err = p.GetSelfTerminating(f);
	assert.Nil(err);
	assert.Equal([]byte{0x00}, x);
}

func TestLength(t *testing.T) {
	for i := uint(1); i < 4; i++ {
		p := parse(t, "01020304");
		p.GetDynamicString(i);
		assert.Equal(t, 4 - i, p.Length());
	}

	for i := uint(1); i <= 8; i++ {
		for j := i; j <= 8; j++ {
			p := parse(t, "0102030401020304");
			err := p.SetFit(j);
			assert.Nil(t, err);
			_, err = p.GetDynamicString(i);
			assert.Nil(t, err);
			assert.Equal(t, j - i, p.Length())
		}
	}

	p := parse(t, "0102030401020304");
	for i := uint(0); i <= 8; i++ {
		assert.Nil(t, p.SetFit(8 - i));
		assert.Equal(t, 8 - i, p.Length());
	}
	for i := uint(0); i < 8; i++ {
		var avail uint;
		if i == uint(0) { avail = 0 } else { avail = 1 }
		met, err := p.TestGoal();
		assert.Nil(t, err);
		assert.Equal(t, (i==0), met);
		x, err := p.GetDynamicString(avail);
		assert.Nil(t, err);
		assert.Equal(t, avail, uint(len(x)));
		met, err = p.TestGoal();
		assert.Nil(t, err);
		assert.Equal(t, true, met);
		assert.Nil(t, p.EnforceGoal());
	}
	met, err := p.TestGoal();
	assert.Nil(t, err);
	assert.Equal(t, false, met);
	assert.Equal(t, uint(1), p.Length());
	_, err = p.GetDynamicString(1);
	assert.Nil(t, err);
	met, err = p.TestGoal();
	assert.Nil(t, err);
	assert.Equal(t, true, met);
	assert.Nil(t, p.EnforceGoal())
	assert.Equal(t, uint(0), p.Length());
	_, err = p.TestGoal();
	assert.NotNil(t, err);
	assert.NotNil(t, p.EnforceGoal());
}

func TestClosure(t *testing.T) {
	f := func(p *byteparser.T) (uint8, error) {
		return p.GetUint8();
	}

	p := parse(t, "01020304");
	for i := uint8(1); i <= 4; i++ {
		x, err := f(p);
		assert.Nil(t, err);
		assert.Equal(t, i, x);
		p, _ = p.Parse();
	}
}

func TestContextLimit(t *testing.T) {
	// cannot consume past target offset
	p := parse(t, "00000001");
	p.SetFit(3);
	_, err := p.GetDynamicString(4);
	assert.NotNil(t, err);
	_, err = p.GetInt32();
	assert.NotNil(t, err);
}

func TestContextStrict(t *testing.T) {
	p := parse(t, "00000001");
	assert.NotNil(t, p.SetFit(100));
	assert.Nil(t, p.SetFit(3));
	assert.NotNil(t, p.SetFit(4));
	_, err := p.GetDynamicString(2);
	assert.Nil(t, err);
	assert.NotNil(t, p.SetFit(2));
}

func TestContextCorrect(t *testing.T) {
	p := parse(t, "00000001" + "00000002");
	_, err := p.TestGoal();
	assert.NotNil(t, err);
	assert.NotNil(t, p.EnforceGoal());
	assert.Nil(t, p.SetFit(8));
	assert.Nil(t, p.SetFit(4));
	assert.Nil(t, p.SetFit(0));
	met, err := p.TestGoal();
	assert.Nil(t, err);
	assert.Equal(t, true, met);
	assert.Nil(t, p.EnforceGoal());

	f := func(p *byteparser.T) {
		for i := uint(0); i < 4; i++ {
			met, err := p.TestGoal();
			assert.Nil(t, err);
			assert.Equal(t, false, met);
			assert.NotNil(t, p.EnforceGoal());
			_, err = p.GetDynamicString(1);
			assert.Nil(t, err);
		}
		met, err := p.TestGoal();
		assert.Nil(t, err);
		assert.Equal(t, true, met);
		assert.Nil(t, p.EnforceGoal());
	}

	f(p);
	f(p);
	_, err = p.TestGoal();
	assert.NotNil(t, err);
	assert.NotNil(t, p.EnforceGoal());
}


func TestContextTightening(t *testing.T) {
	assert := assert.New(t);
	p := parse(t, "00000001");
	var new_fit uint = 3;
	assert.NotNil(p.TightenFit(8));
	assert.Nil(p.SetFit(new_fit));
	assert.NotNil(p.TightenFit(4));
	for i := uint(0); i < new_fit; i++ {
		assert.Nil(p.TightenFit(1));
	}
	assert.NotNil(p.TightenFit(1));

	p = parse(t, "00000001");
	assert.Nil(p.TightenFit(1));
	_, err := p.GetDynamicString(2);
	assert.Nil(err);
	assert.NotNil(p.TightenFit(2));
}