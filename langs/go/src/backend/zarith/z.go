package zarith

import (
	"backend/byteparser"
	"encoding/hex"
	"fmt"
	"math"
	"math/big"
)

type Z struct {
	*big.Int
}

func ReadZ(b []byte) (Z, error) {
	z := big.NewInt(0)
	var bits uint = 0
	var is_negative bool

	if len(b) == 0 {
		return Z{}, fmt.Errorf("cannot parse empty byte-array as Zarith integral")
	}

	for index, octet := range b {
		var val uint8
		if index == 0 {
			is_negative = (octet & 0x40) != 0
			val = octet & 0x3f
			shifted := big.NewInt(int64(val))
			shifted.Lsh(shifted, bits)
			z.Or(z, shifted)
			bits += 6
		} else {
			val = octet & 0x7f
			shifted := big.NewInt(int64(val))
			shifted.Lsh(shifted, bits)
			z.Or(z, shifted)
			bits += 7
			if octet == 0 {
				return Z{}, fmt.Errorf("unexpected trailing zero byte in Zarith integer byte-array")
			}
		}
	}

	if is_negative {
		z.Neg(z)
	}
	return Z{z}, nil
}

func (z Z) BigInt() *big.Int {
	return z.Int
}

func (z Z) Serialize() []byte {
	nn := big.NewInt(0)
	nn.Abs(z.Int)
	n_bits := len(nn.Text(2))
	n_bytes := int(math.Ceil(float64(n_bits-6)/7.)) + 1
	buf := make([]byte, n_bytes)
	i := 0
	last := false
	var sign byte
	if z.Sign() < 0 {
		sign = 0x40
	} else {
		sign = 0x00
	}
	var octet byte
	swap := big.NewInt(0)

	swap.SetUint64(0x3f)
	swap.And(nn, swap)
	octet = byte(swap.Uint64())
	octet |= sign
	nn.Rsh(nn, 6)
	last = nn.Sign() == 0
	if !last {
		octet |= 0x80
	}
	buf[i] = octet
	i++

	for !last {
		swap.SetUint64(0x7f)
		swap.And(nn, swap)
		octet = byte(swap.Uint64())
		nn.Rsh(nn, 7)
		last = nn.Sign() == 0
		if !last {
			octet |= 0x80
		}
		buf[i] = octet
		i++
	}

	return buf
}

func DecimalStringToZ(dec string) (Z, error) {
	val := big.NewInt(0)
	_, ok := val.SetString(dec, 10)
	if !ok {
		return Z{}, fmt.Errorf("could not parse input string as decimal integer: %s", dec)
	}
	return NewZ(val)
}

func (z Z) Encode() string {
	return hex.EncodeToString(z.Serialize())
}

func DecodeZ(input byteparser.DecoderArgument) (Z, error) {
	p, err := input.Parse()
	if err != nil {
		return Z{}, err
	}
	bytes, err := p.GetSelfTerminating(zarith_pred)
	if err != nil {
		return Z{}, err
	}
	return ReadZ(bytes)
}


func (z *Z) Decode(input byteparser.DecoderArgument) error {
	zed, err := DecodeZ(input)
	if err != nil {
		return err
	}
	*z = zed
	return nil
}

func I64toZ(i int64) Z {
	val := big.NewInt(0)
	val.SetInt64(i)
	return Z{val}
}

func NewZ(val *big.Int) (Z, error) {
	switch {
	case val == nil:
		return Z{}, fmt.Errorf("cannot construct Zarith natural from nil pointer")
	default:
		return Z{val}, nil
	}
}
