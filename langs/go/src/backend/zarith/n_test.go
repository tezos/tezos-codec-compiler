package zarith

import (
	"backend/byteparser"
	"math/big"
	"reflect"
	"testing"
)

func TestReadN(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name    string
		args    args
		want    uint64
		wantErr bool
	}{
		{"0", args{[]byte{0x00}}, 0, false},
		{"1", args{[]byte{0x01}}, 1, false},
		{"0x41", args{[]byte{0x41}}, 0x41, false},
		{"0x80", args{[]byte{0x80, 0x01}}, 128, false},
		{"0xc0", args{[]byte{0xc0, 0x01}}, 192, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadN(tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadN() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			u := got.BigInt().Uint64()
			if !reflect.DeepEqual(u, tt.want) {
				t.Errorf("ReadN() = N{Int{%v}}, want %v", u, tt.want)
			}
		})
	}
}

func TestN_Serialize(t *testing.T) {
	tests := []struct {
		name string
		arg  N
		want []byte
	}{
		{"0", U64toN(0), []byte{0x00}},
		{"1", U64toN(1), []byte{0x01}},
		{"0x41", U64toN(0x41), []byte{0x41}},
		{"0x80", U64toN(128), []byte{0x80, 0x01}},
		{"0xc0", U64toN(192), []byte{0xc0, 0x01}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := tt.arg
			if got := n.Serialize(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("N.Serialize() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecimalStringToN(t *testing.T) {
	type args struct {
		dec string
	}
	tests := []struct {
		name    string
		args    args
		want    uint64
		wantErr bool
	}{
		{"0", args{"0"}, 0, false},
		{"1", args{"1"}, 1, false},
		{"1234567", args{"1234567"}, 1234567, false},
		{"0000001", args{"0000001"}, 1, false},
		{"MAX", args{"18446744073709551615"}, 18446744073709551615, false},
		{"negative 0", args{"-0"}, 0, false},
		{"negative 1", args{"-1"}, 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecimalStringToN(tt.args.dec)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecimalStringToN() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil {
				return
			}
			u := got.BigInt().Uint64()
			if !reflect.DeepEqual(u, tt.want) {
				t.Errorf("DecimalStringToN() = N{Int{%v}}, want %v", u, tt.want)
			}
		})
	}
}

func TestDecodeN(t *testing.T) {
	type args struct {
		input byteparser.DecoderArgument
	}
	tests := []struct {
		name    string
		args    args
		want    N
		wantErr bool
	}{
		{"0", args{byteparser.Hexstring("00")}, N{big.NewInt(0)}, false},
		{"1", args{byteparser.Hexstring("01")}, N{big.NewInt(1)}, false},
		{"1<<56", args{byteparser.Hexstring("808080808080808001")}, N{big.NewInt(1 << 56)}, false},
		{"1<<112", args{byteparser.Hexstring("8080808080808080808080808080808001")}, N{big.NewInt(0).Lsh(big.NewInt(1), 112)}, false},
		{"ends early", args{byteparser.Hexstring("80")}, N{}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecodeN(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodeN() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecodeN() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewN(t *testing.T) {
	type args struct {
		val *big.Int
	}
	tests := []struct {
		name    string
		args    args
		want    N
		wantErr bool
	}{
		{ "0", args{big.NewInt(0)}, N{big.NewInt(0)}, false },
		{ "1", args{big.NewInt(1)}, N{big.NewInt(1)}, false },
		{ "-1", args{big.NewInt(-1)}, N{}, true },
		{ "nil", args{nil}, N{}, true },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewN(tt.args.val)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewN() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewN() = %v, want %v", got, tt.want)
			}
		})
	}
}
