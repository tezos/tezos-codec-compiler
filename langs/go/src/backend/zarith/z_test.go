package zarith

import (
	"backend/byteparser"
	"math/big"
	"reflect"
	"testing"
)

func TestReadZ(t *testing.T) {
	type args struct {
		b []byte
	}
	f := I64toZ
	tests := []struct {
		name    string
		args    args
		want    Z
		wantErr bool
	}{
		{"0", args{[]byte{0x00}}, f(0), false},
		{"1", args{[]byte{0x01}}, f(1), false},
		{"-1", args{[]byte{0x41}}, f(-1), false},
		{"64", args{[]byte{0x80, 0x01}}, f(64), false},
		{"-64", args{[]byte{0xc0, 0x01}}, f(-64), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadZ(tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("Z_of_bytes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Z_of_bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestZ_Serialize(t *testing.T) {
	tests := []struct {
		name string
		arg  Z
		want []byte
	}{
		{"0", I64toZ(0), []byte{0x00}},
		{"1", I64toZ(1), []byte{0x01}},
		{"0x41", I64toZ(-1), []byte{0x41}},
		{"0x80", I64toZ(64), []byte{0x80, 0x01}},
		{"0xc0", I64toZ(-64), []byte{0xc0, 0x01}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			z := tt.arg
			if got := z.Serialize(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Z.Serialize() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecimalStringToZ(t *testing.T) {
	type args struct {
		dec string
	}
	tests := []struct {
		name    string
		args    args
		want    Z
		wantErr bool
	}{
		{"0", args{"0"}, I64toZ(0), false},
		{"1", args{"1"}, I64toZ(1), false},
		{"1234567", args{"1234567"}, I64toZ(1234567), false},
		{"0000001", args{"0000001"}, I64toZ(1), false},
		{"MAX", args{"9223372036854775807"}, I64toZ(9223372036854775807), false},
		{"MIN", args{"-9223372036854775808"}, I64toZ(-9223372036854775808), false},
		{"negative 0", args{"-0"}, I64toZ(0), false},
		{"negative 1", args{"-1"}, I64toZ(-1), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecimalStringToZ(tt.args.dec)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecimalStringToZ() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecimalStringToZ() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecodeZ(t *testing.T) {
	type args struct {
		input byteparser.DecoderArgument
	}
	tests := []struct {
		name    string
		args    args
		want    Z
		wantErr bool
	}{
		{"0", args{byteparser.Hexstring("00")}, Z{big.NewInt(0)}, false},
		{"1", args{byteparser.Hexstring("01")}, Z{big.NewInt(1)}, false},
		{"-1", args{byteparser.Hexstring("41")}, Z{big.NewInt(-1)}, false},
		{"1<<55", args{byteparser.Hexstring("808080808080808001")}, Z{big.NewInt(1 << 55)}, false},
		{"1<<111", args{byteparser.Hexstring("8080808080808080808080808080808001")}, Z{big.NewInt(0).Lsh(big.NewInt(1), 111)}, false},
		{"-1<<55", args{byteparser.Hexstring("c08080808080808001")}, Z{big.NewInt(-1 << 55)}, false},
		{"-1<<112", args{byteparser.Hexstring("c080808080808080808080808080808001")}, Z{big.NewInt(0).Lsh(big.NewInt(-1), 111)}, false},
		{"ends early", args{byteparser.Hexstring("80")}, Z{}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecodeZ(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodeZ() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecodeZ() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewZ(t *testing.T) {
	type args struct {
		val *big.Int
	}
	tests := []struct {
		name    string
		args    args
		want    Z
		wantErr bool
	}{
		{ "0", args{big.NewInt(0)}, Z{big.NewInt(0)}, false },
		{ "1", args{big.NewInt(1)}, Z{big.NewInt(1)}, false },
		{ "-1", args{big.NewInt(-1)}, Z{big.NewInt(-1)}, false },
		{ "nil", args{nil}, Z{}, true },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewZ(tt.args.val)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewZ() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewZ() = %v, want %v", got, tt.want)
			}
		})
	}
}
