package zarith

import "math/big"

type BigInter interface {
	BigInt() *big.Int
}

type Serializer interface {
	Serialize() []byte
}
type Zarith interface {
	BigInter
	Serializer
}

var zarith_pred = func (octet byte) bool { return (octet & 0x80) == 0 }