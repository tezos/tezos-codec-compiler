package zarith

import (
	"backend/byteparser"
	"encoding/hex"
	"fmt"
	"math"
	"math/big"
)

type N struct {
	*big.Int
}

func ReadN(b []byte) (N, error) {
	n := big.NewInt(0)
	var bits uint = 0

	if len(b) == 0 {
		return N{}, fmt.Errorf("cannot parse empty byte-array as Zarith integral")
	}

	for index, octet := range b {
		var val uint8 = octet & 0x7f
		shifted := big.NewInt(int64(val))
		shifted.Lsh(shifted, bits)
		n.Or(n, shifted)
		bits += 7
		if index > 0 && octet == 0 {
			return N{}, fmt.Errorf("unexpected trailing zero byte in Zarith integer byte-array")
		}
	}

	return N{n}, nil
}

func (n N) BigInt() *big.Int {
	return n.Int
}

func (n N) Serialize() []byte {
	n_bits := len(n.Text(2))
	n_bytes := int(math.Ceil(float64(n_bits) / 7.))
	if n_bytes == 0 {
		n_bytes = 1
	}
	buf := make([]byte, n_bytes)
	i := 0

	nn := big.NewInt(0)
	swap := big.NewInt(0)
	nn.Set(n.Int)
	last := false

	for !last {
		swap.SetInt64(0x7f)
		swap.And(nn, swap)
		var octet byte = byte(swap.Uint64())
		nn.Rsh(nn, 7)
		last = nn.Sign() == 0
		if !last {
			octet |= 0x80
		}
		buf[i] = octet
		i++
	}

	return buf
}

func DecimalStringToN(dec string) (N, error) {
	val := big.NewInt(0)
	_, ok := val.SetString(dec, 10)
	if !ok {
		return N{}, fmt.Errorf("could not parse input string as decimal natural: %s", dec)
	}
	return NewN(val)
}

func (n N) Encode() string {
	return hex.EncodeToString(n.Serialize())
}

func DecodeN(input byteparser.DecoderArgument) (N, error) {
	p, err := input.Parse()
	if err != nil {
		return N{}, err
	}
	bytes, err := p.GetSelfTerminating(zarith_pred)
	if err != nil {
		return N{}, err
	}
	return ReadN(bytes)
}

func U64toN(i uint64) N {
	val := big.NewInt(0)
	val.SetUint64(i)
	return N{val}
}

func NewN(val *big.Int) (N, error) {
	switch {
	case val == nil:
		return N{}, fmt.Errorf("cannot construct Zarith natural from nil pointer")
	case val.Sign() < 0:
		return N{}, fmt.Errorf("cannot construct zarith natural from negative input big.Int")
	default:
		return N{val}, nil
	}
}
