package util

import "fmt"

type Uintstack struct {
	stack []uint;
}

func New() *Uintstack {
	return &Uintstack{make([]uint, 0)};
}

func Stack(stack []uint) *Uintstack {
	return &Uintstack{stack};
}

func (s *Uintstack) Size() uint {
	return uint(len(s.stack));
}

func (s *Uintstack) Pop() (uint, error) {
	n := len(s.stack);
	if n == 0 {
		return 0, fmt.Errorf("cannot pop from empty stack")
	}
	n--;
	ret := s.stack[n];
	s.stack = s.stack[:n];
	return ret, nil
}

func (s *Uintstack) Peek() (uint, error) {
	n := len(s.stack);
	if n == 0 {
		return 0, fmt.Errorf("cannot peek into empty stack")
	}
	ret := s.stack[n-1];
	return ret, nil
}

func (s *Uintstack) Peek_def(def uint) uint {
	n := len(s.stack);
	if n == 0 {
		return def
	}
	ret := s.stack[n-1];
	return ret
}

func (s *Uintstack) Push(x uint) {
	s.stack = append(s.stack, x)
}