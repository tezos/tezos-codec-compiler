package util_test

import (
	"testing"

	"backend/util"

	"github.com/stretchr/testify/assert"
)

func TestSize(t *testing.T) {
	stack := util.New();
	for i := uint(0); i < 100; i++ {
		assert.Equal(t, i, stack.Size());
		stack.Push(i);
	}
	for i := uint(0); i < 100; i++ {
		j := 99 - i;
		x, err := stack.Pop();
		assert.Nil(t, err);
		assert.Equal(t, j, x);
		assert.Equal(t, j, stack.Size());
	}
}

func TestStack(t *testing.T) {
	stack := util.Stack([]uint{1,2,3,4,5});
	for i := uint(5); i > 0; i-- {
		x, err := stack.Pop();
		assert.Nil(t, err);
		assert.Equal(t, i, x);
	}
	_, err := stack.Pop();
	assert.NotNil(t, err);
}

func TestEmpty(t *testing.T) {
	stack := util.New();
	assert.Equal(t, uint(0), stack.Size());
	_, err := stack.Peek();
	assert.NotNil(t, err);
	_, err = stack.Pop();
	assert.NotNil(t, err);
	x := stack.Peek_def(12);
	assert.Equal(t, uint(12), x);
}

func TestPushPop(t *testing.T) {
	stack := util.New();
	for i := uint(0); i < 100; i++ {
		assert.Equal(t, i, stack.Size())
		stack.Push(i);
		x, err := stack.Peek();
		assert.Nil(t, err);
		assert.Equal(t, i, x);
	}
	assert.Equal(t, uint(100), stack.Size());
	for j := uint(0); j < 100; j++ {
		i := 99 - j;
		x, err := stack.Peek();
		assert.Nil(t, err);
		assert.Equal(t, i, x);
		x, err = stack.Pop();
		assert.Nil(t, err);
		assert.Equal(t, i, x);
		assert.Equal(t, i, stack.Size());
	}

	_, err := stack.Peek();
	assert.NotNil(t, err);
	_, err = stack.Pop();
	assert.NotNil(t, err);
	assert.Equal(t, uint(0), stack.Size());
}

func TestPeekDef(t *testing.T) {
	empty := util.New();
	singl := util.Stack([]uint{0});

	for i := uint(1); i < 100; i++ {
		assert.Equal(t, i, empty.Peek_def(i));
		assert.Equal(t, uint(0), singl.Peek_def(i));
	}
}