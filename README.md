Tezos Codec Compiler
====================

This repository is the official home of the `tezos-codec-compiler` project. It
consists of an executable *compiler*, which is written in OCaml, as well as
several client libraries written in assorted languages, each of which will be
referred to as a *supported backend language* (or *backend*).

Background
----------

The Tezos protocol, because it is self-amending, poses unique challenges in
terms of the ongoing interoperability of client libraries written outside of the
`octez` codebase.  Such libraries, which can be written in virtually any
programming language, must implement the same internal **codec** as the protocol
version they are pinned to, consisting of the set of types used in the Tezos
protocol for P2P messaging, RPC calls, on-disk storage, and raw protocol data.
These types are declared in the `octez` codebase and assigned a dual-format
encoding using both JSON and binary hex-strings, using combinators from the
`data-encoding` library.

From the point of view of a client library, the internally implemented
definitions of RPC types and their associated encoders/decoders must be fully
consistent with the current protocol's binary and JSON codecs, which can be
non-trivial if the only reference-point for a particular type's intended
semantics and pragmatics are underdocumented or unintuitive. Furthermore, the
lack of a language-independent and interoperable specification of the intended
representation and interpretation of each RPC message type means that
implementing the codec in a language other than OCaml still requires sufficient
OCaml experience to interpret the definitions codified into `octez`.  (The
alternative to this is to reference the codec defined by another such library,
but this does not outwardly solve the problem, and introduces its own set of
complications). Although this work is necessary, it is also tedious and must be
done with every protocol version upgrade, and there has been no real alternative
to this process. The disadvantages of this method are as follows:

  - Every type a protocol update occurs, the codec type and transcoder
  definitions must be checked and potentially altered to be conformant with the
  new protocol codec
  - The quality of the translation from codec type to concrete type is dependent
  on the quality of the documentation and the readability of the referenced
  definition
  - The potential bugs in a given client library's codec type layer may be
  unique to that library
  - Most of all, the process requires hand-writing and hand-checking of
  definitions by the maintainers of each library on an ongoing basis

Purpose
-------

The goal of this project is to automate the process of generating the RPC codec
in arbitrarily many back-end languages; this is to be accomplished by providing
a multi-target compiler that can convert a language-agnostic machine-readable
description of a Tezos **codec** and produces a small machine-generated library.
Through this approach, any client library that uses this compiler can simply use
it to generate a new codec for each future protocol version. This not only
ensures that the ongoing work required with each codec change is dramatically
lessened, but also that the cost of fixing implementation bugs is amortized
across every application language and centralized to this project.

Details
-------

The `tezos-codec-compiler` project provides an executable of the same name that
generates a collection of source-files for each *supported backend language*
registered in the compiler's internal library system.

The input to this executable is the output the command `tezos-codec dump
encodings` included in the [octez](https://gitlab.com/tezos/tezos/) ecosystem.
As a result, provided that the user can run the `tezos-codec` executable or
possesses a given snapshot of its output, the `tezos-codec-compiler` project has
no dependency on `octez`; its only dependencies, as an OCaml package, are
`yojson` and `core` (with `dune` and `bisect_ppx` also required for tooling).
In addition, there is no direct dependency on the `data-encoding` library
itself.  This means that the compiler itself is lightweight, rarely needs
recompilation, and requires only a single run to generate the codec library for
each backend from a given codec snapshot.


In addition to the compiler itself, this project also contains minimal
hand-written libraries for each *backend*, which define both the
pseudo-primitive and composite types used as the building blocks for the
machine-generated definitions, as well as a compatability layer that reifies
partially opaque internal types into more user-friendly concrete types, and
translates between the two as a preprocess for encoding (forging) and a
postprocess for decoding (parsing).  These libraries seek to define the bare
minimum amount of code required to ensure reasonably consistent behavior across
every backend language, and expose a more desirable API than the machine-parsed
imputed types (for example, the encoding and decoding of Base58-encoded values
to and from opaque byte-sequences, which cannot currently be inferred directly
from the input codec snapshot by the compiler).

The current implementation of the compiler only supports **TypeScript**, but support for **Golang** is being developed currently.

How to Use
----------

For user convenience, the build process of the executable and its execution against a target codec snapshot is
simplified through the use of a Makefile, which defines at least the following rules:

  - `make library` (default rule executed with bare `make`) - generates source files for each backend, in directory path `langs/<language-name>/compiled-<language-name>/`
  - `make build-deps` - attempts to initialize build environment by installing the target OPAM version and `dune` (currently also installs `tezos-codec-compiler` itself, but this action will be separated into its own production rule)
  - `make paths` - ensure target directories for machine-generated source-files exist
  - `make compiler` - builds compiler library and executable
  - `make test` - runs unit tests for each backend language

Tooling
-------

A non-essential, but potentially useful tool also included in this project is
the Bash script `whatchanged.sh`.  This tool is designed to report on the
differences in implementation between protocol-specific types across different
protocol versions, by computing a diff on the generated source-files belonging
to the two versions. By default, it will merely report which codec identifiers
have differences between the two most recently defined protocol versions, but it
can be configured to report the actual differences as well with the `-v` flag,
to narrow the comparison to a single identifier with `-i <ident>`, followed
optionally by the source protocol version identifier string and the target
protocol version identifier string (e.g. `"009-PsFLoren"`). Even if the compiled
libraries themselves are not directly integrated into a client library, this
tool may be helpful for sanity-checking the changes that need to be made in a
manually-implemented client library.
