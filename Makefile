compiler-ml = $(find src -name '*.ml')
OPAMVERSION = "4.12.0"

library: compiler paths
	dune exec tezos-codec-compiler test_input.json

build-deps:
	opam switch $(OPAMVERSION) || (opam switch create $(OPAMVERSION) && opam switch $(OPAMVERSION))
	opam install .
	opam install dune

paths:
	mkdir -p langs/typescript/compiled-typescript/
	mkdir -p langs/go/compiled-go/

test:
	npx jest langs/typescript/tsapi/

merlin: $(compiler-ml)
	dune build @src/check

compiler: $(compiler-ml)
	dune build src/

help:
	@echo "Usage:"
	@echo "\tmake [library]: Ensure target paths are present and executable is up-to-date, then build compiled libraries"
	@echo "\tmake build-deps: Ensure target opam version (:= $(OPAMVERSION)) is configured and install tezos-codec-compiler and dune"
	@echo "\tmake paths: Ensure target paths for compiled libraries exist (dependency of 'make library')"
	@echo "\tmake test: Run hardcoded unit tests against compiled libraries"
	@echo "\tmake merlin: Generate all intermediate objects for compiler library that are used by merlin"
	@echo "\tmake compiler: Build the compiler executable (dependency of 'make library')"
	@echo "\tmake help: Print this help message"

