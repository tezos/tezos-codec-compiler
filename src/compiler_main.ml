open Core
open Compiler.Codec_compiler

let command =
  Command.basic
    ~summary:"Test for TypeScript Compilation of simple schemas"
    ~readme:(fun () -> "Input JSON schema, get TypeScript source")
    Command.Param.(
      map
        (anon ("filename" %: string))
        ~f:(fun filename () -> generate_typescript filename))

let () = Command.run command
