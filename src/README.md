tezos-codec-compiler
====================

This directory (`src`) contains the OCaml code comprising the compiler
executable and its subordinate libraries.

Executable
----------

The primary executable is generated against `compiler_main.ml`, which provides a
simple command-line interface (using `Core.Command`) that should be
self-documenting. In broad terms, however, it takes as input the relative path
of a codec snapshot (the output of running `tezos-codec dump encodings` from
`octez`), and produces the derived library code in the directory
`./langs/typescript/compiled-typescript/` (as only TypeScript support has been
implemented thus-far) assuming that it is run from the project root.

In addition, a similar executable `test_coverage.ml` is provided, which differs
in that it accepts no user guidance, and is pinned to the codec snapshot
`test_input.json` in the project root; this is used only as a means to generate
code coverage reports for the compiler library code using an unmocked codec
snapshot. It therefore suppresses the actual generation of derived library code
and merely 'dry-runs' the two-pass compilation process.

Compiler Design
---------------

The compilation process, by which we mean the conversion of a codec snapshot into
multi-backend codec libraries, is divided into several phases. Each phase consists
of the conversion of input data in a particular *source representation* into a
*target representation* that iteratively refines a particular abstract codec from
its original format, a JSON array of interoperable schema descriptions, into a
series of codebases in various target languages.

We note that this process is overtly dissimilar from the conventional design of
a standard single-language compiler, in terms of the kind of input and output
produced, and the direction of the data-flow with respect to the relative
properties of the source and target objects of each phase of compilation.  In
most traditional compilers, a collection of source-files written in a high-level
language would be passed into the "front-end" of a compiler, passing first
through a "middle-end" and finally to a "back-end" that produces low-level
output.  In the case of this compiler, however, a low-level codec snapshot
(which is itself a derivative of a collection of OCaml type-encoding
definitions) is parsed by a two-phase "back-end" into an approximate abstract
type-representation, which is then used to generate code in a high-level
language by an approximate "front-end" (although that term is not used in the
compiler libraries themselves).

As a result, the terminology used to describe the phases of the compilation
process, and their grouping, may differ from what might be expected if viewed
from the lens of conventional compiler design.

We also note here that, despite this inversion, we use the term "backend", which
is overloaded to have different meanings in different contexts, which may be
initially confusing:
  - In the compiler libraries, `backend` is both a component library, and the informal term used to refer to the contents of said library
  - A hand-written library that is used to define non-primitive or non-standard constructs that we nevertheless wish to treat
    as being "part" of the language to simplify the compiler logic may be referred to, or even explicitly named, the "backend"
  - In the compiler code used to represent target languages, the term "backend" refers to the union of the language itself
    and the hand-written "backend" library mentioned in the above point
  - Each target language that is output by the compiler is referred to as a "backend" language
  - The code-base produced for a specific language may be referred to as a theoretical "backend" of a client library that integrates it
    
Compiler Phases
---------------

As of the current implementation, there are roughly four distinct phases to compilation,
and five total conversion passes (the final phase is run as a two-pass algorithm), which will be
elaborated on in later sections:

  - **Schema Parsing**: phase in which JSON codec snapshot is directly parsed into an AST
  - **Abstract Type Generation**: phase in which a parsed AST is used to reconstruct a collection of abstract type representations
  - **Language AST Generation**: phase in which abstract type representations are reified into a foreign language ASTs
  - **Production**: phase in which a foreign language AST is serialized into the proposed contents of one or more source-files

(By *foreign language*, we mean the same thing as *supported backend language*)

As mentioned above, the final phase (*production*) is run in two passes. The
reason for this relies on specific details of the implementation that may be
elaborated further, but in essence is a result of the fact that, in each
compiler-generated source-file, named references to types or functions may
precede their declaration, at which point the correct qualified identifier to
use is indeterminate; the first pass therefore produces no direct output, but
instead accumulates a table of 'proper' names for each raw identifier in that
source-file, which is then used in the second pass so that pre-references to
named entities are consistent with the names they are given in the first pass
(the second-pass renames should be identical to the first-pass renames as the
process is deterministic).

Note that, as *schema parsing* and *abstract type generation* are
language-independent, they are each performed only once for a given codec
snapshot, regardless of how many *supported backend languages* are being
compiled overall. Conversely, as *Language AST generation* and both passes of
*production* are language-specific, they are performed once for each *supported
backend language*.

A final pseudo-phase, which consists of writing the generated source-file
contents to disk using file-names produced during the *production* phase in
association with the string contents of a particular file, is also performed by
the compiler executable, and is not considered to be part of the
nominally-terminal *production* phase.  This pseudo-phase, which is not given a
formal name, can be suppressed entirely, as in the case of `test_coverage.ml`
and its corresponding alias `dune runtest`.

Library Layout
--------------

The compiler library is divided into three subordinate sub-libraries, which are
further sub-divided (for specific details, consult the readme for each
sub-directory, if one exists):

  - `backend`: Representation models and conversion functions for the *schema
  parsing* and *abstract type generation* phases ; additionally contains an
  interoperable meta-representation that is reified by the foreign language AST
  associated with each *supported backend language*
  - `language`: Collection of common and language-specific libraries used for
  the *language AST generation* and *production* phases
  - `compiler`: Driver library providing high-level abstractions for executing
  the entire four-phase, five-pass compilation process, additionally (and
  optionally) including the actual I/O required to write the produced
  source-file contents to disk with the appropriate prefix path and filename.
