open Core
open Backend.Codec.Codec_repr
open Backend.Codec.Ast_converter
open Backend.Representation.Abstract_type
open Language.Common.Producer

exception Odd_encoding of string

let parse_type = AbstractConstructor.parse

let parse_subtype spec =
  let name = spec.desc.title in
  match parse_type spec.encoding with
  | Some rep -> name, rep
  | None ->
      let dump = Sexp.to_string ([%sexp_of: type_encoding] spec.encoding) in
      raise (Odd_encoding dump)

let extract_json filename = Yojson.Safe.from_file filename

module Context : sig
  type t = { prod_ctxt : ProducerContext.t }

  val empty : t

  val update : t -> ProducerContext.t -> t
end = struct
  type t = { prod_ctxt : ProducerContext.t }

  let empty = { prod_ctxt = ProducerContext.fresh () }

  let update _ prod_ctxt = { prod_ctxt }
end

let output_codec ~(ctxt : Context.t) ~dryrun ((name, rep), ntd) =
  let open Language.Typescript.Producer in
  let ctxt', (filename, contents) =
    gen_all ~ctxt:ctxt.prod_ctxt ((name, rep), ntd)
  in
  if dryrun then Context.update ctxt ctxt'
  else (
    Stdio.Out_channel.write_lines filename contents;
    ctxt)

let generate_standalone ?(ctxt = Context.empty) ?(dryrun = false) codec =
  let name = codec.ident
  and binary_fields = codec.binary.toplevel
  and subtypes = codec.binary.subtypes in
  let abstract_type = Option.value_exn (parse_type binary_fields)
  and abstract_subtypes = List.map subtypes ~f:parse_subtype in
  output_codec ~dryrun ~ctxt ((name, abstract_type), abstract_subtypes)

let generate_typescript ?(test = false) filename =
  let contents = extract_json filename in
  match json_to_ast contents with
  | Many codecs ->
      let ctxt = Context.empty in
      let ctxt =
        List.fold codecs ~init:ctxt ~f:(fun ctxt codec ->
            generate_standalone ~ctxt ~dryrun:true codec)
      in
      ignore
        (List.fold codecs ~init:ctxt ~f:(fun ctxt codec ->
             generate_standalone ~ctxt ~dryrun:test codec))
  | Single codec ->
      let ctxt = Context.empty in
      let ctxt = generate_standalone ~dryrun:true ~ctxt codec in
      ignore (generate_standalone ~dryrun:test ~ctxt codec)
