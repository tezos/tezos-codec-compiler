compiler
========

This library contains the single-module top-level library `compiler`. It is the main driver module for the compiler
executable, though it merely defines the high-level functions
that are then called by [the compiler executable module](../compiler_main.ml).

As implemented, it implicitly assumes that TypeScript is the only target language the compiler will build, which is
currently true as Golang has not been fully implemented.
As the project evolves, more functions will be added to specifically support each fully-implemented target language.