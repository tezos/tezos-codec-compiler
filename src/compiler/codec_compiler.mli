(** [generate_typescript]: parse a JSON file containing a codec and generate a
    collection of TypeScript source-files, one-per-schema, that include the type
    definitions, encoders, and decoders for the toplevel type of their
    respective schemata.

    @param [test] Boolean used to suppress actual writing of the compiled
    source-files (default: [false]) *)
val generate_typescript : ?test:bool -> string -> unit
