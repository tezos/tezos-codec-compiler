foreign
=======

This directory contains contains the modules belonging to the sub-library `foreign` of the top-level library `backend`.

The modules contained in this library are used to define both invariant low-level constructs and language-specific high-level constructs, which are in turn given both a base definition set and an extensible interface.

Low-Level Constructs
--------------------

- [`binding_mod.ml`](./binding_mod.ml): Access modifier mask for top-level and block-local declarations/definitions
- [`common.ml`](./binding_mod.ml): Assortment of constructs that we expect to be language-independent
- [`ident.ml`](./ident.ml): Discriminated strings for identifiers and identifier fragments
- [`qualification.ml`](./qualification.ml): Qualification strings represented as sequences of labels

High-Level Constructs
---------------------

- [`foreign_type.ml`](foreign_type.ml): Basic definition of mandatory backend types, with accompanying interface signature
- [`foreign_val.ml`](foreign_val.ml): Basic definition of value-expressions, with accompanying interface signature
- [`statement.ml`](statement.ml): Basic definition of common abstract statements, as functor parametrized over value and type modules, along with an accompanying interface signature
- [`foreign_fun.ml`](foreign_fun.ml): Basic definition of function definitions, as functor parametrized over value, type, and statement modules, along with an accompanying interface signature
- [`declaration.ml`](declaration.ml): Basic definition of top-level declarations, as functor parametrized over value, type, statement, and function modules; includes an accompanying interface signature
- [`foreign_ast.ml`](foreign_ast.ml): Basic definition of full AST of a language, as functor parametrized over value, type, statement, function, and declaration modules; includes an accompnaying interface signature
  
Extensibility Model
-------------------

The current model used to directly extend the defined types of a given high-level construct module from the base definition to
a refined type-specific implementation is to take direct (untagged) unions of polymorphic variant types. For practical examples of this process, see [the typescript AST definition](../language/typescript/typescript_ast.ml).


