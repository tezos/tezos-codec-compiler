(** Any scoping label that is prepended to the base-name of a local identifier *)
type label = Ident.scope [@@deriving sexp]

(** Head of list is innermost qualifier; qualification should be serialized in
    reverse. 'prefix' therefore refers any sub-list containing the final
    element, and 'suffix' to any sub-list containing the first element *)
type t = label list [@@deriving sexp]

(** [bisect xs ys] computes the longest common prefix [lcp] of [xs] and [ys],
    and returns [((xt,yt),lcp)] where [xs == xt @ lcp] and [ys == yt @ lcp] *)
val bisect : t -> t -> (t * t) * t

(** [get_rel_prefix scope target] splits [target] into its longest prefix that
    is a sub-sequence of [scope], and all remaining labels that diverge from
    [scope]. *)
val get_rel_prefix : t -> t -> t * t

(** [drop_inferred_prefix q] returns the longest suffix of [q] whose outermost
    label is not an [Inferred_scope] variant *)
val drop_inferred_prefix : t -> t

(** Reverse-order list comparison that forgetfully coerces labels to their
    underlying strings *)
val compare : t -> t -> int

(** [alamgamate ?drop_proto qual base] generates a [label list] consisting of
    one element, computed by accumulating the labels of qual in normalized (i.e.
    reverse) order and prepending them to [base] with intervening "__"
    seperation. If any label contains a namespace-prefix "NS", it is moved to
    the beginning of the label without duplication.

    @param [drop_proto] When set, omits any label with a prefix of "Proto" after
    scrubbing any "NS" prefix. Default [true]. *)
val amalgamate : ?drop_proto:bool -> t -> Ident.Scope.t -> t

(** Produces a non-identifier stringification of a [label list] that normalizes
    the order of the labels and indicates label separation with a ">" separator.
    To be used for debugging rather than construction of new labels. *)
val to_string : t -> string

(** [mk_ns ?prefix ident] wraps [ident] in a [Namespace_name] constructor, after
    optionally adding an "NS" prefix to prevent name shadowing.

    @param [prefix] When set, adds "NS" to the beginning of [ident] before
    wrapping. Default [true]. *)
val mk_ns : ?prefix:bool -> string -> Ident.Scope.t
