module type Fun_intf = sig
  module T : Foreign_type.Type_intf

  module V : Foreign_val.Val_intf

  module S : Statement.Statement_intf with module T := T and module V := V

  type t' = {
    params : (T.t, V.t) Common.fun_args;
    return_type : T.t list;
    body : S.t list;
  }

  type t
end

module Fun
    (T : Foreign_type.Type_intf)
    (V : Foreign_val.Val_intf)
    (S : Statement.Statement_intf with module T := T and module V := V) =
struct
  module T = T
  module V = V
  module S = S

  type t' = {
    params : (T.t, V.t) Common.fun_args;
    return_type : T.t list;
    body : S.t list;
  }

  type t = [ `GlobalFunc of t' | `LocalFunc of t' ]
end
