(** Simple enum for the directions of transcoding *)
type transcoder_dir = Encode | Decode

(** Specification of how a particular field in an object literal is to be filled *)
type 'a field_fill =
  | Assign of Ident.lab_id * 'a
      (** [Assign (fld,v)] indicates that field [fld] is explictly assigned the
          value [v] *)
  | NamedPun of Ident.lab_id
      (** [NamedPun fld] indicates that the field [fld] should inherit the value
          of an in-scope binding of the same name *)

(** Specification of how multiple, unidentified fields in an object literal are
    to be filled *)
type record_wildcard =
  | LocalScoped
      (** Inherit field values from correspondingly named bindings in local
          scope *)
  | FromObject of Ident.var_id
      (** Inherit field values from another object, bound to an identifier *)

(** Specification of how an object literal is to be constructed *)
type 'a record_fill = {
  explicit : 'a field_fill list;
  implicit : record_wildcard option;
}

(** Modifier to a function argument *)
type 'a fun_arg_mod =
  | Mandatory
  | Default of 'a  (** If not provided, use a default value *)
  | Optional
      (** If not provided, use a sentinel not-value (e.g. undefined, None, null) *)

(** List of arguments with that are either all typed or all untyped *)
type ('t, 'v) fun_args =
  [ `Typed of (Ident.var_id * 'v fun_arg_mod * 't) list
    (** All argument have type signatures *)
  | `Untyped of (Ident.var_id * 'v fun_arg_mod) list
    (** No arguments have type signatures *) ]

(** Convert a [('t, 'v) fun_args] value into a list of named and annotated
    arguments with optional type signatures *)
val unify_funargs :
  ('t, 'v) fun_args -> (Ident.var_id * 'v fun_arg_mod * 't option) list

(** Convert [('t1,'v1) fun_args] to [('t2,'v2) fun_args] by a bifunctorial map
    operation

    @param ft Function that converts the type representation
    @param fv Function that converts the value representation *)
val convert_funargs :
  ft:('t1 -> 't2) ->
  fv:('v1 -> 'v2) ->
  ('t1, 'v1) fun_args ->
  ('t2, 'v2) fun_args

type binding_mods = Binding_mod.t

type directive =
  | ImportAll of { qualified_name : string option; source_file : string }
  | ImportBindings of { bindings : string list; source_file : string }
