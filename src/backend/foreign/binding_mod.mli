(** Modifiers to a definition or declaration that determine what
    language-specific keywords are used. *)

(** Visibility of the binding *)
type vis_mod =
  | Local
      (** Binding is local by definition and has no mechanism for access outside
          the current scope *)
  | Hidden
      (** Binding could be made visible outside the current scope, but is not *)
  | Exposed
      (** Binding can be referenced either directly (if global) or by external
          qualification (default) *)

(** Mutability of the RHS value *)
type mut_mod =
  | Immutable  (** Value cannot or will not be mutated *)
  | Mutable  (** Value may be mutated if language allows *)

(** Variability of the LHS binding *)
type var_mod =
  | Invariant  (** Binding cannot be updated or reassigned *)
  | Volatile  (** Binding can be updated and/or reassigned *)

type ext_mod =
  | Definite
      (** Binding is made concrete either here or elsewhere in current scope *)
  | Proxy
      (** Binding is merely an abstract promise that a concrete definition of
          the binding will exist in scope at compile-time or run-time *)

type t = {
  visibility : vis_mod;
  mutability : mut_mod option;
  variability : var_mod option;
  externality : ext_mod;
}

(** Binding modifications with visibility [Exposed] and externality [Definite],
    using the implicit per-language default to determine mutability and
    variability. *)
val exposed_definite : t

(** Produce a [Binding_mod.t] suitable for a local-scope variable, which
    optionally takes a [mut_mod] and [var_mod] *)
val local : ?mut:mut_mod -> ?var:var_mod -> unit -> t

(** Binding modifications suitable when the binding RHS is an anonymous function
    we wish to call by the LHS name *)
val lambda : t
