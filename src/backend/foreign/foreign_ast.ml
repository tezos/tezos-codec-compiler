open Common

module type Ast_intf = sig
  module T : Foreign_type.Type_intf

  module V : Foreign_val.Val_intf

  module S : Statement.Statement_intf with module T := T and module V := V

  module F :
    Foreign_fun.Fun_intf with module T := T and module V := V and module S := S

  module D :
    Declaration.Declaration_intf
      with module T := T
       and module V := V
       and module S := S
       and module F := F

  type source_file

  type foreign_codebase =
    | SingleFile of source_file
    | ManyFile of source_file list
end

module Ast
    (T : Foreign_type.Type_intf)
    (V : Foreign_val.Val_intf)
    (S : Statement.Statement_intf with module T := T and module V := V)
    (F : Foreign_fun.Fun_intf
           with module T := T
            and module V := V
            and module S := S)
    (D : Declaration.Declaration_intf
           with module T := T
            and module V := V
            and module S := S
            and module F := F) =
struct
  module T = T
  module V = V
  module S = S
  module F = F
  module D = D

  type source_file = {
    filename : string;
    preamble : directive list;
    contents : D.v list;
  }

  type foreign_codebase =
    | SingleFile of source_file
    | ManyFile of source_file list
end
