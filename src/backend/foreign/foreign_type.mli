open Representation.Abstract_type
open Codec.Codec_repr

module type Type_intf = sig
  type nat

  type prim

  type linear_type

  type composite_type

  type reference_type

  type atom

  type t
end

(** Native (or defined in backend) types used implicitly for values outside of
    the codec *)
type nat =
  [ `NatBool
  | `NatInt of sign * bit_width option
  | `NatString
  | `NatError
  | `NatDecoderInput
  | `NatParser ]

(** Primitive types we expect to appear either in the foreign language itself,
    or to have defined ourselves in the backend.

    Used only in the context of codec type representation *)
type prim =
  [ `PrimBool
  | `PrimInt of sign * bit_width
  | `PrimIntRanged of (int * int) * byte_width
  | `PrimFloat of float_precision
  | `PrimString
  | `PrimOpaqueBytes
  | `PrimOpaqueBytesFixedLen of int
  | `PrimWidth
  | `PrimPadding of int
  | `PrimZarith_N
  | `PrimZarith_Z ]

type linear_type = [ `LTSequence of t ]

(** Composite types we expect to appear either in the foreign language itself,
    or to have defined ourselves in the backend *)
and composite_type =
  [ `CTLinear of linear_type
  | `CTOption of t
  | `CTTuple2 of t * t
  | `CTDynWidth of width * t ]

and reference_type =
  [ `RTOpaque of Qualification.t * Ident.type_id
  | `RTEnum of (Qualification.t * Ident.type_id) * enum_size ]

and atom =
  [ `TPrim of prim
  | `TNat of nat
  | `TComposite of composite_type
  | `TRefer of reference_type ]

and t = [ `Atom of atom | `Object of (Ident.lab_id * t) list ]
