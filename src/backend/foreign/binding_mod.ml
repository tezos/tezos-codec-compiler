type vis_mod = Local | Hidden | Exposed

type mut_mod = Immutable | Mutable

type var_mod = Invariant | Volatile

type ext_mod = Definite | Proxy

type t = {
  visibility : vis_mod;
  mutability : mut_mod option;
  variability : var_mod option;
  externality : ext_mod;
}

let exposed_definite =
  {
    visibility = Exposed;
    mutability = None;
    variability = None;
    externality = Definite;
  }

let local ?mut ?var () =
  {
    visibility = Local;
    mutability = mut;
    variability = var;
    externality = Definite;
  }

let lambda =
  {
    visibility = Exposed;
    mutability = Some Immutable;
    variability = Some Invariant;
    externality = Definite;
  }
