module type Declaration_intf = sig
  module T : Foreign_type.Type_intf

  module V : Foreign_val.Val_intf

  module S : Statement.Statement_intf with module T := T and module V := V

  module F :
    Foreign_fun.Fun_intf with module T := T and module V := V and module S := S

  type t

  type u

  type v
end

module Declaration
    (T : Foreign_type.Type_intf)
    (V : Foreign_val.Val_intf)
    (S : Statement.Statement_intf with module T := T and module V := V)
    (F : Foreign_fun.Fun_intf
           with module T := T
            and module V := V
            and module S := S) =
struct
  module T = T
  module V = V
  module S = S
  module F = F

  type t' = [ `TypeDec of Qualification.t * Ident.type_id * T.t ]

  type t = [ t' | `FunDec of Qualification.t * Ident.fun_id * F.t ]

  type u' =
    [ `Group of (Binding_mod.t * t) list ]

  type u = 
    [ u' | `Collection of ((Binding_mod.t * t') * u' list * u') ]
  
  type v = u
end
