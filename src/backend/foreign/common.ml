open Core

type transcoder_dir = Encode | Decode

type 'a field_fill = Assign of Ident.lab_id * 'a | NamedPun of Ident.lab_id

type record_wildcard = LocalScoped | FromObject of Ident.var_id

type 'a record_fill = {
  explicit : 'a field_fill list;
  implicit : record_wildcard option;
}

type 'a fun_arg_mod = Mandatory | Default of 'a | Optional

type ('t, 'v) fun_args =
  [ `Typed of (Ident.var_id * 'v fun_arg_mod * 't) list
  | `Untyped of (Ident.var_id * 'v fun_arg_mod) list ]

let unify_funargs = function
  | `Typed ts -> List.map ts ~f:(fun (x, y, z) -> x, y, Some z)
  | `Untyped us -> List.map us ~f:(fun (x, y) -> x, y, None)

let convert_funargs ~(ft : 't1 -> 't2) ~(fv : 'v1 -> 'v2) :
    ('t1, 'v1) fun_args -> ('t2, 'v2) fun_args = function
  | `Typed ts ->
      `Typed
        (List.map ts ~f:(function
            | id, ((Mandatory | Optional) as m), t -> id, m, ft t
            | id, Default v, t -> id, Default (fv v), ft t))
  | `Untyped us ->
      `Untyped
        (List.map us ~f:(function
            | (_, (Mandatory | Optional)) as x -> x
            | id, Default v -> id, Default (fv v)))

type binding_mods = Binding_mod.t

type directive =
  | ImportAll of { qualified_name : string option; source_file : string }
  | ImportBindings of { bindings : string list; source_file : string }
