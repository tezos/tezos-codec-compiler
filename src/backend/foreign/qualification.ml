open Core

type label = Ident.scope [@@deriving sexp]

type t = label list [@@deriving sexp]

let to_string xs =
  String.concat ~sep:">" @@ List.rev_map ~f:Ident.Scope.to_string xs

let bisect xs ys =
  let rxs = List.rev xs and rys = List.rev ys in
  let rxys, rrem = List.zip_with_remainder rxs rys in
  let rcommon, rdiff =
    List.split_while ~f:(fun (x, y) -> Ident.Scope.(x = y)) rxys
  in
  let lcp = List.rev @@ List.map rcommon ~f:(fun (x, _) -> x)
  and rx_uniq, ry_uniq = List.unzip rdiff in
  let x_suff, y_suff =
    match rrem with
    | None -> List.rev rx_uniq, List.rev ry_uniq
    | Some (Either.First rxt) ->
        List.rev_append rxt (List.rev rx_uniq), List.rev ry_uniq
    | Some (Either.Second ryt) ->
        List.rev rx_uniq, List.rev_append ryt (List.rev ry_uniq)
  in
  (x_suff, y_suff), lcp

let drop_inferred_prefix (q : t) : t =
  let qrev = List.rev q in
  List.rev
  @@ List.drop_while
       ~f:(function Ident.Scope.Inferred_scope _ -> true | _ -> false)
       qrev

let drop_proto (q : t) : t =
  let f (prefix, accum) lab =
    match Ident.Scope.to_string lab with
    | x when String.is_prefix ~prefix:"NSProto" x -> true, accum
    | x when String.is_prefix ~prefix:"NS" x ->
        true, Ident.Scope.map ~f:(fun x -> String.drop_prefix x 2) lab :: accum
    | x when String.is_prefix ~prefix:"Proto" x -> prefix, accum
    | _ -> prefix, lab :: accum
  in
  let prefix, labels = List.fold ~f q ~init:(false, []) in
  List.rev_mapi
    ~f:(fun ix lab ->
      if ix = 0 && prefix then Ident.Scope.map ~f:(fun lab -> "NS" ^ lab) lab
      else lab)
    labels

let get_rel_prefix (scope : t) (target : t) : t * t =
  (* REVIEW[epic=stopgap] - drop module-level namespace to fix mismatch *)
  let _toplevel = drop_inferred_prefix scope in
  let scope = List.drop_last_exn scope in
  let (_in_scope, in_target), common = bisect scope (drop_proto target) in
  (* Stdio.eprintf "%s<>%s -> (%s){%s,%s}\n" (to_string scope) (to_string
     target) (to_string common) (to_string _in_scope) (to_string in_target); *)
  in_target, common

let mk_ns ?(prefix = true) id =
  Ident.Scope.Namespace_name (if prefix then "NS" ^ id else id)

let amalgamate ?(drop_proto = true) (q : t) base_name : t =
  let f (prefix, accum) = function
    | x
      when String.is_prefix ~prefix:"NSProto" (Ident.Scope.to_string x)
           && drop_proto ->
        true, accum
    | x when String.is_prefix ~prefix:"NS" (Ident.Scope.to_string x) ->
        true, String.drop_prefix (Ident.Scope.to_string x) 2 :: accum
    | x
      when String.is_prefix ~prefix:"Proto" (Ident.Scope.to_string x)
           && drop_proto ->
        prefix, accum
    | x -> prefix, Ident.Scope.to_string x :: accum
  in
  let prefix, labels = List.fold ~f q ~init:(f (false, []) base_name) in
  let unilabel = String.concat ~sep:"__" labels in
  [ mk_ns ~prefix unilabel ]

let compare xs ys = List.compare Ident.Scope.compare (List.rev xs) (List.rev ys)
