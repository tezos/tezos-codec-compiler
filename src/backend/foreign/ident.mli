(** Identifier used to refer to a type *)
type type_id = string [@@deriving sexp]

(** Identifier used to refer to a function *)
type fun_id = string [@@deriving sexp]

(** Identifier used to refer to fields/labels of an object/record *)
type lab_id = string [@@deriving sexp]

(** Identifier used to refer to a generic or polymorphic type parameter *)
type tparam_id = string [@@deriving sexp]

(** Identifier used to define namespaces *)
type namespace_id = string [@@deriving sexp]

(** Identifier used for module-level constants *)
type const_id = string [@@deriving sexp]

(** Identifier used for variable names, either globally, in scoped namespaces, *
    in function argument lists, or in function bodies. *)
type var_id = string [@@deriving sexp]

(** Identifier used for names of individual values of an * enumerated (enum)
    type *)
type enum_id = string [@@deriving sexp]

module Binding : sig
  type t =
    | Const_name of const_id
    | Variable_name of var_id
    | Function_name of fun_id
    | Label_name of lab_id
    | Enum_name of enum_id
  [@@deriving sexp]

  val ( = ) : t -> t -> bool

  val ( <> ) : t -> t -> bool

  val to_string : t -> string
end

type binding = Binding.t [@@deriving sexp]

module Scope : sig
  type t =
    | Type_name of type_id
    | Inferred_scope of string
    | Namespace_name of namespace_id
  [@@deriving sexp]

  val ( = ) : t -> t -> bool

  val ( <> ) : t -> t -> bool

  val to_string : t -> string

  val compare : t -> t -> int

  val map : f:(string -> string) -> t -> t
end

type scope = Scope.t [@@deriving sexp]

(** Labeled union of all kinds of identifier *)
type t =
  | Binding_name of binding
  | Scope_name of scope
  | TParam_name of tparam_id
[@@deriving sexp]
