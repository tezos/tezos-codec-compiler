open Core

type type_id = string [@@deriving sexp]

type fun_id = string [@@deriving sexp]

type lab_id = string [@@deriving sexp]

type tparam_id = string [@@deriving sexp]

type namespace_id = string [@@deriving sexp]

type const_id = string [@@deriving sexp]

type var_id = string [@@deriving sexp]

type enum_id = string [@@deriving sexp]

module Binding = struct
  type t =
    | Const_name of const_id
    | Variable_name of var_id
    | Function_name of fun_id
    | Label_name of lab_id
    | Enum_name of enum_id
  [@@deriving sexp]

  let ( = ) x y =
    match x, y with
    | Const_name id1, Const_name id2
    | Variable_name id1, Variable_name id2
    | Function_name id1, Function_name id2
    | Label_name id1, Label_name id2
    | Enum_name id1, Enum_name id2 ->
        String.(id1 = id2)
    | _ -> false

  let ( <> ) x y =
    match x, y with
    | Const_name id1, Const_name id2
    | Variable_name id1, Variable_name id2
    | Function_name id1, Function_name id2
    | Label_name id1, Label_name id2
    | Enum_name id1, Enum_name id2 ->
        String.(id1 <> id2)
    | _ -> true

  let to_string = function
    | Const_name id
    | Variable_name id
    | Function_name id
    | Label_name id
    | Enum_name id ->
        id
end

type binding = Binding.t [@@deriving sexp]

module Scope = struct
  type t =
    | Type_name of type_id
    | Inferred_scope of string
    | Namespace_name of namespace_id
  [@@deriving sexp]

  let ( = ) x y =
    match x, y with
    | Type_name id1, Type_name id2
    | Inferred_scope id1, Inferred_scope id2
    | Namespace_name id1, Namespace_name id2 ->
        String.(id1 = id2)
    | _ -> false

  let ( <> ) x y =
    match x, y with
    | Type_name id1, Type_name id2
    | Inferred_scope id1, Inferred_scope id2
    | Namespace_name id1, Namespace_name id2 ->
        String.(id1 <> id2)
    | _ -> true

  let to_string = function
    | Type_name s | Inferred_scope s | Namespace_name s -> s

  let compare x y = String.compare (to_string x) (to_string y)

  let map ~f = function
    | Type_name id -> Type_name (f id)
    | Inferred_scope id -> Inferred_scope (f id)
    | Namespace_name id -> Namespace_name (f id)
end

type scope = Scope.t [@@deriving sexp]

type t =
  | Binding_name of binding
  | Scope_name of scope
  | TParam_name of tparam_id
[@@deriving sexp]
