open Representation.Abstract_type
open Codec.Codec_repr

module type Type_intf = sig
  type nat

  type prim

  type reference_type

  type linear_type

  type composite_type

  type atom

  type t
end

type nat =
  [ `NatBool
  | `NatInt of sign * bit_width option
  | `NatString
  | `NatError
  | `NatDecoderInput
  | `NatParser ]

type prim =
  [ `PrimBool
  | `PrimInt of sign * bit_width
  | `PrimIntRanged of (int * int) * byte_width
  | `PrimFloat of float_precision
  | `PrimString
  | `PrimOpaqueBytes
  | `PrimOpaqueBytesFixedLen of int
  | `PrimWidth
  | `PrimPadding of int
  | `PrimZarith_N
  | `PrimZarith_Z ]

type reference_type =
  [ `RTOpaque of Qualification.t * Ident.type_id
  | `RTEnum of (Qualification.t * Ident.type_id) * enum_size ]

type linear_type = [ `LTSequence of t ]

and composite_type =
  [ `CTLinear of linear_type
  | `CTOption of t
  | `CTTuple2 of t * t
  | `CTDynWidth of width * t ]

and atom =
  [ `TPrim of prim
  | `TNat of nat
  | `TComposite of composite_type
  | `TRefer of reference_type ]

and t = [ `Atom of atom | `Object of (Ident.lab_id * t) list ]
