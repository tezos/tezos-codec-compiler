open Codec.Codec_repr
open Common

module type Val_intf = sig
  type prim

  type literal

  type derived_term

  type unary_op

  type binary_op

  type term

  type t
end

type prim =
  [ `PrimTrue
  | `PrimFalse
  | `PrimNum of string
  | `Prim_L
  | `PrimWidth of size_repr
  | `PrimTagSize of tag_size
  | `PrimEnumSize of enum_size ]

type literal =
  [ `LitString of string | `LitArray of t list | `LitObject of t record_fill ]

and derived_term = [ `Field of Ident.lab_id | `Index of t | `KeyVal of t ]

and unary_op =
  [ `Access of derived_term
  | `CallMethod of Ident.fun_id * t list
  | `BooleanNegate ]

and binary_op = [ `Eq | `NotEq ]

and term = [ `VPrim of prim | `VLit of literal | `VBound of Ident.binding ]

and t =
  [ `Pure of term
  | `CreateParser of t list
  | `ConcatStrings of t list
  | `UnaryOp of unary_op * t
  | `BinaryOp of binary_op * t * t
  | `Paren of t ]
