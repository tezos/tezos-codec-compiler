module type Statement_intf = sig
  module T : Foreign_type.Type_intf

  module V : Foreign_val.Val_intf

  type t
end

module Statement (T : Foreign_type.Type_intf) (V : Foreign_val.Val_intf) =
struct
  module T = T
  module V = V

  type t =
    [ `DeclareLocal of Ident.var_id * Binding_mod.t * T.t option * V.t option
    | `AssignLocal of Ident.var_id * V.derived_term option * V.t
    | `BareExpression of V.t
    | `Return of V.t option
    | `ThrowError of V.t ]
end
