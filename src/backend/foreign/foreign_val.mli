open Codec.Codec_repr
open Common

module type Val_intf = sig
  type prim

  type literal

  type derived_term

  type unary_op

  type binary_op

  type term

  type t
end

(** Value of a primitive (per-language) or otherwise fundamental (per-backend)
    type *)
type prim =
  [ `PrimTrue
  | `PrimFalse
  | `PrimNum of string
    (** Any numeric value, regardless of type representation or formatting *)
  | `Prim_L
    (** _L is meant to look like [⊥]; this is the bottom value (e.g. null) *)
  | `PrimWidth of size_repr
  | `PrimTagSize of tag_size
  | `PrimEnumSize of enum_size ]

type literal =
  [ `LitString of string
    (** String literals should omit their openquote and closequote internally,
        as those are language-dependent *)
  | `LitArray of t list
  | `LitObject of t record_fill ]

and derived_term =
  [ `Field of Ident.lab_id  (** Field of a structure/object with a given name *)
  | `Index of t  (** Index of an array-like structure with a given index *)
  | `KeyVal of t  (** Entry of a dict/map-like structure with a given key*) ]

and unary_op =
  [ `Access of derived_term
  | `CallMethod of Ident.fun_id * t list
  | `BooleanNegate ]

and binary_op = [ `Eq | `NotEq ]

and term = [ `VPrim of prim | `VLit of literal | `VBound of Ident.binding ]

and t =
  [ `Pure of term
  | `CreateParser of t list
    (** Abstract expression that produces a parser object from a list of
        arguments *)
  | `ConcatStrings of t list
    (** Abstract expression representing the most natural approach to
        concatenating many strings at once *)
  | `UnaryOp of unary_op * t
  | `BinaryOp of binary_op * t * t
  | `Paren of t  (** Parenthetical of an expression *) ]
