Backend
=======

Some of the terminology used in this readme is laid out in
the [root](../../README.md) and [parent](../README.md) readme files, which should preferably
be read first.

The `backend` library contains four sub-libraries, each of which may contain their own readme:
  - `codec` consists of modules defining AST types used primarily as the target representation of the
    *schema parsing* phase, and secondarily throughout the compiler libraries as persistent primitive constructs
  - `preconversion` consists of modules defining the actual *schema parsing* process wherein
    the JSON structure comprising the codec snapshot is converted to an AST as defined in
    the `codec` sub-library, which is a dependency of this one
  - `representation` consists of modules associated with the *abstract type generation* phase of compilation,
    containing both the types used in the target representatation of that phase, and the functions that
    are called during that phase. The use of the library name `representation` should
    be understood to mean this library in particular, as it is the representation output
    by the last phase of compilation that is run once per codec rather than once per target language.
  - `foreign` consists of modules that conform to and constitute the general approach 
    taken when modeling the AST of a *foreign language*. Certain constructs, such as **identifiers**
    and **relative scope qualification**, are intended to be universal across all languages, even if
    certain features they model are not directly applicable to a given language; other features, such
    as the modeling of the overall AST and its subordinate modules for types, value expressions, statements,
    functions, and declarations, are given both concrete definitions and general interface signatures, which
    will almost always be selectively cherry-picked, overwritten, or extended by the concrete AST defined for
    each *foreign language* in an external module.

This directory additionally includes a single top-level module `backend.ml` that merely re-exposes
the sub-libraries as modules, so that they are visible to the other compiler libraries.