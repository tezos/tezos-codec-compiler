representation
==============

This directory contains the modules belonging to the sub-library `representation` of the top-level compiler library `backend`.

The library itself consists of modules associated with the *abstract type
generation* phase of compilation, containing definitions both of the types used
in the target representatation, as well as the functions that are called during
that phase.

Abstract Type Generation
------------------------

It is implicitly known that the codec snapshot that the compiler is run against
is the result of a conversion of `'a encoding` values from the `data-encoding`
library, implemented in the `octez` codebase in various sourcepaths.  These, in
turn, are abstract descriptions of how a given type, originally defined using
OCaml, is to be serialized and deserialized between its binary representation
and the language-specific implementation of that type.

As the JSON/binary encodings of these types are interoperable by nature, it is
possible to more or less 'reconstruct' an abstract definition of any type
associated with a schema in the codec, up to isomorphism, based on its binary
representation alone. Note that while this may, in almost all cases, yield a
perfectly sound collection of types, the actual definitions that will be
produced may be less than ideal from the perspective of ensuring a user-friendly
API, as certain types in `octez` may be encoded using `data-encoding`
combinators that substantively change the semantics of value, presently without
any record of having done so that is visible in the codec description. Examples
of this include use of the `conv` combinator, which converts from the encoding
of one type to another using a pair of translation functions; the use of
potentially overlapping `union`s of `case`s; and any form of endomorphic
canonicalization, wherein certain values meeting a heueristic are preferentially
encoded using specialized logic rather than falling into a catch-all category
using the direct naive encoding.

Abstract Type vs. Schema Type
-----------------------------

A careful inspection of the dependency graph between modules in the compiler
library will reveal that, although this library includes two modules,
`abstract_type.ml` and `schema_type.ml`, only the former is currently used.

The former, `abstract_type.ml`, contains definitions of the types
`abstract_type` and `typrep`, both of which are derived solely from the binary
description of the codec schema. The latter, `schema_type.ml`, contains
definitions of the types `schema_type` and `typrep'`, both of which are derived
solely from the `JSON-schema` element of the codec schema.

Although there are several known bugs in the `data-encoding` library that result
in incomplete or incoherent binary schema descriptions, only the binary
description is detailed and low-level enough to use to generate accurate (when
not buggy) representations of the abstract types being represented. In the
absence of any bugfixes or other feature-improvements to the `data-encoding`
library, it is theoretically possible to impute isomorphisms between each
`abstract_type` and its corresponding `schema_type` in order to recover missing
or otherwise misleading information about the `abstract_type` based on the
hierarchical relationship of the less buggy, but insufficiently detailed
`schema_type`. However, it is the view of this project that such bugs should be
fixed instead of being coded around, as the complexity of the type unification
process is far greater than the bugfixes that would obviate its necessity.
