open Codec.Codec_repr

(** [rep_size]: enumeration of the possible sizes (viz. length, in bytes) of a
    primitive or composite type that does not have an implicitly known size. *)
type rep_size =
  | FixedLength of int  (** Fixed length, given as a number of bytes *)
  | VariableLength of width
      (** Variable length, encoded in a self-contained 'dynamic prefix' *)
  | DynamicLength
      (** Dynamic length, which can only be inferred from information associated
          with the context in which the value appears *)
  | IndeterminateLength
      (** Insufficient information available to determine the size of the type
          at time of creation, but which might be resolved once the surrounding
          context is known *)
[@@deriving sexp]

(** Bit-width of an integral type, independent of signedness *)
type bit_width =
  | Bits8  (** Integral type represented using 8 bits *)
  | Bits16  (** Integral type represented using 16 bits *)
  | Bits32
      (** Integral type represented using 32 bits ([int31] and [uint30]
          included) *)
  | Bits64  (** Integral type represented using 64 bits *)
[@@deriving sexp]

(** Signedness of an integral type, independent of its bit-width *)
type sign = Signed  (** Signed integer *) | Unsigned  (** Unsigned integer *)
[@@deriving sexp]

(** Precision of a floating-point numerical type *)
type float_precision =
  | Single  (** Single-precision (32-bit representation) *)
  | Double  (** Double-precision (64-bit representation) *)
[@@deriving sexp]

(** Byte-width of a ranged integral type, independent of signedness *)
type byte_width =
  | Bytes4  (** Ranged integral represented using 4 bytes *)
  | Bytes8  (** Ranged integral represented using 8 bytes *)
[@@deriving sexp]

(** [abstract_type]: an intermediate representation of a type in the codec
    schema, * generated initially from the parsed AST of the raw codec JSON and
    later used to * generate the type and associated transcoder definitions in
    the foreign-language AST. * * Includes both primitive types, pseudo-types
    that are later erased or ignored, * and composite (i.e. recursive) types. *)
type abstract_type =
  | AbstractInt of sign * bit_width  (** an integer value *)
  | AbstractIntRanged of (int * int) * byte_width
      (** an appropriately sized integer value in a given range *)
  | AbstractBytes of rep_size  (** opaque byte-sequence *)
  | AbstractString of rep_size  (** opaque byte-string *)
  | AbstractFloat of float_precision
      (** floating-point number with the specified precision *)
  | AbstractBool  (** boolean value *)
  | AbstractSequence of width option * abstract_type
      (** flat multi-element sequence of element values *)
  | AbstractTuple2 of abstract_type * abstract_type  (** Tuple of two values *)
  | AbstractNamed of width option * string  (** Named reference to a type *)
  | AbstractEnum of Int_size.t * string
      (** Named reference to an type that is known to be an enum *)
  | AbstractVoid  (** Any zero-width type *)
  | AbstractOption of abstract_type
      (** Boolean-tagged union of a value type and void *)
  | AbstractPadding of int  (** Reserved type for pad bytes *)
  | AbstractPseudo of size_repr  (** Reserved type for non-fields *)
  | AbstractDyn of size_repr * abstract_type
      (** Reserved type for outer nested dynamic fields *)
  | AbstractTag of tag_size  (** Reserved type for ADT tag values *)
  | AbstractTagVal of {
      base : string;
      tag_size : tag_size;
      elem : string;
      value : int;
    }
      (** Reserved type for values of ADT tags that should be narrowed when
          possible *)
  | AbstractZarith_Z  (** Dedicated type for Zarith integers (Z.t) *)
  | AbstractZarith_N  (** Dedicated type for Zarith naturals (N.t) *)
[@@deriving sexp]

(** [cons_name]: the name of a constructor (the [title] of a [case], or name of
    an enum element), which might not always be provided *)
type cons_name = string option [@@deriving sexp]

(** [tag_val]: type used for the associated numerical tag-value of a [case] *)
type tag_val = int [@@deriving sexp]

(** [variant]: type used for an enum element, or ADT case *)
type variant =
  | Enum_variant of cons_name * int
      (** [Enum_variant (c, v)] represents an enum element with name [c] and
          value [v] *)
  | Type_variant of cons_name * int * typrep
      (** [Type_variant (c,v,t)] represents a case variant with name [c],
          tag-value [v], and type [t] *)
[@@deriving sexp]

(** [typrep]: representation of a type bound to an type-level identifier in a
    schema *)
and typrep =
  | Anon of abstract_type
      (** Simple type-alias, or alternatively a record with only one concrete
          field which happens to be anonymous *)
  | Record of (string * abstract_type) list
      (** Simple record consisting of an in-order list of fields, each of which
          has a name and a type *)
  | ADT of {
      tag_size : tag_size;
      variant_size : datakind_spec;
      variants : variant list;
    }
      (** Algebraic data type consisting of a list of [variant]s with indicated
          tag-size [tag_size] and payload-size [variant_size] *)
  | Enum of {
      enum_size : enum_size;
      elements : (cons_name * int) list
    } 
    (** Enumerated type consisting of a list of [elements] represented by
        an integer of size [enum_size] *)
[@@deriving sexp]

(** Module used to represent a partially constructed [typrep] value, * which is
    initialized as an empty value and successively populated * with its
    corresponding fields (for simple types) or variants (for ADTs) * until none
    remain, at which point it can be frozen and extracted * *)
module Typrep : sig
  (** Exception raised by [extract_exn] when applied to an [empty] value *)
  exception Extract_empty

  (** Exception raised by [extract_opt] and [extract_exn] when an ADT variant *
      is found to contain another ADT *)
  exception Nested_algebraic

  (** Exception raised by [extract_opt] and [extract_exn] when a field with *
      label ["N.t"] or ["Z.t"] is not typed as the corresponding Zarith *
      abstract-type *)
  exception Mistyped_zarith

  (** Representation of a [typrep] that is in the process of being *
      constructed. It can either be completely empty, partially constructed, *
      or frozen (marked as fully constructed, and unable to be modified any
      further). * * Internally, both fields (in simple types and in individual
      variants) and variants * are stored in a LIFO stack while they are being
      constructed, and later normalized * upon extraction. *)
  type t

  (** Empty value of [t]; must be populated before it can be frozen or *
      extracted *)
  val empty : t

  (** Add a named or anonymous field of the given abstract type to a (possibly *
      empty) simple type-rep, or to the most recently added variant of an ADT *)
  val add_field : t -> ?name:string -> abstract_type -> t

  (** Modify the construction state of a partial [typrep] such that the most
      recently added variant (if there is one) is finalized and a new variant is
      pre-initialized to be annotated and populated by the sequentially
      applied  methods.
      
      Optionally takes parameters [size] and
      [variant_size] to be used for annotating a newly-minted ADT, in the case
      that the partial [typrep] this method is applied to was empty. *)
  val add_variant :
    ?size:enum_size -> ?variant_size:datakind_spec -> t -> t

  (** As the first operation following a call to [add_variant], mint a new,
      initially empty [variant] * to add to a partially-constructed ADT (which
      cannot be empty, as we require that [add_variant] must be called first) *)
  val set_tag : t -> int -> cons_name -> t

  (** Mark a partial typrep as fully defined, preventing any further changes *
      from being made, and allowing it to be extracted safely. * * As
      implemented, this method is idempotent, and does not check whether * its
      argument is already frozen. *)
  val freeze : t -> t

  (** Safely convert a [t] value to a [typrep option], requiring that it has
      been frozen. Attempts to re-normalize the order * of any LIFO-order
      elements (such as fields, or variants), to the extent that they are not
      expected to be re-normalized * by subsequent function calls. (The
      order-normalization logic is not fully correct and should be reviewed). *)
  val extract_opt : t -> typrep option

  (** Unsafely convert a [t] value to a [typrep]. Fails if an empty [t] is
      passed in, or if a presumed-impossible condition is encountered

      @raise Extract_empty
      @raise Nested_algebraic
      @raise Mistyped_zarith *)
  val extract_exn : t -> typrep
end

(** Module extension of [Int_size] that adds a method for conversion to the *
    appropriate [abstract_type] integer primitive *)
module Int_size_ext : sig
  include module type of Int_size

  val to_abstract_type : t -> abstract_type
end

module AbstractConstructor : sig
  (** Fully convert a [type_encoding] to a [typrep option] *)
  val parse : type_encoding -> typrep option
end
