open Codec.Json_ast
open Core

type string_kind = Uni | Hex | Any | Enum of string list [@@deriving sexp]

type schema_type =
  | SchemaInt of int * int
  | SchemaNumber
  | SchemaRef of string * typrep'
  | SchemaUnion of typrep' list
  | SchemaObject of (string * typrep') list
  | SchemaString of string_kind
  | SchemaArray of schema_type list
  | SchemaBool
  | SchemaNull
[@@deriving sexp]

and typrep' =
  | Untitled of { typ : schema_type; _desc : string option }
  | Titled of { title : string; typ : schema_type; _desc : string option }
[@@deriving sexp]

module Typrep' = struct
  let impute_integer min max =
    match min, max with
    | Some min, Some max -> SchemaInt (min, max)
    | _, _ -> assert false

  let impute_number min max =
    match min, max with None, None -> SchemaNumber | _, _ -> assert false

  let rec of_ident ~defs ident =
    match Dictionary.raw_lookup defs ident with
    | None -> assert false
    | Some elt -> of_element ~defs elt

  and of_element ~defs elt =
    let typ = to_schema_type ~defs elt and _desc = elt.description in
    match elt.title with
    | None -> Untitled { typ; _desc }
    | Some title -> Titled { title; typ; _desc }

  and to_schema_type ~defs elt =
    match elt.kind with
    | `Null -> SchemaNull
    | `Boolean -> SchemaBool
    | `Integer { minimum; maximum } -> impute_integer minimum maximum
    | `Number { minimum; maximum } -> impute_number minimum maximum
    | `Ref "unistring" -> SchemaString Uni
    | `Ref tgt -> SchemaRef (tgt, of_ident ~defs tgt)
    | `Union els -> SchemaUnion (List.map els ~f:(of_element ~defs))
    | `Array els -> SchemaArray (List.map els ~f:(to_schema_type ~defs))
    | `Object { properties; _ } ->
        SchemaObject
          (List.map properties ~f:(fun (name, typ) ->
               name, of_element ~defs typ))
    | `String { pattern } ->
        SchemaString
          (match elt.enum with
          | Some els -> Enum els
          | None -> (
              match pattern with
              | Some "^[a-zA-Z0-9]+$" -> Hex
              | Some _ -> assert false
              | None -> Any))
end
