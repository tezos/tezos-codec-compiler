open Codec.Json_ast

(** Sub-kind of a general schema string, inferred from context *)
type string_kind =
  | Uni  (** Unistring: any unicode string *)
  | Hex  (** Hexadecimal *)
  | Any  (** Not enough information to narrow *)
  | Enum of string list  (** String-enum with a fixed list of possible values *)
[@@deriving sexp]

(** Representation of a type defined in the JSON schema *)
type schema_type =
  | SchemaInt of int * int  (** Integer within a range *)
  | SchemaNumber  (** Standard JSON double-precision float *)
  | SchemaRef of string * typrep'
      (** Reference to another identifier, including its dereferenced value *)
  | SchemaUnion of typrep' list
      (** Union of at least one type (but we expect multiple) *)
  | SchemaObject of (string * typrep') list
      (** Object with a list of named fields *)
  | SchemaString of string_kind  (** String of a specific kind *)
  | SchemaArray of schema_type list
      (** Possibly heterogenous array whose element types are listed *)
  | SchemaBool  (** JSON literal [true]/[false] *)
  | SchemaNull  (** JSON literal [null] *)
[@@deriving sexp]

(** Annotated definition of a type in the JSON schema *)
and typrep' =
  | Untitled of { typ : schema_type; _desc : string option }
  | Titled of { title : string; typ : schema_type; _desc : string option }
[@@deriving sexp]

(** Module for generation of [typrep'] values from a [json_schema] *)
module Typrep' : sig
  val of_ident : defs:Dictionary.t -> string -> typrep'
end
