open Codec.Codec_repr
open Core

type rep_size =
  | FixedLength of int
  | VariableLength of width
  | DynamicLength
  | IndeterminateLength
[@@deriving sexp]

type bit_width = Bits8 | Bits16 | Bits32 | Bits64 [@@deriving sexp]

type sign = Signed | Unsigned [@@deriving sexp]

type float_precision = Single | Double [@@deriving sexp]

type byte_width = Bytes4 | Bytes8 [@@deriving sexp]

type abstract_type =
  | AbstractInt of sign * bit_width
  | AbstractIntRanged of (int * int) * byte_width
  | AbstractBytes of rep_size
  | AbstractString of rep_size
  | AbstractFloat of float_precision
  | AbstractBool
  | AbstractSequence of width option * abstract_type
  | AbstractTuple2 of abstract_type * abstract_type
  | AbstractNamed of width option * string
  | AbstractEnum of Int_size.t * string
  | AbstractVoid
  | AbstractOption of abstract_type
  | AbstractPadding of int
  | AbstractPseudo of size_repr
  | AbstractDyn of size_repr * abstract_type
  | AbstractTag of tag_size
  | AbstractTagVal of {
      base : string;
      tag_size : tag_size;
      elem : string;
      value : int;
    }
  | AbstractZarith_Z
  | AbstractZarith_N
[@@deriving sexp]

type cons_name = string option [@@deriving sexp]

type tag_val = int [@@deriving sexp]

type variant =
  | Enum_variant of cons_name * int
  | Type_variant of cons_name * int * typrep
[@@deriving sexp]

and typrep =
  | Anon of abstract_type
  | Record of (string * abstract_type) list
  | ADT of {
      tag_size : tag_size;
      variant_size : datakind_spec;
      variants : variant list;
    }
  | Enum of {
      enum_size : enum_size;
      elements : (cons_name * int) list
  }
[@@deriving sexp]

module Typrep = struct
  type state = Filling | Adding | Frozen [@@deriving sexp]

  type t = (typrep * state) option [@@deriving sexp]

  let empty = None

  let _show_contents mt = [%sexp_of: t] mt |> Sexp.to_string |> print_endline

  let _show_atyp atyp =
    [%sexp_of: abstract_type] atyp |> Sexp.to_string |> print_endline

  let rec fixed_size = function
    | AbstractBytes (FixedLength n) | AbstractString (FixedLength n) -> Some n
    | AbstractBytes _ | AbstractString _ -> None
    | AbstractZarith_Z | AbstractZarith_N -> None
    | AbstractFloat Single -> Some 4
    | AbstractFloat Double -> Some 8
    | AbstractBool -> Some 1
    | AbstractNamed _ -> None
    | AbstractVoid -> Some 0
    | AbstractPadding i -> Some i
    | AbstractTuple2 (fst, snd) ->
        Option.map2 (fixed_size fst) (fixed_size snd) ~f:( + )
    | AbstractPseudo size -> (
        match size with `Uint8 -> Some 1 | `Uint30 -> Some 4)
    | AbstractTag tag_size | AbstractTagVal { tag_size; _ } -> (
        match tag_size with `Uint8 -> Some 1 | `Uint16 -> Some 2)
    | AbstractOption _ | AbstractSequence _ | AbstractDyn _ -> None
    | AbstractInt (_, bits) ->
        let bytes =
          match bits with Bits8 -> 1 | Bits16 -> 2 | Bits32 -> 4 | Bits64 -> 8
        in
        Some bytes
    | AbstractIntRanged (_, bytes) ->
        Some (match bytes with Bytes4 -> 4 | Bytes8 -> 8)
    | AbstractEnum (is, _) ->
        Some
          (match is with
          | Uint8 | Int8 -> 1
          | Uint16 | Int16 -> 2
          | Uint32 | Int32 -> 4
          | Uint64 | Int64 -> 8)

  let rec reverse_fields ?(width = Some 0) ?(accum = []) rflds =
    match rflds with
    | [] -> accum
    | ((name, rep) as hd) :: tl -> (
        match rep with
        | AbstractSequence (None, typ) ->
            let hd' =
              ( name,
                AbstractSequence
                  (Option.map ~f:(fun w -> ReserveSuffix w) width, typ) )
            in
            reverse_fields ~width:None ~accum:(hd' :: accum) tl
        | AbstractBytes IndeterminateLength ->
            let hd' =
              ( name,
                AbstractBytes
                  (Option.value_map
                     ~default:IndeterminateLength
                     ~f:(fun w -> VariableLength (ReserveSuffix w))
                     width) )
            in
            reverse_fields ~width:None ~accum:(hd' :: accum) tl
        | AbstractString IndeterminateLength ->
            let hd' =
              ( name,
                AbstractString
                  (Option.value_map
                     ~default:IndeterminateLength
                     ~f:(fun w -> VariableLength (ReserveSuffix w))
                     width) )
            in
            reverse_fields ~width:None ~accum:(hd' :: accum) tl
        | _ ->
            reverse_fields
              ~width:(Option.map2 width (fixed_size rep) ~f:( + ))
              ~accum:(hd :: accum)
              tl)

  let reserve_zero = function
    | AbstractSequence (None, typ) ->
        AbstractSequence (Some (ReserveSuffix 0), typ)
    | AbstractBytes IndeterminateLength ->
        AbstractBytes (VariableLength (ReserveSuffix 0))
    | AbstractString IndeterminateLength ->
        AbstractString (VariableLength (ReserveSuffix 0))
    | rep -> rep

  let rec add_field mt ?name atyp =
    match mt with
    | None ->
        let f = function
          | "Tag" ->
              failwith
                "Adding Tag field on empty typerep! Should add a variant and \
                 set tag first!"
          | n -> Some (Record [ n, atyp ], Filling)
        in
        Option.value_map ~default:(Some (Anon atyp, Filling)) ~f name
    | Some (_, Frozen) -> failwith "Cannot add field to frozen typrep"
    | Some (Anon t, Filling) -> (
        match name with
        | Some n -> Some (Record [ n, atyp; "_anon", t ], Filling)
        | None -> (
            match t with
            | AbstractPseudo _ ->
                Some (Record [ "_anon", atyp; "_pseudo", t ], Filling)
            | AbstractTuple2 _ -> failwith "3Tuples unimplemented"
            | _ -> Some (Anon (AbstractTuple2 (t, atyp)), Filling)))
    | Some (Record rls, Filling) -> (
        match name with
        | None -> (
            (* REVIEW[epic=stopgap] - handle anonymous fields generated to
               preserve flatness *)
            match atyp with
            | AbstractNamed (_, tname) when String.is_prefix ~prefix:"X_" tname
              ->
                Some (Record ((tname, atyp) :: rls), Filling)
            | _ ->
                failwith
                  "Record field must have a name (is this really a record?)")
        | Some n -> Some (Record ((n, atyp) :: rls), Filling))
    | Some (Anon _, Adding) ->
        failwith
          "Cannot add new variant to an anonymous alias (is this really an \
           anonymous alias?)"
    | Some (Record _, Adding) ->
        failwith
          "Cannot add new variant to a simple record (is this really a record?)"
    | Some (ADT _, Adding) ->
        failwith
          "Cannot add field to ADT variant until tag is set (use set_tag)"
    | Some (ADT { variants = []; _ }, Filling) ->
        failwith "Cannot add field to nonexistent variant of empty ADT"
    | Some (Enum { enum_size = (`Uint16 | `Uint8) as size; elements }, Filling) ->
        add_field (Some (ADT { tag_size = (size :> tag_size); variant_size=DynamicKind; variants = List.map ~f:(fun (name, size) -> Enum_variant (name, size)) elements }, Filling)) ?name atyp
    | Some (Enum { enum_size = `Uint30; _ }, _) ->
      failwith "Cannot coerce 30-bit pure enum to ADT"
    | Some (Enum _, Adding) ->
      failwith "Cannot add field to converted enum without setting tag first"
    | Some (ADT ({ variants = hd :: tl; _ } as adt), Filling) -> (
        match atyp with
        | AbstractVoid -> Some (ADT adt, Adding)
        | _ -> (
            match hd with
            | Enum_variant (nm, i) ->
                let new_rep =
                  match name with
                  | None -> Anon atyp
                  | Some n -> Record [ n, atyp ]
                in
                Some
                  ( ADT
                      {
                        adt with
                        variants = Type_variant (nm, i, new_rep) :: tl;
                      },
                    Filling )
            | Type_variant (nm, i, Anon t) -> (
                match name with
                | None -> (
                    match t with
                    | AbstractTuple2 _ -> failwith "3Tuples unimplemented"
                    | _ ->
                        let new_rep = Anon (AbstractTuple2 (t, atyp)) in
                        Some
                          ( ADT
                              {
                                adt with
                                variants = Type_variant (nm, i, new_rep) :: tl;
                              },
                            Filling ))
                | Some n ->
                    let new_rep = Record [ n, atyp; "_anon", t ] in
                    Some
                      ( ADT
                          {
                            adt with
                            variants = Type_variant (nm, i, new_rep) :: tl;
                          },
                        Filling ))
            | Type_variant (nm, i, Record rfs) ->
                let name =
                  match name with
                  | None -> failwith "Record field must have name"
                  | Some name -> name
                in
                let new_field = name, atyp in
                let new_record = Record (new_field :: rfs) in
                let new_variant = Type_variant (nm, i, new_record) in
                let new_adt = ADT { adt with variants = new_variant :: tl } in
                Some (new_adt, Filling)
            | Type_variant (_, _, ADT _) ->
                failwith "Variant cannot contain another ADT"
            | Type_variant (_, _, Enum _) ->
                failwith "Variant cannot contain an enum"))

  let set_tag mt tag name =
    match mt with
    | Some (ADT ({ variants = rvars; _ } as adt), Adding) ->
        Some
          ( ADT { adt with variants = Enum_variant (name, tag) :: rvars },
            Filling )
    | None | Some (ADT _, Filling) ->
        failwith "Must add variant before setting tag"
    | Some (Enum ({ elements; _ } as enum), Adding) ->
      Some ( Enum { enum with elements = (name, tag) :: elements }, Filling)
    | Some (_, _) -> failwith "Cannot set tag of non-ADT typrep"

  let add_variant ?(size = `Uint8) ?(variant_size = DynamicKind) =
    let to_tag_size = function
      | (`Uint8 | `Uint16) as ts -> ts
      | _ -> assert false
    in
    function
    | None ->
      begin
      match size with
      | `Uint30 -> Some (Enum { enum_size = size; elements = [] }, Adding)
      | _ -> Some
          ( ADT { tag_size = to_tag_size size; variant_size; variants = [] }, Adding )
      end
    | Some ((ADT _ as adt), _) -> Some (adt, Adding)
    | Some _ -> failwith "Cannot add variant to non-ADT typrep"

  let freeze = function
    | None -> failwith "Cannot freeze None"
    | Some (x, _) -> Some (x, Frozen)

  exception Extract_empty

  exception Nested_algebraic

  exception Mistyped_zarith

  exception Not_frozen

  let extract_safe = function
    | None -> Error Extract_empty
    | Some (_, (Filling | Adding)) -> Error Not_frozen
    | Some (Enum enum, Frozen) ->
        (* REVIEW[epic=design] - keeping variants in reverse order because they
           get reversed again by the foreign code generator *)
        Ok (Enum enum)
    | Some (ADT ({ variants = rvars; _ } as adt), Frozen) ->
        (* REVIEW[epic=design] - keeping variants in reverse order because they
           get reversed again by the foreign code generator *)
        let reverse_variant = function
          | (Enum_variant _ | Type_variant (_, _, Anon _)) as a -> Ok a
          | Type_variant (_, _, ADT _) -> Error Nested_algebraic
          | Type_variant (_, _, Enum _) -> Error Nested_algebraic
          | Type_variant (n, i, Record rfs) ->
              Ok (Type_variant (n, i, Record (reverse_fields rfs)))
        in
        let vars = Result.all @@ List.map rvars ~f:reverse_variant in
        Result.map ~f:(fun variants -> ADT { adt with variants }) vars
    (* REVIEW[epic=stopgap] - converting wrapped zariths to aliased zariths *)
    | Some (Record [ ("N.t", AbstractZarith_N) ], Frozen) ->
        Ok (Anon AbstractZarith_N)
    | Some (Record [ ("Z.t", AbstractZarith_Z) ], Frozen) ->
        Ok (Anon AbstractZarith_Z)
    | Some (Record [ (("N.t" | "Z.t"), _) ], Frozen) -> Error Mistyped_zarith
    | Some (Record rfs, Frozen) -> Ok (Record (reverse_fields rfs))
    | Some (Anon v, Frozen) -> Ok (Anon (reserve_zero v))

  let extract_opt mt = extract_safe mt |> Result.ok

  let extract_exn mt = freeze mt |> extract_safe |> Result.ok_exn
end

module Int_size_ext : sig
  include module type of Int_size

  val to_abstract_type : t -> abstract_type
end = struct
  include Int_size

  let to_abstract_type = function
    | Uint8 -> AbstractInt (Unsigned, Bits8)
    | Uint16 -> AbstractInt (Unsigned, Bits16)
    | Uint32 -> AbstractInt (Unsigned, Bits32)
    | Uint64 -> AbstractInt (Unsigned, Bits64)
    | Int8 -> AbstractInt (Signed, Bits8)
    | Int16 -> AbstractInt (Signed, Bits16)
    | Int32 -> AbstractInt (Signed, Bits32)
    | Int64 -> AbstractInt (Signed, Bits64)
end

let rec to_abstract_type ?width ?data_kind layout =
  match layout, data_kind with
  | `Zero_width, _ -> AbstractVoid
  | `Int is, _ -> Int_size_ext.to_abstract_type is
  | `Bool, _ -> AbstractBool
  | `RangedInt (min, max), kind -> (
      match kind with
      | Some (FloatKind 8) -> AbstractIntRanged ((min, max), Bytes8)
      | Some (FloatKind 4) -> AbstractIntRanged ((min, max), Bytes4)
      | _ -> assert false)
  | `Float, kind -> (
      match kind with
      | Some (FloatKind 8) -> AbstractFloat Double
      | Some (FloatKind 4) -> AbstractFloat Single
      | Some (FloatKind n) ->
          failwith @@ Printf.sprintf "Unexpected float byte-width %d" n
      | Some _ -> failwith "Unexpected non-fixed-length float"
      | _ -> failwith "Unexpected unknown-length float")
  | (`Bytes | `String), kind -> (
      let length =
        match kind with
        | Some VariableKind -> (
            match width with
            | None -> IndeterminateLength
            | Some w -> VariableLength w)
        | Some (FloatKind n) -> FixedLength n
        | Some DynamicKind -> DynamicLength
        | None -> IndeterminateLength
      in
      match layout with
      | `Bytes -> AbstractBytes length
      | `String -> AbstractString length
      | _ -> assert false)
  | `Ref name, _ -> (
      match name with
      (* NOTE[epic=design] - reference-level Zarith patching happens here *)
      | "N.t" -> AbstractZarith_N
      | "Z.t" -> AbstractZarith_Z
      | _ -> AbstractNamed (width, name))
  | `Seq layout, Some VariableKind ->
      AbstractSequence (width, to_abstract_type (Layout.widen layout))
  | `Padding, Some (FloatKind i) -> AbstractPadding i
  | `Enum (is, ref), _ -> AbstractEnum (is, ref)
  | `Padding, _ -> assert false
  | _ -> failwith "unhandled field format"

let get_field_type ?width field =
  match field with
  | NamedField { name = "Tag"; layout = `Int is; data_kind = FloatKind _ } ->
      let size =
        match Int_size.to_tag_size is with
        | Some sz -> sz
        | None -> assert false
      in
      AbstractTag size
  | NamedField { name = "Tag"; _ } -> assert false
  (* REVIEW[epic=stopgap] - field-level Zarith patching because ref-level isn't
     enough apparently *)
  | NamedField { name = "N.t"; _ } -> AbstractZarith_N
  | NamedField { name = "Z.t"; _ } -> AbstractZarith_Z
  | NamedField { layout; data_kind; _ } | AnonField { layout; data_kind; _ } ->
      to_abstract_type layout ?width ~data_kind
  | OptionalField _ -> AbstractBool
  | DynField { width; _ } -> AbstractPseudo width

let get_field_name = function NamedField { name; _ } -> Some name | _ -> None

module AbstractConstructor = struct
  type memo =
    | DynMemo of string option * int * size_repr
    | OptionMemo of string
  [@@deriving sexp]

  type u =
    | Spec of type_encoding
    | Fields of field_spec list
    | Cases of tagtype_repr list
    | Elems of (string * int) list

  type t = u * Typrep.t * memo list

  let create t_enc = Spec t_enc, Typrep.empty, []

  let get_typrep ((q, o, s) : t) : typrep option =
    match q, s with
    | (Fields [] | Cases [] | Elems []), [] -> Typrep.(extract_opt @@ freeze o)
    | _ -> None

  let get_type_memo fld stack =
    let rec _get_type_memo ?(nfields = 1) ?typ fld stack =
      match stack with
      | [] -> Option.value ~default:(get_field_type fld) typ, stack
      | hd :: tl -> (
          match hd with
          | DynMemo (fname, n, sz) when n = nfields -> (
              match nfields with
              | 1 ->
                  let width = DynPrefix sz in
                  let typ, stack' =
                    match fld, fname with
                    | NamedField { name; _ }, Some name' ->
                        if String.(name = name') then
                          get_field_type ~width fld, tl
                        else
                          (* REVIEW[epic=stopgap] - ignoring mismatch in prefix
                             field name and actual field name until patch *)
                          get_field_type ~width fld, tl
                          (* failwith (Printf.sprintf "Dynamic pseudo-field for
                             field name \"%s\" followed by field name \"%s\""
                             name' name) *)
                    | (NamedField _ | AnonField _), None ->
                        get_field_type ~width fld, tl
                    | AnonField _, Some _ ->
                        failwith
                          "Dynamic psuedo-field with name followed by field \
                           without name"
                    | _, _ ->
                        failwith
                          "Dynamic pseudo-field followed by another \
                           pseudo-field"
                  in
                  _get_type_memo ~nfields:(nfields + 1) ~typ fld stack'
              | m when m > 1 -> (
                  match typ with
                  | None ->
                      failwith "impossible case: nfields > 1 but typ is unset"
                  | Some typ -> AbstractDyn (sz, typ), tl)
              | _ -> assert false)
          | DynMemo (_, _, _) -> failwith "Dynamic field count does not match"
          | OptionMemo fname -> (
              match fld with
              | NamedField { name; _ } when String.(fname = name) ->
                  AbstractOption (get_field_type fld), tl
              | NamedField { name; _ } ->
                  failwith
                    (Printf.sprintf
                       "Optional pseudo-field for field name \"%s\" followed \
                        by field name \"%s\""
                       fname
                       name)
              | AnonField _ ->
                  failwith
                    (Printf.sprintf
                       "Optional pseudo-field for field name \"%s\" followed \
                        by anonymous field"
                       fname)
              | _ ->
                  failwith
                    "Optional pseudo-field followed by another pseudo-field"))
    in
    _get_type_memo fld stack

  let rec _push_cases ?(current : field_spec list option)
      ((fields, tr, stack) as x) =
    match current, fields with
    | _, (Spec _ | Fields _ | Elems _) -> assert false
    | None, Cases [] -> x, false
    | None, Cases ({ tag; fields; name } :: tl) ->
        let tr' = Typrep.(set_tag (add_variant tr) tag name) in
        _push_cases ~current:fields (Cases tl, tr', stack)
    | Some [], _ -> x, true
    | Some (hd :: tl), _ -> (
        match hd with
        | OptionalField { name } ->
            _push_cases ~current:tl (fields, tr, OptionMemo name :: stack)
        | DynField { name; num_fields; width } -> (
            match num_fields with
            | n when n > 0 ->
                _push_cases
                  ~current:tl
                  (fields, tr, DynMemo (name, num_fields, width) :: stack)
            | _ -> assert false)
        | NamedField { name = "Tag"; _ } ->
            _push_cases ~current:tl (fields, tr, stack)
        | NamedField _ | AnonField _ ->
            let name = get_field_name hd
            and typ, stack' = get_type_memo hd stack in
            let tr' = Typrep.(add_field tr ?name typ) in
            _push_cases ~current:tl (fields, tr', stack'))

  let rec _push_elems ((queue, tr, stack) as x) =
    match queue with
    | Spec _ | Fields _ | Cases _ -> assert false
    | Elems [] -> x, false
    | Elems ((elem_tag, elem_val) :: tl) ->
        let tr' = Typrep.(set_tag (add_variant tr) elem_val (Some elem_tag)) in
        _push_elems (Elems tl, tr', stack)

  let push ((fields, tr, stack) as x) =
    match fields with
    | Fields [] | Cases [] | Elems [] -> (
        match stack with
        | [] -> x, false
        | _ -> failwith @@ Sexp.to_string @@ [%sexp_of: memo list] stack)
    | Spec (Fielded { fields }) -> (Fields fields, tr, stack), true
    | Spec (Tagged { tag_size; kind; cases }) ->
        let size = (tag_size :> enum_size) in
        let variant_size = kind in
        ( (Cases cases, Typrep.add_variant ~size ~variant_size tr, stack),
          true )
    | Spec (Enumerated { size = tag_size; cases = elems }) ->
        let size = (tag_size :> enum_size) in
        let variant_size = FloatKind 0 in
        ( (Elems elems, Typrep.add_variant ~size ~variant_size tr, stack),
          true )
    | Elems _ -> _push_elems x
    | Cases _ -> _push_cases x
    | Fields flds -> (
        match flds with
        | [] -> assert false
        | OptionalField { name } :: tl ->
            (Fields tl, tr, OptionMemo name :: stack), true
        | DynField { name; num_fields; width } :: tl -> (
            match num_fields with
            | n when n > 0 ->
                ( (Fields tl, tr, DynMemo (name, num_fields, width) :: stack),
                  true )
            | _ -> assert false)
        | NamedField { name = "Tag"; _ } :: _ ->
            failwith "Tag only possible for Cases (we are in fields)"
        | hd :: tl ->
            let name = get_field_name hd
            and typ, stack' = get_type_memo hd stack in
            let tr' = Typrep.add_field ?name tr typ in
            (Fields tl, tr', stack'), true)

  let rec construct t =
    match push t with t', true -> construct t' | t', false -> get_typrep t'

  let parse fields = construct @@ create fields
end
