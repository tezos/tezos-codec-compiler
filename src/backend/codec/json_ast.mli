(** Type used to represent elements of a particular schema's definition table *)
type schema_element = {
  title : string option;
  description : string option;
  kind : schema_kind;
  enum : schema_enum list option;
}

(** Abstract representation of a schema type *)
and schema_kind =
  [ `Integer of numeric_specs
  | `Number of numeric_specs
  | `Ref of string
  | `Union of schema_element list
  | `Object of object_specs
  | `String of string_specs
  | `Array of schema_element list
  | `Boolean
  | `Null ]

(** Abstract representation of an enum member in the JSON schema *)
and schema_enum = string

(** Specification of a numeric type in the JSON schema *)
and numeric_specs = { minimum : int option; maximum : int option }

(** Specification of an object type in the JSON schema *)
and object_specs = {
  properties : (string * schema_element) list;
  required : string list option;
  additional_properties : bool option;
}

(** Specification of a string type in the JSON schema *)
and string_specs = { pattern : string option }

(** Lookup table used for the definitions listed in the JSON schema *)
module Dictionary : sig
  (** Definition associated with a particular identifier *)
  type u = schema_element

  (** Structure type containing identifier-definition associations *)
  type t

  (** Create an empty definition table *)
  val init : unit -> t

  (** Return the number of entries in a definition table *)
  val size : t -> int

  (** Return an updated table including the specified definition *)
  val add_entry : t -> string -> u -> t

  (** Return the direct definition of a given identifier in a table *)
  val raw_lookup : t -> string -> u option

  (** Return the dereferenced definition of a given identifier *)
  val lookup : t -> string -> u option
end

(** Convert a raw identifier in the JSON schema to its corresponding base-name *)
val to_ident : string -> string

(** Representation of the JSON schema associated with a top-level identifier *)
type json_schema = { toplevel : string; definitions : Dictionary.t }
