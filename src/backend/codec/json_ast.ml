type schema_element = {
  title : string option;
  description : string option;
  kind : schema_kind;
  enum : schema_enum list option;
}

and schema_kind =
  [ `Integer of numeric_specs
  | `Number of numeric_specs
  | `Ref of string
  | `Union of schema_element list
  | `Object of object_specs
  | `String of string_specs
  | `Array of schema_element list
  | `Boolean
  | `Null ]

and schema_enum = string

and numeric_specs = { minimum : int option; maximum : int option }

and object_specs = {
  properties : (string * schema_element) list;
  required : string list option;
  additional_properties : bool option;
}

and string_specs = { pattern : string option }

module Dictionary = struct
  module M = Map.Make (String)

  type u = schema_element

  type t = u M.t

  let init () = M.empty

  let size dict = M.to_seq dict |> Seq.fold_left (fun acc _ -> acc + 1) 0

  let add_entry table key value = M.add key value table

  let raw_lookup dict key = M.find_opt key dict

  let rec lookup dict key =
    match raw_lookup dict key with
    | Some { kind = `Ref key'; _ } -> lookup dict key'
    | _ as v -> v
end

let to_ident = Core.String.chop_prefix_if_exists ~prefix:"#/definitions/"

type json_schema = { toplevel : string; definitions : Dictionary.t }
