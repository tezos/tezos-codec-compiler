codec
=====

This directory contains the modules belonging to the sub-library `codec` of the
top-level compiler library `backend`.

The modules included in this library define several primitive and composite
type-definitions, as well as the conversion function `json_to_ast`, which
together constitute the target representation and conversion pipeline for the
first compiler phase, known as *schema parsing*.

Schema Parsing
--------------

The *schema parsing* phase is processed by calling the function `json_to_ast`
from `ast_converter.ml`, which takes as input a codec snapshot and parses it
from JSON (using the `yojson` library) into a value of type `codec_tree`,
defined in `codec_repr.ml`. Said AST cannot be empty (as empty codecs are
considered invalid by the compiler), and must consist of at least one
encoding-representation.

Codec Schema Format
-------------------

As this compiler's input format is implicitly pinned to the output format of the
`octez` command `tezos-codec dump encodings`, it may be liable to change at any
point if the implementation of `octez` or `data-encoding` change in such a way
as to alter the output of said command.

As of the most recent version of `octez`, the codec format is a single JSON
array consisting of a number of *schemata* (singular: *schema*) resembling the
following (an actual schema from the real codec):

```json
{ "id": "ground.bytes",
  "json":
    { "$schema": "http://json-schema.org/draft-04/schema#",
      "$ref": "#/definitions/ground.bytes",
      "definitions":
        { "ground.bytes": { "type": "string", "pattern": "^[a-zA-Z0-9]+$" } } },
  "binary":
    { "toplevel":
        { "fields":
            [ { "kind": "dyn", "num_fields": 1, "size": "Uint30" },
              { "layout": { "kind": "Bytes" }, "kind": "anon",
                "data_kind": { "kind": "Variable" } }
	    ]
	}
    , "fields": []
    }
}
```

In particular, each contain the following:
  - An `"id"` field, which should be unique schema identifier within that codec
  snapshot
  - A `"json"` field, consisting of a `json-schema` object with a schema version
  reference, a toplevel entrypoint identifier, and a `"defintions"` field, which
  contains a dictionary mapping type-identifiers to the JSON representation of
  their constituent values. In the case shown above, only the toplevel
  definition is present, as the type represented by the schema is representable
  using only `data-encoding` primitive types (specifically the `bytes`
  encoding); in other cases, however, additional definitions may be provided,
  each of which must be traceable back to the toplevel identifier through chains
  of references.
  - A `"binary"` field, containing an object with two fields: - `"toplevel"`,
  which contains a representation of the type the schema is associated with -
  `"fields"`, which contains a possibly-empty list of subordinate types that, as
  in the case of `"json"`, should (but may not always, due to known bugs)
  correspond to the inductive set of named references to non-primitive types
  with the `"toplevel"` type as the origin of structural induction

Codec AST Format
----------------

Each encoding-representation contains the following:
    - An identifier, which is the unmodified string-value associated with the
    `"id"` field
    - A dictionary produced from the JSON schema representation, along with a
    toplevel entrypoint identifier
    - An AST containing nominally isomorphic representations to those parsed
    from the JSON field `"binary"`

The model used for the binary representations closely matches the model
implemented in `data-encoding`, with subtle modifications. The differences in
implementation between the two are not expected to cause any compiler failure at
this time, but changes made to `data-encoding` may necessitate changes in the
AST model used within this library.

While this need could be obviated by adding `data-encoding` as a dependency, and
directly using the AST defined there, this has numerous disadvantages over the
current approach. 
  - Locally re-implementing the codec AST guarantees that it is never changed in
  complete isolation to the downstream compiler components that would be affected by 
  those changes, which cannot be guaranteed in the case of a direct import
  - Conversely, changes to the codec AST that are appropriate for this project
  in particular, may not be appropriate to implement in `data-encoding`, or might 
  require API-breaking changes that would be undesirable from the
  perspective of the `data-encoding` library
  - The design of the codec AST used locally is only nominally isomorphic to the
  codec AST used in `data-encoding`; specifically, certain distinctions that are
  made in `data-encoding` can be erased by abstraction for the purposes of
  simplifying compiler logic, and certain distinctions added to the local codec
  AST may otherwise require breaking the `data-encoding` API to implement upstream 
  - The version of `data-encoding` used to generate a given codec snapshot will always
  be the version of `data-encoding` used by the build of `octez` that generated said
  snapshot, which will always trail the most recent version of `data-encoding`. Allowing snapshots built with older versions of `octez` (and therefore of `data-encoding`) to still be compiled even if `data-encoding` itself changes
  allows the compiler to achieve greater portability and backwards-compatability
