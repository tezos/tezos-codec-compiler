open Codec_repr

exception Empty_input_json_array

exception Illegal_json_input of Yojson.Safe.t

exception Nested_sequence

(** Parse a JSON structure describing a codec into a corresponding [codec_tree].
    * * If the JSON structure is a multi-element array, it is treated as a codec
    containing multiple schemata. * * If the JSON structure is an object or a
    singleton array, it is treated as codec consisting of a single schema. *

    @raise [Empty_input_json_array] When called on an empty JSON array
    @raise [Illegal_json_input] When called on a JSON structure is neither an
    array nor an object
    @raise [Nested_sequence] When a Seq is nested in another Seq directly in the
    JSON codec
    @raise [Invalid_int_size] When an unsupported Int_size is encountered
    @raise [Type_error] When a value in the JSON structure has an unexpected
    type
    @raise [Failure] When an internal parser encounters an unsupported value in
    a certain field *)
val json_to_ast : Yojson.Safe.t -> codec_tree
