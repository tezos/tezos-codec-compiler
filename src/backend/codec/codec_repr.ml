open Core

type size_repr = [ `Uint8 | `Uint30 ] [@@deriving sexp]

type tag_size = [ `Uint8 | `Uint16 ] [@@deriving sexp]

type enum_size = [ size_repr | tag_size ] [@@deriving sexp]

type width = DynPrefix of size_repr | ReserveSuffix of int [@@deriving sexp]

type subtype_desc = { title : string; _description : string option }
[@@deriving sexp]

type datakind_spec = VariableKind | DynamicKind | FloatKind of int
[@@deriving sexp]

module Int_size : sig
  type t = Uint8 | Uint16 | Uint32 | Uint64 | Int8 | Int16 | Int32 | Int64
  [@@deriving sexp]

  exception Invalid_int_size of string

  val of_string_opt : string -> t option

  val of_string_exn : string -> t

  val to_size_repr : t -> size_repr option

  val to_tag_size : t -> tag_size option

  val to_enum_size : t -> enum_size option
end = struct
  type t = Uint8 | Uint16 | Uint32 | Uint64 | Int8 | Int16 | Int32 | Int64
  [@@deriving sexp]

  exception Invalid_int_size of string

  let of_string_opt = function
    | "Uint8" -> Some Uint8
    | "Uint16" -> Some Uint16
    | "Uint32" -> Some Uint32
    | "Uint64" -> Some Uint64
    | "Int8" -> Some Int8
    | "Int16" -> Some Int16
    | "Int32" -> Some Int32
    | "Int64" -> Some Int64
    | _ -> None

  let of_string_exn s =
    match of_string_opt s with
    | Some is -> is
    | None -> raise (Invalid_int_size s)

  let to_size_repr = function
    | Uint8 -> Some (`Uint8 :> size_repr)
    | Uint32 -> Some (`Uint30 :> size_repr)
    | _ -> None

  let to_tag_size = function
    | Uint8 -> Some (`Uint8 :> tag_size)
    | Uint16 -> Some (`Uint16 :> tag_size)
    | _ -> None

  let to_enum_size = function
    | Uint8 -> Some (`Uint8 :> enum_size)
    | Uint16 -> Some (`Uint16 :> enum_size)
    | Uint32 -> Some (`Uint30 :> enum_size)
    | _ -> None
end

type int_size = Int_size.t [@@deriving sexp]

module Layout : sig
  type t' =
    [ `Zero_width
    | `Int of int_size
    | `Bool
    | `RangedInt of int * int
    | `RangedFloat of float * float
    | `Float
    | `Bytes
    | `String
    | `Enum of int_size * string
    | `Ref of string
    | `Padding ]
  [@@deriving sexp]

  type t = [ `Seq of t' | t' ] [@@deriving sexp]

  val narrow : t -> t'

  val widen : t' -> t
end = struct
  type t' =
    [ `Zero_width
    | `Int of int_size
    | `Bool
    | `RangedInt of int * int
    | `RangedFloat of float * float
    | `Float
    | `Bytes
    | `String
    | `Enum of int_size * string
    | `Ref of string
    | `Padding ]
  [@@deriving sexp]

  type t = [ `Seq of t' | t' ] [@@deriving sexp]

  let narrow : t -> t' = function
    | `Seq _ -> assert false
    | ( `Zero_width | `Int _ | `Bool | `RangedInt _ | `RangedFloat _ | `Float
      | `Bytes | `String | `Enum _ | `Ref _ | `Padding ) as x ->
        x

  let widen x = (x :> t)
end

type layout_spec = Layout.t [@@deriving sexp]

type tagtype_repr = {
  tag : int;
  fields : field_spec list;
  name : string option;
}
[@@deriving sexp]

and type_encoding =
  | Tagged of {
      tag_size : tag_size;
      kind : datakind_spec;
      cases : tagtype_repr list;
    }
  | Fielded of { fields : field_spec list }
  | Enumerated of { size : enum_size; cases : (string * int) list }
[@@deriving sexp]

and subtype_spec = { desc : subtype_desc; encoding : type_encoding }
[@@deriving sexp]

and field_spec =
  | DynField of { name : string option; num_fields : int; width : size_repr }
  | NamedField of {
      name : string;
      layout : layout_spec;
      data_kind : datakind_spec;
    }
  | AnonField of { layout : layout_spec; data_kind : datakind_spec }
  | OptionalField of { name : string }
[@@deriving sexp]

and binary_spec = { toplevel : type_encoding; subtypes : subtype_spec list }
[@@deriving sexp]

and codec_type = {
  ident : string;
  _json : (Json_ast.json_schema[@sexp.opaque]);
  binary : binary_spec;
}
[@@deriving sexp]

and codec_tree = Many of codec_type list | Single of codec_type
[@@deriving sexp]
