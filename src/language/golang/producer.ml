(* open Core open Common.Language_repr open Common.Language_converter open
   Backend.Foreign open Backend.Foreign.Common open Backend.Foreign.Foreign_ast
   open Backend.Codec.Codec_repr

   let unimplemented () = failwith "unimplemented" module Producer : Productions
   = functor (L : Backend) (A : Foreign_ast.Ast_intf) -> struct include
   ProducerCore let produce_directive ~ctxt = function | ImportAll {
   qualified_name = Some q; source_file } -> ctxt, L.import_all q source_file |
   ImportBindings { bindings; source_file } -> ctxt, L.import bindings
   source_file | _ -> failwith @@ "Unsupported directive syntax for language " ^
   L.lang_props.language_name

   let rec produce_type ~(ctxt : ctxt) : [> Foreign_type.t ] -> ctxt * string =
   function | `Object ls -> let ctxt', fields = seq produce_field ~ctxt ls in
   ctxt', L.make_fields fields |> L.join_fields ~inline:true | `Atom t -> let t'
   = match (t :> [ Foreign_type.atom | `TParam of string | `TLiteral of string |
   `TNarrow of (Qualification.t * Ident.type_id) * Ident.enum_id ]) with | `TNat
   nat -> L.t_nat nat | `TPrim prim -> L.t_prim prim | `TComposite comp ->
   L.t_composite_of (ign produce_type ~ctxt) comp | `TParam ident -> ident |
   `TRefer (`RTOpaque (qual,base_name) | `RTEnum ((qual,base_name),_)) -> let
   base_name', requal = ProducerContext.requalify ~base_name ~qual ~ctxt
   ~debug:false () in let qual_q, common = Qualification.get_rel_prefix
   ctxt.scope requal in let qualified_typename = L.qualify_ident ~q:qual_q
   ~common base_name' in (* let show_q = Qualification.to_string in
   Stdio.eprintf "[Ref]: %s:%s => %s:%s => %s\n" (show_q qual) base_name (show_q
   requal) base_name' qualified_typename; *) qualified_typename | `TLiteral
   verbatim -> verbatim | `TNarrow ((qual,base_name),narrow) -> let base_name',
   requal = ProducerContext.requalify ~base_name ~qual ~ctxt ~debug:false () in
   let qual_q, common = Qualification.get_rel_prefix ctxt.scope requal in let
   qualified_typename = L.qualify_ident ~q:qual_q ~common base_name' in
   L.narrow_enum qualified_typename narrow in ctxt, t'

   and produce_field ~ctxt (label, t) = let ctxt', t' = produce_type ~ctxt t in
   ctxt', (L.remap_keyword label, t')

   and produce_elem ~ctxt (name, value) = ctxt, (L.remap_keyword name,
   Int.to_string value)

   let produce_typedef ~ctxt = function | `Union ts -> let ctxt', members = seq
   produce_type ~ctxt ts in ctxt', L.type_union ~tag:tt' members | Enum elems ->
   let ctxt', elems' = seq produce_elem ~ctxt elems in ctxt', `MultiLine
   (L.make_enum_elems elems') | Simple (Object ls) -> let ctxt', fields = seq
   produce_field ~ctxt ls in ctxt', `MultiLine (L.make_fields fields) | Simple
   (Atom _ as t) -> let ctxt', t' = produce_type ~ctxt t in ctxt', `Line t' | _
   -> assert false

   let produce_param ~ctxt = function | varname, Mandatory, t -> let name' =
   L.remap_keyword varname in map (opt produce_type) ~f:(fun t' -> name', t')
   ~ctxt t | _ -> failwith "unhandled type of argument found for produce_param"

   let rec produce_value ~(ctxt : ctxt) = let open Foreign_val in function |
   Paren t -> let t' = ign produce_value ~ctxt t in ctxt, L.group_expr t' |
   BinaryOp (op, x, y) -> let x' = ign produce_value ~ctxt x and y' = ign
   produce_value ~ctxt y in ctxt, L.binary_op op x' y' | UnaryOp (op, x) -> (
   let x' = ign produce_value ~ctxt x in ( ctxt, match op with | BooleanNegate
   -> L.bool_negate x' | CallMethod { meth; args } -> let args' = ign (seq
   produce_value) ~ctxt args in L.call_method ~on:x' ~meth ~args:args' | Access
   (Field lab) -> L.at_field ~lab:(L.remap_keyword lab) x' | Access (Index ix)
   -> L.at_index ~ix:(ign produce_value ~ctxt ix) x' | Access (KeyVal key) ->
   L.at_key ~key:(ign produce_value ~ctxt key) x' | Access _ -> failwith
   "Encountered unhandled case in produce_value case logic \ @UnaryOp->Access" |
   Coerce (Cast t) -> let t' = ign produce_type ~ctxt t in L.type_cast ~expr:x'
   ~typ:t' | Coerce (Conv t) -> let t' = ign produce_type ~ctxt t in L.type_conv
   ~expr:x' ~typ:(t', t) )) | CreateParser { args } -> let args' = ign (seq
   produce_value) ~ctxt args in ctxt, L.create_parser ~from:args' |
   ConstructorCall { qual; f; generics; args } -> let args' = ign (seq
   produce_value) ~ctxt args and generics' = ign (opt (seq produce_type)) ~ctxt
   generics and qual_q, common = Qualification.get_rel_prefix ctxt.scope qual in
   let qualified_name = L.qualify_ident ~q:qual_q ~common f in ctxt, L.create
   ~what:qualified_name ?generics:generics' ~args:args' () | ConcatStrings strs
   -> let strs' = ign (seq produce_value) ~ctxt strs in ctxt, L.concat_strings
   strs' | FunApp { qual; f; generics; args } -> let args' = ign (seq (seq
   produce_value)) ~ctxt args and generics' = ign (opt (seq produce_type)) ~ctxt
   generics and qual_q, common = Qualification.get_rel_prefix ctxt.scope qual in
   let qualified_name = L.qualify_ident ~q:qual_q ~common f in ( ctxt,
   L.curried_funcall ~args:args' ~f:qualified_name ?generics:generics' ) |
   TranscoderCall { dir; for_type; generics; args } -> let args' = ign (seq (seq
   produce_value)) ~ctxt args and generics' = ign (opt (seq produce_type)) ~ctxt
   generics and dirstr = match dir with Encode -> "encoder" | Decode ->
   "decoder" in let pref = let for_type' = match for_type with | Atom (TRefer {
   qual; base_name }) -> let base_name', requal = ProducerContext.requalify
   ~base_name ~qual ~ctxt ~debug:false () in let qual_q, common =
   Qualification.get_rel_prefix ctxt.scope requal in let qualified_typename =
   L.qualify_ident ~q:qual_q ~common base_name' in (*let show_q =
   Qualification.to_string in Stdio.eprintf "[TC]: %s:%s => %s:%s => %s\n"
   (show_q qual) base_name (show_q requal) base_name' qualified_typename; *)
   Foreign_type.( Atom (TRefer { qual = []; base_name = qualified_typename })) |
   _ -> for_type in L.prefix_for for_type' in ( ctxt, L.curried_funcall
   ~args:args' ~f:(pref ^ dirstr) ?generics:generics' ) | Pure (VPrim prim) ->
   ctxt, L.v_prim prim | Pure (VLit (LitString str)) -> ctxt, L.v_lit_str str |
   Pure (VLit (LitArray arr)) -> map (seq produce_value) ~f:L.v_lit_arr ~ctxt
   arr | Pure (VLit (LitObject ({ explicit; _ } as fill))) -> let explicit = ign
   (seq produce_field_fill) ~ctxt explicit in ctxt, L.v_lit_obj { fill with
   explicit } | Pure Refl -> if L.lang_props.has_reflexive_value then ctxt,
   L.v_refl else failwith "language does not have reflexive value keyword" |
   Pure (Bound id) -> ctxt, Ident.Binding.to_string id |> L.remap_keyword | Pure
   (Ternary tern) -> let p', x', y' = ign (tup3 produce_value) ~ctxt tern in
   ctxt, L.ternary_expr p' x' y' | TestExpr (ValidAsEnum { candidate; enum_qual;
   enum_base_name; enum_values }) -> let q, common =
   Qualification.get_rel_prefix ctxt.scope enum_qual and _, candidate' =
   produce_value ~ctxt candidate in ( ctxt, L.valid_as_enum
   ~ref:(L.qualify_ident ~q ~common enum_base_name) ~vals:enum_values candidate'
   )

   and produce_field_fill ~ctxt = function | Assign (lab, v) -> map ~f:(fun v'
   -> Assign (L.remap_keyword lab, v')) produce_value ~ctxt v | NamedPun lab ->
   ctxt, NamedPun (L.remap_keyword lab)

   let rec produce_statement ~ctxt stmt = let open Statement in let
   produce_statement' ~ctxt = function | Basic (Return ret) -> let ret' = ign
   (opt produce_value) ~ctxt ret in ctxt, L.(terminate_phrase @@
   return_statement ret') | Basic (ThrowError value) -> let value' = ign
   produce_value ~ctxt value in ctxt, L.(terminate_phrase @@ throw_error (Some
   value')) | Basic (BareExpression value) -> let value' = ign produce_value
   ~ctxt value in ctxt, L.terminate_phrase value' | Branching (IfThenElse {
   test_expr; true_branch; false_branch }) -> let test_expr' = ign produce_value
   ~ctxt test_expr and true_branch' = ign produce_branch ~ctxt true_branch and
   false_branch' = ign produce_branch ~ctxt false_branch in ctxt, L.if_then_else
   test_expr' true_branch' (Some false_branch') | Branching (Switch { test_expr;
   cases; default }) -> let test_expr' = ign produce_value ~ctxt test_expr and
   cases' = let f = function | Monic v, stmts -> let v' = ign produce_value
   ~ctxt v and stmts' = ign (seq produce_statement) ~ctxt stmts in [ v' ],
   stmts' | Multic vs, stmts -> let vs' = ign (seq produce_value) ~ctxt vs and
   stmts' = ign (seq produce_statement) ~ctxt stmts in vs', stmts' in List.map
   cases ~f and default' = ign (opt (seq produce_statement)) ~ctxt default in
   ctxt, L.switch test_expr' cases' default' | Basic (AssignLocal { bind_name;
   derived; new_value }) -> let new_value = new_value |> L.pp_universal_val |>
   fun x -> Option.value_map ~default:x ~f:(fun d -> UnaryOp (Access d, x))
   derived in ( ctxt, L.( terminate_phrase @@ assign ~bind:(remap_keyword
   bind_name) ~value:(ign produce_value ~ctxt new_value)) ) | Basic
   (DeclareLocal { bind_name; mods; type_sig; initial_value }) -> let
   initial_value = initial_value |> Option.map ~f:L.pp_universal_val in ( ctxt,
   L.( terminate_phrase @@ create_bind ~mods ~bind_name:
   (Ident.Binding.Variable_name (remap_keyword bind_name)) ?type_sig:(ign (opt
   produce_type) ~ctxt type_sig) ~value:(ign (opt produce_value) ~ctxt
   initial_value)) ) | Reserved (InjectCode code) -> ctxt, code | Ephemeral _ ->
   assert false | Abstract _ -> assert false in L.pp_statement stmt |> map (seq
   produce_statement') ~ctxt ~f:(String.concat ~sep:"\n")

   and produce_branch ~ctxt = (* REVIEW[epic=design] - L.if_then_else takes
   lists, not contents, so the map is currently extraneous *) map (seq
   produce_statement) ~f:(fun x -> (* `MultiLine *) x) ~ctxt

   let rec produce_declaration ~ctxt decl = let rec produce_declaration' ~ctxt =
   function | mods, Define_namespace { namespace_name; contents } -> let ctxt' =
   ProducerContext.enter_namespace namespace_name ctxt in let ctxt'', output =
   seq produce_declaration ~ctxt:ctxt' contents in (
   ProducerContext.leave_namespace ctxt'', String.concat ~sep:"\n" @@
   L.wrap_namespace ~mods namespace_name output ) | mods, TypeDec { qual;
   base_name; definition } -> let base_name', requal = ProducerContext.requalify
   ~base_name ~qual ~ctxt () in let qual_q, common =
   Qualification.get_rel_prefix ctxt.scope requal in let qualified_name =
   L.generate_ident ~q:qual_q ~common base_name' in let kind = match definition
   |> L.pp_universal_type with | Atom _ -> Kind.Alias | Object _ -> Kind.Obj in
   let definition = ign produce_typedef ~ctxt definition in ctxt, L.define_type
   ~mods ~kind ~type_name:qualified_name ~definition | ( mods, FunDec { qual;
   base_name; definition = FunDef.(LocalFunc (KindLambda, { params; return_type;
   body })); } ) -> let base_name', requal = ProducerContext.requalify
   ~base_name ~qual ~ctxt () in let qual_q, common =
   Qualification.get_rel_prefix ctxt.scope requal in let val_name =
   L.generate_ident ~q:qual_q ~common base_name' in let outtype = ign (seq
   produce_type) ~ctxt return_type in let args = convert_funargs ~ft:(ign
   produce_type ~ctxt) ~fv:(ign produce_value ~ctxt) params and definition = ign
   (map (seq produce_statement) ~f:(fun x -> `MultiLine x)) ~ctxt body in let
   value = L.lambda ~args ~outtype ~definition and typ = None in ctxt,
   L.define_val ~mods ~val_name ?typ ~value | ( mods, FunDec { qual; base_name;
   definition = FunDef.( GlobalFunc (KindFunction, { params; return_type; body
   })); } ) -> let base_name', requal = ProducerContext.requalify ~base_name
   ~qual ~ctxt () in let qual_q, common = Qualification.get_rel_prefix
   ctxt.scope requal in let fun_name = L.generate_ident ~q:qual_q ~common
   base_name' in let args = convert_funargs ~ft:(ign produce_type ~ctxt)
   ~fv:(ign produce_value ~ctxt) params and definition = ign (map (seq
   produce_statement) ~f:(fun x -> `MultiLine x)) ~ctxt body and outtype = ign
   (seq produce_type) ~ctxt return_type in ctxt, L.define_fun ~mods ~args
   ~fun_name ~outtype ~definition | ( _, FunDec { qual; base_name; definition =
   FunDef.(LocalFunc _ | GlobalFunc _); } ) -> . in

   let decls = L.pp_declaration decl in map (seq produce_declaration')
   ~f:(String.concat ~sep:"\n") ~ctxt decls

   let produce_sourcefile ~ctxt { filename; preamble; contents } = let comment =
   L.comment @@ `Inline (Printf.sprintf "auto-generated %s source file %s"
   L.lang_props.language_name filename) in let ctxt', head_matter = seq
   produce_directive ~ctxt preamble in let ctxt'', payload = seq
   produce_declaration ~ctxt:ctxt' contents in ctxt'', comment :: (head_matter @
   payload)

   let produce_codebase ~ctxt = function | SingleFile srcfile ->
   produce_sourcefile ~ctxt srcfile | ManyFile [] -> assert false | ManyFile
   files -> map (seq produce_sourcefile) ~f:List.concat ~ctxt files end

   let gen_all (module L : Language_repr.Backend) ~(ctxt : ProducerContext.t)
   ?(preamble = L.preamble) ((name, rep), subs) = let open Producer (L) in let
   rescopes', bound_decls = convert_toplevel ~name ~rep ~ctxt:ctxt.rescopes subs
   in let ctxt = { ctxt with rescopes = rescopes' } in let langname =
   String.lowercase L.lang_props.language_name in let filename = L.(
   Printf.sprintf "langs/%s/compiled-%s/%s.%s" langname langname name
   lang_props.file_extension) in let ctxt', output = map produce_codebase
   ~f:(fun x -> filename, x) ~ctxt (SingleFile { filename; preamble =
   L.patch_imports name preamble; contents = bound_decls; }) in
   ProducerContext.reset_local ctxt', output *)
