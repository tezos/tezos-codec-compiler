open Core
open Common.Language_converter
open Representation.Abstract_type
open Foreign
open Foreign.Common
open Codec.Codec_repr
open Golang_ast

let to_receiver : F.recv -> V.t option = function
  | { F.binding = None; _ } -> None
  | { F.binding = Some rcv; _ } -> Some (name_to_var rcv)

let rec convert_abstract_type : abstract_type -> [ `Atom of GO_AST.T.atom ] =
  function
  | AbstractBool -> `Atom (`TPrim `PrimBool)
  | AbstractInt (s, b) -> `Atom (`TPrim (`PrimInt (s, b)))
  | AbstractIntRanged ((min, max), bytes) ->
      `Atom (`TPrim (`PrimIntRanged ((min, max), bytes)))
  | AbstractString (VariableLength w) ->
      `Atom (`TComposite (`CTDynWidth (w, `Atom (`TPrim `PrimString))))
  | AbstractBytes (VariableLength w) ->
      `Atom (`TComposite (`CTDynWidth (w, `Atom (`TPrim `PrimOpaqueBytes))))
  | AbstractBytes (FixedLength n) -> `Atom (`TPrim (`PrimOpaqueBytesFixedLen n))
  | AbstractSequence (w, elem) -> (
      (* REVIEW[epic=stopgap] - handle sequence of amorphous blobs as a blob
         itself *)
      match elem with
      | AbstractBytes (DynamicLength | IndeterminateLength) -> (
          match w with
          | None ->
              failwith
                "Both sequence and its elements have indeterminate byte-length"
          | Some width ->
              `Atom
                (`TComposite
                  (`CTDynWidth (width, `Atom (`TPrim `PrimOpaqueBytes)))))
      | AbstractBytes (VariableLength (ReserveSuffix _)) -> assert false
      | _ -> (
          match w with
          | Some width ->
              `Atom
                (`TComposite
                  (`CTLinear
                    (`LTSequence (width, (convert_abstract_type elem :> T.t)))))
          | _ -> assert false))
  | AbstractOption value ->
      `Atom (`TComposite (`CTOption (convert_abstract_type value :> T.t)))
  | AbstractNamed (mwidth, name) -> (
      let qualids, base_name = reformat name in
      let qual =
        let f = function
          | x when String.is_suffix x ~suffix:"_tag" -> Ident.Scope.Type_name x
          | x -> Ident.Scope.Inferred_scope x
        in
        List.map ~f qualids
      in
      let base = `Atom (`TRefer (`RTOpaque (qual, base_name))) in
      match mwidth with
      | None -> base
      | Some width -> `Atom (`TComposite (`CTDynWidth (width, base))))
  | AbstractDyn (sz, AbstractSequence (None, inner_typ)) ->
      convert_abstract_type (AbstractSequence (Some (DynPrefix sz), inner_typ))
  | AbstractDyn (sz, inner_type) ->
      `Atom
        (`TComposite
          (`CTDynWidth
            (DynPrefix sz, (convert_abstract_type inner_type :> T.t))))
  | AbstractPadding n -> `Atom (`TPrim (`PrimPadding n))
  (* REVIEW[epic=design] - We are treating AbstractVoid as zero bytes of padding *)
  | AbstractVoid -> `Atom (`TPrim (`PrimPadding 0))
  | AbstractZarith_N -> `Atom (`TPrim `PrimZarith_N)
  | AbstractZarith_Z -> `Atom (`TPrim `PrimZarith_Z)
  | AbstractBytes DynamicLength -> `Atom (`TPrim `PrimOpaqueBytes)
  | AbstractString DynamicLength -> `Atom (`TPrim `PrimString)
  | AbstractEnum (sz, name) ->
      let qualids, base_name = reformat name in
      let qual =
        let f x = Ident.Scope.Inferred_scope x in
        List.map ~f qualids
      in
      `Atom
        (`TRefer
          (`RTEnum
            ((qual, base_name), Option.value_exn (Int_size.to_enum_size sz))))
  | AbstractTuple2 _ -> assert false
  | AbstractFloat precision -> `Atom (`TPrim (`PrimFloat precision))
  | AbstractTagVal { base; _ } ->
      let qualids, base_name = reformat base in
      let qual =
        let f x = Ident.Scope.Inferred_scope x in
        List.map ~f qualids
      in
      `Atom (`TRefer (`RTOpaque (qual, base_name)))
  | other ->
      failwith
        ("unhandled type: " ^ Sexp.to_string ([%sexp_of: abstract_type] other))

(* FIXME[epic=backend] - gen_transcoder_call needs basis *)
let gen_transcoder_call ~dir ?rcv ?(apply = true) typ : GO_AST.V.t =
  let for_type = convert_abstract_type typ
  and final_args =
    match dir with
    | Decode when apply -> [ name_to_var "p" ]
    | Encode | Decode -> []
  in
  match for_type with
  | `Atom (`TPrim (`PrimPadding n)) -> (
      let args = int_to_fv n :: final_args in
      match dir with
      | Encode ->
          `FunApp
            (([ Ident.Scope.Namespace_name "primitive" ], "EncodePadding"), args)
      | Decode ->
          `FunApp
            (([ Ident.Scope.Namespace_name "primitive" ], "DecodePadding"), args)
      )
  | `Atom (`TPrim _) | `Atom (`TRefer _) -> (
      match rcv with
      | None ->
          failwith
            "gen_transcoder_call@golang: non-padding type requires a receiver"
      | Some operand -> (
          match dir with
          | Encode -> `UnaryOp (`CallMethod ("Encode", []), operand)
          | Decode ->
              `UnaryOp (`CallMethod ("Decode", [ name_to_var "p" ]), operand)))
  | `Atom (`TNat _) ->
      failwith "Unexpected nat-type in output of convert_abstract_type@golang"
  | `Atom (`TComposite (`CTDynWidth (w, inner_type))) -> (
      match rcv with
      | None ->
          failwith
            "gen_transcoder_call@golang: composite type Sequence requires a \
             receiver"
      | Some seq -> (
          let dyn : V.t =
            match w with
            | DynPrefix sz -> `Pure (`VPrim (`PrimWidth sz))
            | ReserveSuffix n ->
                `UnaryOp (`Conv (`Atom (`TPrim `PrimReserveSuffix)), int_to_fv n)
          in
          match dir with
          | Encode ->
              let args = dyn :: seq :: final_args in
              `FunApp
                ( ([ Ident.Scope.Namespace_name "primitive" ], "EncodeSequence"),
                  args )
          | Decode ->
              let gen = `Create inner_type in
              let args = dyn :: seq :: gen :: final_args in
              `FunApp
                ( ([ Ident.Scope.Namespace_name "primitive" ], "DecodeSequence"),
                  args )))
  | `Atom (`TComposite (`CTOption inner_type)) -> (
      match rcv with
      | None ->
          failwith
            "gen_transcoder_call@golang: non-padding type requires a receiver"
      | Some rcv -> (
          match dir with
          | Encode ->
              ignore (inner_type, rcv);
              failwith "Unimplemented backend: Golang Encode@Option"
          | Decode ->
              ignore (inner_type, rcv);
              failwith "Unimplemented backend: Golang Decode@Option"))
  | `Atom (`TComposite (`CTLinear (`LTSequence (w, inner_type)))) -> (
      match rcv with
      | None ->
          failwith
            "gen_transcoder_call@golang: non-padding type requires a receiver"
      | Some rcv -> (
          match dir with
          | Encode ->
              ignore (w, inner_type, rcv);
              failwith "Unimplemented backend: Golang Encode@Sequence"
          | Decode ->
              ignore (w, inner_type, rcv);
              failwith "Unimplemented backend: Golang Decode@Sequence"))
  | `Atom (`TComposite (`CTLinear _)) -> assert false

module Gen = struct
  module Context = GenContext

  let mk_ident = patch_ident
  (* let mk_legal = function | '-' -> "_" | ',' -> "_COMMA_" | '+' -> "_PLUS_" |
     '(' | ')' | ' ' -> "" | c -> String.of_char c in id |> String.concat_map
     ~sep:"" ~f:mk_legal *)

  let mk_ns ?prefix id = Qualification.mk_ns ?prefix id

  let transcoder_ident ~dir id =
    let join_id ~affix base = String.concat ~sep:"_" [ mk_ident base; affix ] in
    let affix = match dir with Encode -> "encoder" | Decode -> "decoder" in
    join_id id ~affix

  let enum_ident ~ix id =
    let anon_prefix = "ANON" in
    match id with
    | None -> anon_prefix ^ Int.to_string ix
    | Some name -> mk_ident name

  let tag_ident id =
    (* Printf.eprintf "Tagging ident: %s\n" id; *)
    let affix = "tag" in
    String.concat ~sep:"_" [ id; affix ]

  module Simple = struct
    let cast_record ?ix = function
      | Anon t ->
          let pseudo =
            match ix with
            | None -> "_anon"
            | Some n -> "_anon" ^ Int.to_string n
          in
          Record [ pseudo, t ]
      | Record _ as r -> r
      | _ -> assert false

    let add_tag ~tag_type_name ~tag_size ~tag_name tag =
      let tag_field =
        let name = mk_ident tag_name in
        ( "_tag",
          AbstractTagVal
            { base = tag_type_name; tag_size; elem = name; value = tag } )
      in
      fun x ->
        match cast_record ~ix:tag x with
        | Record rs -> Record (tag_field :: rs)
        | _ -> assert false

    (* REVIEW[epic=design] - AbstractVoid as 0-width padding *)
    let remove_padding =
      List.filter ~f:(function
          | _, (AbstractPadding _ | AbstractVoid) -> false
          | _ -> true)

    module Anon = struct
      let typedec ~qual ~base_name typ : binding_mods * D.t =
        let definition = (convert_abstract_type typ :> T.t) in
        Binding_mod.exposed_definite, `TypeDec (qual, base_name, definition)

      let encoder ~type_name typ =
        let dir = Encode
        and arg_name = "val"
        and return_type = [ `Atom (`TNat `NatString) ] in
        let base_name = "Encode" and qual = [] in
        let binding = Some arg_name and kind = `Value in
        let rcv = { F.binding; qual; type_name; kind } and params = `Typed [] in
        let body =
          [
            `Return (Some (gen_transcoder_call ?rcv:(to_receiver rcv) ~dir typ));
          ]
        in
        let definition = `TypeMethod (rcv, { F.params; return_type; body }) in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)

      let decoder ~type_name typ =
        let dir = Decode
        and arg_name = "input"
        and intype = `Atom (`TNat `NatDecoderInput) in
        let params = `Typed [ arg_name, Mandatory, intype ] in
        let base_name = "Decode" and qual = [] in
        let binding = Some "val" and kind = `Pointer in
        let rcv = { F.binding; qual; type_name; kind } in
        let body =
          [
            `Return (Some (gen_transcoder_call ?rcv:(to_receiver rcv) ~dir typ));
          ]
        and return_type = [ `Atom (`TNat `NatError) ] in
        let definition = `TypeMethod (rcv, { F.params; return_type; body }) in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)
    end

    module Record = struct
      let devoid = function AbstractVoid -> AbstractPadding 0 | x -> x

      module Field = struct
        let get_fields = function Record flds -> flds | _ -> []

        let get_names =
          List.map ~f:(fun (name, _) -> mk_ident (sanitize_label name))

        (* STUB[epic=critical] - broken implementation just so we can compile *)
        let gen_bind ~dir ~struct_name (name, rep) : S.t =
          match dir with
          | Encode -> (
              match rep |> devoid with
              | AbstractPadding _ as pad ->
                  let bind_name = mk_ident (sanitize_label name) in
                  `DeclareLocal
                    ( bind_name,
                      Binding_mod.local (),
                      Some (`Atom (`TNat `NatString)),
                      gen_transcoder_call ~dir pad |> Option.some )
              | _ as fld ->
                  let field_name = mk_ident (sanitize_label name) in
                  let bind_name = field_name in
                  let rcv =
                    `UnaryOp
                      (`Access (`Field field_name), name_to_var struct_name)
                  in
                  `DeclareLocal
                    ( bind_name,
                      Binding_mod.local (),
                      Some (`Atom (`TNat `NatString)),
                      gen_transcoder_call ~dir ~rcv fld |> Option.some ))
          | Decode -> (
              (* let parser = "p" in *)
              (* FIXME[epic=hardcoded] - we should accept make this a param *)
              match rep |> devoid with
              | AbstractPadding _ as pad ->
                  `BareExpression (gen_transcoder_call ~dir pad)
              | _ as fld ->
                  let field_name = mk_ident (sanitize_label name) in
                  let rcv' =
                    `UnaryOp
                      (`Access (`Field field_name), name_to_var struct_name)
                  in
                  let rcv =
                    match convert_abstract_type rep with
                    | `Atom (`TPrim _ | `TRefer _) ->
                        `UnaryOp (`Access `Reference, rcv')
                    | `Atom (`TComposite _) -> rcv'
                    | `Atom (`TNat _) -> assert false
                  in
                  let call = gen_transcoder_call ~dir ~rcv fld in
                  `ThrowResultIfError call)
      end

      let typedec ~qual ~base_name ?(has_tag = false) flds =
        let f (fname, ftype) =
          mk_ident (sanitize_label fname), (convert_abstract_type ftype :> T.t)
        in
        let foreign_fields = remove_padding flds |> List.map ~f in
        let definition =
          if has_tag then
            match foreign_fields with
            (* STUB[epic=critical] - fix tagging logic in Go *)
            | _ :: _ -> failwith "haven't handled this logic yet"
            | [] -> assert false
          else `Object foreign_fields
        in
        ( Binding_mod.exposed_definite,
          `TypeDec (qual, mk_ident base_name, definition) )

      let encoder ~type_name flds =
        let return_type = [ `Atom (`TNat `NatString) ] in
        let dir = Encode and arg_name = "val" in
        let base_name = "Encode" and qual = [] in
        let binding = Some arg_name and kind = `Value in
        let rcv = { F.binding; qual; type_name; kind } and params = `Typed [] in
        let body : S.t list =
          let names = Field.get_names flds
          and binds =
            List.map flds ~f:(Field.gen_bind ~dir ~struct_name:arg_name)
          in
          let ret : V.t = `ConcatStrings (List.map ~f:name_to_var names) in
          binds @ [ `Return (Some ret) ]
        in
        let definition = `TypeMethod (rcv, { F.params; return_type; body }) in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)

      let decoder ~type_name flds =
        let dir = Decode
        and arg_name = "input"
        and intype = `Atom (`TNat `NatDecoderInput) in
        let params = `Typed [ arg_name, Mandatory, intype ] in
        let base_name = "Decode" and qual = [] in
        let struct_name = "val" in
        let binding = Some struct_name and kind = `Pointer in
        let rcv = { F.binding; qual; type_name; kind } in
        let body =
          let parser_bind = "p" in
          let bindp =
            let bind_name = parser_bind
            and mods = Binding_mod.local ~mut:Binding_mod.Mutable ()
            and type_sig = Some (`Atom (`TNat `NatParser))
            and initial_value = Some (`CreateParser [ name_to_var arg_name ]) in
            `DeclareLocal (bind_name, mods, type_sig, initial_value)
          and binds = List.map flds ~f:(Field.gen_bind ~dir ~struct_name) in
          let ret = Some (`Pure (`VPrim `Prim_L)) in
          bindp :: binds @ [ `Return ret ]
        and return_type = [ `Atom (`TNat `NatError) ] in
        let definition = `TypeMethod (rcv, { F.params; return_type; body }) in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)
    end

    (* ANCHOR - Gen.Simple.convert *)
    let convert ~qual ~base_name = function
      | Anon typ ->
          let typedec = Anon.typedec ~qual ~base_name typ
          and encoder = Anon.encoder ~type_name:base_name typ
          and decoder = Anon.decoder ~type_name:base_name typ in
          `Group [ typedec; encoder; decoder ]
      | Record flds ->
          let typedec = Record.typedec ~qual ~base_name flds
          and encoder = Record.encoder ~type_name:base_name flds
          and decoder = Record.decoder ~type_name:base_name flds in
          `Group [ typedec; encoder; decoder ]
      | _ -> assert false
  end

  module Algebraic = struct
    module EnumType = struct
      let get_elems variants =
        let f = function
          | Enum_variant (mcons, ix) ->
              let cons = enum_ident ~ix mcons in
              cons, ix
          | _ -> assert false
        in
        List.rev_map ~f variants

      let typedec ~qual ~base_name ~enum_size variants =
        let elems = get_elems variants in
        let definition = `Enum (enum_size, elems) in
        Binding_mod.exposed_definite, `TypeDec (qual, base_name, definition)

      let encoder ~type_name ~(enum_size : enum_size) variants =
        let rcv_name = "val" and return_type = [ `Atom (`TNat `NatString) ] in
        let base_name = "Encode" and qual = [] in
        let binding = Some rcv_name and kind = `Value in
        let rcv = { F.binding; qual; type_name; kind }
        and params = `Typed []
        and body =
          let return_name = "ret" in
          let return_val : V.t = name_to_var return_name in
          let bse : S.block_scope_expr =
            let for_type =
              `Atom (`TPrim (`PrimInt (Unsigned, to_bits enum_size)))
            in
            let on_value : V.t =
              `UnaryOp (`Conv for_type, name_to_var rcv_name)
            in
            let rhs : V.t = `UnaryOp (`CallMethod ("Encode", []), on_value) in
            let init : S.initialized = `Mono (return_name, rhs) in
            `InitRefer (init, return_val)
          in
          let matches =
            get_elems variants
            |> List.map ~f:(fun (id, _) ->
                   `Pure (`VBound (Ident.Binding.Const_name id)))
          in
          let cases = [ matches, [ `Return (Some return_val) ] ]
          and default =
            let fmt_string =
              string_to_lit
              @@ Printf.sprintf "Encode@%s: %%v is not a valid value" type_name
            in
            Some
              [
                `ThrowError
                  (`FunApp
                    ( ([ Ident.Scope.Namespace_name "fmt" ], "Errorf"),
                      [ fmt_string; return_val ] ));
              ]
          in
          [ `Switch (bse, cases, default) ]
        in
        let definition = `TypeMethod (rcv, { F.params; return_type; body }) in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)

      let decoder ~type_name ~(enum_size : enum_size) variants =
        let dir = Decode
        and arg_name = "input"
        and intype = `Atom (`TNat `NatDecoderInput)
        and return_type = [ `Atom (`TNat `NatError) ] in
        let params = `Typed [ arg_name, Mandatory, intype ] in
        let base_name = "Decode" and qual = [] in
        let rcv_name = "val" in
        let binding = Some rcv_name and kind = `Pointer in
        let rcv = { F.binding; qual; type_name; kind } in
        let body =
          let bind_p = `InitParser (`CreateParser [ name_to_var arg_name ]) in
          let raw_name = "raw" and ret_name = "ret" in
          let abs, for_type =
            let bits = to_bits enum_size in
            ( AbstractInt (Unsigned, bits),
              `Atom (`TPrim (`PrimInt (Unsigned, bits))) )
          in
          let bind_raw : S.t =
            let initial_value = gen_transcoder_call ~dir abs
            and type_sig = Some for_type
            and mods = Binding_mod.(local ~mut:Immutable ~var:Invariant ()) in
            `DeclareLocal (raw_name, mods, type_sig, Some initial_value)
          in
          let return_as_enum : S.t =
            let bse : S.block_scope_expr =
              let rhs : V.t =
                let op =
                  `Conv
                    (`Atom (`TRefer (`RTEnum ((qual, type_name), enum_size))))
                in
                `UnaryOp (op, name_to_var raw_name)
              in
              let init = `Mono (ret_name, rhs) in
              `InitRefer (init, name_to_var ret_name)
            in
            let matches : V.t list =
              get_elems variants
              |> List.map ~f:(fun (id, _) ->
                     `Pure (`VBound (Ident.Binding.Const_name id)))
            in
            let cases : (V.t list * S.t list) list =
              [
                ( matches,
                  [
                    `AssignLocal
                      (rcv_name, Some `Dereference, name_to_var ret_name);
                    `Return (Some (`Pure (`VPrim `Prim_L)));
                  ] );
              ]
            and default =
              let fmt_string =
                string_to_lit
                @@ Printf.sprintf
                     "Decode@%s: %%v is not a valid value value"
                     type_name
              in
              Some
                [
                  `ThrowError
                    (`FunApp
                      ( ([ Ident.Scope.Namespace_name "fmt" ], "Errorf"),
                        [ fmt_string; name_to_var raw_name ] ));
                ]
            in
            `Switch (bse, cases, default)
          in
          [ bind_p; bind_raw; return_as_enum ]
        in
        let definition = `TypeMethod (rcv, { F.params; return_type; body }) in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)
    end

    module ADT = struct
      module VariantType = struct
        let generate ~qual ~base_name ~cons ~tag_val ~tag_size ~tag_type_name
            ?typ () =
          let tag_name, subtype_name =
            match cons with
            | None ->
                let n = Int.to_string tag_val in
                "Tag" ^ n, mk_ident base_name ^ n
            | Some name -> mk_ident name, mk_ident base_name ^ mk_ident name
          in
          let enum_elem = tag_name, tag_val in
          let raw_subtype =
            match typ with
            | Some typ -> Simple.cast_record ~ix:tag_val typ
            | None -> Simple.cast_record (Anon AbstractVoid)
          in
          let tagged_subtype =
            Simple.add_tag
              ~tag_type_name:(mk_ident tag_type_name)
              ~tag_size
              ~tag_name
              tag_val
              raw_subtype
          in
          let typedec =
            let flds = Simple.Record.Field.get_fields tagged_subtype
            and base_name = subtype_name in
            Simple.Record.typedec ~qual ~base_name ~has_tag:true flds
          and encoder =
            let flds = Simple.Record.Field.get_fields raw_subtype
            and type_name = subtype_name in
            Simple.Record.encoder ~type_name flds
          and decoder =
            let flds = Simple.Record.Field.get_fields raw_subtype
            and type_name = subtype_name in
            Simple.Record.decoder ~type_name flds
          in
          let subtype_type =
            match typedec with
            | _, `TypeDec (_, _, definition) -> definition
            | _, _ -> assert false
          in
          ( (subtype_name, subtype_type),
            enum_elem,
            `Group [ typedec; encoder; decoder ] )
      end

      let generate ~qual ~base_name ~tag_size variants ~ctxt =
        let ctxt', qual =
          let qual' =
            Qualification.amalgamate qual (mk_ns ~prefix:true base_name)
          in
          let ctxt' =
            Context.add_assoc
              (Ident.Scope.Type_name base_name :: qual)
              (Ident.Scope.Type_name base_name :: qual')
              ctxt
          in
          ctxt', qual'
        in
        let tag_type_name = tag_ident base_name in
        let f (subtypes, enum_elems, declarations) var =
          let new_subtype, new_enum_elem, new_declarations =
            match var with
            | Enum_variant (cons, tag_val) ->
                VariantType.generate
                  ~qual
                  ~base_name
                  ~cons
                  ~tag_val
                  ~tag_size
                  ~tag_type_name
                  ()
            | Type_variant (cons, tag_val, typ) ->
                VariantType.generate
                  ~qual
                  ~base_name
                  ~cons
                  ~tag_val
                  ~tag_size
                  ~tag_type_name
                  ~typ
                  ()
          in
          ( new_subtype :: subtypes,
            new_enum_elem :: enum_elems,
            new_declarations :: declarations )
        in
        let subtypes, enum_elems, declarations =
          List.fold ~init:([], [], []) ~f variants
        in
        let tagtype_def = `Enum ((tag_size :> enum_size), enum_elems) in
        let tagtype_dec =
          let base_name = tag_type_name in
          Binding_mod.exposed_definite, `TypeDec (qual, base_name, tagtype_def)
        and adt_typedec =
          let f (name, _) = `Atom (`TRefer (`RTOpaque (qual, name))) in
          let definition =
            `TaggedUnion (f (tag_type_name, tagtype_def), List.map subtypes ~f)
          in
          Binding_mod.exposed_definite, `TypeDec (qual, base_name, definition)
        in
        let adt_encoder =
          let rcv_name = "val" and return_type = [ `Atom (`TNat `NatString) ] in
          let type_name = base_name in
          let base_name = "Encode" and qual = [] in
          let binding = Some rcv_name and kind = `Value in
          let rcv = { F.binding; qual; type_name; kind } in
          let params = `Typed []
          and body =
            let tag_name = "tag" and payload_name = "payload" in
            let tag_bind =
              let bind_name = tag_name
              and mods = Binding_mod.(local ~mut:Immutable ~var:Invariant ())
              and type_sig = List.hd return_type
              and initial_value =
                Some (`ADTTagEncoding (name_to_var rcv_name))
              in
              `DeclareLocal (bind_name, mods, type_sig, initial_value)
            and payload_bind =
              let bind_name = payload_name
              and mods = Binding_mod.(local ~mut:Immutable ~var:Invariant ())
              and type_sig = List.hd return_type
              and initial_value =
                Some (`ADTValEncoding (name_to_var rcv_name))
              in
              `DeclareLocal (bind_name, mods, type_sig, initial_value)
            and ret =
              `Return
                (Some
                   (`ConcatStrings
                     [ name_to_var tag_name; name_to_var payload_name ]))
            in
            [ tag_bind; payload_bind; ret ]
          in
          let definition = `TypeMethod (rcv, { F.params; return_type; body }) in
          Binding_mod.lambda, `FunDec (qual, base_name, definition)
        and adt_decoder =
          let arg_name = "input"
          and intype = `Atom (`TNat `NatDecoderInput)
          and return_type = [ `Atom (`TNat `NatError) ] in
          let type_name = base_name in
          let base_name = "decode" and qual = [] and rcv_name = "v" in
          let binding = Some rcv_name and kind = `Pointer in
          let rcv = { F.binding; qual; type_name; kind } in
          let body =
            let dtu =
              let input_binding = arg_name
              and tag_case_map = List.zip_exn enum_elems subtypes
              and container = rcv_name in
              `DecodeTaggedUnion
                (input_binding, tag_type_name, tag_case_map, container)
            in
            [ dtu; `Return (Some (`Pure (`VPrim `Prim_L))) ]
          in
          let params = `Typed [ arg_name, Mandatory, intype ] in
          let definition = `TypeMethod (rcv, { F.params; return_type; body }) in
          Binding_mod.lambda, `FunDec (qual, base_name, definition)
        in
        let contents =
          `Collection
            ( tagtype_dec,
              declarations,
              `Group [ adt_typedec; adt_encoder; adt_decoder ] )
        in
        ctxt', `Namespace (Ident.Scope.to_string (List.last_exn qual), contents)
    end
  end
end

let convert_enum_type ~qual ~base_name ~(enum_size : enum_size) variants =
  let typedec =
    Gen.Algebraic.EnumType.typedec ~qual ~base_name ~enum_size variants
  and encoder =
    Gen.Algebraic.EnumType.encoder ~type_name:base_name ~enum_size variants
  and decoder =
    Gen.Algebraic.EnumType.decoder ~type_name:base_name ~enum_size variants
  in
  `Group [ typedec; encoder; decoder ]

let convert_variants_type ~qual ~base_name ~tag_size variants =
  Gen.Algebraic.ADT.generate ~qual ~base_name ~tag_size variants

let convert_adt_type ~qual ~base_name ~(tag_size : tag_size) ~variants ~ctxt =
  let is_enum = function Enum_variant _ -> true | _ -> false in
  if List.for_all ~f:is_enum variants then
    ( ctxt,
      convert_enum_type
        ~qual
        ~base_name
        ~enum_size:(tag_size :> enum_size)
        variants )
  else convert_variants_type ~qual ~base_name ~tag_size ~ctxt variants

let convert_type ~name ~ctxt =
  let qualids, base_name = reformat name in
  let qual = List.map ~f:(fun x -> Ident.Scope.Inferred_scope x) qualids in
  function
  | ADT { tag_size; variants; _ } ->
      convert_adt_type ~qual ~base_name ~tag_size ~variants ~ctxt
  | Enum { enum_size; elements } ->
      ( ctxt,
        convert_enum_type
          ~qual
          ~base_name
          ~enum_size
          (List.map ~f:(fun (name, size) -> Enum_variant (name, size)) elements)
      )
  | x -> ctxt, Gen.Simple.convert ~qual ~base_name x

let convert_toplevel ~name ~ctxt ~rep subtypes =
  let qualids, base_name = reformat name in
  let qual = List.map ~f:(fun x -> Ident.Scope.Inferred_scope x) qualids in
  let ctxt', qual =
    let qual' =
      Qualification.amalgamate
        ~drop_proto:false
        qual
        (Gen.mk_ns ~prefix:false base_name)
    in
    ( Gen.Context.add_assoc
        (Ident.Scope.Namespace_name base_name :: qual)
        qual'
        ctxt,
      qual' )
  in
  let ctxt', rsubs =
    List.fold subtypes ~init:(ctxt', []) ~f:(fun (ctxt, decls) (name, rep) ->
        convert_type ~name ~ctxt rep |> fun (ctxt', decl) ->
        ctxt', decl :: decls)
  in
  let ctxt'', top = convert_type ~name:"t" ~ctxt:ctxt' rep in
  ( ctxt'',
    [
      ( Binding_mod.exposed_definite,
        `Define_namespace
          ( Ident.Scope.to_string (List.hd_exn qual),
            List.(rev_append rsubs [ top ]) ) );
    ] )
