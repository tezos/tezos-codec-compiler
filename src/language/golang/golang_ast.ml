open Backend.Foreign
open Backend.Foreign.Common
open Backend.Codec.Codec_repr

module T = struct
  type nat = Foreign_type.nat

  type prim = [ Foreign_type.prim | `PrimReserveSuffix ]

  type reference_type = Foreign_type.reference_type

  type linear_type =
    [ `LTArray of t * int | `LTSlice of t | `LTSequence of width * t ]

  and composite_type =
    [ `CTLinear of linear_type | `CTOption of t | `CTDynWidth of width * t ]

  and atom =
    [ `TPrim of prim
    | `TNat of nat
    | `TComposite of composite_type
    | `TRefer of reference_type ]

  and t =
    [ `Atom of atom
    | `Object of (string * t) list
    | `Enum of Codec.Codec_repr.enum_size * (string * int) list
    | `TaggedUnion of t * t list ]
end

module V = struct
  type prim = Foreign_val.prim

  type binary_op = Foreign_val.binary_op

  type literal =
    [ `LitString of string
    | `LitArray of t list
    | `LitSlice of t list
    | `LitObject of t record_fill ]

  and derived_term =
    [ `Field of Ident.lab_id
    | `Index of t
    | `KeyVal of t
    | `Dereference
    | `Reference ]

  and unary_op =
    [ `Access of derived_term
    | `CallMethod of Ident.fun_id * t list
    | `BooleanNegate
    | `Conv of T.t
    | `Assert of T.t ]

  and term = [ `VPrim of prim | `VLit of literal | `VBound of Ident.binding ]

  and t =
    [ `Pure of term
    | `CreateParser of t list
    | `ConcatStrings of t list
    | `UnaryOp of unary_op * t
    | `BinaryOp of binary_op * t * t
    | `Paren of t
    | `FunApp of (Qualification.t * Ident.fun_id) * t list
    | `Create of T.t
    | `ADTValEncoding of t
    | `ADTTagEncoding of t ]
end

module S = struct
  module T = T
  module V = V

  type initialized =
    [ `Mono of Ident.var_id * V.t
    | `First of Ident.var_id * V.t
    | `Second of Ident.var_id * V.t
    | `Both of (Ident.var_id * Ident.var_id) * V.t ]

  type block_scope_expr = [ V.t | `InitRefer of initialized * V.t ]

  type t =
    [ `DeclareLocal of Ident.var_id * Binding_mod.t * T.t option * V.t option
    | `AssignLocal of Ident.var_id * V.derived_term option * V.t
    | `BareExpression of V.t
    | `Return of V.t option
    | `ThrowError of V.t
    | `InitParser of V.t
    | `ThrowResultIfError of V.t
    | `DecodeTaggedUnion of
      string * string * ((string * int) * (string * T.t)) list * string
    | `IfThenElse of block_scope_expr * t list * t list
    | `Switch of block_scope_expr * (V.t list * t list) list * t list option ]
end

module F = struct
  module T = T
  module V = V
  module S = S

  type kind = [ `Value | `Pointer ]

  type recv = {
    binding : string option;
    kind : kind;
    qual : Qualification.t;
    type_name : Ident.type_id;
  }

  type t' = {
    params : (T.t, V.t) Common.fun_args;
    return_type : T.t list;
    body : S.t list;
  }

  type t = [ `GlobalFunc of t' | `LocalFunc of t' | `TypeMethod of recv * t' ]
end

module D = struct
  module T = T
  module V = V
  module S = S
  module F = F

  type t' = Declaration.Declaration(T)(V)(S)(F).t'
  type t = Declaration.Declaration(T)(V)(S)(F).t

  type u' = Declaration.Declaration(T)(V)(S)(F).u'
  type u = Declaration.Declaration(T)(V)(S)(F).u

  type v = 
    [ Declaration.Declaration(T)(V)(S)(F).v
    | `Namespace of Ident.namespace_id * v list ]
end

module GO_AST = struct
  module T = T
  module V = V
  module S = S
  module F = F
  module D = D

  type source_file = {
    filename : string;
    preamble : directive list;
    contents : D.v list;
  }

  type foreign_codebase =
    | SingleFile of source_file
    | ManyFile of source_file list
end