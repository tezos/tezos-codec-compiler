open Core
open Golang_ast
open Common.Language_repr
open Backend.Representation.Abstract_type
open Backend.Foreign
open Backend.Foreign.Common
open Backend.Codec.Codec_repr

module Golang_backend_types : 
sig 
include Types_backend
val t_decoder_input : string
val t_error : string
val t_parser : string
end = struct
  let t_parser = "byteparser.T"

  let t_decoder_input = "byteparser.DecoderArgument"

  let t_width_uint30 = "primitive.Width"

  let t_width_uint8 = "primitive.Width"

  let t_tagsize_uint8 = "primitive.TagSize"

  let t_tagsize_uint16 = "primitive.TagSize"

  let t_padding = "struct{}"

  let t_reservesuffix_of ~reserve typ = ignore reserve; typ

  let t_dynprefix_of ~width typ = ignore width; typ

  let t_tagless_of = Printf.sprintf "Tagless<%s>"

  let t_tuple_of = function
    | [] ->
        (* REVIEW[epic=design] - t_tuple_of should not be called with zero
           arguments in the current model *)
        assert false
    | [ singleton ] ->
        (* REVIEW[epic=design] - 1-Tuple of a type is just that type, for now *)
        singleton
    | [ fst; snd ] -> Printf.sprintf "Tuple<%s,%s>" fst snd
    | _ -> failwith "Golang backend does not support N-tuples for N > 2"

  let t_width = "width"

  let t_variant_encoder_param = "map[uint32]encoder.Encoder"

  let t_variant_decoder_param =
    "map[uint32]func(byteparser.Decoder_argument)(interface{}, error)"

  let t_error = "error"
end

module Golang_types : Types_language with module T := T = struct
  module T = T
  let t_nat = function
  | `NatBool -> "bool"
  | `NatInt (Signed, Some Bits8) -> "int8"
  | `NatInt (Signed, Some Bits16) -> "int16"
  | `NatInt (Signed, Some Bits32) -> "int32"
  | `NatInt (Signed, Some Bits64) -> "int64"
  | `NatInt (Unsigned, Some Bits8) -> "uint8"
  | `NatInt (Unsigned, Some Bits16) -> "uint16"
  | `NatInt (Unsigned, Some Bits32) -> "uint32"
  | `NatInt (Unsigned, Some Bits64) -> "uint64"
  | `NatInt (Signed, None) -> "int"
  | `NatInt (Unsigned, None) -> "uint"
  | `NatString -> "string"
  | `NatError -> Golang_backend_types.t_error
  | `NatDecoderInput -> Golang_backend_types.t_decoder_input
  | `NatParser -> Golang_backend_types.t_parser

  let t_int = function
    | Signed, Bits8 -> "primitive.Int8"
    | Signed, Bits16 -> "primitive.Int16"
    | Signed, Bits32 -> "primitive.Int32"
    | Signed, Bits64 -> "primitive.Int64"
    | Unsigned, Bits8 ->  "primitive.Uint8"
    | Unsigned, Bits16 -> "primitive.Uint16"
    | Unsigned, Bits32 -> "primitive.Uint32"
    | Unsigned, Bits64 -> "primitive.Uint64"

  let t_int_ranged ((min, _), bytes) =
    let sign =
      if min >= 0 then Unsigned else Signed
    and bits =
      match bytes with
      | Bytes4 -> Bits32
      | Bytes8 -> Bits64
    in t_int (sign, bits)

  let t_float : float_precision -> string = function
    | Single -> "float32"
    | Double -> "float64"

  let t_bool = "primitive.Bool"

  let t_string = "primitive.String"

  let t_array_of ?fixed elem =
    match fixed with
    | None ->
        "[]" ^ elem (* REVIEW[epic=design] - should we be mixing slices here? *)
    | Some n -> Printf.sprintf "[%d]%s" n elem

  let t_slice_of elem = "[]" ^ elem

  let t_option_of value = "map[struct{}]" ^ value

  let t_orerror_of t = "(" ^ t ^ ", error)"

  let t_zarith_n = "zarith.N"

  let t_zarith_z = "zarith.Z"
 
  let t_linear_of ft = function
  | `LTArray (t, n) -> t_array_of ~fixed:n (ft t)
  | `LTSlice t | `LTSequence (_,t) -> t_slice_of @@ ft t
    
  let t_prim : T.prim -> string = function
  | `PrimBool -> t_bool
  | `PrimInt sw -> t_int sw
  | `PrimIntRanged (min, max) -> t_int_ranged (min, max)
  | `PrimString -> t_string
  | `PrimOpaqueBytes -> "[]byte"
  | `PrimOpaqueBytesFixedLen n -> Printf.sprintf "[%d]byte" n
  | `PrimWidth -> Golang_backend_types.t_width
  | `PrimFloat p -> t_float p
  | `PrimPadding _ -> Golang_backend_types.t_padding
  | `PrimZarith_N -> t_zarith_n
  | `PrimZarith_Z -> t_zarith_z
  | `PrimReserveSuffix -> "Suffix"
  let t_composite_of ft =
    let open Golang_backend_types in
    function
    | `CTLinear lin -> t_linear_of ft lin
    | `CTOption elem -> t_option_of @@ ft elem
    | `CTDynWidth (DynPrefix w, t) -> t_dynprefix_of ~width:w @@ ft t
    | `CTDynWidth (ReserveSuffix n, t) -> t_reservesuffix_of ~reserve:n @@ ft t
end

module Golang_meta : Language_meta = struct
  let lang_props =
    {
      language_name = "Go";
      file_extension = "go";
      has_reflexive_typing = false;
      has_reflexive_value = false;
      has_native_indirection = true;
      has_generic_types = false;
      has_unboxed_strings = true;
      has_native_enums = false;
      has_native_unions = false;
      has_type_constraints = false;
      has_native_slices = true;
      has_native_typecast = false;
      has_native_typeconv = true;
    }

  let import_all qname src =
    Printf.sprintf "import * as %s from '%s';" qname src

  let import bindings src =
    Printf.sprintf
      "import { %s } from '%s'"
      (String.concat ~sep:", " bindings)
      src
end

module Golang_tokens : Syntax_tokens = struct
  let declare_sep = "="

  let define_sep = ":="

  let field_sep = ";"

  let elem_sep = ","

  let open_block = "{"

  let close_block = "}"

  let open_record = "{"

  let close_record = "}"
end

module Golang_rules_core :
sig
include Syntax_rules_core
val type_conv : expr:string -> typ:(string * T.t) -> string
end
= struct
  let group_expr = Printf.sprintf "(%s)"

  let terminate_phrase phr = phr ^ ";"

  let assign ~bind ~value =
    String.concat ~sep:" " [ bind; Golang_tokens.declare_sep; value ]

  let annotate ~type_sig ~bind_name = bind_name ^ " " ^ type_sig

  let at_index ~ix expr = Printf.sprintf "%s[%s]" expr ix

  let at_field ~lab expr = Printf.sprintf "%s.%s" expr lab

  let at_key ~key expr = Printf.sprintf "%s[%s]" expr key

  let make_fields flds =
    List.map
      ~f:(fun (bind_name, type_sig) -> annotate ~type_sig ~bind_name)
      flds

  let make_enum_elems elems =
    List.map ~f:(fun (bind, value) -> assign ~bind ~value) elems

  let join_fields ?(inline = false) =
    let sep = Golang_tokens.field_sep ^ if inline then " " else "\n" in
    String.concat ~sep

  let join_elems ?(inline = false) =
    let sep = Golang_tokens.elem_sep ^ if inline then " " else "\n" in
    String.concat ~sep

  let call_method ~on ~meth ~args =
    Printf.sprintf "%s.%s(%s)" on meth (String.concat ~sep:"," args)

  let type_conv ~expr ~typ =
    match typ with
    | name, `Atom (`TPrim _ | `TRefer _) -> Printf.sprintf "%s(%s)" name expr
    | _, `Atom (`TComposite (`CTDynWidth (DynPrefix `Uint8, _))) -> "primitive.LenPref{primitive.WidthUint8," ^ expr ^ "}"
    | _, `Atom (`TComposite (`CTDynWidth (DynPrefix `Uint30, _))) -> "primitive.LenPref{primitive.WidthUint30," ^ expr ^ "}"
    | _, `Atom (`TComposite (`CTDynWidth (ReserveSuffix n, _))) -> Printf.sprintf "primitive.ReserveSuffix{primitive.Suffix(%d), %s}" n expr
    | _, _ -> assert false
      (*
      | AbstractVoid -> "struct{}"
      | AbstractTuple2 _ -> assert false
      | AbstractSequence (_, typ) ->
          Golang_types.t_slice_of @@ nominate_type typ
      | AbstractZarith_N | AbstractZarith_Z ->
          assert false
          (* NOTE - We can't do much about this without knowing the input type *)
      | AbstractOption _ ->
          assert false (* NOTE - We can't do much about this, period *)
      | AbstractTagVal { tag_size = `Uint8; _ } ->
          Golang_types.t_int (Unsigned, Bits8)
      | AbstractTagVal { tag_size = `Uint16; _ } ->
          Golang_types.t_int (Unsigned, Bits16)
      | AbstractPadding _ | AbstractDyn _ | AbstractTag _ | AbstractPseudo _ ->
          assert false
          *)

  let bool_negate = Printf.sprintf "!(%s)"

  let binary_op op x y =
    match op with
    | `Eq  -> Printf.sprintf "%s == %s" x y
    | `NotEq -> Printf.sprintf "%s != %s" x y
    | _ -> assert false

  let comment = function
    | `Inline com -> "// " ^ com
    | `Block coms -> Printf.sprintf "/* %s */" (String.concat ~sep:"\n   " coms)

  let funcall ?(args = []) ?generics ~f =
    ignore generics;
    Printf.sprintf "%s(%s)" f @@ String.concat ~sep:", " args

  let curried_funcall ?(args = [ [] ]) ?generics ~f =
    ignore generics;
    let group params = Printf.sprintf "(%s)" (String.concat ~sep:", " params) in
    Printf.sprintf "%s%s" f @@ String.concat ~sep:"" (List.map ~f:group args)

  let create_bind ?(mods = Binding_mod.exposed_definite) ~bind_name ?type_sig
      ~value =
    let open Ident.Binding in
    let open Binding_mod in
    let as_export = String.capitalize and as_internal = String.uncapitalize in
    let lhs =
      let visname =
        (match mods.visibility with
        | Exposed -> as_export
        | Hidden | Local -> as_internal)
          (to_string bind_name)
      in
      match bind_name with
      | Const_name _ -> "const " ^ visname
      | Variable_name _ -> "var " ^ visname
      | Function_name _ -> "func " ^ visname
      | Label_name _ -> visname
      | Enum_name name -> name
    in
    match type_sig, value with
    | None, Some rhs -> assign ~bind:lhs ~value:rhs
    | Some typ, Some rhs ->
        assign ~bind:(annotate ~bind_name:lhs ~type_sig:typ) ~value:rhs
    | Some typ, None -> annotate ~type_sig:typ ~bind_name:lhs
    | None, None -> lhs
end

module Golang_syntax_core : Syntax_core = struct
  let define_type ?(mods = Binding_mod.exposed_definite) ?(kind = Kind.Obj)
      ~type_name ~(definition : contents) =
    let open Kind in
    let open Golang_tokens in
    let visname =
      match mods.visibility with
      | Local | Hidden -> String.uncapitalize type_name
      | Exposed -> String.capitalize type_name
    in
    match kind with
    | Obj ->
        (match definition with
        | `Line stmt -> stmt
        | `Inline stmts -> Golang_rules_core.join_fields ~inline:true stmts
        | `MultiLine stmts ->
            "\n" ^ Golang_rules_core.join_fields ~inline:false stmts ^ "\n")
        |> Printf.sprintf "type %s struct {%s}" visname
    | Enum ->
        (match definition with
        | `Line stmt -> stmt
        | `Inline stmts -> Golang_rules_core.join_elems ~inline:true stmts
        | `MultiLine stmts -> Golang_rules_core.join_elems ~inline:false stmts)
        |> Printf.sprintf "type %s int\n\nconst (\n%s\n)" visname
    | Alias -> (
        match definition with
        | `Line stmt -> Printf.sprintf "type %s %s" visname stmt
        | _ ->
            failwith
              "define_type: Expected single-line definition for type-alias")
    | Union -> (
        match definition with
        | `Line stmt -> Printf.sprintf "%s %s" declare_sep stmt
        | _ ->
            failwith
              "define_type: Expected single-line definition for alias to \
               anonymous union")

  let declare_type ?(mods = Binding_mod.exposed_definite) ?(kind = Kind.Obj)
      ~type_name =
    let open Golang_meta in
    ignore (mods, kind, type_name);
    failwith
      (Printf.sprintf
         "Error: %s does not support type declarations without accompanying \
          definitions"
         lang_props.language_name)

  let define_fun ?(mods = Binding_mod.exposed_definite) ~fun_name
      ?(args = `Typed []) ?(outtype = []) ~(definition : contents) =
    let visname =
      match mods.visibility with
      | Binding_mod.(Hidden | Local) -> String.uncapitalize fun_name
      | Binding_mod.Exposed -> String.capitalize fun_name
    in
    let params =
      match args with
      | `Typed args ->
          let f = function
            | bind_name, Mandatory, type_sig ->
                Golang_rules_core.annotate ~bind_name ~type_sig
            | bind_name, Optional, type_sig ->
                Golang_rules_core.annotate
                  ~bind_name:(bind_name ^ "?")
                  ~type_sig
            | bind, Default value, _ -> Golang_rules_core.assign ~bind ~value
          in
          let arg_list = List.map ~f args in
          Printf.sprintf "(%s)" @@ String.concat ~sep:", " arg_list
      | `Untyped _ ->
          failwith
          @@ Printf.sprintf
               "Error: expected typed argument list for %s function definition"
               Golang_meta.lang_props.language_name
    in
    let params_and_sig =
      match outtype with
      | [] -> params
      | [type_sig] -> Golang_rules_core.annotate ~bind_name:params ~type_sig
      | ret_list -> Golang_rules_core.annotate ~bind_name:params ~type_sig:(String.concat [Printf.sprintf "(%s)" @@ String.concat ~sep:", " ret_list])
    in
    let rhs =
      match definition with
      | `Line stmt ->
          Golang_tokens.(
            Printf.sprintf "%s\n%s\n%s" open_block stmt close_block)
      | `Inline stmts ->
          Golang_tokens.(
            Printf.sprintf
              "%s %s %s"
              open_block
              (String.concat ~sep:" " stmts)
              close_block)
      | `MultiLine stmts ->
          Golang_tokens.(
            Printf.sprintf
              "%s\n%s\n%s"
              open_block
              (String.concat ~sep:"\n" stmts)
              close_block)
    in
    Printf.sprintf "func %s%s %s" visname params_and_sig rhs

  let declare_fun ?(mods = Binding_mod.exposed_definite) ~fun_name
      ?(args = `Typed []) ~outtype =
    let fun_keyword = "function" in
    let keywords =
      match mods.externality with
      | Binding_mod.Definite -> fun_keyword
      | Binding_mod.Proxy -> "declare " ^ fun_keyword
    in
    let receiver, params =
      match args with
      | `Typed args ->
          let f = function
            | bind_name, Mandatory, type_sig ->
                Golang_rules_core.annotate ~bind_name ~type_sig
            | bind_name, Optional, type_sig ->
                Golang_rules_core.annotate
                  ~bind_name:(bind_name ^ "?")
                  ~type_sig
            | bind, Default value, _ -> Golang_rules_core.assign ~bind ~value
          in
          let arg_list = List.map ~f args in
          "", Printf.sprintf "(%s)" @@ String.concat ~sep:", " arg_list
      | `Untyped _ ->
          failwith
          @@ Printf.sprintf
               "Error: expected typed argument list for %s function definition"
               Golang_meta.lang_props.language_name
    in
    let params_and_sig =
      match outtype with
      | [] -> params
      | [type_sig] -> Golang_rules_core.annotate ~bind_name:params ~type_sig
      | ret_list -> Golang_rules_core.annotate ~bind_name:params ~type_sig:(String.concat [Printf.sprintf "(%s)" @@ String.concat ~sep:", " ret_list])
    in
    Golang_rules_core.terminate_phrase
    @@ Printf.sprintf "%s %s%s%s" keywords receiver fun_name params_and_sig

  let define_val ?mods ?typ ~val_name ~value =
    Golang_rules_core.(
      terminate_phrase
      @@ create_bind
           ?mods
           ?type_sig:typ
           ~bind_name:(Ident.Binding.Variable_name val_name)
           ~value:(Some value))

  let declare_val ?(mods = Binding_mod.exposed_definite) ?typ ~val_name =
    match mods.externality with
    | Binding_mod.Definite ->
        Golang_rules_core.(
          terminate_phrase
          @@ create_bind
               ~mods
               ?type_sig:typ
               ~bind_name:(Ident.Binding.Variable_name val_name)
               ~value:None)
    | Binding_mod.Proxy ->
        Golang_rules_core.(
          terminate_phrase @@ "declare "
          ^ create_bind
              ~mods:{ mods with visibility = Hidden }
              ?type_sig:typ
              ~bind_name:(Ident.Binding.Variable_name val_name)
              ~value:None)
end

module Golang_metasyntax : Syntax_meta = struct
  let wrap_namespace name contents = ignore (name, contents); assert false
  let throw_error = function
    | Some err -> Printf.sprintf "throw new Error(%s)" err
    | None -> "throw new Error()"

  let lambda ?(args = (`Typed [] : string_fun_args)) ?outtype ~(definition : contents) =
    let outtype =
      match outtype with Some outtype -> " " ^ Golang_rules_core.group_expr( String.concat ~sep:", " outtype) | None -> ""
    in
    let params =
      match args with
      | `Typed args ->
          let f = function
            | bind_name, Mandatory, type_sig ->
                Golang_rules_core.annotate ~bind_name ~type_sig
            | bind_name, Optional, type_sig ->
                Golang_rules_core.annotate
                  ~bind_name:(bind_name ^ "?")
                  ~type_sig
            | bind, Default value, _ -> Golang_rules_core.assign ~bind ~value
          in
          let arg_list = List.map ~f args in
          Printf.sprintf "(%s)" @@ String.concat ~sep:", " arg_list
      | `Untyped args ->
          let f = function
            | bind_name, Mandatory -> bind_name
            | bind_name, Optional -> bind_name ^ "?"
            | bind, Default value -> Golang_rules_core.assign ~bind ~value
          in
          let arg_list = List.map ~f args in
          Printf.sprintf "(%s)" @@ String.concat ~sep:", " arg_list
    in
    Printf.sprintf
      "%s%s => %s"
      params
      outtype
      (match definition with
      | `Line stmt -> stmt
      | `Inline stmts -> Printf.sprintf "{ %s }" @@ String.concat ~sep:" " stmts
      | `MultiLine stmts ->
          Printf.sprintf "{\n%s\n}" @@ String.concat ~sep:"\n" stmts)

  let valid_as_enum ~ref ~vals candidate =
    ignore vals;
    Printf.sprintf "typeof %s[%s] !== \"undefined\"" ref candidate

  let return_statement = function None -> "return" | Some s -> "return " ^ s

  let if_then_else test_expr true_branch = function
    | Some false_branch ->
        Printf.sprintf
          "if (%s) {\n%s\n} else {\n%s\n}"
          test_expr
          (String.concat ~sep:"\n" true_branch)
          (String.concat ~sep:"\n" false_branch)
    | None ->
        Printf.sprintf
          "if (%s) {\n%s\n}"
          test_expr
          (String.concat ~sep:"\n" true_branch)
  
  let switch test_expr cases default =
    let cases' =
      let f (case_list, stmt_list) = 
        (Printf.sprintf "case %s:" @@ String.concat ~sep:", " case_list)
        :: stmt_list
      in List.concat_map cases ~f
    and default' =
      match default with
      | None -> []
      | Some stmt_list -> "default:" :: stmt_list
    in Printf.sprintf "switch %s {\n%s\n}" test_expr @@
    String.concat ~sep:"\n" (cases' @ default')

  (* REVIEW[epic=design] - concatenate strings more efficiently, perhaps? *)
  let concat_strings = function
    | [] -> "\"\""
    | xs -> Golang_rules_core.group_expr @@ String.concat ~sep:" + " xs

  let narrow_enum enum elem = enum ^ "." ^ elem

  let type_union _ = assert false
  let tagged_union ?tag _ =
    match tag with
    | Some qname -> Printf.sprintf "interface { Tag() %s } " qname
    | None -> "interface { Tag() int }"
end

module Golang_syntax : Syntax_language = struct
  include Golang_tokens
  include Golang_rules_core
  include Golang_syntax_core
  include Golang_metasyntax
end

module Golang_lex : Lex_language = struct
  let remap_keyword id =
    match id with
    | "default" | "func" | "interface" | "select" | "case" | "defer" | "go"
    | "map" | "struct" | "chan" | "else" | "goto" | "package" | "switch"
    | "const" | "fallthrough" | "if" | "range" | "type" | "continue" | "for"
    | "import" | "return" | "var" | "break" ->
        "id_" ^ id
    | _ -> id

  let qualify_ident ~q ?(common = []) ident =
    ignore common;
    String.concat
    @@ List.rev_map_append q [ ident ] ~f:(function
           | Ident.Scope.Inferred_scope id -> id ^ "__"
           | Ident.Scope.Namespace_name id | Ident.Scope.Type_name id ->
               id ^ ".")
    |> remap_keyword

  let generate_ident ~q ?(common = []) ident =
    ignore common;
    String.concat
    @@ List.rev_map_append q [ ident ] ~f:(function
           | Ident.Scope.Inferred_scope id | Ident.Scope.Namespace_name id ->
               id ^ "__"
           | Ident.Scope.Type_name _ -> "")
    |> remap_keyword
end

module Golang_repr : Language with module T := T = struct
  module T = T
  include Golang_types
  include Golang_meta
  include Golang_syntax
  include Golang_lex
end


(* FIXME[epic=raw-copy] - implement Golang backend and fix this *)
module Golang_value_table : Backend_value_table with module V := V = struct
  module V = V
  let v_prim : V.prim -> string = function
    | `PrimTrue -> "true"
    | `PrimFalse -> "false"
    | `PrimNum num -> num
    | `Prim_L -> "nil"
    | `PrimWidth `Uint8 -> "WidthUint8"
    | `PrimWidth `Uint30 -> "WidthUint30"
    | `PrimTagSize `Uint8 -> "TagSizeUint8"
    | `PrimTagSize `Uint16 -> "TagSizeUint16"
    | `PrimEnumSize `Uint8 -> "EnumSizeUint8"
    | `PrimEnumSize `Uint16 -> "EnumSizeUint16"
    | `PrimEnumSize `Uint30 -> "EnumSizeUint30"

  let v_lit_str str = Printf.sprintf "\"%s\"" str

  let v_lit_arr arr = Printf.sprintf "[...]interface{}{%s}" (Golang_repr.join_elems ~inline:true arr)

  let v_lit_obj { explicit; implicit } =
    assert (Option.is_none implicit);
    let f = function
      | Assign (lab, v) -> Golang_repr.assign ~bind:lab ~value:v
      | NamedPun lab -> lab
    in
    let explicit' = List.map ~f explicit in
    Golang_repr.(
      Printf.sprintf
        "%s %s %s"
        open_record
        (join_elems ~inline:true explicit')
        close_record)
end

(* FIXME[epic=raw-copy] - implement Golang backend and fix this *)
module Golang_symbol_table : Backend_symbol_table with module T := T = struct
  module T = T
  let prefix_for = assert false
  (*
    let open Foreign_type in
    let module L = Golang in
    function
    | `Atom a -> (
        match a with
        | `TPrim p -> (
            match p with
            | `PrimBool -> L.t_bool
            | `PrimInt (s, w) -> L.t_int (s, w)
            | `PrimIntRanged ((min, max), bytes) ->
                L.t_int_ranged ((min, max), bytes)
            | PrimFloat prec -> L.t_float prec
            | PrimOpaqueBytes | PrimOpaqueBytesFixedLen _ -> L.t_bytes None
            | PrimString -> L.t_string
            (* REVIEW[epic=needs_refactor] - make a padding type attribute in
               repr? *)
            | PrimPadding _ -> "padding"
            | PrimZarith_N -> L.t_zarith_n
            | PrimZarith_Z -> L.t_zarith_z
            | _ -> assert false)
        | TComposite ct -> (
            match ct with
            | CTLinear _ -> "sequence"
            | CTOption _ -> "option"
            | CTDynWidth (DynPrefix _, _) -> "lenpref_"
            | CTDynWidth (ReserveSuffix _, _) -> "reservesuffix_"
            | CTTuple2 _ -> "tuple_"
            | _ -> assert false)
        | TRefer { qual; base_name } ->
            if List.is_empty qual then base_name ^ "_"
            else
              failwith
                "cannot refer to value in namespace without scope context"
        | _ -> assert false)
    | _ -> assert false
    *)
end

(*
module Golang_preprocessor : Backend_preprocessor = struct
  let pp_universal_type =
    let open Universal_type in
    function
    | Simple t -> t
    | TaggedSimple (_, pt) -> pt
    | TaggedUnion _ -> failwith "cannot construct anonymous type-union"
    | Enum _ -> failwith "cannot construct anonymous enum"

  let pp_universal_val =
    let open Universal_val in
    function
    | Simple v -> v
    | ADTTagEncoding v -> UnaryOp (CallMethod { meth="Encode"; args=[] }, UnaryOp (CallMethod { meth="Tag"; args=[] }, v))
    | ADTValEncoding v -> UnaryOp (CallMethod { meth="Encode"; args=[] }, UnaryOp (CallMethod { meth="Payload"; args=[] }, v))
  
  let rec pp_statement = let open Statement in function
  | Reserved _ | Basic _ as stmt -> [stmt]
  | Branching IfThenElse ({ true_branch; false_branch; _ } as cond) ->
    [Branching (IfThenElse { cond with true_branch = List.concat_map ~f:pp_statement true_branch; false_branch = List.concat_map ~f:pp_statement false_branch })]
  | Branching Switch ({ cases; default; _ } as branches) ->
    [Branching (Switch { branches
      with cases = List.map cases ~f:(fun (lhs, stmts) -> lhs, List.concat_map ~f:pp_statement stmts);
           default = Option.map default ~f:(List.concat_map ~f:pp_statement)
       })]
  | Abstract DecodePadding { amount; parser } ->
    let open Foreign_val in
    let parser_bind = Pure (Bound (Ident.Binding.Variable_name parser))
    and padding_amount  = Pure (VPrim (PrimNum (string_of_int amount)))
    in let ret = UnaryOp (CallMethod { meth="SkipPadding"; args=[padding_amount] }, parser_bind) in
    let test_expr = BinaryOp (NotEq, ret, Pure (VPrim Prim_L))
    and true_branch = [ Basic (ThrowError (Pure (VLit (LitString "unable to parse padding")))) ]
    and false_branch = []
    in [Branching (IfThenElse { test_expr; true_branch; false_branch })]
  | Abstract EncodePadding { amount; bind_name } ->
    let mods = Binding_mod.local ()
    and type_sig = Some (Atom (TNat NatString))
    and initial_value = Some (Universal_val.Simple Foreign_val.(FunApp { qual=[Ident.Scope.Namespace_name "primitive"]; f="MakePadding"; args=[[Foreign_val.(Pure (VPrim (PrimNum (string_of_int amount))))]]; generics=None }))
    in
    [Basic (DeclareLocal { bind_name; mods; type_sig; initial_value })]
  | Abstract DecodeInto { struct_name; field_name; parser; _ } ->
    let open Foreign_val in
    let obj = Pure (Bound (Ident.Binding.Variable_name struct_name))
    and p = Pure (Bound (Ident.Binding.Variable_name parser))
    and err = "err" in
    let field = UnaryOp (Access (Field field_name), obj) in
    let expr = Paren field in
    let decode_call = UnaryOp (CallMethod { meth="Decode"; args=[p] }, expr) in
    let v_err = Pure (Bound (Ident.Binding.Variable_name err)) in
    [
      Basic (AssignLocal { bind_name=err; derived=None; new_value=Simple decode_call });
      Branching (IfThenElse {
        test_expr=BinaryOp (NotEq, v_err, Pure (VPrim Prim_L));
        true_branch=[Basic (Return (Some v_err))];
        false_branch=[]
      })
    ]
  | Abstract EncodeInto { struct_name; field_name; bind_name; _ } ->
    let open Foreign_val in
    let initial_value =
      let obj = Pure (Bound (Ident.Binding.Variable_name struct_name)) in
      let field = UnaryOp (Access (Field field_name), obj) in
      let expr = Paren field in
      let encode_call = UnaryOp (CallMethod { meth="Encode"; args=[] }, expr) in
      Some (Universal_val.Simple encode_call)
    in
    [
      Basic (DeclareLocal { bind_name; mods=Binding_mod.(local ()); type_sig=Some (Atom (TNat (NatString))); initial_value })
    ]
  | Ephemeral ReturnAsEnum { candidate; valid_set; if_valid; if_invalid; _ } ->
    pp_statement @@ Branching (Switch {
      test_expr=candidate;
      cases=[
        Multic (List.map valid_set ~f:(fun (id,_) -> Foreign_val.(Pure (Bound (Ident.Binding.Const_name id))))), if_valid candidate
        ];
      default=Some (if_invalid candidate)
    })
  | Ephemeral DecodeTaggedUnion { input_binding; tag_type_name; tag_case_map; container } ->
    let open Foreign_val in
    let v_container = Pure (Bound (Ident.Binding.Variable_name container)) in
    [
      Branching (
        IfThenElse {
          test_expr=BinaryOp (Eq, v_container, Pure (VPrim (Prim_L)));
          true_branch=[Basic (Return (Some (FunApp { qual=[Ident.Scope.Namespace_name "fmt"]; f="Errorf"; generics=None; args=[[Pure (VLit (LitString "cannot decode nil ADT pointer"))]]})))];
          false_branch=[];
        }
      );
      (* ANCHOR - pick up here*)
    ]

  let rec ns_qualify ~ns_stack = function
  | mods, TypeDec ({ qual; _ } as td) ->
    [mods, TypeDec { td with qual = ns_stack @ qual }]
  | mods, FunDec ({ qual; definition=(LocalFunc _); _ } as fd) ->
    [mods, FunDec { fd with qual = ns_stack @ qual}]
  | (_, FunDec { definition=(GlobalFunc _); _ }) as fd ->
    [fd]
  | mods, Transcoder { as_method; as_lambda } ->
    ns_qualify ~ns_stack (mods, as_method) @ ns_qualify ~ns_stack (mods, as_lambda)
  | mods, GlobalDec ({ qual; _ } as gd) ->
    [mods, GlobalDec { gd with qual = ns_stack @ qual }]


  let rec pp_declaration = function
  | _, Define_namespace { namespace_name; contents } ->
    List.concat_map (List.concat_map ~f:(ns_qualify ~ns_stack:[Ident.Scope.Namespace_name namespace_name]) contents)
    ~f:pp_declaration
  | mods, 

    
end
*)

module Golang_backend : Backend with module A := GO_AST = struct
  module A = GO_AST
  include Golang_repr
  include Golang_backend_types
  include Golang_value_table
  include Golang_symbol_table

  let preamble =
    let relmod = "backend/" in
    [
      ImportAll { qualified_name = None; source_file = relmod ^ "encoder" };
      ImportAll { qualified_name = None; source_file = relmod ^ "zarith" };
      ImportAll { qualified_name = None; source_file = relmod ^ "byteparser" };
      ImportAll { qualified_name = None; source_file = relmod ^ "util" };
      ImportAll { qualified_name = None; source_file = "math/big" };
    ]

  let patch_imports ident =
    if String.is_suffix ~suffix:"script.expr" ident then fun imports ->
      ImportAll
        { qualified_name = None; source_file = "goapi/script_expr_patch" }
      :: imports
    else fun xs -> xs

  let create_parser ~from =
    match from with
    | [ hd ] -> Printf.sprintf "%s.Parse()" hd
    | _ ->
        failwith
          "Golang create_parser expects one receiver and no arguments exactly"
end
