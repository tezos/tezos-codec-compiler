open Backend.Foreign

module T : sig
  type nat = Foreign.Foreign_type.nat

  type prim = [ Foreign.Foreign_type.prim | `PrimReserveSuffix ]

  type reference_type = Foreign.Foreign_type.reference_type

  type linear_type =
    [ `LTArray of t * int
    | `LTSequence of Codec.Codec_repr.width * t
    | `LTSlice of t ]

  and composite_type =
    [ `CTDynWidth of Codec.Codec_repr.width * t
    | `CTLinear of linear_type
    | `CTOption of t ]

  and atom =
    [ `TComposite of composite_type
    | `TNat of nat
    | `TPrim of prim
    | `TRefer of reference_type ]

  and t =
    [ `Atom of atom
    | `Object of (string * t) list
    | `Enum of Codec.Codec_repr.enum_size * (string * int) list
    | `TaggedUnion of t * t list ]
end

module V : sig
  type prim =
    [ `PrimEnumSize of Codec.Codec_repr.enum_size
    | `PrimFalse
    | `PrimNum of string
    | `PrimTagSize of Codec.Codec_repr.tag_size
    | `PrimTrue
    | (* | `PrimVariantTranscoderParamValue of (int * Foreign.Ident.binding)
         list *)
      `PrimWidth of
      Codec.Codec_repr.size_repr
    | `Prim_L ]

  type binary_op = Foreign.Foreign_val.binary_op

  type literal =
    [ `LitArray of t list
    | `LitObject of t Foreign.Common.record_fill
    | `LitSlice of t list
    | `LitString of string ]

  and derived_term =
    [ `Dereference | `Field of string | `Index of t | `KeyVal of t | `Reference ]

  and unary_op =
    [ `Access of derived_term
    | `Assert of T.t
    | `BooleanNegate
    | `CallMethod of string * t list
    | `Conv of T.t ]

  and term =
    [ `VBound of Foreign.Ident.binding | `VLit of literal | `VPrim of prim ]

  and t =
    [ `ADTValEncoding of t
    | `ADTTagEncoding of t
    | `BinaryOp of binary_op * t * t
    | `ConcatStrings of t list
    | `CreateParser of t list
    | `FunApp of (Foreign.Qualification.t * string) * t list
    | `Create of T.t
    | `Paren of t
    | `Pure of term
    | `UnaryOp of unary_op * t ]
end

module S : sig
  module T = T
  module V = V

  type initialized =
    [ `Mono of Ident.var_id * V.t
    | `First of Ident.var_id * V.t
    | `Second of Ident.var_id * V.t
    | `Both of (Ident.var_id * Ident.var_id) * V.t ]

  type block_scope_expr = [ V.t | `InitRefer of initialized * V.t ]

  type t =
    [ `DeclareLocal of Ident.var_id * Binding_mod.t * T.t option * V.t option
    | `AssignLocal of Ident.var_id * V.derived_term option * V.t
    | `BareExpression of V.t
    | `DecodeTaggedUnion of
      string * string * ((string * int) * (string * T.t)) list * string
    | `InitParser of V.t
    | `Return of V.t option
    | `ThrowError of V.t
    | `ThrowResultIfError of V.t
    | `IfThenElse of block_scope_expr * t list * t list
    | `Switch of block_scope_expr * (V.t list * t list) list * t list option ]
end

module F : sig
  module T = T
  module V = V
  module S = S

  type kind = [ `Pointer | `Value ]

  type recv = {
    binding : string option;
    kind : kind;
    qual : Foreign.Qualification.t;
    type_name : string;
  }

  type t' = {
    params : (T.t, V.t) Foreign.Common.fun_args;
    return_type : T.t list;
    body : S.t list;
  }

  type t = [ `GlobalFunc of t' | `LocalFunc of t' | `TypeMethod of recv * t' ]
end

module D : sig
  module T = T
  module V = V
  module S = S
  module F = F

  type t' = [ `TypeDec of Foreign.Qualification.t * Foreign.Ident.type_id * T.t ]

  type t =
    [ `TypeDec of Foreign.Qualification.t * Foreign.Ident.type_id * T.t
    | `FunDec of Foreign.Qualification.t * Foreign.Ident.fun_id * F.t ]

  type u' =
    [ `Group of (Foreign.Binding_mod.t * t) list ]

  type u = 
    [ u' | `Collection of ((Foreign.Binding_mod.t * t') * u' list * u') ]

  type v = [ u | `Namespace of string * v list ]

end

module GO_AST : sig
  module T = T

  module V = V

  module S = S

  module F = F
  module D = D

  type source_file = {
    filename : string;
    preamble : Foreign.Common.directive list;
    contents : D.v list;
  }

  type foreign_codebase =
    | SingleFile of source_file
    | ManyFile of source_file list
end
