language
========

Some of the terminology used in this readme is laid out in
the [root](../../README.md) and [parent](../README.md) readme files, which should preferably
be read first.

The `language` library contains the following sub-libraries:
  - [`common`](./common): Assorted modules defining various constructs that are tied directly to the *language AST generation* or *production* phases of the compiler, but which are not themselves tied to any specific target language.
  - One sub-library per language, named after the target language:
    - [`typescript`](./typescript): Library providing support for TypeScript as a target language of the compiler
    - [`golang`](./golang): (__WIP__) Library providing partial support for Golang as a target language of the compiler 

As new target languages are provisionally added to the project, new sub-libraries will be created under this directory,
each containing the set of definitions required for the compiler to fully support each such target language.

As each supported language requires its own library, which
may not have many interesting unique features, a README
will only be included for language-specific libraries that
require additional explanation for whatever reason. At this time,
no such languages have been implemented.
