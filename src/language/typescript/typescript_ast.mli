module T : sig
  type nat = Foreign.Foreign_type.nat

  type prim = Foreign.Foreign_type.prim

  type reference_type = Foreign.Foreign_type.reference_type

  type linear_type = [ `LTSequence of t ]

  and composite_type =
    [ `CTDynWidth of Codec.Codec_repr.width * t
    | `CTLinear of linear_type
    | `CTOption of t
    | `CTTagless of t
    | `CTTuple2 of t * t ]

  and atom =
    [ `TComposite of composite_type
    | `TLiteral of string
    | `TNarrow of (Foreign.Qualification.t * string) * string
    | `TNat of nat
    | `TParam of string
    | `TPrim of prim
    | `TRefer of reference_type ]

  and t =
    [ `Atom of atom
    | `Enum of (string * int) list
    | `Object of (string * t) list
    | `Union of t list ]
end

module V : sig
  type prim =
    [ `PrimEnumSize of Codec.Codec_repr.enum_size
    | `PrimFalse
    | `PrimNum of string
    | `PrimTagSize of Codec.Codec_repr.tag_size
    | `PrimTrue
    | `PrimVariantTranscoderParamValue of (int * Foreign.Ident.binding) list
    | `PrimWidth of Codec.Codec_repr.size_repr
    | `Prim_L ]

  type binary_op = Foreign.Foreign_val.binary_op

  type literal =
    [ `LitArray of t list
    | `LitObject of t Foreign.Common.record_fill
    | `LitString of string ]

  and derived_term = [ `Field of string | `Index of t | `KeyVal of t ]

  and unary_op =
    [ `Access of derived_term
    | `BooleanNegate
    | `CallMethod of string * t list
    | `Cast of T.t ]

  and term =
    [ `VBound of Foreign.Ident.binding | `VLit of literal | `VPrim of prim ]

  and test_expr =
    [ `ValidAsEnum of t * Foreign.Qualification.t * string * (string * int) list
    ]

  and t =
    [ `BinaryOp of binary_op * t * t
    | `ConcatStrings of t list
    | `CreateParser of t list
    | `FunApp of
      (Foreign.Qualification.t * string) * (T.t list option * t list list)
    | `Paren of t
    | `Pure of term
    | `TestExpr of test_expr
    | `TranscoderCall of
      (Foreign.Common.transcoder_dir * T.t) * (T.t list option * t list list)
    | `UnaryOp of unary_op * t ]
end

module S : sig
  module T = T
  module V = V

  type t =
    [ `AssignLocal of string * V.derived_term option * V.t
    | `BareExpression of V.t
    | `DeclareLocal of string * Foreign.Binding_mod.t * T.t option * V.t option
    | `IfThenElse of V.t * t list * t list
    | `Return of V.t option
    | `ThrowError of V.t ]
end

module F : sig
  module T = T
  module V = V
  module S = S

  type t' = {
    params : (T.t, V.t) Foreign.Common.fun_args;
    return_type : T.t list;
    body : S.t list;
  }

  type t = [ `GlobalFunc of t' | `LocalFunc of t' ]
end

module D : sig
  module T = T
  module V = V
  module S = S
  module F = F

  type t' = [ `TypeDec of Foreign.Qualification.t * Foreign.Ident.type_id * T.t ]

  type t =
    [ `TypeDec of Foreign.Qualification.t * Foreign.Ident.type_id * T.t
    | `FunDec of Foreign.Qualification.t * Foreign.Ident.fun_id * F.t ]

  type u' =
    [ `Group of (Foreign.Binding_mod.t * t) list ]

  type u = 
    [ u' | `Collection of ((Foreign.Binding_mod.t * t') * u' list * u') ]

  type v = [ u | `Namespace of string * v list ]

end

module TS_AST : sig
  module T = T
  module V = V
  module S = S
  module F = F
  module D = D

  type source_file = {
    filename : string;
    preamble : Foreign.Common.directive list;
    contents : D.v list;
  }

  type foreign_codebase =
    | SingleFile of source_file
    | ManyFile of source_file list
end
