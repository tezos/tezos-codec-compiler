open Common.Language_converter
open Backend.Representation.Abstract_type
open Typescript_ast

(** Generate a namespace-wrapped set of definitions of a toplevel type and all
    of its named internal types, returning an updated version of the parameter
    [ctxt].

    @param name Raw identifier used for the toplevel type
    @param ctxt Generator context from previous passes
    @param rep [typrep] representing the toplevel type
    @return Tuple consisting of the updated context and the namespace-wrapped
    definitions *)
val convert_toplevel :
  name:string ->
  ctxt:GenContext.t ->
  rep:typrep ->
  (string * typrep) list ->
  GenContext.t * D.v list
