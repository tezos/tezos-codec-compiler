open Core
open Common.Language_converter
open Representation.Abstract_type
open Foreign
open Foreign.Common
open Codec.Codec_repr
open Typescript_ast

let rec convert_abstract_type : abstract_type -> TS_AST.T.t = function
  | AbstractBool -> `Atom (`TPrim `PrimBool)
  | AbstractInt (s, b) -> `Atom (`TPrim (`PrimInt (s, b)))
  | AbstractIntRanged ((min, max), bytes) ->
      `Atom (`TPrim (`PrimIntRanged ((min, max), bytes)))
  | AbstractString (VariableLength w) ->
      `Atom (`TComposite (`CTDynWidth (w, `Atom (`TPrim `PrimString))))
  | AbstractBytes (VariableLength w) ->
      `Atom (`TComposite (`CTDynWidth (w, `Atom (`TPrim `PrimOpaqueBytes))))
  | AbstractBytes (FixedLength n) -> `Atom (`TPrim (`PrimOpaqueBytesFixedLen n))
  | AbstractSequence (w, elem) -> (
      (* REVIEW[epic=stopgap] - handle sequence of amorphous blobs as a blob
         itself *)
      match elem with
      | AbstractBytes (DynamicLength | IndeterminateLength) -> (
          match w with
          | None ->
              failwith
                "Both sequence and its elements have indeterminate byte-length"
          | Some width ->
              `Atom
                (`TComposite
                  (`CTDynWidth (width, `Atom (`TPrim `PrimOpaqueBytes)))))
      | AbstractBytes (VariableLength (ReserveSuffix _)) -> assert false
      | _ ->
          `Atom
            (`TComposite (`CTLinear (`LTSequence (convert_abstract_type elem))))
      )
  | AbstractOption value ->
      `Atom (`TComposite (`CTOption (convert_abstract_type value)))
  | AbstractNamed (mwidth, name) -> (
      let qualids, base_name = reformat name in
      let qual =
        let f = function
          | x when String.is_suffix x ~suffix:"_tag" -> Ident.Scope.Type_name x
          | x -> Ident.Scope.Inferred_scope x
        in
        List.map ~f qualids
      in
      let base = `Atom (`TRefer (`RTOpaque (qual, base_name))) in
      match mwidth with
      | None -> base
      | Some width -> `Atom (`TComposite (`CTDynWidth (width, base))))
  | AbstractDyn (sz, AbstractSequence (None, inner_typ)) ->
      convert_abstract_type (AbstractSequence (Some (DynPrefix sz), inner_typ))
  | AbstractDyn (sz, inner_type) ->
      `Atom
        (`TComposite
          (`CTDynWidth (DynPrefix sz, convert_abstract_type inner_type)))
  | AbstractPadding n -> `Atom (`TPrim (`PrimPadding n))
  (* REVIEW[epic=design] - We are treating AbstractVoid as zero bytes of padding *)
  | AbstractVoid -> `Atom (`TPrim (`PrimPadding 0))
  | AbstractZarith_N -> `Atom (`TPrim `PrimZarith_N)
  | AbstractZarith_Z -> `Atom (`TPrim `PrimZarith_Z)
  | AbstractBytes DynamicLength -> `Atom (`TPrim `PrimOpaqueBytes)
  | AbstractString DynamicLength -> `Atom (`TPrim `PrimString)
  | AbstractEnum (sz, name) ->
      let qualids, base_name = reformat name in
      let qual =
        let f x = Ident.Scope.Inferred_scope x in
        List.map ~f qualids
      in
      `Atom
        (`TRefer
          (`RTEnum
            ((qual, base_name), Option.value_exn (Int_size.to_enum_size sz))))
  | AbstractTuple2 (t1, t2) ->
      let t1' = convert_abstract_type t1 and t2' = convert_abstract_type t2 in
      `Atom (`TComposite (`CTTuple2 (t1', t2')))
  | AbstractFloat precision -> `Atom (`TPrim (`PrimFloat precision))
  | AbstractTagVal { base; elem; _ } ->
      let qualids, base_name = reformat base in
      let qual =
        let f x = Ident.Scope.Inferred_scope x in
        List.map ~f qualids
      in
      `Atom (`TNarrow ((qual, base_name), elem))
  | other ->
      failwith
        ("unhandled type: " ^ Sexp.to_string ([%sexp_of: abstract_type] other))

(* ANCHOR - gen_transcoder_call *)
let rec gen_transcoder_call ~dir ?on ?(apply = true) typ : TS_AST.V.t =
  let for_type = convert_abstract_type typ
  and final_args =
    if not apply then []
    else
      match dir with
      | Encode -> [ [ Option.value ~default:(name_to_var "val") on ] ]
      | Decode -> [ [ Option.value ~default:(name_to_var "p") on ] ]
  in
  match typ |> devoid with
  | AbstractVoid -> assert false
  | AbstractPadding n ->
      let final_args =
        if not apply then []
        else match dir with Encode -> [ [] ] | Decode -> final_args
      in
      let args = [ int_to_fv n ] :: final_args in
      `TranscoderCall ((dir, for_type), (None, args))
  | AbstractBool | AbstractInt _ | AbstractFloat _ | AbstractIntRanged _
  | AbstractZarith_N | AbstractZarith_Z
  | AbstractNamed (None, _) ->
      `TranscoderCall ((dir, for_type), (None, final_args))
  | AbstractSequence (None, AbstractBytes IndeterminateLength) ->
      failwith "AbstractSequence cannot be transcoded without a width"
  | AbstractSequence (Some w, AbstractBytes IndeterminateLength) ->
      (* REVIEW[epic=needs_refactor] - Pinning behavior of blob-array as single
         blob *)
      gen_transcoder_call ~dir ?on ~apply (AbstractBytes (VariableLength w))
  | AbstractOption inner_typ | AbstractSequence (_, inner_typ) ->
      let inner_call = gen_transcoder_call ~dir ~apply:false inner_typ in
      let args =
        match typ with
        | AbstractOption _ -> [ inner_call ] :: final_args
        | AbstractSequence (Some (DynPrefix w), _) ->
            [ inner_call; `Pure (`VPrim (`PrimWidth w)) ] :: final_args
        | AbstractSequence (Some (ReserveSuffix n), _) ->
            [ inner_call; int_to_fv n ] :: final_args
        | AbstractSequence (None, _) ->
            [%sexp_of: abstract_type] typ |> Sexp.to_string |> fun x ->
            failwith
              ("AbstractSequence cannot be transcoded without a width: " ^ x)
        | _ -> assert false
      in
      `TranscoderCall ((dir, for_type), (None, args))
  | AbstractNamed (Some (ReserveSuffix n), inner_typ) ->
      let inner_typ = AbstractNamed (None, inner_typ) in
      let inner_call = gen_transcoder_call ~dir ~apply:false inner_typ in
      let args = [ int_to_fv n ] :: [ inner_call ] :: final_args
      and generics =
        Some
          [
            convert_abstract_type inner_typ; `Atom (`TLiteral (Int.to_string n));
          ]
      in
      `TranscoderCall ((dir, for_type), (generics, args))
  | AbstractNamed (Some (DynPrefix w), inner_typ) ->
      let inner_typ = AbstractNamed (None, inner_typ) in
      let inner_call = gen_transcoder_call ~dir ~apply:false inner_typ in
      let args = [ width_to_fv w ] :: [ inner_call ] :: final_args
      and generics =
        Some [ convert_abstract_type inner_typ; `Atom (`TPrim `PrimWidth) ]
      in
      `TranscoderCall ((dir, for_type), (generics, args))
  | AbstractDyn (sz, AbstractSequence (None, inner_typ)) ->
      gen_transcoder_call
        ~dir
        ?on
        ~apply
        (AbstractSequence (Some (DynPrefix sz), inner_typ))
  | AbstractDyn (sz, inner_typ) ->
      let inner_call = gen_transcoder_call ~dir ~apply:false inner_typ in
      let args = [ width_to_fv sz ] :: [ inner_call ] :: final_args
      and generics =
        Some [ convert_abstract_type inner_typ; `Atom (`TPrim `PrimWidth) ]
      in
      `TranscoderCall ((dir, for_type), (generics, args))
  | AbstractString kind -> (
      match kind with
      | FixedLength n ->
          let args = [ int_to_fv n ] :: final_args in
          `TranscoderCall ((dir, for_type), (None, args))
      | VariableLength (DynPrefix w) ->
          let inner_call =
            gen_transcoder_call ~dir ~apply:false (AbstractString DynamicLength)
          in
          let args = [ width_to_fv w ] :: [ inner_call ] :: final_args
          and generics =
            Some [ `Atom (`TPrim `PrimString); `Atom (`TPrim `PrimWidth) ]
          in
          `TranscoderCall ((dir, for_type), (generics, args))
      | VariableLength (ReserveSuffix n) ->
          let inner_call =
            gen_transcoder_call ~dir ~apply:false (AbstractString DynamicLength)
          in
          let args = [ int_to_fv n ] :: [ inner_call ] :: final_args
          and generics =
            Some
              [
                `Atom (`TPrim `PrimString); `Atom (`TLiteral (Int.to_string n));
              ]
          in
          `TranscoderCall ((dir, for_type), (generics, args))
      | DynamicLength -> `TranscoderCall ((dir, for_type), (None, final_args))
      | IndeterminateLength ->
          failwith
            "Cannot produce transcoder for indeterminate-length AbstractString")
  | AbstractBytes kind -> (
      match kind with
      | FixedLength n ->
          let args = [ int_to_fv n ] :: final_args in
          `TranscoderCall ((dir, for_type), (None, args))
      | VariableLength (DynPrefix w) ->
          let inner_call =
            gen_transcoder_call ~dir ~apply:false (AbstractBytes DynamicLength)
          in
          let args = [ width_to_fv w ] :: [ inner_call ] :: final_args
          and generics =
            Some [ `Atom (`TPrim `PrimOpaqueBytes); `Atom (`TPrim `PrimWidth) ]
          in
          `TranscoderCall ((dir, for_type), (generics, args))
      | VariableLength (ReserveSuffix n) ->
          let inner_call =
            gen_transcoder_call ~dir ~apply:false (AbstractBytes DynamicLength)
          in
          let args = [ int_to_fv n ] :: [ inner_call ] :: final_args
          and generics =
            Some
              [
                `Atom (`TPrim `PrimOpaqueBytes);
                `Atom (`TLiteral (Int.to_string n));
              ]
          in
          `TranscoderCall ((dir, for_type), (generics, args))
      | DynamicLength ->
          `TranscoderCall ((dir, for_type), (None, [] :: final_args))
      | IndeterminateLength ->
          failwith
            "Cannot produce transcoder for indeterminate-length AbstractBytes")
  | AbstractEnum _ -> `TranscoderCall ((dir, for_type), (None, final_args))
  | AbstractTuple2 (t1, t2) ->
      let inner_call1 = gen_transcoder_call ~dir ~apply:false t1
      and inner_call2 = gen_transcoder_call ~dir ~apply:false t2 in
      let generics = Some [ convert_abstract_type t1; convert_abstract_type t2 ]
      and args = [ inner_call1; inner_call2 ] :: final_args in
      `TranscoderCall ((dir, for_type), (generics, args))
  | other ->
      failwith
        ("Unhandled transcoder case " ^ Sexp.to_string
        @@ [%sexp_of: abstract_type] other)

module Gen = struct
  module Context = GenContext

  let mk_ident = patch_ident
  (* let mk_legal = function | '-' -> "_" | ',' -> "_COMMA_" | '+' -> "_PLUS_" |
     '(' | ')' | ' ' -> "" | c -> String.of_char c in id |> String.concat_map
     ~sep:"" ~f:mk_legal *)

  let mk_ns ?prefix id = Qualification.mk_ns ?prefix id

  let transcoder_ident ~dir id =
    let join_id ~affix base = String.concat ~sep:"_" [ mk_ident base; affix ] in
    let affix = match dir with Encode -> "encoder" | Decode -> "decoder" in
    join_id id ~affix

  let enum_ident ~ix id =
    let anon_prefix = "ANON" in
    match id with
    | None -> anon_prefix ^ Int.to_string ix
    | Some name -> mk_ident name

  let tag_ident id =
    (* Printf.eprintf "Tagging ident: %s\n" id; *)
    let affix = "tag" in
    String.concat ~sep:"_" [ id; affix ]

  module Simple = struct
    let cast_record ?ix = function
      | Anon t ->
          let pseudo =
            match ix with
            | None -> "_anon"
            | Some n -> "_anon" ^ Int.to_string n
          in
          Record [ pseudo, t ]
      | Record _ as r -> r
      | _ -> assert false

    let add_tag ~tag_type_name ~tag_size ~tag_name tag =
      let tag_field =
        let name = mk_ident tag_name in
        ( "_tag",
          AbstractTagVal
            { base = tag_type_name; tag_size; elem = name; value = tag } )
      in
      fun x ->
        match cast_record ~ix:tag x with
        | Record rs -> Record (tag_field :: rs)
        | _ -> assert false

    (* REVIEW[epic=design] - AbstractVoid as 0-width padding *)
    let remove_padding =
      List.filter ~f:(function
          | _, (AbstractPadding _ | AbstractVoid) -> false
          | _ -> true)

    module Anon = struct
      let typedec ~qual ~base_name typ : binding_mods * D.t =
        let definition = convert_abstract_type typ in
        Binding_mod.exposed_definite, `TypeDec (qual, base_name, definition)

      let encoder ~qual ~type_name typ =
        let dir = Encode
        and arg_name = "val"
        and return_type = [ `Atom (`TNat `NatString) ] in
        let body = [ `Return (Some (gen_transcoder_call ~dir typ)) ] in
        let intype = `Atom (`TRefer (`RTOpaque (qual, mk_ident type_name)))
        and base_name = transcoder_ident ~dir type_name in
        let params = `Typed [ arg_name, Mandatory, intype ] in
        let definition = `LocalFunc { F.params; return_type; body } in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)

      let decoder ~qual ~type_name typ =
        let dir = Decode
        and arg_name = "input"
        and intype = `Atom (`TNat `NatDecoderInput) in
        let params = `Typed [ arg_name, Mandatory, intype ] in
        let body =
          [
            `Return
              (Some (gen_transcoder_call ~dir ~on:(name_to_var arg_name) typ));
          ]
        and base_name = transcoder_ident ~dir type_name
        and return_type =
          [ `Atom (`TRefer (`RTOpaque (qual, mk_ident type_name))) ]
        in
        let definition = `LocalFunc { F.params; return_type; body } in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)
    end

    module Record = struct
      module Field = struct
        let get_fields = function Record flds -> flds | _ -> []

        let get_names =
          List.map ~f:(fun (name, _) -> mk_ident (sanitize_label name))

        let gen_bind ~dir ~src (name, rep) : S.t =
          match dir with
          | Encode ->
              let bind_name = mk_ident (sanitize_label name) in
              let mods = Binding_mod.local ~mut:Mutable ~var:Invariant ()
              and type_sig = Some (`Atom (`TNat `NatString))
              and initial_value =
                let field =
                  match rep with
                  | AbstractPadding n -> int_to_fv n
                  | AbstractVoid -> int_to_fv 0
                  | _ -> `UnaryOp (`Access (`Field bind_name), src)
                in
                Some (gen_transcoder_call ~dir rep ~on:field)
              in
              `DeclareLocal (bind_name, mods, type_sig, initial_value)
          | Decode -> (
              match rep with
              | AbstractVoid | AbstractPadding _ ->
                  `BareExpression (gen_transcoder_call ~dir ~on:src rep)
              | _ ->
                  let bind_name = mk_ident (sanitize_label name)
                  and mods =
                    Binding_mod.(local ~mut:Immutable ~var:Invariant ())
                  and type_sig = Some (convert_abstract_type rep)
                  and initial_value =
                    Some (gen_transcoder_call ~dir ~on:src rep)
                  in
                  `DeclareLocal (bind_name, mods, type_sig, initial_value))
      end

      let typedec ~qual ~base_name flds : Foreign.Binding_mod.t * D.t =
        let f (fname, ftype) =
          mk_ident (sanitize_label fname), convert_abstract_type ftype
        in
        let foreign_fields = remove_padding flds |> List.map ~f in
        let definition = `Object foreign_fields in
        ( Binding_mod.exposed_definite,
          `TypeDec (qual, mk_ident base_name, definition) )

      let encoder ~qual ~type_name ?(tagless = false) flds :
          Foreign.Binding_mod.t * D.t =
        let return_type = [ `Atom (`TNat `NatString) ] in
        let dir = Encode and arg_name = "val" in
        let intype =
          `Atom (`TRefer (`RTOpaque (qual, mk_ident type_name))) |> fun x ->
          if tagless then `Atom (`TComposite (`CTTagless x)) else x
        in
        let body =
          let names = Field.get_names flds
          and binds =
            List.map flds ~f:(Field.gen_bind ~dir ~src:(name_to_var arg_name))
          in
          let ret = `ConcatStrings (List.map ~f:name_to_var names) in
          binds @ [ `Return (Some ret) ]
        and base_name = transcoder_ident ~dir type_name in
        let params = `Typed [ arg_name, Mandatory, intype ] in
        let definition = `LocalFunc { F.params; return_type; body } in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)

      let decoder ~qual ~type_name ?(tagless = false) flds :
          Foreign.Binding_mod.t * D.t =
        let dir = Decode
        and arg_name = "input"
        and intype = `Atom (`TNat `NatDecoderInput) in
        let params = `Typed [ arg_name, Mandatory, intype ] in
        let body =
          let parser_bind = "p" in
          let names = remove_padding flds |> Field.get_names
          and bindp =
            let bind_name = parser_bind
            and mods = Binding_mod.local ~mut:Binding_mod.Mutable ()
            and type_sig = Some (`Atom (`TNat `NatParser))
            and initial_value = Some (`CreateParser [ name_to_var arg_name ]) in
            `DeclareLocal (bind_name, mods, type_sig, initial_value)
          and binds =
            List.map
              flds
              ~f:(Field.gen_bind ~dir ~src:(name_to_var parser_bind))
          in
          let ret =
            let obj =
              let explicit = List.map names ~f:(fun x -> NamedPun x) in
              `LitObject { explicit; implicit = None }
            in
            Some (`Pure (`VLit obj))
          in
          bindp :: binds @ [ `Return ret ]
        and base_name = transcoder_ident ~dir type_name
        and return_type =
          `Atom (`TRefer (`RTOpaque (qual, mk_ident type_name))) |> fun x ->
          if tagless then [ `Atom (`TComposite (`CTTagless x)) ] else [ x ]
        in
        let definition = `LocalFunc { F.params; return_type; body } in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)
    end

    (* ANCHOR - Gen.Simple.convert *)
    let convert ~qual ~base_name = function
      | Anon typ ->
          let typedec = Anon.typedec ~qual ~base_name typ
          and encoder = Anon.encoder ~qual ~type_name:base_name typ
          and decoder = Anon.decoder ~qual ~type_name:base_name typ in
          `Group [ typedec; encoder; decoder ]
      | Record flds ->
          let typedec = Record.typedec ~qual ~base_name flds
          and encoder = Record.encoder ~qual ~type_name:base_name flds
          and decoder = Record.decoder ~qual ~type_name:base_name flds in
          `Group [ typedec; encoder; decoder ]
      | _ -> assert false
  end

  module Algebraic = struct
    module EnumType = struct
      let get_elems variants =
        let f = function
          | Enum_variant (mcons, ix) ->
              let cons = enum_ident ~ix mcons in
              cons, ix
          | _ -> assert false
        in
        List.rev_map ~f variants

      let typedec ~qual ~base_name variants =
        let elems = get_elems variants in
        let definition = `Enum elems in
        Binding_mod.exposed_definite, `TypeDec (qual, base_name, definition)

      let encoder ~qual ~type_name ~(enum_size : enum_size) =
        let dir = Encode
        and arg_name = "val"
        and return_type = [ `Atom (`TNat `NatString) ] in
        let intype = `Atom (`TRefer (`RTOpaque (qual, mk_ident type_name))) in
        let base_name = transcoder_ident ~dir type_name
        and params = `Typed [ arg_name, Mandatory, intype ]
        and body =
          let ret =
            let for_type =
              `Atom (`TPrim (`PrimInt (Unsigned, to_bits enum_size)))
            in
            let on_value = `UnaryOp (`Cast for_type, name_to_var arg_name) in
            `TranscoderCall ((dir, for_type), (None, [ [ on_value ] ]))
          in
          [ `Return (Some ret) ]
        in
        let definition = `LocalFunc { F.params; return_type; body } in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)

      let decoder ~qual ~type_name ~(enum_size : enum_size) variants =
        let dir = Decode
        and arg_name = "input"
        and intype = `Atom (`TNat `NatDecoderInput) in
        let params = `Typed [ arg_name, Mandatory, intype ] in
        let base_name = transcoder_ident ~dir type_name in
        let body =
          let bind_name = "ret" in
          let for_type =
            `Atom (`TPrim (`PrimInt (Unsigned, to_bits enum_size)))
          in
          let bind_ret =
            let initial_value =
              let on_value = name_to_var arg_name in
              Some (`TranscoderCall ((dir, for_type), (None, [ [ on_value ] ])))
            and type_sig = Some for_type
            and mods = Binding_mod.(local ~mut:Immutable ~var:Invariant ()) in
            `DeclareLocal (bind_name, mods, type_sig, initial_value)
          and cond_ret =
            let test_expr =
              let candidate = name_to_var bind_name
              and enum_qual = qual
              and enum_base_name = type_name
              and enum_values = get_elems variants in
              `TestExpr
                (`ValidAsEnum
                  (candidate, enum_qual, enum_base_name, enum_values))
            and true_branch = [ `Return (Some (name_to_var bind_name)) ]
            and false_branch =
              [
                `ThrowError
                  (string_to_lit
                  @@ Printf.sprintf
                       "%s: illegal value for enum %s"
                       base_name
                       type_name);
              ]
            in
            `IfThenElse (test_expr, true_branch, false_branch)
          in
          [ bind_ret; cond_ret ]
        and return_type =
          [ `Atom (`TRefer (`RTOpaque (qual, mk_ident type_name))) ]
        in
        let definition = `LocalFunc { F.params; return_type; body } in
        Binding_mod.lambda, `FunDec (qual, base_name, definition)
    end

    module ADT = struct
      module VariantType = struct
        let generate ~qual ~base_name ~cons ~tag_val ~tag_size ~tag_type_name
            ?typ () : string * (string * int) * D.u' =
          let tag_name, subtype_name =
            match cons with
            | None ->
                let n = Int.to_string tag_val in
                "Tag" ^ n, mk_ident base_name ^ n
            | Some name -> mk_ident name, mk_ident base_name ^ mk_ident name
          in
          let enum_elem = tag_name, tag_val in
          let raw_subtype =
            match typ with
            | Some typ -> Simple.cast_record ~ix:tag_val typ
            | None -> Simple.cast_record (Anon AbstractVoid)
          in
          let tagged_subtype =
            Simple.add_tag
              ~tag_type_name:(mk_ident tag_type_name)
              ~tag_size
              ~tag_name
              tag_val
              raw_subtype
          in
          let typedec : Foreign.Binding_mod.t * D.t =
            let flds = Simple.Record.Field.get_fields tagged_subtype
            and base_name = subtype_name in
            Simple.Record.typedec ~qual ~base_name flds
          and encoder : Foreign.Binding_mod.t * D.t =
            let flds = Simple.Record.Field.get_fields raw_subtype
            and type_name = subtype_name in
            Simple.Record.encoder ~qual ~type_name ~tagless:true flds
          and decoder : Foreign.Binding_mod.t * D.t =
            let flds = Simple.Record.Field.get_fields raw_subtype
            and type_name = subtype_name in
            Simple.Record.decoder ~qual ~type_name ~tagless:true flds
          in
          subtype_name, enum_elem, `Group [ typedec; encoder; decoder ]
      end

      let generate ~qual ~base_name ~tag_size variants ~ctxt : GenContext.t * D.v =
        let ctxt', qual =
          let qual' =
            Qualification.amalgamate qual (mk_ns ~prefix:true base_name)
          in
          let ctxt' =
            Context.add_assoc
              (Ident.Scope.Type_name base_name :: qual)
              (Ident.Scope.Type_name base_name :: qual')
              ctxt
          in
          ctxt', qual'
        in
        let tag_type_name = tag_ident base_name in
        let f (subtypes, enum_elems, declarations) var =
          let new_subtype, new_enum_elem, new_declarations =
            match var with
            | Enum_variant (cons, tag_val) ->
                VariantType.generate
                  ~qual
                  ~base_name
                  ~cons
                  ~tag_val
                  ~tag_size
                  ~tag_type_name
                  ()
            | Type_variant (cons, tag_val, typ) ->
                VariantType.generate
                  ~qual
                  ~base_name
                  ~cons
                  ~tag_val
                  ~tag_size
                  ~tag_type_name
                  ~typ
                  ()
          in
          ( new_subtype :: subtypes,
            new_enum_elem :: enum_elems,
            new_declarations :: declarations )
        in
        let subtypes, enum_elems, declarations =
          List.fold ~init:([], [], []) ~f variants
        in
        let tagtype_dec =
          let base_name = tag_type_name and definition = `Enum enum_elems in
          Binding_mod.exposed_definite, `TypeDec (qual, base_name, definition)
        and adt_typedec : binding_mods * D.t =
          let f name : T.t =
            `Atom (`TRefer (`RTEnum ((qual, name), (tag_size :> enum_size))))
          in
          let definition = `Union (List.map subtypes ~f) in
          Binding_mod.exposed_definite, `TypeDec (qual, base_name, definition)
        and assoc ~dir =
          let f subtype (_, tag_val) =
            tag_val, Ident.Binding.Variable_name (transcoder_ident ~dir subtype)
          in
          List.map2_exn subtypes enum_elems ~f
        in
        let adt_encoder : binding_mods * D.t =
          let dir = Encode
          and arg_name = "val"
          and return_type = [ `Atom (`TNat `NatString) ] in
          let body =
            let arg : V.t =
              `Pure (`VPrim (`PrimVariantTranscoderParamValue (assoc ~dir)))
            and f = transcoder_ident ~dir "variant"
            and tag_size = `Pure (`VPrim (`PrimTagSize tag_size)) in
            let args = [ [ arg ]; [ tag_size ]; [ name_to_var arg_name ] ]
            and generics =
              Some
                [
                  `Atom (`TRefer (`RTOpaque ([], tag_type_name)));
                  `Atom (`TRefer (`RTOpaque ([], base_name)));
                ]
            in
            [ `Return (Some (`FunApp (([], f), (generics, args)))) ]
          and base_name = transcoder_ident ~dir base_name
          and intype = `Atom (`TRefer (`RTOpaque (qual, base_name))) in
          let params = `Typed [ arg_name, Mandatory, intype ] in
          let definition = `LocalFunc { F.params; return_type; body } in
          Binding_mod.lambda, `FunDec (qual, base_name, definition)
        and adt_decoder : binding_mods * D.t =
          let dir = Decode
          and arg_name = "input"
          and intype = `Atom (`TNat `NatDecoderInput) in
          let params = `Typed [ arg_name, Mandatory, intype ] in
          let as_lambda =
            let body =
              let arg =
                `Pure (`VPrim (`PrimVariantTranscoderParamValue (assoc ~dir)))
              and f = transcoder_ident ~dir "variant"
              and tag_size = `Pure (`VPrim (`PrimTagSize tag_size)) in
              let args = [ [ arg ]; [ tag_size ]; [ name_to_var arg_name ] ]
              and generics =
                Some
                  [
                    `Atom (`TRefer (`RTOpaque ([], tag_type_name)));
                    `Atom (`TRefer (`RTOpaque ([], base_name)));
                  ]
              in
              [ `Return (Some (`FunApp (([], f), (generics, args)))) ]
            and return_type =
              [ `Atom (`TRefer (`RTOpaque (qual, mk_ident base_name))) ]
            and base_name = transcoder_ident ~dir base_name in
            let definition = `LocalFunc { F.params; return_type; body } in
            `FunDec (qual, base_name, definition)
          in
          Binding_mod.lambda, as_lambda
        in
        let contents : D.v =
          `Collection
            ( tagtype_dec,
              declarations,
              `Group [ adt_typedec; adt_encoder; adt_decoder ] )
        in
        ( ctxt', `Namespace (Ident.Scope.to_string (List.last_exn qual), [contents]) )
    end
  end
end

let convert_enum_type ~qual ~base_name ~(enum_size : enum_size) variants : D.v =
  let typedec = Gen.Algebraic.EnumType.typedec ~qual ~base_name variants
  and encoder =
    Gen.Algebraic.EnumType.encoder ~qual ~type_name:base_name ~enum_size
  and decoder =
    Gen.Algebraic.EnumType.decoder
      ~qual
      ~type_name:base_name
      ~enum_size
      variants
  in
  `Group [ typedec; encoder; decoder ]

let convert_variants_type ~qual ~base_name ~tag_size variants ~ctxt : GenContext.t * D.v =
  Gen.Algebraic.ADT.generate ~qual ~base_name ~tag_size ~ctxt variants

let convert_adt_type ~qual ~base_name ~(tag_size : tag_size) ~variants ~ctxt : GenContext.t * D.v =
  let is_enum = function Enum_variant _ -> true | _ -> false in
  if List.for_all ~f:is_enum variants then
    ( ctxt,
      convert_enum_type
        ~qual
        ~base_name
        ~enum_size:(tag_size :> enum_size)
        variants )
  else convert_variants_type ~qual ~base_name ~tag_size ~ctxt variants

let convert_type ~name ~ctxt : typrep -> GenContext.t * D.v =
  let qualids, base_name = reformat name in
  let qual = List.map ~f:(fun x -> Ident.Scope.Inferred_scope x) qualids in
  function
  | ADT { tag_size; variants; _ } ->
      convert_adt_type ~qual ~base_name ~tag_size ~variants ~ctxt
  | Enum { enum_size; elements } ->
      ( ctxt,
        convert_enum_type
          ~qual
          ~base_name
          ~enum_size
          (List.map ~f:(fun (name, size) -> Enum_variant (name, size)) elements)
      )
  | x -> ctxt, Gen.Simple.convert ~qual ~base_name x

let convert_toplevel ~name ~ctxt ~rep subtypes : GenContext.t * D.v list =
  let qualids, base_name = reformat name in
  let qual = List.map ~f:(fun x -> Ident.Scope.Inferred_scope x) qualids in
  let ctxt', qual =
    let qual' =
      Qualification.amalgamate
        ~drop_proto:false
        qual
        (Gen.mk_ns ~prefix:false base_name)
    in
    ( Gen.Context.add_assoc
        (Ident.Scope.Namespace_name base_name :: qual)
        qual'
        ctxt,
      qual' )
  in
  let ctxt', rsubs =
    List.fold subtypes ~init:(ctxt', []) ~f:(fun (ctxt, decls) (name, rep) ->
        convert_type ~name ~ctxt rep |> fun (ctxt', decl) ->
        ctxt', decl :: decls)
  in
  let ctxt'', top = convert_type ~name:"t" ~ctxt:ctxt' rep in
  ( ctxt'',
    [ `Namespace
          ( Ident.Scope.to_string (List.hd_exn qual),
            List.(rev_append rsubs [ top ]) );
    ] )
