open Backend.Foreign
open Backend.Foreign.Common
open Codec.Codec_repr

module T = struct
  type nat = Foreign_type.nat

  type prim = Foreign_type.prim

  type reference_type = Foreign_type.reference_type

  type linear_type = [ `LTSequence of t ]

  and composite_type =
    [ `CTLinear of linear_type
    | `CTOption of t
    | `CTTuple2 of t * t
    | `CTDynWidth of width * t
    | `CTTagless of t ]

  and atom =
    [ `TPrim of prim
    | `TNat of nat
    | `TComposite of composite_type
    | `TRefer of reference_type
    | `TParam of Ident.tparam_id
    | `TLiteral of string
    | `TNarrow of (Qualification.t * Ident.type_id) * Ident.enum_id ]

  and t =
    [ `Atom of atom
    | `Object of (string * t) list
    | `Enum of (string * int) list
    | `Union of t list ]
end

module V = struct
  type prim =
    [ Foreign_val.prim
    | `PrimVariantTranscoderParamValue of (int * Ident.binding) list ]

  type binary_op = Foreign_val.binary_op

  type literal =
    [ `LitString of string | `LitArray of t list | `LitObject of t record_fill ]

  and derived_term = [ `Field of Ident.lab_id | `Index of t | `KeyVal of t ]

  and unary_op =
    [ `Access of derived_term
    | `CallMethod of Ident.fun_id * t list
    | `BooleanNegate
    | `Cast of T.t ]

  and term = [ `VPrim of prim | `VLit of literal | `VBound of Ident.binding ]

  and test_expr =
    [ `ValidAsEnum of
      t * Qualification.t * Ident.type_id * (Ident.enum_id * int) list ]

  and t =
    [ `Pure of term
    | `CreateParser of t list
    | `ConcatStrings of t list
    | `UnaryOp of unary_op * t
    | `BinaryOp of binary_op * t * t
    | `TestExpr of test_expr
    | `Paren of t
    | `FunApp of
      (Qualification.t * Ident.fun_id) * (T.t list option * t list list)
    | `TranscoderCall of
      (transcoder_dir * T.t) * (T.t list option * t list list) ]
end

module S = struct
  module T = T
  module V = V

  type t =
    [ `DeclareLocal of Ident.var_id * Binding_mod.t * T.t option * V.t option
    | `AssignLocal of Ident.var_id * V.derived_term option * V.t
    | `BareExpression of V.t
    | `Return of V.t option
    | `ThrowError of V.t
    | `IfThenElse of V.t * t list * t list ]
end

module F = struct
  module T = T
  module V = V
  module S = S

  type t' = {
    params : (T.t, V.t) Common.fun_args;
    return_type : T.t list;
    body : S.t list;
  }

  type t = [ `GlobalFunc of t' | `LocalFunc of t' ]
end

module D = struct
  module T = T
  module V = V
  module S = S
  module F = F

  type t' = Declaration.Declaration(T)(V)(S)(F).t'
  type t = Declaration.Declaration(T)(V)(S)(F).t
  type u' = Declaration.Declaration(T)(V)(S)(F).u'
  type u = Declaration.Declaration(T)(V)(S)(F).u

  type v =
    [ Declaration.Declaration(T)(V)(S)(F).v
    | `Namespace of Ident.namespace_id * v list ]
end

module TS_AST = struct
  module T = T
  module V = V
  module S = S
  module F = F
  module D = D

  type source_file = {
    filename : string;
    preamble : directive list;
    contents : D.v list;
  }

  type foreign_codebase =
    | SingleFile of source_file
    | ManyFile of source_file list
end
