open Core
open Common.Language_repr
open Backend.Representation.Abstract_type
open Backend.Foreign
open Backend.Foreign.Common
open Backend.Codec.Codec_repr
open Typescript_ast

module Typescript_backend_types : Types_backend = struct
  let t_width_uint30 = "\"uint30\""

  let t_width_uint8 = "\"uint8\""

  let t_tagsize_uint8 = "\"uint8\""

  let t_tagsize_uint16 = "\"uint16\""

  let t_padding = "void"

  let t_reservesuffix_of ~reserve typ =
    Printf.sprintf "ReserveSuffix<%s,%d>" typ reserve

  let t_dynprefix_of ~width typ =
    match width with
    | `Uint8 -> Printf.sprintf "LenPref<%s,%s>" typ t_width_uint8
    | `Uint30 -> Printf.sprintf "LenPref<%s,%s>" typ t_width_uint30

  let t_tagless_of = Printf.sprintf "Tagless<%s>"

  let t_tuple_of = function
    | [] ->
        (* REVIEW[epic=design] - t_tuple_of should not be called with zero
           arguments in the current model *)
        assert false
    | [ singleton ] ->
        (* REVIEW[epic=design] - 1-Tuple of a type is just that type, for now *)
        singleton
    | [ fst; snd ] -> Printf.sprintf "Tuple<%s,%s>" fst snd
    | _ -> failwith "Typescript backend does not support N-tuples for N > 2"

  let t_width = "width"

  let t_variant_encoder_param = "{ [key: number]: Encoder<Object> }"

  let t_variant_decoder_param = "{ [key: number]: Decoder<Object> }"
end

module Typescript_types : Types_language with module T := T = struct
  module T = T

  let t_int = function
    | Signed, Bits8 -> "int8"
    | Signed, Bits16 -> "int16"
    | Signed, Bits32 -> "int32"
    | Signed, Bits64 -> "int64"
    | Unsigned, Bits8 -> "uint8"
    | Unsigned, Bits16 -> "uint16"
    | Unsigned, Bits32 -> "uint32"
    | Unsigned, Bits64 -> "uint64"

  let t_int_ranged ((min, _), bytes) =
    match bytes with
    | Bytes4 when min >= 0 -> "uint32"
    | Bytes4 -> "int32"
    | Bytes8 when min >= 0 -> "uint64"
    | Bytes8 -> "int64"

  let t_float : float_precision -> string = function
    | Single -> "float32"
    | Double -> "float64"

  let t_bool = "boolean"

  let t_string = "string"

  let t_linear_of ft = function `LTSequence elem -> ft elem ^ "[]"

  let t_option_of value = "Optional<" ^ value ^ ">"

  let t_orerror_of t = t ^ " | undefined"

  let t_zarith_n = "zarith_n" (* or : "N.t" *)

  let t_zarith_z = "zarith_z" (* or : "Z.t" *)

  let t_nat = function
    | `NatBool -> t_bool
    | `NatInt (s, Some w) -> t_int (s, w)
    | `NatInt (_, None) -> "number"
    | `NatString -> t_string
    | `NatError -> "Error"
    | `NatDecoderInput -> "decoder_argument"
    | `NatParser -> "TypedParser"

  let t_prim = function
    | `PrimBool -> t_bool
    | `PrimInt sw -> t_int sw
    | `PrimIntRanged (min, max) -> t_int_ranged (min, max)
    | `PrimString -> t_string
    | `PrimOpaqueBytes | `PrimOpaqueBytesFixedLen _ -> "bytes"
    | `PrimWidth -> Typescript_backend_types.t_width
    | `PrimFloat p -> t_float p
    | `PrimPadding _ -> Typescript_backend_types.t_padding
    | `PrimZarith_N -> t_zarith_n
    | `PrimZarith_Z -> t_zarith_z

  let t_composite_of ft =
    let open Typescript_backend_types in
    function
    | `CTLinear lin -> t_linear_of ft lin
    | `CTOption elem -> t_option_of @@ ft elem
    | `CTTuple2 (t1, t2) -> t_tuple_of [ ft t1; ft t2 ]
    | `CTDynWidth (DynPrefix w, t) -> t_dynprefix_of ~width:w @@ ft t
    | `CTDynWidth (ReserveSuffix n, t) -> t_reservesuffix_of ~reserve:n @@ ft t
    | `CTTagless t -> t_tagless_of @@ ft t
end

module Typescript_meta : Language_meta = struct
  let lang_props =
    {
      language_name = "TypeScript";
      file_extension = "ts";
      has_reflexive_typing = true;
      has_reflexive_value = true;
      has_native_indirection = false;
      has_generic_types = true;
      has_unboxed_strings = true;
      has_native_enums = true;
      has_native_unions = true;
      has_type_constraints = true;
      has_native_slices = true;
      has_native_typecast = true;
      has_native_typeconv = false;
    }

  let import_all qname src =
    Printf.sprintf "import * as %s from '%s';" qname src

  let import bindings src =
    Printf.sprintf
      "import { %s } from '%s'"
      (String.concat ~sep:", " bindings)
      src
end

module Typescript_tokens : Syntax_tokens = struct
  let declare_sep = "="

  let define_sep = "="

  let field_sep = ";"

  let elem_sep = ","

  let open_block = "{"

  let close_block = "}"

  let open_record = "{"

  let close_record = "}"
end

module Typescript_rules_core = struct
  let group_expr = Printf.sprintf "(%s)"

  let terminate_phrase phr = phr ^ ";"

  let assign ~bind ~value =
    String.concat ~sep:" " [ bind; Typescript_tokens.declare_sep; value ]

  let annotate ~type_sig ~bind_name = bind_name ^ " : " ^ type_sig

  let at_index ~ix expr = Printf.sprintf "%s[%s]" expr ix

  let at_field ~lab expr = Printf.sprintf "%s.%s" expr lab

  let at_key ~key expr = Printf.sprintf "%s[%s]" expr key

  let make_fields flds =
    List.map
      ~f:(fun (bind_name, type_sig) -> annotate ~type_sig ~bind_name)
      flds

  let make_enum_elems elems =
    List.map ~f:(fun (bind, value) -> assign ~bind ~value) elems

  let join_fields ?(inline = false) =
    let sep = Typescript_tokens.field_sep ^ if inline then " " else "\n" in
    String.concat ~sep

  let join_elems ?(inline = false) =
    let sep = Typescript_tokens.elem_sep ^ if inline then " " else "\n" in
    String.concat ~sep

  let call_method ~on ~meth ~args =
    Printf.sprintf "%s.%s(%s)" on meth (String.concat ~sep:"," args)

  let type_cast ~expr ~typ = Printf.sprintf "(%s as %s)" expr typ

  let bool_negate = Printf.sprintf "!(%s)"

  let binary_op op x y =
    match op with
    | `NotEq -> Printf.sprintf "%s !== %s" x y
    | `Eq -> Printf.sprintf "%s === %s" x y
    | _ -> assert false

  let comment = function
    | `Inline com -> "// " ^ com
    | `Block coms -> Printf.sprintf "/* %s */" (String.concat ~sep:"\n   " coms)

  let funcall ?(args = []) ?generics ~f =
    let tparams =
      match generics with
      | None -> ""
      | Some ts -> Printf.sprintf "<%s>" @@ String.concat ~sep:", " ts
    in
    Printf.sprintf "%s%s(%s)" f tparams @@ String.concat ~sep:", " args

  let curried_funcall ?(args = [ [] ]) ?generics ~f =
    let tparams =
      match generics with
      | None -> ""
      | Some ts -> Printf.sprintf "<%s>" @@ String.concat ~sep:", " ts
    and group params = Printf.sprintf "(%s)" (String.concat ~sep:", " params) in
    Printf.sprintf "%s%s%s" f tparams
    @@ String.concat ~sep:"" (List.map ~f:group args)

  let create_bind ?(mods = Binding_mod.exposed_definite) ~bind_name ?type_sig
      ~value =
    let open Ident.Binding in
    let open Binding_mod in
    let lhs =
      match bind_name, mods with
      | Const_name name, { visibility = Exposed; _ } -> "export const " ^ name
      | Const_name name, _ -> "const " ^ name
      | ( Variable_name name,
          { visibility = Exposed; variability = Some Invariant; _ } ) ->
          "export const " ^ name
      | ( Variable_name name,
          { visibility = Local; variability = Some Invariant; _ } ) ->
          "const " ^ name
      | Variable_name name, { visibility = Exposed; _ } -> "export var " ^ name
      | Variable_name name, { visibility = Local; _ } -> "var " ^ name
      | Function_name name, { visibility = Exposed; _ } ->
          "export function " ^ name
      | Function_name name, { visibility = Hidden; _ } -> "function " ^ name
      | Label_name name, { variability = Some Invariant; _ } ->
          "readonly " ^ name
      | Label_name name, _ -> name
      | Enum_name name, _ -> name
      | _ -> failwith "unhandled case in create_bind for keywords pattern-match"
    in
    match type_sig, value with
    | None, Some rhs -> assign ~bind:lhs ~value:rhs
    | Some typ, Some rhs ->
        assign ~bind:(annotate ~bind_name:lhs ~type_sig:typ) ~value:rhs
    | Some typ, None -> annotate ~type_sig:typ ~bind_name:lhs
    | None, None -> lhs
end

module Typescript_syntax_core : Syntax_core = struct
  let define_type ?(mods = Binding_mod.exposed_definite) ?(kind = Kind.Obj)
      ~type_name ~(definition : contents) =
    let open Kind in
    let type_keyword =
      match kind with
      | Obj -> "interface"
      | Alias | Union -> "type"
      | Enum -> "enum"
    in
    let keywords =
      match mods.visibility with
      | Binding_mod.Hidden -> type_keyword
      | Binding_mod.Exposed -> "export " ^ type_keyword
      | _ ->
          failwith
            "Unexpected visibility modification for type declaration/definition"
    in
    let rhs =
      let open Typescript_tokens in
      match kind with
      | Obj -> (
          match definition with
          | `Line stmt ->
              Printf.sprintf "%s %s %s" open_record stmt close_record
          | `Inline stmts ->
              let inlined =
                Typescript_rules_core.join_fields ~inline:true stmts
              in
              Printf.sprintf "%s %s %s" open_record inlined close_record
          | `MultiLine stmts ->
              let block =
                Typescript_rules_core.join_fields ~inline:false stmts
              in
              Printf.sprintf "%s\n%s\n%s" open_record block close_record)
      | Enum -> (
          match definition with
          | `Line stmt -> Printf.sprintf "%s %s %s" open_block stmt close_block
          | `Inline stmts ->
              let inlined =
                Typescript_rules_core.join_elems ~inline:true stmts
              in
              Printf.sprintf "%s %s %s" open_block inlined close_block
          | `MultiLine stmts ->
              let block =
                Typescript_rules_core.join_elems ~inline:false stmts
              in
              Printf.sprintf "%s\n%s\n%s" open_block block close_block)
      | Alias -> (
          match definition with
          | `Line stmt -> Printf.sprintf "%s %s" declare_sep stmt
          | _ ->
              failwith
                "define_type: Expected single-line definition for type-alias")
      | Union -> (
          match definition with
          | `Line stmt -> Printf.sprintf "%s %s" declare_sep stmt
          | _ ->
              failwith
                "define_type: Expected single-line definition for alias to \
                 anonymous union")
    in
    Printf.sprintf "%s %s %s" keywords type_name rhs

  let declare_type ?(mods = Binding_mod.exposed_definite) ?(kind = Kind.Obj)
      ~type_name =
    let open Typescript_meta in
    ignore (mods, kind, type_name);
    failwith
      (Printf.sprintf
         "Error: %s does not support type declarations without accompanying \
          definitions"
         lang_props.language_name)

  let define_fun ?(mods = Binding_mod.exposed_definite) ~fun_name
      ?(args = (`Typed [] : string_fun_args)) ?(outtype = [])
      ~(definition : contents) =
    let fun_keyword = "function" in
    let keywords =
      match mods.visibility with
      | Binding_mod.Hidden -> fun_keyword
      | Binding_mod.Exposed -> "export " ^ fun_keyword
      | _ ->
          failwith
            "Unexpected visibility modification for function \
             declaration/definition"
    in
    let params =
      match args with
      | `Typed args ->
          let f = function
            | bind_name, Mandatory, type_sig ->
                Typescript_rules_core.annotate ~bind_name ~type_sig
            | bind_name, Optional, type_sig ->
                Typescript_rules_core.annotate
                  ~bind_name:(bind_name ^ "?")
                  ~type_sig
            | bind, Default value, _ ->
                Typescript_rules_core.assign ~bind ~value
          in
          let arg_list = List.map ~f args in
          Printf.sprintf "(%s)" @@ String.concat ~sep:", " arg_list
      | `Untyped _ ->
          failwith
          @@ Printf.sprintf
               "Error: expected typed argument list for %s function definition"
               Typescript_meta.lang_props.language_name
    in
    let params_and_sig =
      match outtype with
      | [] -> params
      | [ type_sig ] ->
          Typescript_rules_core.annotate ~bind_name:params ~type_sig
      | _ -> assert false
    in
    let rhs =
      match definition with
      | `Line stmt ->
          Typescript_tokens.(
            Printf.sprintf "%s\n%s\n%s" open_block stmt close_block)
      | `Inline stmts ->
          Typescript_tokens.(
            Printf.sprintf
              "%s %s %s"
              open_block
              (String.concat ~sep:" " stmts)
              close_block)
      | `MultiLine stmts ->
          Typescript_tokens.(
            Printf.sprintf
              "%s\n%s\n%s"
              open_block
              (String.concat ~sep:"\n" stmts)
              close_block)
    in
    Printf.sprintf "%s %s%s %s" keywords fun_name params_and_sig rhs

  let declare_fun ?(mods = Binding_mod.exposed_definite) ~fun_name
      ?(args = `Typed []) ~outtype =
    let fun_keyword = "function" in
    let keywords =
      match mods.externality with
      | Binding_mod.Definite -> fun_keyword
      | Binding_mod.Proxy -> "declare " ^ fun_keyword
    in
    let params =
      match args with
      | `Typed args ->
          let f = function
            | bind_name, Mandatory, type_sig ->
                Typescript_rules_core.annotate ~bind_name ~type_sig
            | bind_name, Optional, type_sig ->
                Typescript_rules_core.annotate
                  ~bind_name:(bind_name ^ "?")
                  ~type_sig
            | bind, Default value, _ ->
                Typescript_rules_core.assign ~bind ~value
          in
          let arg_list = List.map ~f args in
          Printf.sprintf "(%s)" @@ String.concat ~sep:", " arg_list
      | `Untyped _ ->
          failwith
          @@ Printf.sprintf
               "Error: expected typed argument list for %s function definition"
               Typescript_meta.lang_props.language_name
    in
    let params_and_sig =
      match outtype with
      | [] -> params
      | [ type_sig ] ->
          Typescript_rules_core.annotate ~bind_name:params ~type_sig
      | _ -> assert false
    in
    Typescript_rules_core.terminate_phrase
    @@ Printf.sprintf "%s %s%s" keywords fun_name params_and_sig

  let define_val ?mods ?typ ~val_name ~value =
    Typescript_rules_core.(
      terminate_phrase
      @@ create_bind
           ?mods
           ?type_sig:typ
           ~bind_name:(Ident.Binding.Variable_name val_name)
           ~value:(Some value))

  let declare_val ?(mods = Binding_mod.exposed_definite) ?typ ~val_name =
    match mods.externality with
    | Binding_mod.Definite ->
        Typescript_rules_core.(
          terminate_phrase
          @@ create_bind
               ~mods
               ?type_sig:typ
               ~bind_name:(Ident.Binding.Variable_name val_name)
               ~value:None)
    | Binding_mod.Proxy ->
        Typescript_rules_core.(
          terminate_phrase @@ "declare "
          ^ create_bind
              ~mods:{ mods with visibility = Hidden }
              ?type_sig:typ
              ~bind_name:(Ident.Binding.Variable_name val_name)
              ~value:None)
end

module Typescript_metasyntax : Syntax_meta = struct
  let wrap_namespace name contents =
    let prologue =
      let keywords = "export namespace" in
      Printf.sprintf "%s %s {" keywords name
    and epilogue = "}" in
    prologue :: contents @ [ epilogue ]

  let throw_error = function
    | Some err -> Printf.sprintf "throw new Error(%s)" err
    | None -> "throw new Error()"

  let lambda ?(args = (`Typed [] : string_fun_args)) ?(outtype = [])
      ~(definition : contents) =
    let outtype =
      match outtype with
      | [ outtype ] -> ": " ^ outtype
      | [] -> ""
      | _ -> assert false
    in
    let params =
      match args with
      | `Typed args ->
          let f = function
            | bind_name, Mandatory, type_sig ->
                Typescript_rules_core.annotate ~bind_name ~type_sig
            | bind_name, Optional, type_sig ->
                Typescript_rules_core.annotate
                  ~bind_name:(bind_name ^ "?")
                  ~type_sig
            | bind, Default value, _ ->
                Typescript_rules_core.assign ~bind ~value
          in
          let arg_list = List.map ~f args in
          Printf.sprintf "(%s)" @@ String.concat ~sep:", " arg_list
      | `Untyped args ->
          let f = function
            | bind_name, Mandatory -> bind_name
            | bind_name, Optional -> bind_name ^ "?"
            | bind, Default value -> Typescript_rules_core.assign ~bind ~value
          in
          let arg_list = List.map ~f args in
          Printf.sprintf "(%s)" @@ String.concat ~sep:", " arg_list
    in
    Printf.sprintf
      "%s%s => %s"
      params
      outtype
      (match definition with
      | `Line stmt -> stmt
      | `Inline stmts -> Printf.sprintf "{ %s }" @@ String.concat ~sep:" " stmts
      | `MultiLine stmts ->
          Printf.sprintf "{\n%s\n}" @@ String.concat ~sep:"\n" stmts)

  let valid_as_enum ~ref ~vals candidate =
    ignore vals;
    Printf.sprintf "typeof %s[%s] !== \"undefined\"" ref candidate

  let return_statement = function None -> "return" | Some s -> "return " ^ s

  let if_then_else test_expr true_branch = function
    | Some false_branch ->
        Printf.sprintf
          "if (%s) {\n%s\n} else {\n%s\n}"
          test_expr
          (String.concat ~sep:"\n" true_branch)
          (String.concat ~sep:"\n" false_branch)
    | None ->
        Printf.sprintf
          "if (%s) {\n%s\n}"
          test_expr
          (String.concat ~sep:"\n" true_branch)

  let switch test_expr cases default =
    let cases' =
      let f (case_list, stmt_list) =
        List.map case_list ~f:(Printf.sprintf "case %s:")
        @ stmt_list @ [ "break;" ]
      in
      List.concat_map cases ~f
    and default' =
      match default with
      | None -> []
      | Some stmt_list -> "default:" :: stmt_list
    in
    Printf.sprintf "switch %s {\n%s\n}" test_expr
    @@ String.concat ~sep:"\n" (cases' @ default')

  (* REVIEW[epic=design] - concatenate strings more efficiently, perhaps? *)
  let concat_strings = function
    | [] -> "\"\""
    | xs -> Typescript_rules_core.group_expr @@ String.concat ~sep:" + " xs

  let narrow_enum enum elem = enum ^ "." ^ elem

  let type_union members = String.concat ~sep:" | " members

  let tagged_union ?tag =
    ignore tag;
    type_union
end

module Typescript_syntax : Syntax_language = struct
  include Typescript_tokens
  include Typescript_rules_core
  include Typescript_syntax_core
  include Typescript_metasyntax
end

module Typescript_lex : Lex_language = struct
  let remap_keyword = function
    | "null" -> "id_null"
    | "interface" -> "id_interface"
    | safe -> safe

  let qualify_ident ~q ?(common = []) ident =
    ignore common;
    String.concat
    @@ List.rev_map_append q [ ident ] ~f:(function
           | Ident.Scope.Inferred_scope id -> id ^ "__"
           | Ident.Scope.Namespace_name id | Ident.Scope.Type_name id ->
               id ^ ".")
    |> remap_keyword

  let generate_ident ~q ?(common = []) ident =
    ignore common;
    String.concat
    @@ List.rev_map_append q [ ident ] ~f:(function
           | Ident.Scope.Inferred_scope id | Ident.Scope.Namespace_name id ->
               id ^ "__"
           | Ident.Scope.Type_name _ -> "")
    |> remap_keyword
end

module Typescript_repr : Language with module T := T = struct
  module T = T
  include Typescript_types
  include Typescript_meta
  include Typescript_syntax
  include Typescript_lex
end

module Typescript_value_table : Backend_value_table with module V := V = struct
  module V = V

  let v_prim : V.prim -> string = function
    | `PrimTrue -> "true"
    | `PrimFalse -> "false"
    | `PrimNum num -> num
    | `Prim_L -> "undefined"
    | `PrimWidth `Uint8 | `PrimTagSize `Uint8 | `PrimEnumSize `Uint8 ->
        "\"uint8\""
    | `PrimTagSize `Uint16 | `PrimEnumSize `Uint16 -> "\"uint16\""
    | `PrimWidth `Uint30 | `PrimEnumSize `Uint30 -> "\"uint30\""
    | `PrimVariantTranscoderParamValue table ->
        let f (tag, id) =
          Printf.sprintf "[%d]: %s" tag (Ident.Binding.to_string id)
        in
        Printf.sprintf "{ %s }" (Typescript_repr.join_elems @@ List.map table ~f)

  let v_lit_str str = Printf.sprintf "\"%s\"" str

  let v_lit_arr arr =
    Printf.sprintf "[%s]" (Typescript_repr.join_elems ~inline:true arr)

  let v_lit_obj { explicit; implicit } =
    assert (Option.is_none implicit);
    let f = function
      | Assign (lab, v) -> Typescript_repr.assign ~bind:lab ~value:v
      | NamedPun lab -> lab
    in
    let explicit' = List.map ~f explicit in
    Typescript_repr.(
      Printf.sprintf
        "%s %s %s"
        open_record
        (join_elems ~inline:true explicit')
        close_record)
end

module Typescript_symbol_table : Backend_symbol_table with module T := T =
struct
  let prefix_for =
    let module L = Typescript_repr in
    function
    | `Atom a -> (
        match a with
        | `TPrim p -> (
            match p with `PrimPadding _ -> "padding" | _ -> L.t_prim p)
        | `TComposite ct -> (
            match ct with
            | `CTLinear (`LTSequence _) -> "sequence"
            | `CTOption _ -> "option"
            | `CTDynWidth (DynPrefix _, _) -> "lenpref_"
            | `CTDynWidth (ReserveSuffix _, _) -> "reservesuffix_"
            | `CTTuple2 _ -> "tuple_"
            | _ -> assert false)
        | `TRefer (`RTOpaque (qual, base_name) | `RTEnum ((qual, base_name), _))
          ->
            if List.is_empty qual then base_name ^ "_"
            else
              failwith
                "cannot refer to value in namespace without scope context"
        | _ -> assert false)
    | _ -> assert false
end

module Typescript_backend : Backend with module A := TS_AST = struct
  module A = TS_AST
  include Typescript_repr
  include Typescript_backend_types
  include Typescript_value_table
  include Typescript_symbol_table

  let enc_dec ?(uscore = false) x =
    List.map
      ~f:(String.concat ~sep:(if uscore then "_" else ""))
      [ [ x; "encoder" ]; [ x; "decoder" ] ]

  let def_enc_dec ?(uscore = false) x = x :: enc_dec ~uscore x

  let preamble =
    let reldir = "../src/" in
    let intports =
      let signs = [ Unsigned; Signed ]
      and bits = [ Bits8; Bits16; Bits32; Bits64 ] in
      List.cartesian_product signs bits
      |> List.map ~f:(fun (s, b) -> t_prim (`PrimInt (s, b)))
      |> List.concat_map ~f:def_enc_dec
    and primports =
      List.concat
        [
          def_enc_dec (t_prim `PrimOpaqueBytes);
          [ "width" ];
          [ "LenPref" ];
          [ "ReserveSuffix" ];
          enc_dec "sequence";
          enc_dec "option";
          enc_dec ~uscore:true "lenpref";
          enc_dec ~uscore:true "reservesuffix";
          enc_dec "boolean";
          enc_dec "string";
          enc_dec "padding";
        ]
    and parseports = [ "TypedParser" ]
    and decports = [ "Decoder"; "decoder_argument" ]
    and encports = [ "Encoder" ]
    and adtports = [ "Variant"; "Tagless" ] @ enc_dec ~uscore:true "variant"
    and utilports = [ "Optional" ]
    and zarithports =
      List.concat
        [ [ "Z"; "N" ]; def_enc_dec "zarith_z"; def_enc_dec "zarith_n" ]
    and tupleports = "Tuple" :: enc_dec ~uscore:true "tuple"
    and floatports =
      List.concat
        [
          def_enc_dec (t_prim (`PrimFloat Single));
          def_enc_dec (t_prim (`PrimFloat Double));
        ]
    in
    [
      ImportBindings { bindings = intports; source_file = reldir ^ "integral" };
      ImportBindings
        { bindings = primports; source_file = reldir ^ "primitive" };
      ImportBindings
        { bindings = parseports; source_file = reldir ^ "byteparser" };
      ImportBindings { bindings = decports; source_file = reldir ^ "decoder" };
      ImportBindings { bindings = encports; source_file = reldir ^ "encoder" };
      ImportBindings { bindings = adtports; source_file = reldir ^ "adt" };
      ImportBindings { bindings = utilports; source_file = reldir ^ "util" };
      ImportBindings { bindings = zarithports; source_file = reldir ^ "zarith" };
      ImportBindings { bindings = tupleports; source_file = reldir ^ "tuple" };
      ImportBindings { bindings = floatports; source_file = reldir ^ "float" };
    ]

  let patch_imports ident =
    if String.is_suffix ~suffix:"script.expr" ident then fun imports ->
      ImportBindings
        {
          bindings = def_enc_dec ~uscore:true "X_8";
          source_file = "../tsapi/script_expr_patch";
        }
      :: imports
    else fun xs -> xs

  let create_parser ~from =
    match from with
    | [ hd ] -> Printf.sprintf "TypedParser.parse(%s)" hd
    | _ -> failwith "TypeScript create_parser expects one argument exactly"
end
