open Core
open Backend.Representation.Abstract_type
open Backend.Foreign

let int_to_fv x = `Pure (`VPrim (`PrimNum (Int.to_string x)))

let width_to_fv w = `Pure (`VPrim (`PrimWidth w))

let name_to_var x = `Pure (`VBound (Ident.Binding.Variable_name x))

let string_to_lit s = `Pure (`VLit (`LitString s))

let devoid = function AbstractVoid -> AbstractPadding 0 | x -> x

let get_fields = function Record flds -> flds | _ -> []

let to_bits = function `Uint8 -> Bits8 | `Uint16 -> Bits16 | `Uint30 -> Bits32

let reformat raw_name =
  let label_to_namespace_id = function
    | s when String.(prefix s 1 = "0") -> "Proto" ^ s
    | s -> s
  in
  let from_singleton = function
    | [ hd ] -> hd
    | [] -> failwith "Empty list is not a singleton"
    | _ -> failwith "Multi-element list is not a singleton"
  in
  let init_last l =
    let i = List.length l - 1 in
    List.split_n l i
  in
  let undash = String.substr_replace_all ~pattern:"-" ~with_:"_" in
  String.split ~on:'.' raw_name |> List.map ~f:undash |> init_last
  |> fun (x, y) -> List.rev_map ~f:label_to_namespace_id x, from_singleton y

(** Sanitizes a label (field) identifier by replacing all ['.'] characters with
    ["__"] and all ['-'] characters with ['_']. *)
let sanitize_label lab_id =
  String.split ~on:'.' lab_id
  |> List.map ~f:(String.substr_replace_all ~pattern:"-" ~with_:"_")
  |> String.concat ~sep:"__"

module GenContext = struct
  module S = Stdlib.Map.Make (Qualification)

  type t = Qualification.t S.t

  let fresh () = S.empty

  let add_assoc k v ctxt = S.add k v ctxt

  let lookup k ctxt =
    match S.find_opt k ctxt with
    | Some v -> v
    | None -> ( match k with [] -> assert false | _ -> k)

  let reset_local ctxt =
    S.filter
      (fun k _ ->
        match k with
        | [ q ] ->
            not @@ String.is_prefix ~prefix:"X_" @@ Ident.Scope.to_string q
        | _ -> true)
      ctxt
end

(* REVIEW[epic=stopgap] - pending merge of octez MR 3022 *)
let patch_ident = function
  | "Generic prim (any number of args with or without annot)" -> "GenericPrim"
  | "Prim (no args, annot)" -> "Prim00"
  | "Prim (no args + annot)" -> "Prim01"
  | "Prim (1 arg, no annot)" -> "Prim10"
  | "Prim (1 arg + annot)" -> "Prim11"
  | "Prim (2 args, no annot)" -> "Prim20"
  | "Prim (2 args + annot)" -> "Prim21"
  | s -> s