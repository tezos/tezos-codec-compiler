common
======

This directory contains the modules belonging to the sub-library `common` of the top-level compiler library `language`.

The modules included in this library are intended as implementation aids
for the language-specific library associated with an arbitrary target language.

Modules:
  - `language_converter.ml`: Helper functions for simple conversions, context module for *language AST generation* phase, identifier-level patching for specificly known invalid strings
  - `language_repr.ml`: Collection of hierarchically linked
  module type declarations that specify a set of common tokens, named entities, production rules, general language properties, and backend-specific import-lists
  - `producer.ml`: Language-invariant Context module for *production*, core module of meta-production rules, and
  module type declaration for language-specific production
  rules that we expect every language to define according
  to the features of their individualized AST
  