open Language_repr
open Backend.Foreign
open Backend.Foreign.Common
open Language_converter

val unimplemented : unit -> 'a

(** Internal state passed between combinators in the [Producer] functor, used
    for determining the appropriate structural qualification of generated
    identifiers based on accumulated rescoping rules and the current
    namespace-stack. *)
module ProducerContext : sig
  type t = { scope : Qualification.t; rescopes : GenContext.t }

  (** Adjust the context to reflect entry into a namespace of the given name *)
  val enter_namespace : Ident.namespace_id -> t -> t

  (** Adjust the context to reflect exit of the most recently entered namespace *)
  val leave_namespace : t -> t

  (** Adjust the context to prune local rescopes to avoid histeresis *)
  val reset_local : t -> t

  (** Generate a 'fresh' context for first-pass production *)
  val fresh : unit -> t

  (** Impute the proper qualification of an opaque named reference to a type
      based on the accumulated rescoping rules and the current namespace-stack

      @param debug When true, prints non-identity requalifications to stderr *)
  val requalify :
    base_name:Ident.type_id ->
    qual:Qualification.t ->
    ctxt:t ->
    ?debug:bool ->
    unit ->
    string * Qualification.t
end

(** Functor for generating sourcecode in a particular backend language from
    foreign-code AST *)
module ProducerCore : sig
  (** Producer-context that is referred to and potentially updated by a
      production method *)
  type ctxt = ProducerContext.t

  (** [('a,'b) t] is the type of a function that converts a value of ['a] to a
      value of type ['b] in a context-aware and context-mutating fashion *)
  type ('a, 'b) t = ctxt:ctxt -> 'a -> ctxt * 'b

  (** Sequentially apply a production rule to each value in a list, using the
      returned context of each element as the input context for the following
      element; the context returned is the context returned after producing the
      last element of the list *)
  val seq : ('a, 'b) t -> ('a list, 'b list) t

  (** [map ~f prod ~ctxt x] applies [f] to the return value of [prod ~ctxt x],
      leaving the output context unchanged *)
  val map : f:('b -> 'c) -> ('a, 'b) t -> ('a, 'c) t

  (** [opt prod ~ctxt x] applies [prod ~ctxt] to [x] of type ['a option] and
      returns [ctxt * 'b option], in a similar manner as [Option.map] *)
  val opt : ('a, 'b) t -> ('a option, 'b option) t

  (** [ign prod ~ctxt x] drops the returned context of [prod ~ctxt x] and
      returns only the converted value *)
  val ign : ('a, 'b) t -> ctxt:ctxt -> 'a -> 'b

  (** [tup3] is a variant of [seq] specialized or 3-tuples rather than lists of
      arbitrary length; like [seq], it threads the returned context through each
      successive operation, rather than computing each production using the same
      fixed initial context. *)
  val tup3 : ('a, 'b) t -> ('a * 'a * 'a, 'b * 'b * 'b) t
end

module type Productions = sig
  include module type of ProducerCore

  module A : Foreign_ast.Ast_intf

  module L : Backend with module A := A

  val produce_directive : (directive, string) ProducerCore.t

  val produce_type : (A.T.t, string) ProducerCore.t

  val produce_field : (string * A.T.t, string * string) ProducerCore.t

  val produce_elem : (string * int, string * string) ProducerCore.t

  val produce_typedef : (A.T.t, contents) ProducerCore.t

  val produce_param :
    ( string * A.V.t fun_arg_mod * A.T.t option,
      string * string option )
    ProducerCore.t

  val produce_value : (A.V.t, string) ProducerCore.t

  val produce_field_fill : (A.V.t field_fill, string field_fill) ProducerCore.t

  val produce_statement : (A.S.t, string) ProducerCore.t

  val produce_branch : (A.S.t list, string list) ProducerCore.t

  val produce_declaration : (A.D.v, string) ProducerCore.t

  val produce_sourcefile : (A.source_file, string list) ProducerCore.t

  val produce_codebase : (A.foreign_codebase, string list) ProducerCore.t
end
