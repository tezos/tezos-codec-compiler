open Backend.Foreign
open Backend.Foreign.Common
open Backend.Codec.Codec_repr

module Kind = struct
  type t = Alias | Obj | Union | Enum
end

(** Contents of a comment, or a distinguished collection of statements within a
    shared logical block. Inlining is done only when possible, and is treated as
    multiline in languages that cannot inline separate statements *)
type contents =
  [ `Line of string | `Inline of string list | `MultiLine of string list ]

type string_fun_args = (string, string) fun_args

(** Core properties of a language, used for helpful error messages and for
    enforcing limitations of a language when we attempt to produce code
    corresponding to unsupported features of that language *)
type language_properties = {
  language_name : string;
  file_extension : string;
  has_reflexive_typing : bool;
  has_reflexive_value : bool;
  has_native_indirection : bool;
  has_generic_types : bool;
  has_unboxed_strings : bool;
  has_native_enums : bool;
  has_native_unions : bool;
  has_type_constraints : bool;
  has_native_slices : bool;
  has_native_typecast : bool;
  has_native_typeconv : bool;
}

(** Non-codec types we expect to deal with in every backend language, even if
    some of them may be represented by the same unified primitive type *)
module type Types_nat = sig
  module T : sig
    type nat
  end

  val t_nat : T.nat -> string
end

(** In-codec types we expect to deal with in every backend language, even if
    some of them may be represented by the same unified primitive type *)
module type Types_prim = sig
  module T : sig
    type prim
  end

  val t_prim : T.prim -> string
end

(** Types we expect to deal with in every backend language, but which may not be
    primitive, and which we may have to define ourselves as part of the backend
    API if they can be supported at all; includes all primitive types as well *)
module type Types_core = sig
  module T : Foreign_type.Type_intf

  include Types_prim with module T := T

  include Types_nat with module T := T

  val t_composite_of : (T.t -> string) -> T.composite_type -> string
end

(** Types that are natively supported only in some languages, either natively or
    in a standard library, and which we may have to define ourselves as part of
    the backend API. *)
module type Types_extended = sig
  (** Composite type representing the type-level union of a singleton wrapper
      around a value of a parametric type (polymorphic or generic), or an
      nullary wrapper representing the absence of any value. Cf. OCaml
      [Option.t] and Haskell [Maybe] *)
  val t_option_of : string -> string

  (** Composite type representing the type-level union of a positive wrapper
      around a value of a parametric type (polymorphic or generic), or a
      negative wrapper around a value carrying some error or failure context.
      Cf. OCaml [Result.t] and Haskell [Either String] *)
  val t_orerror_of : string -> string
end

(** Core and extended types for a given backend language *)
module type Types_language = sig
  include Types_core

  include Types_extended
end

(** Concrete bindings for abstract types we will have to implement or import as
    part of the backend API on a per-language basis. These correspond to
    features of the current backend API that we wish to be able to give concrete
    names to in any backend language. *)
module type Types_backend = sig
  val t_width : string

  val t_padding : string

  val t_width_uint8 : string

  val t_width_uint30 : string

  val t_tagsize_uint8 : string

  val t_tagsize_uint16 : string

  val t_dynprefix_of : width:size_repr -> string -> string

  val t_reservesuffix_of : reserve:int -> string -> string

  val t_tagless_of : string -> string

  val t_tuple_of : string list -> string

  val t_variant_encoder_param : string

  val t_variant_decoder_param : string
end

(** Attributes and methods of a language that are invariant, which are outside
    of the main token-set and syntactical production rules that the compiler
    will generate as the majority of the file contents *)
module type Language_meta = sig
  val lang_props : language_properties

  val import_all : string -> string -> string

  val import : string list -> string -> string
end

(** Logical tokens used in various core syntactical productions whose lexical
    definition may vary across languges but which we expect to find in every
    language; does not contain tokens used in production rules that may have
    completely different syntax across languages *)
module type Syntax_tokens = sig
  (** Character sequence used to separate the LHS and RHS of a declarative
      assignment.

      In most languages, this will be ["="]. *)
  val declare_sep : string

  (** Character sequence used to separate the LHS and RHS of a definitional
      assignment

      This corresponds to the ShortVarDecl separator [":="] for Golang, and will
      mostly be identical to [declare_sep] otherwise. *)
  val define_sep : string

  (** Character sequence used to separate successive fields of an
      object/structure *)
  val field_sep : string

  (** Character sequence used to separate successive elements of a (possibly
      simulated) enum type *)
  val elem_sep : string

  val open_record : string

  val close_record : string

  val open_block : string

  val close_block : string
end

module type Syntax_rules_core = sig
  (** Convert a possibly compound syntactical expression into an atomic
      syntactical expression with the same semantics. Almost all of the time
      this will be parenthesization. *)
  val group_expr : string -> string

  (** Produces a line-terminated version of the input phrase (statement or
      declaration). In C-like languages this would be appending a [';']
      character, and in Python this would be a no-op *)
  val terminate_phrase : string -> string

  (** Make a (possibly mutable) reference to a field of a record, class, or
      object, both for readonly access, and potentially for mutational
      assignment. *)
  val at_field : lab:string -> string -> string

  (** Make a (possibly mutable) reference to an offset position in a linear
      structure such as an array or list, both for readonly access, and
      potentionally for mutational assignment. *)
  val at_index : ix:string -> string -> string

  (** Make a (possibly mutable) reference to an entry in a keyed structure such
      as a map, dictionary, or table, both for readonly access, and potentially
      for mutational assignment. *)
  val at_key : key:string -> string -> string

  (** Convert a list of [(field name, field type)] pairs into a list of properly
      qualified fields, to be joined later by [join_fields] *)
  val make_fields : (string * string) list -> string list

  (** Convert a list of already-qualified record/struct fields (via
      [make_fields]) into a single string representing their properly-delimited
      concatenation. By default uses ['\n'] as the field separator, but can be
      made to produce an inlined version (using [' ']) by setting
      `~inline:true`, if they can be inlined in the current language. *)
  val join_fields : ?inline:bool -> string list -> string

  (** Convert a list of [(enum name, enum value)] pairs into a list of properly
      qualified elements, to be joined later by [join_elems] *)
  val make_enum_elems : (string * string) list -> string list

  (** Convert a list of already-qualified enum elements (via [make_enum_elems])
      into a single string representing their properly-delimited concatenation.
      By default uses ['\n'] as the field separator, but can be made to produce
      an inlined version (using [' ']) by setting `~inline:true`, if they can be
      inlined in the current language. *)
  val join_elems : ?inline:bool -> string list -> string

  (** Produces the expression corresponding to calling method [meth] on a
      variable or expression [on] with arguments [args] *)
  val call_method : on:string -> meth:string -> args:string list -> string

  (** Produces an expression corresponding to the boolean negation of an input
      expression, which will typically end up being grouped via [group_expr]
      either internally or externally. *)
  val bool_negate : string -> string

  (** Produces an expression corresponding to the application of binary
      operation [op] to the remaining two arguments, in the order in which they
      are provided *)
  val binary_op : [> `Eq | `NotEq ] -> string -> string -> string

  (** Takes an lhs binding [bind] and an rhs expression [value] and produces an
      expression that assigns [value] to [bind]. This is currently used
      interchangeably for type-level and value-level assignments, but may be
      split in future, or take an extra parameter to distinguish those cases. *)
  val assign : bind:string -> value:string -> string

  (** Produce an expression which evalutates to the result of calling the
      function [f] with arguments [args] It is assumed that when [args] is
      empty, the function is called with no arguments rather than not being
      called at all, in languages where such a distinction exists. *)
  val funcall : ?args:string list -> ?generics:string list -> f:string -> string

  (** Produce an expression which evaluates to a sequential application of a
      curried function over multiple lists of arguments in turn. If the outer
      list is empty, the function is merely referenced without being called,
      while an empty list of arguments as an element of the list [args]
      indicates that the function corresponding to the partial application of
      all arguments up to that point is then applied to an empty parameter list,
      followed by any remaining curried applications. *)
  val curried_funcall :
    ?args:string list list -> ?generics:string list -> f:string -> string

  (** Produce a statement in which a binding [bind_name] is declared (and
      possibly shadowed) to have type signature [type_sig] (if provided and
      supported) and optionally assigned the value [value].

      Distinct from [assign], which can be used for value updates, while this
      method is only intended for the creation of new bindings. *)
  val create_bind :
    ?mods:binding_mods ->
    bind_name:Ident.binding ->
    ?type_sig:string ->
    value:string option ->
    string

  (** Produce a sub-expression consisting of the annotation of a binding
      [bind_name] with the type signature [type_sig]. *)
  val annotate : type_sig:string -> bind_name:string -> string

  (** Produce a line or block comment containing the included text, verbatim *)
  val comment : [ `Inline of string | `Block of string list ] -> string
end

(** Core featuers of the syntax for toplevel (and namespace-scoped) definitions
    and declarations *)
module type Syntax_core = sig
  (** Define a new type with the given binding mods, kind, name, and definition *)
  val define_type :
    ?mods:binding_mods ->
    ?kind:Kind.t ->
    type_name:string ->
    definition:contents ->
    string

  (** Declare a new type with the given binding mods, kind, and name, without
      defining it *)
  val declare_type :
    ?mods:binding_mods -> ?kind:Kind.t -> type_name:string -> string

  (** Define a new function with the given binding mods, arguments, output type,
      name, and definition *)
  val define_fun :
    ?mods:binding_mods ->
    fun_name:string ->
    ?args:string_fun_args ->
    ?outtype:string list ->
    definition:contents ->
    string

  (** Declare a new function with the given binding mods, arguments, output
      type, and name, without defining it *)
  val declare_fun :
    ?mods:binding_mods ->
    fun_name:string ->
    ?args:string_fun_args ->
    outtype:string list ->
    string

  (** Define a new value-binding in the current scope with the given binding
      mods, type, and name, and definition *)
  val define_val :
    ?mods:binding_mods ->
    ?typ:string ->
    val_name:string ->
    value:string ->
    string

  (** Declare a new value-binding in the current scope with the given binding
      mods, type, and name, without defining it *)
  val declare_val :
    ?mods:binding_mods -> ?typ:string -> val_name:string -> string
end

(** Syntactical conventions of each language that can vary drastically between
    different languages and may not even necessarily be well-defined for a given
    language. *)
module type Syntax_meta = sig
  (** Language specific implementation for an anonymous function, which may not
      always be available *

      @param args List of named arguments on the LHS of the lambda definition
      @param outtype Type signature of the return value of the lambda
      @param definition RHS body of the lambda expression
      @return string representing the lambda expression specified by the input
      parameters *)
  val lambda :
    ?args:string_fun_args ->
    ?outtype:string list ->
    definition:contents ->
    string

  val wrap_namespace : string -> string list -> string list

  val valid_as_enum : ref:string -> vals:(string * int) list -> string -> string

  val throw_error : string option -> string

  val return_statement : string option -> string

  val narrow_enum : string -> string -> string

  val concat_strings : string list -> string

  val if_then_else : string -> string list -> string list option -> string

  val switch :
    string -> (string list * string list) list -> string list option -> string

  (** Produces a type-level expression corresponding to a tag-discriminated
      union of several types for the RHS of a type-definition. *)
  val tagged_union : ?tag:string -> string list -> string

  (** Produces a type-level expression corresponding to an anonymous union of
      several types, either for the RHS of a type-definition or within a
      type-signature for a variable binding, record field, or function argument. *)
  val type_union : string list -> string
end

module type Syntax_language = sig
  include Syntax_tokens

  include Syntax_rules_core

  include Syntax_core

  include Syntax_meta
end

module type Lex_language = sig
  val qualify_ident :
    q:Qualification.t -> ?common:Qualification.t -> string -> string

  val generate_ident :
    q:Qualification.t -> ?common:Qualification.t -> string -> string

  val remap_keyword : string -> string
end

module type Language = sig
  include Language_meta

  include Types_language

  include Syntax_language

  include Lex_language
end

module type Backend_value_table = sig
  module V : Foreign_val.Val_intf

  val v_prim : V.prim -> string

  val v_lit_str : string -> string

  val v_lit_arr : string list -> string

  val v_lit_obj : string record_fill -> string
end

module type Backend_symbol_table = sig
  module T : Foreign_type.Type_intf

  val prefix_for : T.t -> string
end

module type Backend = sig
  module A : Foreign_ast.Ast_intf

  include Language with module T := A.T

  include Types_backend

  include Backend_symbol_table with module T := A.T

  include Backend_value_table with module V := A.V

  val preamble : directive list

  val patch_imports : string -> directive list -> directive list

  val create_parser : from:string list -> string
end
