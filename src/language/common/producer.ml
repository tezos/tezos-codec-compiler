open Core
open Language_repr
open Language_converter
open Backend.Foreign
open Backend.Foreign.Common

let unimplemented () = failwith "unimplemented"

module ProducerContext = struct
  type t = { scope : Qualification.t; rescopes : GenContext.t }

  let enter_namespace nsid ({ scope; _ } as ctxt) =
    { ctxt with scope = Ident.Scope.Namespace_name nsid :: scope }

  let leave_namespace = function
    | { scope = _ :: scope'; _ } as ctxt -> { ctxt with scope = scope' }
    | _ -> assert false

  let reset_local = function
    | { rescopes; _ } as ctxt ->
        { ctxt with rescopes = GenContext.reset_local rescopes }

  let fresh () = { scope = []; rescopes = GenContext.fresh () }

  let requalify ~base_name ~qual ~ctxt ?(debug = false) () =
    let base_name', requal =
      match
        GenContext.lookup
          (Ident.Scope.Type_name base_name :: qual)
          ctxt.rescopes
      with
      | hd :: tl -> Ident.Scope.to_string hd, tl
      | _ -> assert false
    in
    (if
     debug
     && (String.(base_name <> base_name')
        || Qualification.compare qual requal <> 0)
    then
     Qualification.(
       Stdio.eprintf
         "%s:%s => %s:%s\n"
         (to_string qual)
         base_name
         (to_string requal)
         base_name'));
    base_name', requal
end

module ProducerCore = struct
  type ctxt = ProducerContext.t

  type ('a, 'b) t = ctxt:ctxt -> 'a -> ctxt * 'b

  let rec seq (f : ('a, 'b) t) ~ctxt = function
    | [] -> ctxt, []
    | head :: tail ->
        let ctxt', head' = f ~ctxt head in
        let ctxt'', tail' = seq f ~ctxt:ctxt' tail in
        ctxt'', head' :: tail'

  let map ~f (prod : ('a, 'b) t) ~ctxt input =
    let ctxt', x = prod ~ctxt input in
    ctxt', f x

  let opt (prod : ('a, 'b) t) ~ctxt = function
    | None -> ctxt, None
    | Some x -> map ~f:(fun y -> Some y) prod ~ctxt x

  let ign (prod : ('a, 'b) t) ~ctxt inp =
    let _, ret = prod ~ctxt inp in
    ret

  let tup3 (prod : ('a, 'b) t) ~ctxt (a, b, c) =
    let ctxt_a, a' = prod ~ctxt a in
    let ctxt_b, b' = prod ~ctxt:ctxt_a b in
    let ctxt_c, c' = prod ~ctxt:ctxt_b c in
    ctxt_c, (a', b', c')
end

module type Productions = sig
  include module type of ProducerCore

  module A : Foreign_ast.Ast_intf

  module L : Backend with module A := A

  val produce_directive : (directive, string) ProducerCore.t

  val produce_type : (A.T.t, string) ProducerCore.t

  val produce_field : (string * A.T.t, string * string) ProducerCore.t

  val produce_elem : (string * int, string * string) ProducerCore.t

  val produce_typedef : (A.T.t, contents) ProducerCore.t

  val produce_param :
    ( string * A.V.t fun_arg_mod * A.T.t option,
      string * string option )
    ProducerCore.t

  val produce_value : (A.V.t, string) ProducerCore.t

  val produce_field_fill : (A.V.t field_fill, string field_fill) ProducerCore.t

  val produce_statement : (A.S.t, string) ProducerCore.t

  val produce_branch : (A.S.t list, string list) ProducerCore.t

  val produce_declaration : (A.D.v, string) ProducerCore.t

  val produce_sourcefile : (A.source_file, string list) ProducerCore.t

  val produce_codebase : (A.foreign_codebase, string list) ProducerCore.t
end
