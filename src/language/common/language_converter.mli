open Backend.Foreign
open Representation.Abstract_type

(** Wrap an integer as a foreign-language numeric value-literal *)
val int_to_fv : int -> [> `Pure of [> `VPrim of [> `PrimNum of string ] ] ]

(** Wrap a width value as a foreign-language width value-literal *)
val width_to_fv : 'a -> [> `Pure of [> `VPrim of [> `PrimWidth of 'a ] ] ]

(** Wrap a string as a foreign-language string-literal *)
val string_to_lit : 'a -> [> `Pure of [> `VLit of [> `LitString of 'a ] ] ]

(** Reify a foreign-language variable from its string identifier *)
val name_to_var : string -> [> `Pure of [> `VBound of Foreign.Ident.binding ] ]

(** abstract_type automorphism that converts [AbstractVoid] to
    [AbstractPadding 0] and preserves all other values *)
val devoid : abstract_type -> abstract_type

(** Extract a (possibly empty) list of fields from a [typrep] *)
val get_fields : typrep -> (string * abstract_type) list

(** Convert an [enum_size] or [tag_size] to an integral [bit_width] *)
val to_bits : [< `Uint16 | `Uint30 | `Uint8 ] -> bit_width

(** Split a '.' separated chain of labels into a reverse-order stack of prefixes
    for the innermost label *)
val reformat : string -> string list * string

(** Perform character-level cleanup of a label to ensure it is a valid and
    unambiguous identifier fragment *)
val sanitize_label : string -> string

(** Perform string-level patching of known-invalid identifiers using
    a custom lookup table *)
val patch_ident : string -> string

(** Context monad that accumulates remappings from nominal qualified names to
    their renamings to allow for preemptive substitution in the first pass when
    references follow their referant and catch-all substitution for
    pre-references during the second pass of production *)
module GenContext : sig
  module S : Map.S

  type t = Qualification.t S.t

  val fresh : unit -> t

  (** Add a new remapping rule that causes nominal references to the first
      label-sequence to be substituted with the second *)
  val add_assoc : Qualification.t -> Qualification.t -> t -> t

  (** Find the proper qualification, including base_name as prefix, of a given
      raw identifier *)
  val lookup : Qualification.t -> t -> Qualification.t

  (** Clear the definitions of locally defined ad-hoc types for re-use in other
      modules *)
  val reset_local : t -> t
end
